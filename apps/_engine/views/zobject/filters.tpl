{include file='zobject/header.tpl' inline='true'}

{* Error message *}
{if isset($smarty.get.err) && $smarty.get.err == 'access'}
    <div class="alert alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>Ошибка. У вас нет доступа к данному разделу</p>
    </div>
{elseif isset($error_access) && $error_access == 1}
    <div class="alert alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>Ошибка. У вас нет доступа к данному разделу</p>
    </div>
{/if}

{*{include file='zobject/blocks/list_controll.tpl' inline='true'}*}


<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
    {if !empty($object.subobjects)}
        <div class="well well-panel">
            {foreach item=i from=$object.subobjects}<a
                href="{$request->url->path}:add{$request->url->view}/-type/{$i.type}/" class="btn btn-mini btn-info">
                <span class="icon-plus-sign icon-white"></span>
                {$i.labels.add}</a> &nbsp;&nbsp;&nbsp;{/foreach}

            <a href="{$request->url->path}:delete" onclick="return confirm('Удалить?')"
               class="delete-object btn btn-mini btn-danger pull-right"><span
                        class="icon-remove-sign icon-white"></span>
                Удалить</a>
        </div>
    {/if}


    {if !empty($filters_all)}
        <form method="post">
            <h4>Настройка фильтра</h4>
            <p>&nbsp;</p>
            {foreach item=filter from=$filters_all key=field name=n}
                {if !empty($filter.0.tech)}
                    <div class="row-fluid">
                        <div class="span4">
                            {$smarty.foreach.n.iteration}. {$filter.0.tech}
                        </div>
                        <div class="span4">
                            <select name="type[{$field}]">
                                <option value="checkbox"
                                        {if !empty($filters_current[$field]) && $filters_current[$field]['type'] == 'checkbox'} selected="selected" {/if}
                                >Галочки (checkbox)</option>
                                <option value="range"
                                        {if !empty($filters_current[$field]) && $filters_current[$field]['type'] == 'range'} selected="selected" {/if}
                                >Интервал (range)</option>
                            </select>
                        </div>
                        <div class="span4">
                            <input type="checkbox"
                                   name="use[{$field}]" {if !empty($filters_current[$field])} checked {/if}>
                            <input type="hidden" name="name[{$field}]" value="{$filter.0.tech}">
                        </div>
                    </div>
                {/if}
            {/foreach}

            <div>
                <input type="submit" class="btn btn-primary" value="Сохранить">
            </div>

        </form>
    {else}
        <p>- empty -</p>
    {/if}

    {*    {include file='zobject/blocks/edit_object.tpl' inline='true'}*}

</div>
{include file='zobject/footer.tpl' inline='true'}