﻿<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>
        {if isset($seo.title) && !empty($seo.title)}{$seo.title}{else}{$title|default:''} / ООО &laquo;Дарья-Металл-Групп&raquo;{/if}</title>

    {strip}
        <meta name="keywords" content="{if isset($seo.key) && !empty($seo.key)}{$seo.key}{/if}"/>
        <meta name="description" content="{if isset($seo.desc) && !empty($seo.desc)}{$seo.desc}{/if}"/>
    {/strip}

    <script src="https://www.google.com/recaptcha/api.js?render={$smarty.const.reCaptcha_pub}"></script>

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <link rel="icon" type="image/x-icon" href="/theme/dmg_markup/build/images/favicon/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="57x57"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon-precomposed" sizes="60x60"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152"
          href="/theme/dmg_markup/build/images/favicon/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="/theme/dmg_markup/build/images/favicon/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="/theme/dmg_markup/build/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/theme/dmg_markup/build/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/theme/dmg_markup/build/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/theme/dmg_markup/build/images/favicon/favicon-128.png" sizes="128x128">
    <meta name="application-name" content=" ">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="/theme/dmg_markup/build/images/favicon/mstile-144x144.png">
    <meta name="msapplication-square70x70logo" content="/theme/dmg_markup/build/images/favicon/mstile-70x70.png">
    <meta name="msapplication-square150x150logo" content="/theme/dmg_markup/build/images/favicon/mstile-150x150.png">
    <meta name="msapplication-wide310x150logo" content="/theme/dmg_markup/build/images/favicon/mstile-310x150.png">
    <meta name="msapplication-square310x310logo" content="/theme/dmg_markup/build/images/favicon/mstile-310x310.png">
    <link rel="stylesheet" href="/theme/dmg_markup/build/css/main.css" type="text/css">
    <script>(screen.width >= 768 && screen.width <= 1240) ? document.getElementsByName('viewport')[0].setAttribute('content', 'width=1240, maximum-scale=1') : ''</script>
</head>

<body>

<header class="header is-animated">
    <div class="header-nav">
        <div class="header-nav__list">
            <div class="header-nav__elem"><a class="header-nav__link" href="/catalog/">Каталог</a></div>
            <div class="header-nav__elem"><a class="header-nav__link" href="/company/">Компания</a></div>
            <div class="header-nav__elem"><a class="header-nav__link" href="/clients/">Клиентам</a></div>
            <div class="header-nav__elem"><a class="header-nav__link" href="/suppliers/">Поставщикам</a></div>
            <div class="header-nav__elem"><a class="header-nav__link" href="/news/">Новости</a></div>
            <div class="header-nav__elem"><a class="header-nav__link" href="/contacts/">Контакты</a></div>
        </div>
    </div>
    <div class="header-top">
        <div class="header-left">
            <div class="header-logo"><a class="logo" href="/"></a></div>
            <div class="header-burger js-burgerMenu">
                <div class="burger">
                    <div class="burger__line"></div>
                    <div class="burger__line"></div>
                    <div class="burger__line"></div>
                </div>
                <svg class="icon icon-menu ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-menu"></use>
                </svg>
                <svg class="icon icon-m-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-close"></use>
                </svg>
            </div>
        </div>
        <div class="header-right">
            <div class="header-cart">

                {if $authorized}
                    {include file="_cart_header.tpl"}
                {/if}

                <a class="header-calc" href="/calculator/">
                    <div class="header-calc__icon">
                        <svg class="icon icon-calc ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-calc"></use>
                        </svg>
                        <svg class="icon icon-m-calc ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-calc"></use>
                        </svg>
                    </div>
                    <div class="header-calc__title">Калькулятор металла</div>
                </a>

                {include file="_cart_dropdown.tpl"}

            </div>

            {if $authorized}
            <div class="header-search">
                <div class="header-search__icon js-searchButton">
                    <svg class="icon icon-search ">
                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-search"></use>
                    </svg>
                </div>
                <form class="search-form  aj1ax" action="/catalog/:result" method="get" rel="search" id="header-search-form">
                    <input class="search-form__input js-searchInput" type="text" name="search" autocomplete="off" placeholder="Поиск по сайту">
                    <button class="search-form__submit">
                        <svg class="icon icon-search ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-search"></use>
                        </svg>
                    </button>
                    <div class="search-form__close js-searchClose">
                        <svg class="icon icon-close ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                        </svg>
                    </div>
                    <button class="search-form__next" type="submit" value="mobile-submit">
                        <svg class="icon icon-arrow-triangle ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                        </svg>
                    </button>
                </form>

                <div class="search-result" id="search-result">
                </div>
            </div>
            {/if}

        </div>
    </div>
    <div class="header-bottom">
        <div class="header-city js-citySelect">
            <span class="header-city__title js-headerCityTitle">
                г. Краснодар, ул. Старокубанская, д. 2
            </span>
            <span class="header-city__arrow">
                <svg class="icon icon-city-dropdown-down ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-city-dropdown-down"></use>
                </svg>
            </span>
        </div>
        <div class="header-contact js-headerContact">тел.: <a href="tel:88612270635">8 (861) 227-06-35</a></div>
    </div>
</header>

<div class="city-select">
    <div class="city-select__close js-citySelectClose"><span>Закрыть</span>
        <svg class="icon icon-city-dropdown-up ">
            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-city-dropdown-up"></use>
        </svg>
    </div>

    <div class="city-select__container">
        {foreach item=contact from=$contacts}
            <div class="city-elem js-citySelectElem">
                <div class="city-elem__left">
                    <div class="city-elem__address js-citySelectAddress" data-id="{$contact.cid}"
                         data-title="{$contact.city}, {$contact.addr}">
                        <b>{$contact.city}</b> {$contact.addr}
                    </div>
                    <div class="city-elem__controls">
                        <div class="city-elem__control js-citySelectControlPhone">
                            <svg class="icon icon-m-phone ">
                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-phone"></use>
                            </svg>
                        </div>
                        <div class="city-elem__control js-citySelectControlMail">
                            <svg class="icon icon-mail ">
                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-mail"></use>
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="city-elem__right js-citySelectRight">
                    <div class="city-elem__phones js-citySelectPhones">
                        {foreach item=number from=$contact.phone}
                            <a class="city-elem__phone" href="tel:{$number}">{$number}</a>
                        {/foreach}
                    </div>
                    <div class="city-elem__mail js-citySelectMail">
                        <div class="city-mail">
                            <div class="city-mail__icon">
                                <svg class="icon icon-mail ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-mail"></use>
                                </svg>
                            </div>
                            <div class="city-mail__info">
                                <a class="city-mail__url" href="mailto:{$contact.email}">{$contact.email}</a>
                                <a class="city-mail__label" href="/contacts/#feedback">Отправить заявку</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/foreach}

    </div>

</div>

<div class="menu">
    <div class="menu-close js-menuClose">
        <svg class="icon icon-close ">
            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
        </svg>
    </div>
    <div class="menu-container">
        <div class="menu-logo"></div>
        <div class="menu-nav">
            <ul class="menu-nav__list">
                {foreach item=item from=$menu[1]}
                    <li class="menu-nav__elem">
                        {if !empty($menu[$item.id])}
                            <a class="menu-nav__link js-menuNavToggle" href="javascript:;">
                                <div class="menu-nav__title">
                                    {$item.title}

                                    <svg class="icon icon-menu-arrow-right ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-menu-arrow-right"></use>
                                    </svg>
                                </div>
                            </a>
                        {else}
                            <a class="menu-nav__link" href="{$item.path}">
                                <div class="menu-nav__title">
                                    {$item.title}
                                </div>
                            </a>
                        {/if}
                        {if !empty($menu[$item.id])}
                            <div class="menu-nav__sublist">
                                <ul>
                                    {foreach item=sub_item from=$menu[$item.id]}
                                        <li><a href="{$sub_item.path}">{$sub_item.title}</a></li>
                                    {/foreach}
                                </ul>
                            </div>
                        {/if}
                    </li>
                {/foreach}
            </ul>
        </div>

        <div class="menu-social">
            <div class="social">

                <div class="social__elem">
                    <a class="social__link" href="{$cfg.link_instagram|default:'#'}">
                        <svg class="icon icon-instagram ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-instagram"></use>
                        </svg>
                    </a>
                </div>
                <div class="social__elem">
                    <a class="social__link" href="{$cfg.link_whatsapp|default:'#'}">
                        <svg class="icon icon-whatsapp ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-whatsapp"></use>
                        </svg>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="page-wrap">
    {* Content *}
    {block name=content}{/block}

    <footer class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="footer-flex">
                    <div class="footer-left">
                        <ul class="footer-menu">

                            {foreach item=item from=$menu[1]}
                                <li class="footer-menu__elem">
                                    {if !empty($menu[$item.id])}
                                        <a class="footer-menu__link js-footerNavToggle" href="javascript:;">
                                            {$item.title}
                                            <svg class="icon icon-menu-arrow-right ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-menu-arrow-right"></use>
                                            </svg>
                                        </a>
                                    {else}
                                        <a class="footer-menu__link" href="{$item.path}">{$item.title}</a>
                                    {/if}
                                    {if !empty($menu[$item.id])}
                                        <div class="footer-menu__subnav">
                                            <ul>
                                                {foreach item=sub_item from=$menu[$item.id]}
                                                    <li><a href="{$sub_item.path}">{$sub_item.title}</a></li>
                                                {/foreach}
                                            </ul>
                                        </div>
                                    {/if}
                                </li>
                            {/foreach}

                        </ul>
                    </div>
                    <div class="footer-right">
                        <div class="footer-nav">
                            {foreach item=category from=$menu_category[0]}
                                <div class="footer-nav__group">
                                    <div class="footer-nav__header js-footerNavToggle">
                                        <div class="footer-nav__title">{$category.title}</div>
                                        <svg class="icon icon-m-menu-dropdown-down ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-menu-dropdown-down"></use>
                                        </svg>
                                    </div>

                                    {if !empty($menu_category[$category.nodes_id])}
                                        <ul class="footer-nav__list">
                                            {foreach item=sub_category from=$menu_category[$category.nodes_id]}
                                                <li><a href="{$sub_category.path}">{$sub_category.title}</a></li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>

                <div class="footer-flex">
                    <div class="footer-left">
                        <div class="footer-contact">
                            <div class="footer-contact__elem">
                                <div class="footer-contact__title">г. Краснодар, ул. Старокубанская, д. 2</div>
                                <div class="footer-contact__icon">
                                    <svg class="icon icon-city ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-city"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="footer-contact__elem">
                                <div class="footer-contact__title">ПН-ПТ: с 8:00 до 17:00, СБ-ВС: выходной</div>
                                <div class="footer-contact__icon">
                                    <svg class="icon icon-time ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-time"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="footer-contact__elem">
                                <div class="footer-contact__title">45.003888, 39.034171</div>
                                <div class="footer-contact__icon">
                                    <svg class="icon icon-location ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-location"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-social">
                                <div class="social">
                                    <div class="social__elem">
                                        <a class="social__link" href="{$cfg.link_instagram|default:'#'}">
                                            <svg class="icon icon-instagram ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-instagram"></use>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="social__elem">
                                        <a class="social__link" href="{$cfg.link_whatsapp|default:'#'}">
                                            <svg class="icon icon-whatsapp ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-whatsapp"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a class="footer-mail" href="mailto:mail@d-m-g.ru">
                                <div class="footer-mail__title">mail@d-m-g.ru</div>
                                <div class="footer-mail__icon">
                                    <svg class="icon icon-mail ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-mail"></use>
                                    </svg>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="footer-right">
                        <div class="footer-partners">
                            <div class="footer-partners__logos">
                                <a class="partner-block" href="http://www.metall-prom.net" target="_blank">
                                    <div class="partner"
                                         style="background-image: url('/theme/dmg_markup/build/images/metallprom_back.jpg')">
                                        <div class="partner__image"><img
                                                    src="/theme/dmg_markup/build/images/metallprom_white.png"></div>
                                    </div>
                                    <div class="partner__title">Технологическое оборудование</div>
                                    <div class="partner__site"><span>www.metall-prom.net</span>
                                        <svg class="icon icon-link ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-link"></use>
                                        </svg>
                                    </div>
                                </a>
                                <a class="partner-block" href="http://www.inoxmim.net" target="_blank">
                                    <div class="partner"
                                         style="background-image: url('/theme/dmg_markup/build/images/inoxmim_back.jpg')">
                                        <div class="partner__image"><img
                                                    src="/theme/dmg_markup/build/images/inoxmim.png"></div>
                                    </div>
                                    <div class="partner__title">Насосы, эмульгаторы, мешалки</div>
                                    <div class="partner__site"><span>www.inoxmim.net</span>
                                        <svg class="icon icon-link ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-link"></use>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="footer-copy">
                    ООО «Дарья-Металл-Групп» <br>
                    © 2014-2019 Все права защищены.
                </div>
                <div class="footer-info">
                    <div class="footer-metrics">
                        <div class="footer-metrics__elem"><img src="/theme/dmg_markup/build/images/metric_1.png"></div>
                        <div class="footer-metrics__elem"><img src="/theme/dmg_markup/build/images/metric_2.png"></div>
                        <div class="footer-metrics__elem"><img src="/theme/dmg_markup/build/images/metric_3.png"></div>
                        <div class="footer-metrics__elem"><img src="/theme/dmg_markup/build/images/metric_4.png"></div>
                    </div>
                    <div class="footer-dev"><a class="footer-dev__link" href="http://www.glukhanko.ru/" target="_blank"><span>Дизайн сайта <br>www.glukhanko.ru</span></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>


{block name=modal_products}{/block}
{block name=modal}
    <div class="modal mfp-hide" id="modalProduct">
        <div class="modal-block">
            <div class="modal-close close_with_fill js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-product">
                <div class="modal-product__left">
                    <div class="modal-product__swiper">
                        <div class="modal-product__slider js-modalSlider">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/catalog_image.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/category-6.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/textpage.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/index-back-3.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/index-back-2.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/index-back-4.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/hero_image.jpg')"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination visible-xs"></div>
                        </div>
                        <div class="modal-product__preview js-modalSliderPreview">
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/catalog_image.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/category-6.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/textpage.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/index-back-3.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/index-back-2.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/index-back-4.jpg')"></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="modal-product__image"
                                             style="background-image: url('/theme/dmg_markup/build/images/hero_image.jpg')"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-product__right">
                    <div class="modal-product__title">Уголок 25.0x25.0x1.5, AISI304, 08X18H10, Grit400, Гнуты</div>
                    <div class="modal-product__main">
                        <div class="modal-product__count">
                            <div class="modal-product__price">1 200 руб. / лист</div>
                            <div class="count">
                                <div class="count-button">
                                    <svg class="icon icon-minus ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-minus"></use>
                                    </svg>
                                </div>
                                <input class="count-input" type="text" value="1" readonly>
                                <div class="count-button">
                                    <svg class="icon icon-plus ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-plus"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="modal-product__cart">
                            <button class="button style_green js-modalClose" data-mfp-src="#modalAddedToCart"><span
                                        class="button__text">Добавить в корзину</span>
                            </button>
                            <div class="modal-product__total">Итого: 2 400 руб.</div>
                        </div>
                    </div>
                    <div class="modal-product__char">
                        <table>
                            <tr>
                                <td>
                                    <div class="modal-product__char-label">Марка стали:</div>
                                    AISI304L
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Химический состав:</div>
                                    03X18H11
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Поверхность:</div>
                                    2B
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="modal-product__char-label">Размер полки:</div>
                                    25 мм
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Толщина стенки:</div>
                                    Т1,5 мм
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Изготовление:</div>
                                    Гнутый
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-cancel js-modalClose">
                <div class="modal-cancel__title">Закрыть</div>
            </div>
        </div>
    </div>
    <div class="modal mfp-hide" id="modalProductNophoto">
        <div class="modal-block">
            <div class="modal-close js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-product modal_nophoto">
                <div class="modal-product__left">
                    <div class="modal-product__cover"
                         style="background-image: url('/theme/dmg_markup/build/images/no_image.png')"></div>
                </div>
                <div class="modal-product__right">
                    <div class="modal-product__title">Уголок 25.0x25.0x1.5, AISI304, 08X18H10, Grit400, Гнуты</div>
                    <div class="modal-product__main">
                        <div class="modal-product__count">
                            <div class="modal-product__price">1 200 руб. / лист</div>
                            <div class="count">
                                <div class="count-button">
                                    <svg class="icon icon-minus ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-minus"></use>
                                    </svg>
                                </div>
                                <input class="count-input" type="text" value="1" readonly>
                                <div class="count-button">
                                    <svg class="icon icon-plus ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-plus"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="modal-product__cart">
                            <button class="button style_green js-modalClose" data-mfp-src="#modalAddedToCart"><span
                                        class="button__text">Добавить в корзину</span>
                            </button>
                            <div class="modal-product__total">Итого: 2 400 руб.</div>
                        </div>
                    </div>
                    <div class="modal-product__char">
                        <table>
                            <tr>
                                <td>
                                    <div class="modal-product__char-label">Марка стали:</div>
                                    AISI304L
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Химический состав:</div>
                                    03X18H11
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Поверхность:</div>
                                    2B
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="modal-product__char-label">Размер полки:</div>
                                    25 мм
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Толщина стенки:</div>
                                    Т1,5 мм
                                </td>
                                <td>
                                    <div class="modal-product__char-label">Изготовление:</div>
                                    Гнутый
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-cancel js-modalClose">
                <div class="modal-cancel__title">Закрыть</div>
            </div>
        </div>
    </div>
    <div class="modal modal_size_middle mfp-hide" id="modalOrderErrors">
        <div class="modal-block">
            <div class="modal-close hidden-xs js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-box">
                <div class="modal-header">
                    <div class="modal-title">Ошибка заполнения</div>
                </div>
                <div class="modal-text">
                    <p>

                    </p>
                </div>

            </div>
            <div class="modal-cancel js-modalClose">
                <div class="modal-cancel__title">Закрыть</div>
            </div>
        </div>
    </div>
    <div class="modal modal_size_middle mfp-hide" id="modalAddedToCart">
        <div class="modal-block">
            <div class="modal-close hidden-xs js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-box">
                <div class="modal-header">
                    <div class="modal-title">Товар добавлен в&nbsp;корзину</div>
                </div>
                <div class="modal-cart">
                    <div class="modal-cart__top">
                        <div class="modal-cart__title">Лист 0.5x1250x2500, AISI304, 08X18H10, BA, PE</div>
                        <div class="modal-cart__char">
                            <div class="modal-cart__char-el"><span>Сталь: </span><span>AISI304L</span></div>
                            <div class="modal-cart__char-el"><span>Состав: </span><span>03X18H11</span></div>
                            <div class="modal-cart__char-el"><span>Поверхность: </span><span>2B</span></div>
                        </div>
                    </div>
                    <div class="modal-cart__main">
                        <div class="modal-cart__price">1 200 руб. / лист</div>
                        <div class="modal-cart__count">13</div>
                        <div class="modal-cart__total">15 600 руб.</div>
                    </div>
                    <div class="modal-cart__footer"><a class="button style_green" href="checkout.html"><span
                                    class="button__text">Оформить заказ</span></a>
                        <button class="button style_gray js-modalClose hidden-xs"><span class="button__text">Закрыть окно</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-cancel js-modalClose">
                <div class="modal-cancel__title">Закрыть</div>
            </div>
        </div>
    </div>
    <div class="modal modal_size_middle modal_style_green mfp-hide" id="modalAddedToCart2">
        <div class="modal-block">
            <div class="modal-close hidden-xs js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-box">
                <div class="modal-header">
                    <div class="modal-title">Товар добавлен в&nbsp;корзину</div>
                </div>

                {include file="_cart_product_added.tpl"}

            </div>
            <div class="modal-cancel js-modalClose">
                <div class="modal-cancel__title">Закрыть</div>
            </div>
        </div>
    </div>
    <div class="modal modal_size_middle mfp-hide" id="modalSuccess">
        <div class="modal-block">
            <div class="modal-close hidden-xs js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-box">
                <div class="modal-success">
                    <div class="modal-header">
                        <div class="modal-title">Заявка отправлена</div>
                    </div>
                    <div class="modal-text">
                        <p>
                            Спасибо за ваше обращение! <br>
                            Наши менеджеры уже получили вашу заявку <br>
                            и свяжутся с вами в ближайшее время.
                        </p>
                    </div>
                    <button class="button style_green js-modalClose"><span class="button__text">Понятно, спасибо</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    {*  popup  *}
    <div class="modal modal_partners_size mfp-hide" id="modalPartners">
        <div class="modal-partners">
            <div class="modal-close close_with_fill js-modalClose">
                <svg class="icon icon-close ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                </svg>
            </div>
            <div class="modal-partners__header">
                <div class="modal-partners__title">
                    <b>Дарья Металл Групп это часть группы компаний, </b><br/>
                    охватывающей все сферы производства.
                </div>
            </div>
            <div class="modal-partners__content">
                <div class="partners-grid">
                    <div class="partners-col">
                        <a class="partners-box theme_dmg js-modalClose" href="javascript:;">
                            <div class="partners-box__back"
                                 style="background-image: url('/theme/dmg_markup/build/images/partners/back-dmg.jpg')"></div>
                            <div class="partners-box__logo"><img
                                        src="/theme/dmg_markup/build/images/partners/dmg-logo.png" width="94"></div>
                            <div class="partners-box__text">
                                Поставки нержавеющей
                                стали
                            </div>
                            <div class="partners-box__more"><span class="partners-box__link">продолжить</span></div>
                        </a>
                    </div>
                    <div class="partners-col">
                        <a class="partners-box" href="http://metall-prom.net/" target="_blank">
                            <div class="partners-box__back"
                                 style="background-image: url('/theme/dmg_markup/build/images/partners/back-metallprom.jpg')"></div>
                            <div class="partners-box__logo"><img
                                        src="/theme/dmg_markup/build/images/partners/metallprom-logo.png" width="125">
                            </div>
                            <div class="partners-box__text">Инжиниринг <br>и производство</div>
                            <div class="partners-box__more"><span class="partners-box__link">metall-prom.net</span>
                            </div>
                        </a>
                    </div>
                    <div class="partners-col">
                        <a class="partners-box theme_inoxmin" href="http://inoxmim.net/" targer="_blank">
                            <div class="partners-box__back"
                                 style="background-image: url('/theme/dmg_markup/build/images/partners/back-inoxmim.jpg')"></div>
                            <div class="partners-box__logo"><img
                                        src="/theme/dmg_markup/build/images/partners/inoxmim-logo.png" width="104">
                            </div>
                            <div class="partners-box__text">Насосы, эмульгаторы, мешалки</div>
                            <div class="partners-box__more"><span class="partners-box__link">inoxmim.net</span></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-bottom-close js-modalClose">Закрыть</div>
        </div>
    </div>
{/block}

<script src="//api-maps.yandex.ru/2.1/?lang=ru" type="text/javascript"></script>
<script src="/theme/dmg_markup/build/js/main.js"></script>
<script src="/theme/s.js"></script>


{if !empty($env) && $env != 'dev'}
    <script src="//code.jivosite.com/widget/Z4rlGb2Z5P" async></script>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function () {
            var widget_id = 'qAxgOfkh6z';
            var d = document;
            var w = window;

            function l() {
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = '//code.jivosite.com/script/widget/' + widget_id;
                var ss = document.getElementsByTagName('script')[0];
                ss.parentNode.insertBefore(s, ss);
            }

            if (d.readyState == 'complete') {
                l();
            } else {
                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })();</script>
    <!-- {/literal} END JIVOSITE CODE -->
{/if}


{if $smarty.session.show_popup === 1}
    <script type="text/javascript">
        window.modalOpen('#modalPartners');
    </script>
{/if}


</body>
</html>
{if isset($smarty.get.d)}{debug}{/if}