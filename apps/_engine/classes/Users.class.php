<?php
class Users
{
    public function __construct()
    {
    }

    public static function checkAuth()
    {
        @session_start();
        //     User not authorized
        if (!(isset($_SESSION['user']['id']) && $_SESSION['user']['id'] > 0)) {
            return false;
        }
        return true;

    }

    public static function login($login, $pass)
    {
        //  Ищем пользователя
        $user = Q("SELECT `id`, `login`, `admin`, `root`, `dealer`, `updated`, `pass` FROM @@users WHERE login=?s", array($login))->row();

        //  Есть ли вообще пользователь?
        if (empty($user)) {
            return false;
        }

        //  Проверяем правильность пароля
        if (!password_verify($pass, $user['pass'])) {
            return false;
        }

        @session_start();
        unset($_SESSION['user'], $user['updated'], $user['pass']);
        $_SESSION['user'] = $user;
        return true;
    }

    public function logout()
    {
    }

    public function addUser($login, $pass, $admin = 0)
    {
        $u = $this->getUser($login);
        if (!empty($u)) {
            return false;
        }

        //    Add new user
        $res = Q("INSERT INTO @@users SET login=?s, pass=?s, updated=NOW(), admin=?i", array($login, Users::genPass($pass), $admin));
        $u = $this->getUser($login);
        return $u;
    }

    public function editUser($id, $login, $pass = '', $admin = 0)
    {
        $u = $this->getUser($login);

        if (!empty($u) && $id != $u['id']) {
            return false;
        }

        if ($pass == '') {
            Q("UPDATE @@users SET login=?s, updated=NOW(), admin=?i WHERE id=?i", array($login, $admin, $id));
        } else {
            Q("UPDATE @@users SET login=?s, updated=NOW(), pass=?s WHERE id=?i", array($login, Users::genPass($pass), $id));
        }

        $u = $this->getUser($login);
        return $u;
    }

    public function delUser()
    {
    }

    public static function genPass($pass)
    {
        return password_hash($pass, PASSWORD_BCRYPT);
    }

    public static function getUser($login)
    {
        return Q("SELECT id, login, admin, root, updated FROM @@users WHERE `login`=?s", array($login))->row();
    }

    public function createTable()
    {
    }
}
