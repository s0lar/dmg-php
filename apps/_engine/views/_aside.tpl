﻿{*<div class="page-flex__aside">*}
{*    <div class="aside-select">*}
{*        <div class="aside-select__control">*}
{*            <div class="aside-select__button">*}
{*                <div class="aside-select__title">{$title}</div>*}
{*                <svg class="icon icon-sort-arrow-down ">*}
{*                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-sort-arrow-down"></use>*}
{*                </svg>*}
{*            </div>*}
{*            <select class="aside-select__list js-asideSelectList">*}
{*                <option value="1">Услуги</option>*}
{*                <option value="2">Все о нержавеющей стали</option>*}
{*                <option value="3">Справочники</option>*}
{*                <option value="4">Вопрос-ответ</option>*}
{*                <option value="5">Глоссарий</option>*}
{*                <option value="6">Реквизиты</option>*}
{*            </select>*}
{*        </div>*}
{*    </div>*}
{*    <div class="aside-nav">*}
{*        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Услуги</a></div>*}
{*        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Все о нержавеющей стали</a></div>*}
{*        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Справочники</a></div>*}
{*        <div class="aside-nav__elem is-active"><a class="aside-nav__link" href="#">Вопрос-ответ</a></div>*}
{*        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Глоссарий</a></div>*}
{*        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Реквизиты</a></div>*}
{*    </div>*}
{*</div>*}

<div class="page-flex__aside">
    <div class="aside-nav">
        {if !empty($sub_menu)}
            {foreach item=menu from=$sub_menu}
                <div class="aside-nav__elem {if $menu.path == $node.path} is-active {/if}">
                    <a class="aside-nav__link" href="{$menu.path}">{$menu.title|stripcslashes}</a>
                </div>
            {/foreach}
        {/if}
    </div>
</div>