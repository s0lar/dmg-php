<?php


abstract class Abstract1cXml
{
    const XPATH_ID = 'Ид';
    const XPATH_TITLE = 'Наименование';

    /** @var SimpleXMLElement */
    protected $xml;

    /** @var Logger */
    protected $logger;

    public function __construct(SimpleXMLElement $xml, Logger $logger)
    {
        $this->xml = $xml;
        $this->logger = $logger;

        $this->logger->info('New 1c xml class: ' . static::class);
    }

    abstract public function execute();
}