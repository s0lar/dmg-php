<?php

return [
    # types of fields
    'fields' => [
        'title'   => 'string',
        'group'   => [
            'type'   => 'pointer',
            'object' => [
                'table'   => 'group',
                'fields'  => 'id,title',
                'display' => '%title%',
            ],
        ],
        'preview' => 'gs_file',
        'files'   => 'gs_file',
        'sort'    => 'int',
    ],

    # labels of fields
    'ui'     => [
        'title'   => 'Название',
        'group'   => 'Категория',
        'preview' => 'Картинка (превью)',
        'files'   => 'Картинка (превью)',
        'sort'    => 'Порядок',
    ],

    # node configuration
    'node'   => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-auto',
    ],

    'input_cfg' => [
        'preview' => ['type' => 'image', 'mask' => 'docs_preview'],
        'files'   => ['type' => 'file', 'mask' => 'docs_files'],
    ],

    'view'   => [
        'mode'       => 'list',
        'fields'     => ['title', 'group', 'sort'],
        'orderby'    => ' `sort` DESC ',
        'edit_field' => 'title',
        'limit'      => 150,
    ],

    # labels for actions
    'labels' => [
        'list'    => 'Файлы',
        'add'     => 'Новый файл',
        'adding'  => 'Создание',
        'edit'    => 'Редактировать',
        'editing' => 'Редактирование',
        'delete'  => 'Удалить',
    ],
];
