﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_checkout">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">Оформить заказ</div>
                </div>
                <div class="checkout-step">
                    <div class="checkout-step__elem is-active">
                        <div class="checkout-step__title">01. Состав заказа</div>
                    </div>
                    <div class="checkout-step__elem is-active">
                        <div class="checkout-step__title">02. Контакты и доставка</div>
                    </div>
                    <div class="checkout-step__elem">
                        <div class="checkout-step__title">03. подтверждение</div>
                    </div>
                </div>
                <div class="checkout-counter">
                    <div class="checkout-counter__title">Контакты и доставка</div>
                    <div class="checkout-counter__number">02 / 03</div>
                </div>
            </div>
            <div class="checkout">
                <div class="checkout-flex">
                    <div class="checkout-flex__left">
                        <form class="checkout-form  ajax" action="/order/:confirm" method="post" rel="checkout_order">
                            <div class="checkout-radio">
                                <div class="radio-group">
                                    <label class="radio-button">
                                        <input class="js-checkoutDeliveryRadio" type="radio" name="delivery" value="pickup" checked data-checkout-type="pickup"><span class="radio-button__title">Самовывоз</span>
                                    </label>
                                    <label class="radio-button">
                                        <input class="js-checkoutDeliveryRadio" type="radio" name="delivery" value="delivery" data-checkout-type="delivery"><span class="radio-button__title">Доставка</span>
                                    </label>
                                </div>
                            </div>
                            <div class="checkout-delivery js-checkoutTypePickup">
                                <div class="checkout-help size_small">
                                    <p>
                                        Вы можете забрать товар самостоятельно <br>
                                        в одном из филиалов Дарья-Металл-Групп.
                                    </p>
                                    <p>
                                        График работы: ПН—ПТ с 8:00 до 17:00, <br>
                                        в предпраздничные дни с 8:00 до 16:00. <br>
                                        ВС—СБ выходные. 
                                    </p>
                                    <div class="checkout-links">
                                        <a class="link" href="/clients/oplata-dostavka-dokumenty/">Порядок оплаты</a>
                                        <a class="link" href="/clients/oplata-dostavka-dokumenty/">Порядок доставки</a>
                                    </div>
                                </div>
                                <div class="delivery">
                                    {foreach item=city from=$contacts}
                                        <div class="delivery-block js-deliveryBlock">
                                            <label class="delivery-block__header">
                                                <input type="radio" name="pickup_address" value="{$city.cid}">
                                                <div class="delivery-block__tile">
                                                    <div class="delivery-block__left"><b class="delivery-block__city">{$city.city|stripcslashes}</b>{$city.addr|stripcslashes}</div>
                                                    <div class="delivery-block__right"><b>{$city.lat_lon|stripcslashes}</b>
                                                        <div class="delivery-block__showmap js-deliveryShowMapLink" data-label="Показать на карте" data-second-label="Скрыть карту">Показать на карте</div>
                                                    </div>
                                                    <div class="delivery-block__icon js-deliveryShowMapIcon">
                                                        <svg class="icon icon-location ">
                                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-location"></use>
                                                        </svg>
                                                        <svg class="icon icon-close ">
                                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </label>
                                            <div class="delivery-block__map js-deliveryBlockMap">
                                                <div class="map js-deliveryMap" id="mapCheckout1" data-center="{$city.lat_lon|stripcslashes}" data-pin="/theme/dmg_markup/build/images/pin.png"></div>
                                            </div>
                                        </div>
                                    {/foreach}

                                </div>
                            </div>
                            <div class="checkout-fields">
                                <div class="checkout-form__group">
                                    <div class="checkout-form__hidden js-checkoutTypeDelivery">
                                        <div class="checkout-help">
                                            <p>Вы можете заказать доставку товара до конкретного адреса, мы свяжемся с вами и предложим варианты доставки.
                                                <br>Если вам требуется доставка до терминала транспортной компании, укажите, пожалуйста, адрес этого филиала.</p>
                                            <div class="checkout-links">
                                                <a class="link" href="/clients/oplata-dostavka-dokumenty/">Порядок оплаты</a>
                                                <a class="link" href="/clients/oplata-dostavka-dokumenty/">Порядок доставки</a>
                                            </div>
                                        </div>
                                        <div class="checkout-form__row">
                                            <div class="checkout-form__col">
                                                <div class="form-field size_middle">
                                                    <input class="form-input" name="city" type="text" placeholder="Город">
                                                    <div class="form-icon">
                                                        <svg class="icon icon-city ">
                                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-city"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkout-form__row">
                                            <div class="checkout-form__col">
                                                <div class="form-field size_middle">
                                                    <input class="form-input" name="address" type="text" placeholder="Адрес доставки">
                                                    <div class="form-icon">
                                                        <svg class="icon icon-location ">
                                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-location"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-form__row">
                                        <div class="checkout-form__col">
                                            <div class="form-field size_middle">
                                                <input class="form-input" name="date" type="text" placeholder="Желаемая дата">
                                                <div class="form-icon">
                                                    <svg class="icon icon-calendar ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-calendar"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkout-form__col">
                                            <div class="form-field size_middle">
                                                <input class="form-input" name="time" type="text" placeholder="Желаемое время">
                                                <div class="form-icon">
                                                    <svg class="icon icon-time ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-time"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkout-form__group">
                                    <div class="checkout-help">
                                        <p>
                                            Пожалуйста, укажите реальные контактные данные,
                                            мы будем использовать их, чтобы связаться с вами
                                            и уточнить детали заказа.
                                        </p>
                                    </div>
                                    <div class="checkout-form__row">
                                        <div class="checkout-form__col">
                                            <div class="form-field size_middle">
                                                <input class="form-input" name="name" type="text" placeholder="Представьтесь, пожалуйста">
                                                <div class="form-icon">
                                                    <svg class="icon icon-form-user ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-form-user"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-form__row">
                                        <div class="checkout-form__col">
                                            <div class="form-field size_middle">
                                                <input class="form-input" name="email" type="email" placeholder="E-mail">
                                                <div class="form-icon">
                                                    <svg class="icon icon-mail ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-mail"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkout-form__col">
                                            <div class="form-field size_middle">
                                                <input class="form-input" name="phone" type="tel" placeholder="Телефон">
                                                <div class="form-icon">
                                                    <svg class="icon icon-form-phone ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-form-phone"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="checkout-form__group">
                                    <div class="checkout-help">
                                        <p>Вы можете указать любую дополнительную информацию, которую посчитаете нужной. Например, паспортные данные получателя груза, номер доверенности и т.д.</p>
                                    </div>
                                    <div class="checkout-form__row">
                                        <div class="checkout-form__col">
                                            <div class="form-field size_middle">
                                                <textarea class="form-input" name="comment" placeholder="Комментарий"></textarea>
                                                <div class="form-icon">
                                                    <svg class="icon icon-form-comment ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-form-comment"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-form__agree">
                                        <label class="checkbox">
                                            <input type="checkbox" name="agreement">
                                            <span class="checkbox__title">Я согласен с <a href="/personal/" target="_blank">политикой обработки персональных данных</a>.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="checkout-next">
                                <button class="button style_green size_middle justify_space_between">
                                    <span class="button__text">
                                        Отправить заявку
                                        <svg class="icon icon-arrow-triangle ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}

    </div>
{/block}

{block name=footer}
{/block}