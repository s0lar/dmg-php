﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="main">
        <div class="page-head single_head">
            <div class="container">
                <div data-aos="fade-up" data-aos-delay="500" data-aos-duration="400" data-aos-easing="ease-out-cubic"
                     class="page-head__desc">
                    <div class="page-head__title">Регистрация</div>
                </div>
            </div>
        </div>
        <div class="page-main">
            <div class="container">
                <div class="page-order">
                    {*<div class="page-order__nav">*}
                    {*<div class="cart-aside__nav">*}
                    {*<div class="cart-aside__nav-elem"><a href="#" class="cart-aside__nav-link">Состав заказа</a></div>*}
                    {*<div class="cart-aside__nav-elem is-active"><a href="#" class="cart-aside__nav-link">Контактные данные</a></div>*}
                    {*<div class="cart-aside__nav-elem"><a href="#" class="cart-aside__nav-link">Заказ оформлен</a></div>*}
                    {*</div>*}
                    {*</div>*}
                    <form action="/customer/:customer_registry" class="form-account" method="post">

                        <div class="form-tab">
                            <div class="form-tab__nav">
                                <label class="form-tab__elem">
                                    <input type="radio" name="type" checked value="0" class="js-formTabRadio">
                                    <span class="form-tab__title">
                                        <span class="hidden-xs">Физическое лицо</span>
                                        <span class="visible-xs">Физ. лицо</span>
                                    </span>
                                </label>
                                <label class="form-tab__elem">
                                    <input type="radio" name="type" value="1" class="js-formTabRadio">
                                    <span class="form-tab__title">
                                        <span class="hidden-xs">Юридическое лицо</span>
                                        <span class="visible-xs">Юр. лицо</span>
                                    </span>
                                </label>
                            </div>
                        </div>

                        {if isset($customer_error)}
                            <p style="color:#f66">{$customer_error}</p>
                        {/if}

                        <div class="form-account__group">
                            <div class="form-account__aside">
                                <div class="form-account__title">Данные</div>
                            </div>
                            <div class="form-account__section">
                                <div style="display: none;" class="form-account__fields js-formTabLegalFields">
                                    <div class="form-fieldset">
                                        <div class="form-fieldset__input">
                                            <input name="title" type="text" placeholder="Название компании *"
                                                   class="form-input" value="{$smarty.session.post.title|default:''}"/>
                                        </div>
                                        {if isset($customer_error_code) && $customer_error_code == 3 && isset($customer_error)}
                                            <div>
                                                <p style="color:#f66">{$customer_error}</p>
                                            </div>
                                        {/if}
                                    </div>

                                    <div class="form-fieldset">
                                        <div class="form-fieldset__input">
                                            <input name="addr" type="text" placeholder="Юридический адрес"
                                                   class="form-input" value="{$smarty.session.post.addr|default:''}"/>
                                        </div>
                                    </div>
                                    <div class="form-fieldset fieldset_flex">
                                        <div class="form-fieldset__input">
                                            <input name="inn" type="text" placeholder="ИНН *" class="form-input"
                                                   value="{$smarty.session.post.inn|default:''}"/>
                                            {if isset($customer_error_code) && $customer_error_code == 4 && isset($customer_error)}
                                                <div>
                                                    <p style="color:#f66">{$customer_error}</p>
                                                </div>
                                            {/if}
                                        </div>
                                        <div class="form-fieldset__input">
                                            <input name="kpp" type="text" placeholder="КПП" class="form-input"
                                                   value="{$smarty.session.post.kpp|default:''}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-fieldset">
                                    <div class="form-fieldset__input">
                                        <input name="person" type="text" placeholder="Контактное лицо *"
                                               class="form-input" value="{$smarty.session.post.person|default:''}"/>
                                    </div>
                                </div>
                                <div class="form-fieldset fieldset_flex">
                                    <div class="form-fieldset__input">
                                        <input name="email" type="email" placeholder="E-Mail *" class="form-input"
                                               value="{$smarty.session.post.email|default:''}"/>
                                        {if isset($customer_error_code) && $customer_error_code == 1 && isset($customer_error)}
                                            <div>
                                                <p style="color:#f66">{$customer_error}</p>
                                            </div>
                                        {/if}
                                    </div>
                                    <div class="form-fieldset__input input_col_3">
                                        <input name="phone" type="tel" placeholder="Телефон" class="form-input"
                                               value="{$smarty.session.post.phone|default:''}"/>
                                    </div>
                                    <div class="form-fieldset__input input_col_3">
                                        <input name="fax" type="tel" placeholder="Факс" class="form-input"
                                               value="{$smarty.session.post.fax|default:''}"/>
                                    </div>
                                </div>

                                <div class="form-fieldset fieldset_flex">
                                    <div class="form-fieldset__input">
                                        <input name="password1" type="password" placeholder="Пароль *"
                                               class="form-input"/>

                                        {if isset($customer_error_code) && $customer_error_code == 2 && isset($customer_error)}
                                            <div>
                                                <p style="color:#f66">{$customer_error}</p>
                                            </div>
                                        {/if}
                                    </div>
                                    <div class="form-fieldset__input"><input name="password2" type="password"
                                                                             placeholder="Повторите пароль *"
                                                                             class="form-input"/></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-account__group">
                            <div class="form-account__aside">
                                <div class="form-account__title">город</div>
                            </div>
                            <div class="form-account__section">
                                <div class="form-fieldset">
                                    <div class="form-fieldset__input">
                                        <input name="city" type="text" placeholder="Введите город *" class="form-input"
                                               value="{$smarty.session.post.city|default:''}"/>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {* <div class="form-account__group">
                            <div class="form-account__aside"><div class="form-account__title">Доставка</div></div>
                            <div class="form-account__section">
                                <div class="form-account__grid">
                                    <div class="form-account__grid-col">
                                        <label class="form-tile">
                                            <input type="radio" name="delivery" value="0" checked />
                                            <div class="form-tile__block">
                                                <div class="form-tile__head"><div class="form-tile__title">Самовывоз</div></div>
                                                <div class="form-tile__body">Вы можете самостоятельно забрать заказ с нашего склада.</div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="form-account__grid-col">
                                        <label class="form-tile">
                                            <input type="radio" name="delivery" value="1" />
                                            <div class="form-tile__block">
                                                <div class="form-tile__head"><div class="form-tile__title">Доставка</div></div>
                                                <div class="form-tile__body">Транспортные компании: Деловые линии. Стоимость доставки согласно тарифов ТК.</div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> *}


                        {* <div class="form-account__group">
                            <div class="form-account__aside"><div class="form-account__title">Оплата</div></div>
                            <div class="form-account__section">
                                <div class="form-account__grid">
                                    <div class="form-account__grid-col">
                                        <label class="form-tile">
                                            <input type="radio" name="payment" checked />
                                            <div class="form-tile__block">
                                                <div class="form-tile__head"><div class="form-tile__title">Банковский перевод</div></div>
                                                <div class="form-tile__body">Мы пришлем вам договор и счет на оплату.</div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> *}


                        {* <div class="form-account__group">
                            <div class="form-account__aside"><div class="form-account__title">Комментарий</div></div>
                            <div class="form-account__section">
                                <div class="form-fieldset">
                                    <div class="form-fieldset__input"><input name="comment" type="text" placeholder="Комментарии к заказу" class="form-input" value="{$smarty.session.post.comment|default:''}" /></div>
                                </div>
                            </div>
                        </div> *}



                        {* <div class="form-account__group group_cart">
                            <div class="form-account__aside"><div class="form-account__title">Корзина</div></div>
                            <div class="form-account__section">
                                <div class="form-account__cart">
                                    <div class="cart-list">
                                        <div class="cart-elem no_control">
                                            <a href="#" style="background-image: url('images/prod-tile-image.jpg')" class="cart-elem__image"></a>
                                            <div class="cart-elem__info">
                                                <div class="cart-elem__info-left">
                                                    <div class="cart-elem__title"><a href="#" class="small-title">Табурет складной средний с сумкой</a></div>
                                                    <div class="cart-elem__char hidden-xs">Арт. KP-03</div>
                                                    <div class="cart-elem__char visible-xs">
                                                        Арт. KP-03<br />
                                                        Тип цены: Базовая цена
                                                    </div>
                                                </div>
                                                <div class="cart-elem__info-right">
                                                    <div class="cart-elem__control">
                                                        <div class="cart-elem__count">1 шт.</div>
                                                        <div class="cart-elem__price">590 руб.</div>
                                                    </div>
                                                    <div class="cart-elem__char hidden-xs">Тип цены: Базовая цена</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-elem no_control">
                                            <a href="#" style="background-image: url('images/prod-tile-image.jpg')" class="cart-elem__image"></a>
                                            <div class="cart-elem__info">
                                                <div class="cart-elem__info-left">
                                                    <div class="cart-elem__title"><a href="#" class="small-title">Табурет складной средний с сумкой</a></div>
                                                    <div class="cart-elem__char hidden-xs">Арт. KP-03</div>
                                                    <div class="cart-elem__char visible-xs">
                                                        Арт. KP-03<br />
                                                        Тип цены: Базовая цена
                                                    </div>
                                                </div>
                                                <div class="cart-elem__info-right">
                                                    <div class="cart-elem__control">
                                                        <div class="cart-elem__count">1 шт.</div>
                                                        <div class="cart-elem__price">590 руб.</div>
                                                    </div>
                                                    <div class="cart-elem__char hidden-xs">Тип цены: Базовая цена</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-account__submit">
                                    <button type="submit" class="button style_green"><span class="button__text">отправить заказ</span></button>
                                </div>
                            </div>
                        </div> *}

                        <div class="form-account__group">
                            <div class="form-account__aside"></div>
                            <div class="form-account__section">
                                <div class="form-account__submit">
                                    <button type="submit" class="button style_green"><span class="button__text">зарегистрироваться</span>
                                    </button>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}