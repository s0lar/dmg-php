<?php

class Careers_Controller extends ZObject_Controller
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_index()
    {
        if ($this->node['object_type'] === 'careers') {
            redirect('../');
        }

        $res = Q('SELECT * FROM @@careers WHERE visible=1 ORDER BY sort DESC')->all();
        RS('careers', $res);

        RS('cls_body', '');
        V('careers');
    }
}
