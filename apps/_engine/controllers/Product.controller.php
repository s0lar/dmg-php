<?php

class Product_Controller extends ZObject_Controller
{

    public function action_index()
    {
        //  Load Category
        $category = Q('SELECT N.path, P.* FROM @@category as P
            LEFT JOIN @@nodes as N
            ON P.id = N.object_id
            WHERE N.object_type="category" AND N.id=?i',
            array($this->node['parent_id'])
        )->row();

        $category['background'] = $this->getFM($category['background'], 'a');
        RS('category', $category);

        //  Filters
        $series = Q('SELECT id, title FROM @@series WHERE `category`=?i', array($this->node['object_id']))->all();
        RS('filter_series', $series);

        $config = $this->config = import::config('app:configs/objects/product.php');
        $material = @$config->fields['material']['items'];
        unset($material[0]);
        RS('filter_material', $material);

        //  Pager
        $sql = Qb("SELECT 1 FROM `@@nodes` WHERE `object_type`=?s AND `path` LIKE ?s", ['product', RQ()->url->path . '%']);
        $pg = PG($sql, 12);
        RS('pg', $pg);

        //  Products
        $product = Q('SELECT * FROM @@product WHERE id=?i ', array($this->node['object_id']))->row();

        if (empty($product)) {

            throw new XException('page not found', 404);
        }

        $product['photo'] = $this->getFM($product['photo']);
        $product['gallery'] = $this->getFM($product['gallery']);
        $product['gallery2'] = $this->getFM($product['gallery2']);
        $product['prod_files'] = $this->getFM($product['prod_files']);

        $product['feature1'] = $this->nl2br($product['feature1']);
        $product['feature2'] = $this->nl2br($product['feature2']);
        $product['feature3'] = $this->nl2br($product['feature3']);
        $product['feature4'] = $this->nl2br($product['feature4']);
        $product['feature5'] = $this->nl2br($product['feature5']);
        $product['feature6'] = $this->nl2br($product['feature6']);
        $product['feature7'] = $this->nl2br($product['feature7']);
        $product['feature8'] = $this->nl2br($product['feature8']);

        $product['video_preview'] = $this->getFM($product['video_preview'], 'a');

        //  Включаем оптовые цены
        $customer = new Customers();
        if ($customer->isAuth() && $customer->getCustomer()['opt'] == 1) {
            //  Оптовая цена
            $product['cost'] = $product['cost_opt'];
        }

        //
        $product['docs_more'] = $this->getDocs($product['docs_more']);

        RS('product', $product);

        $products_more = [];
        if (!empty($product['prod_more'])) {
            $products_more = $this->getMore($product['prod_more']);
        } else {
            $res = Q('SELECT `object_id` as `oid` FROM `@@nodes` WHERE `parent_id`=?i AND `object_type`="product" AND `object_id` <> ?i LIMIT 8', [
                $this->node['parent_id'],
                $this->node['object_id'],
            ]);
            while ($r = $res->each()) {
                $arr[] = $r['oid'];
            }
            $products_more = $this->getMore($arr);
        }
        RS('products_more', $products_more);

        V('product');
    }

    public function getMore($links)
    {
        if (!is_array($links)) {
            $links = explode("|", $links);
        }

        //  Включаем оптовые цены
        $customer = new Customers();
        if ($customer->isAuth() && $customer->getCustomer()['opt'] == 1) {
            //  Оптовая цена
            $cost = ', P.`cost_opt` as `cost`';
        } else {
            //  Обычная цена
            $cost = ', P.`cost`';
        }

        //    Collections
        $arr = [];
        $res = Q('SELECT N.`path`, P.`id`, P.`title`, P.`art`, P.`material`, P.`series`, P.`new`, P.`top`, P.`photo` ' . $cost . '  FROM `@@nodes` as N
				LEFT JOIN `@@product` as P
				ON N.`object_id` = P.`id`
				WHERE N.`object_type`=?s AND P.`id` IN (?ls)',
            ['product', $links]
        );

        while ($r = $res->each()) {
            $r['photo'] = $this->getFM($r['photo']);
            $arr[] = $r;
        }

        return $arr;
    }

    public function getDocs($links)
    {
        if (!is_array($links)) {
            $links = explode("|", $links);
        }

        $arr = [];
        $res = Q('SELECT * FROM @@docs WHERE `id` IN (?ls)',
            [$links]
        );

        while ($r = $res->each()) {
            $r['prod_files'] = $this->getFM($r['prod_files']);
            $arr[] = $r['prod_files'][0];
        }

        return $arr;
    }

    /**
     * Разбиваем строки и получаем массив
     *
     * @param [type] $str
     * @return void
     */
    private function nl2br($str)
    {
        if (!empty($str)) {
            return preg_split('/\n|\r\n?/', $str);
        } else {
            return $str;
        }
    }
}
