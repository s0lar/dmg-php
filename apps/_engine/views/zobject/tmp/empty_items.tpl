{include file='zobject/header.tpl' inline='true'}
	{*
	<div class="tabs2" style="height:auto">
		<a class="tabs tabs_curr" href="{$request->url->path}:edit">Редактировать раздел</a>
		<a class="tabs" href="{$request->url->path}:edit_node">Редактировать список</a>
	</div>
	*}
	
	<div class="" style="padding:20px;border:1px solid #ccc;margin-top:-1px">
		
		<div class="" style="margin-bottom:20px">
			{if !isset($smarty.get.act)}Пустое краткое описание{else}<a href="{$request->url->path}:empty_items/">Пустое краткое описание</a>{/if}
			&nbsp;&nbsp;&nbsp;
			{if isset($smarty.get.act) && $smarty.get.act == 'desc'}Пустое подробное описание{else}<a href="{$request->url->path}:empty_items/?act=desc">Пустое подробное описание</a>{/if}
			&nbsp;&nbsp;&nbsp;
			{if isset($smarty.get.act) && $smarty.get.act == 'pic'}Нет картинок{else}<a href="{$request->url->path}:empty_items/?act=pic">Нет картинок</a>{/if}
			&nbsp;&nbsp;&nbsp;
		</div>
		
		
		<table class="table">
			<tr>
				<th></th>
				<th></th>
				<th>Краткое описание</th>
				<th>Подробное описание</th>
				<th>Фото в ТДТ</th>
				<th>Фото на сайте</th>
				<th>Цена</th>
			</tr>
		{foreach item=i from=$__list}
		<tr>
			<td><a href="{$i.path}:edit" target="_blank">{$i.title}</a></td>
			<td>{$i.cat1} / {$i.cat2} / {$i.prod}</td>
			<td align=center>{if empty($i.anons)}&mdash;{/if}</td>
			<td align=center>{if empty($i.desc)}&mdash;{/if}</td>
			<td align=center>{if empty($i.pic)}&mdash;{/if}</td>
			<td align=center>{if empty($i.pic_arr) || $i.pic_arr == 'a:5:{i:0;b:0;i:1;b:0;i:2;b:0;i:3;b:0;i:4;b:0;}'}&mdash;{/if}</td>
			<td align=right>{$i.cost4}</td>
		</tr>
		{/foreach}
		</table>
	</div>
{include file='zobject/footer.tpl' inline='true'}