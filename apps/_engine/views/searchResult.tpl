﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="catalog">
{*        <div class="catalog-image" style="background-image: url('')"></div>*}
        <div class="container">
            <div class="catalog-head">
                <div class="catalog-head__left">
                    <div class="catalog-head__icon">
                    </div>
                    <div class="catalog-head__content">
                        <div class="page-head__top">
                            <div class="page-head__title">Результаты поиска</div>
                            <a class="page-head__return mobile_is_show" href="../">
                                <svg class="icon icon-return ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-return"></use>
                                </svg>
                            </a>
                        </div>

                        {include file="blocks/bc.tpl"}
                    </div>

                </div>
            </div>
            <div class="catalog-content {if $catalog_nofilter} catalog_nofilter {/if}">
                <div class="catalog-aside">
                    <div class="catalog-aside__view">

{*                        <div class="catalog-view">*}
{*                            <div class="catalog-view__control is-active js-catalogViewControl" data-view="list">*}
{*                                <svg class="icon icon-tile ">*}
{*                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-tile"></use>*}
{*                                </svg>*}
{*                            </div>*}
{*                            <div class="catalog-view__control js-catalogViewControl" data-view="table">*}
{*                                <svg class="icon icon-list ">*}
{*                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-list"></use>*}
{*                                </svg>*}
{*                            </div>*}
{*                        </div>*}

                    </div>

{*                    <div class="catalog-aside__sort">*}
{*                        <div class="sort-select">*}
{*                            <div class="sort-select__label">Сортировать:</div>*}
{*                            <div class="sort-select__control js-sortSelectControl">*}
{*                                <div class="sort-select__button">*}
{*                                    <div class="sort-select__title js-sortSelectTitle">по имени</div>*}
{*                                    <svg class="icon icon-m-menu-dropdown-down ">*}
{*                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-menu-dropdown-down"></use>*}
{*                                    </svg>*}
{*                                </div>*}
{*                                <select class="sort-select__list js-sortSelectList">*}
{*                                    <option value="1">по имени</option>*}
{*                                    <option value="2">по стоимости</option>*}
{*                                    <option value="3">по размеру</option>*}
{*                                    <option value="4">по толщине</option>*}
{*                                </select>*}
{*                            </div>*}
{*                        </div>*}
{*                    </div>*}

                    {if !$catalog_nofilter}
                        <div class="filter">
                            <div class="filter-header js-filterToggle">
                                <div class="filter-header__title">Фильтр</div>
                                <div class="filter-header__icon">
                                    <svg class="icon icon-m-filter ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-filter"></use>
                                    </svg>
                                </div>
                            </div>

                        </div>
                    {/if}
                </div>

                <div class="catalog-section">
                    <div class="catalog-panel">
                        <div class="catalog-sort">
                            <div class="sort-list">
                            </div>
                        </div>
                    </div>

                    <div class="catalog-products">
                        <div class="product-list  is_product_table_grid ">

                            {foreach item=product from=$products}
                                <div class="product-block">
                                    <a class="product-block__title js-modalLink" href="javascript:;"
                                       data-mfp-src="#modalProduct-{$product.id}">
                                        {$product.title|stripcslashes}
                                    </a>
                                    <div class="product-block__main">
                                        <div class="product-block__preview">
                                            <svg class="icon icon-nophoto ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-nophoto"></use>
                                            </svg>
                                        </div>
                                        <a class="product-block__price js-modalLink" href="javascript:;"
                                           data-mfp-src="#modalProduct-{$product.id}">
                                            {$product.cost_view|stripcslashes}
                                        </a>
                                    </div>

                                    <div class="product-block__char">
                                        <table>
                                            {if !empty($product.tech1)}
                                                <tr>
                                                    <td>{$product.tech1_name|stripcslashes}</td>
                                                    <td>{$product.tech1|stripcslashes}</td>
                                                </tr>
                                            {/if}
                                            {if !empty($product.tech2)}
                                                <tr>
                                                    <td>{$product.tech2_name|stripcslashes}</td>
                                                    <td>{$product.tech2|stripcslashes}</td>
                                                </tr>
                                            {/if}
                                            {if !empty($product.tech3)}
                                                <tr>
                                                    <td>{$product.tech3_name|stripcslashes}</td>
                                                    <td>{$product.tech3|stripcslashes}</td>
                                                </tr>
                                            {/if}
                                        </table>
                                    </div>
                                </div>
                            {/foreach}

                        </div>
                    </div>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                    {include file="blocks/pg.tpl"}

                </div>
            </div>
        </div>
    </div>
    {include file='_footer_form.tpl'}
{/block}


{block name=modal_products}

    {foreach item=product from=$products}
        <div class="modal modal_size_small mfp-hide" id="modalProduct-{$product.id}">
            <div class="modal-block">
                <div class="modal-close js-modalClose">
                    <svg class="icon icon-close ">
                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                    </svg>
                </div>
                <div class="modal-product">
                    <div class="modal-product__center">
                        <div class="modal-product__title">{$product.title|stripcslashes}</div>

                        <form action="/order/:order" class="ajax" rel="cart_add">
                            <div class="modal-product__main">
                                <div class="modal-product__count">
                                    <div class="modal-product__price">{$product.cost_view|stripcslashes}</div>
                                    <div class="count js-count">
                                        <div class="count-button js-countMinus">
                                            <svg class="icon icon-minus ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-minus"></use>
                                            </svg>
                                        </div>
                                        <input class="count-input js-countInput" type="text" name="count"
                                               value="{$cart.prods[$product.id]['count']|default:1}"
                                        >
                                        <input type="hidden" value="{$product.id}" name="id">
                                        <div class="count-button js-countPlus">
                                            <svg class="icon icon-plus ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-plus"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-product__cart">
                                    <button class="button style_green">
                                        <span class="button__text">Добавить в корзину</span>
                                    </button>
                                    <div class="modal-product__total">
                                        Итого:
                                        {if !empty($cart.prods[$product.id])}
                                            {$cart.prods[$product.id].total_view}
                                        {else}
                                            {$product.cost_view|stripcslashes}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="modal-product__table">
                            <table>
                                {if !empty($product.tech1)}
                                    <tr>
                                        <td>{$product.tech1_name|stripcslashes}</td>
                                        <td>{$product.tech1|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech2)}
                                    <tr>
                                        <td>{$product.tech2_name|stripcslashes}</td>
                                        <td>{$product.tech2|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech3)}
                                    <tr>
                                        <td>{$product.tech3_name|stripcslashes}</td>
                                        <td>{$product.tech3|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech4)}
                                    <tr>
                                        <td>{$product.tech4_name|stripcslashes}</td>
                                        <td>{$product.tech4|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech5)}
                                    <tr>
                                        <td>{$product.tech5_name|stripcslashes}</td>
                                        <td>{$product.tech5|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech6)}
                                    <tr>
                                        <td>{$product.tech6_name|stripcslashes}</td>
                                        <td>{$product.tech6|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech7)}
                                    <tr>
                                        <td>{$product.tech7_name|stripcslashes}</td>
                                        <td>{$product.tech7|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech8)}
                                    <tr>
                                        <td>{$product.tech8_name|stripcslashes}</td>
                                        <td>{$product.tech8|stripcslashes}</td>
                                    </tr>
                                {/if}
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-cancel js-modalClose">
                    <div class="modal-cancel__title">Закрыть</div>
                </div>
            </div>
        </div>
    {/foreach}

{/block}


{block name=footer}
{/block}

