<?php

class Order_Controller extends ZObject_Controller
{
    //  Типы доставки
    const TYPE_DELIVERY = 'delivery';
    const TYPE_PICKUP = 'pickup';

    const DELIVERY = [
        self::TYPE_PICKUP   => 'Самовывоз',
        self::TYPE_DELIVERY => 'Доставка',
    ];

    /**
     * Корзина
     * /order/
     */
    public function action_index()
    {
        V('checkout');
    }

    /**
     * Корзина. 2 шаг. Ввод перснальных данных
     * /order/:delivery
     */
    public function action_delivery()
    {
//        $customer = new Customers();
        $cart = new Cart();

        //  Если пустая корзина, редиект
        if ($cart->isEmpty()) {
            redirect('/order/');
        }

        V('checkout-2');
    }

    /**
     * Корзина. 3 шаг. Отправка заказа
     * /order/:confirm
     */
    public function action_confirm()
    {
        $errors = [];
        $cart   = new Cart();

        if ($cart->isEmpty()) {
            redirect('/order/');
            $errors[] = 'Корзина пуста';
        }

        $delivery      = $_POST['delivery'] ?? self::TYPE_PICKUP;
        $pickupAddress = $_POST['pickup_address'] ?? null;
        $city          = _post('city');
        $address       = _post('address');
        $date          = _post('date');
        $time          = _post('time');
        $name          = _post('name');
        $email         = _post('email');
        $phone         = _post('phone');
        $comment       = _post('comment');
        $agreement     = _post('agreement');

        if ($delivery === self::TYPE_PICKUP && empty($pickupAddress)) {
            $errors[] = 'Укажите пункт самовывоза';
        }

        if ($delivery === self::TYPE_DELIVERY) {
            if (empty($city)) {
                $errors[] = 'Укажите город доставки';
            }

            if (empty($address)) {
                $errors[] = 'Укажите адрес доставки';
            }
        }

        if (empty($name)) {
            $errors[] = 'Укажите имя';
        }

        if (empty($email) && empty($phone)) {
            $errors[] = 'Укажите email или телефон для связи';
        }

        if (empty($agreement)) {
            $errors[] = 'Необходимо согласие на обработку персональных данных';
        }

        if (!empty($errors)) {
            $this->jsonError($errors);
        }

        $pickupAddressArr = $this->getPickupAddress($pickupAddress ?? 0);


        //  Добавляем заказ
        $orderId = Q('INSERT INTO @@orders SET 
                        `delivery` = ?s, `pickup_address` = ?s, 
                        `city` = ?s, `address` = ?s, `date` = ?s, `time` = ?s,
                        `name` = ?s, `email` = ?s, `phone` = ?s, `comment` = ?s,
                        `html` = ?s, `cart` = ?s,
                        `created_at` = now()',
            [
                self::DELIVERY[$delivery],
                $pickupAddressArr['addr_full'] ?? '',
                $city,
                $address,
                $date,
                $time,
                $name,
                $email,
                $phone,
                $comment,
                json_encode($cart->cart),
            ]
        );

        //  Заказ
        $order = [
            'id'            => $orderId,
            'title'         => $orderId,
            'deliveryType'  => $delivery,
            'delivery'      => self::DELIVERY[$delivery],
            'pickupAddress' => $pickupAddressArr['addr_full'] ?? '',
            'city'          => $city,
            'address'       => $address,
            'date'          => $date,
            'time'          => $time,
            'name'          => $name,
            'email'         => $email,
            'phone'         => $phone,
            'comment'       => $comment,
            'cart'          => $cart->cart,
            'created'       => (new \DateTime())->format('d.m.Y H:i'),
        ];

        //  Рендер шаблон
        $sm = SM();
        $sm->assign('cart', $cart->cart);
        $sm->assign('order', $order);
        $cart_html = $sm->fetch('mail/_order_render.tpl');

        //  Обновляем заказ
        Q('UPDATE @@orders SET `title`=?s, `html`=?s WHERE `id`=?i', [
            $orderId,
            $cart_html,
            $orderId,
        ]);

        //  Данные для рендера письма
        $sm->assign('cart_html', $cart_html);
        $emailTpl = $sm->fetch('mail/order.tpl');

        //    Include PHP mailer
        require_once SHARED_PATH . '/mmailer.php';
        $mm          = mmailer();
        $mm->Subject = 'Новый заказ';
        $mm->Body    = $emailTpl;

        //  Отправляем заказ покупателю
        if (!empty($email)) {
            $mm->addAddress($email);
        }

        //  Отправляем заказ филиал
        $branchEmail = $this->getContactsEmail(
            $_POST['pickup_address'] ?? 1
        );
        $mm->addAddress(trim($branchEmail));

        //  Отправляем заказ администратору сайта
        $to = $this->getZcfg('order_feedback');
        $to = explode(',', $to['order_feedback']);
        foreach ($to as $key => $value) {
            $mm->addAddress(trim($value));
        }

        //  Отправляем
        $rrr = $mm->send();

        //  Очищаем корзину
        $cart->clear();

        $this->jsonSuccess([]);
    }

    /**
     *
     */
    public function action_success()
    {
        V('checkout-3');
    }

    /**
     * Добавить в корзину
     *
     * @throws SmartyException
     */
    public function action_order()
    {
        $result = [
            'success' => 'false',
        ];

        //  Пробуем добавить товар в корзину
        $cart = $this->addToCart(
            _get('id'),
            _get('count')
        );

        //  Если товар добавился, готовим ответ
        if ($cart !== false) {
            //  Результат
            $result['success'] = true;
            $result['cart']    = $cart;
            $result['product'] = $cart->prods(_get('id'));

            $sm = SM();
            $sm->assign('cart', $cart->cart);
            $sm->assign('product', $result['product']);
            $result['_cart_dropdown'] = $sm->fetch('_cart_dropdown.tpl');
            $result['_cart_added']    = $sm->fetch('_cart_product_added.tpl');
            $result['_cart_header']   = $sm->fetch('_cart_header.tpl');
        }

        //  Возвращаем ответ в JSON
        exit(json_encode($result));
    }

    /**
     * Функция добавления в корзину
     *
     * @param [int] $id
     * @param [int] $count
     *
     * @return Cart|false
     */
    private function addToCart($id, $count)
    {
        //  Инициализируем корзину
        $cart = new Cart();

        //  Ищем товар, чтобы добавить в корзину
        $product = Q(
            'SELECT N.`path`, P.`id`, P.`title`, P.`cost`, P.`cost_view`, P.`tech1`, P.`tech1_name`, P.`tech2`, P.`tech2_name`, P.`tech3`, P.`tech3_name`  FROM @@product as P
            LEFT JOIN @@nodes as N
            ON P.id = N.object_id
            WHERE N.object_type="product" AND  P.id = ?i',
            [$id]
        )->row();

        if (!empty($product)) {
            //  Добавляем в корзину товар
            $cart->add(
                (int)$product['id'],
                (int)$count,
                (float)$product['cost'],
                $product
            );

            //  Возвращаем корзину
            return $cart;
        } else {
            //  Возвращаем false
            return false;
        }
    }

    /**
     * Удаляем из корзины
     *
     * @return void
     */
    public function action_remove()
    {
        $cart = new Cart();

        //  Удаляем из корзины
        $cart->clear(
            _get('id')
        );

        //  Результат
        $result['success'] = true;
        $result['cart']    = $cart;
        $result['product'] = _get('id');

        $sm = SM();
        $sm->assign('cart', $cart->cart);

        //  Возвращаем ответ в JSON
        exit(
        json_encode($result)
        );
    }

    /**
     * @param int $contactId
     * @return mixed
     */
    protected function getPickupAddress(int $contactId)
    {
        return Q('select * from @@contacts where id=?i', [$contactId])->row();
    }
}
