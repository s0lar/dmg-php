<?php


class Offers1cXml extends Abstract1cXml
{
    const XPATH_PRODUCT = '/КоммерческаяИнформация/ПакетПредложений/Предложения/Предложение';
    const XPATH_PRODUCT_TECH = 'ХарактеристикиТовара/ХарактеристикаТовара';
    const XPATH_PRODUCT_COST = 'Цены/Цена';
    const XPATH_PRODUCT_COST_TOTAL = 'ЦенаЗаЕдиницу';
    const XPATH_PRODUCT_COST_VIEW = 'Представление';
    const XPATH_PRODUCT_COST_VIEW_CURRENCY = 'Валюта';
    const XPATH_PRODUCT_COST_VIEW_UNIT = 'Единица';
    const XPATH_PRODUCT_NAME = 'Наименование';
    const XPATH_PRODUCT_VALUE = 'Значение';

    const SKIP_TECH = [
        'КодХарактеристики',
        'ЕдиницаИзмеренияЦены',
    ];


    public function execute()
    {
        $updatedCount = 0;

        foreach ($this->xml->xpath(self::XPATH_PRODUCT) as $product) {
            $id1c  = $product->{self::XPATH_ID}->__toString();
            $id1c  = substr($id1c, 0, strpos($id1c, '#'));
            $title = $product->{self::XPATH_TITLE}->__toString();
            $tech  = $product->xpath(self::XPATH_PRODUCT_TECH);

            //  Cost
            if (!empty($product->xpath(self::XPATH_PRODUCT_COST)[0])) {
                $costValue = $product->xpath(self::XPATH_PRODUCT_COST)[0]->{self::XPATH_PRODUCT_COST_TOTAL}->__toString();
                $costView  = sprintf(
                    '%s %s за %s',
                    number_format($costValue, $decimals = 2, $dec_point = ",", $thousands_sep = " "),
                    $product->xpath(self::XPATH_PRODUCT_COST)[0]->{self::XPATH_PRODUCT_COST_VIEW_CURRENCY}->__toString(),
                    $product->xpath(self::XPATH_PRODUCT_COST)[0]->{self::XPATH_PRODUCT_COST_VIEW_UNIT}->__toString()
                );
            }

            $tech = $this->prepareTech($tech);

            $res = Q('UPDATE @@product SET `title`=?s, `cost`=?f, `cost_view`=?s, 
                        `tech1_name`=?s, `tech1`=?s,
                        `tech2_name`=?s, `tech2`=?s,
                        `tech3_name`=?s, `tech3`=?s,
                        `tech4_name`=?s, `tech4`=?s,
                        `tech5_name`=?s, `tech5`=?s,
                        `tech6_name`=?s, `tech6`=?s,
                        `tech7_name`=?s, `tech7`=?s,
                        `tech8_name`=?s, `tech8`=?s,
                        `tech9_name`=?s, `tech9`=?s,
                        `tech10_name`=?s, `tech10`=?s,
                        `tech11_name`=?s, `tech11`=?s,
                        `tech12_name`=?s, `tech12`=?s,
                        `tech13_name`=?s, `tech13`=?s,
                        `tech14_name`=?s, `tech14`=?s,
                        `tech15_name`=?s, `tech15`=?s,
                        `tech16_name`=?s, `tech16`=?s,
                        `updated_at`=NOW()
                        WHERE `1c_id`=?s',
                [
                    $title,
                    $costValue ?? 0,
                    $costView ?? 'Цена по запросу',
                    $tech[0]['name'] ?? '',
                    $tech[0]['value'] ?? '',
                    $tech[1]['name'] ?? '',
                    $tech[1]['value'] ?? '',
                    $tech[2]['name'] ?? '',
                    $tech[2]['value'] ?? '',
                    $tech[3]['name'] ?? '',
                    $tech[3]['value'] ?? '',
                    $tech[4]['name'] ?? '',
                    $tech[4]['value'] ?? '',
                    $tech[5]['name'] ?? '',
                    $tech[5]['value'] ?? '',
                    $tech[6]['name'] ?? '',
                    $tech[6]['value'] ?? '',
                    $tech[7]['name'] ?? '',
                    $tech[7]['value'] ?? '',
                    $tech[8]['name'] ?? '',
                    $tech[8]['value'] ?? '',
                    $tech[9]['name'] ?? '',
                    $tech[9]['value'] ?? '',
                    $tech[10]['name'] ?? '',
                    $tech[10]['value'] ?? '',
                    $tech[11]['name'] ?? '',
                    $tech[11]['value'] ?? '',
                    $tech[12]['name'] ?? '',
                    $tech[12]['value'] ?? '',
                    $tech[13]['name'] ?? '',
                    $tech[13]['value'] ?? '',
                    $tech[14]['name'] ?? '',
                    $tech[14]['value'] ?? '',
                    $tech[15]['name'] ?? '',
                    $tech[15]['value'] ?? '',
                    $id1c,
                ]);

            if ($res > 0) {
                $updatedCount++;
            }
        }

        $this->logger->info('Updated items: ' . $updatedCount);
    }

    /**
     * @param array $techs
     * @return array
     */
    public function prepareTech(array $techs)
    {
        $result = [];

        foreach ($techs as $tech) {
            $name  = $tech->{self::XPATH_PRODUCT_NAME}->__toString();
            $value = $tech->{self::XPATH_PRODUCT_VALUE}->__toString();

            //  Ignore tech
            if (in_array($name, self::SKIP_TECH)) {
                continue;
            }

            $result[] = [
                'value' => $value,
                'name'  => $name,
            ];
        }

        return $result;
    }
}