<?
	/**
	 * sysem base config setting
	 * @author Logan Cai (cailongqun@yahoo.com.cn)
	 * @link www.phpletter.com
	 * @since 22/April/2007
	 *
	 */
	define('DATE_TIME_FORMAT', 'd/M/Y H:i:s');
	//Label
		//Top Action
		define('LBL_ACTION_REFRESH', 'Обновить');
		define("LBL_ACTION_DELETE", 'Удалить');
		//File Listing
	define('LBL_NAME', 'Название');
	define('LBL_SIZE', 'Размер');
	define('LBL_MODIFIED', 'Изменен:');
		//File Information
	define('LBL_FILE_INFO', 'Инфо о файле:');
	define('LBL_FILE_NAME', 'Название:');	
	define('LBL_FILE_CREATED', 'Создан:');
	define("LBL_FILE_MODIFIED", 'Изменен:');
	define("LBL_FILE_SIZE", 'Размер:');
	define('LBL_FILE_TYPE', 'Тип:');
	define("LBL_FILE_WRITABLE", 'Перезаписать?');
	define("LBL_FILE_READABLE", 'Защищен от записи?');
		//Folder Information
	define('LBL_FOLDER_INFO', 'Инфо о директории');
	define("LBL_FOLDER_PATH", 'Путь:');
	define("LBL_FOLDER_CREATED", 'Создан:');
	define("LBL_FOLDER_MODIFIED", 'Изменен:');
	define('LBL_FOLDER_SUDDIR', 'Поддиректории:');
	define("LBL_FOLDER_FIELS", 'Файлы:');
	define("LBL_FOLDER_WRITABLE", 'Перезаписать?');
	define("LBL_FOLDER_READABLE", 'Защищен от записи?');
		//Preview
	define("LBL_PREVIEW", 'Превью');
	//Buttons
	define('LBL_BTN_SELECT', 'Выбрать');
	define('LBL_BTN_CANCEL', 'Отменить');
	define("LBL_BTN_UPLOAD", 'Закачать');
	define('LBL_BTN_CREATE', 'Создать');
	define("LBL_BTN_NEW_FOLDER", 'Новая папка');
	//ERROR MESSAGES
		//deletion
	define('ERR_NOT_FILE_SELECTED', 'Выберите файл.');
	define('ERR_NOT_DOC_SELECTED', 'Не выбраны файлы для удаления.');
	define('ERR_DELTED_FAILED', 'Невозможно удалить выбранные файлы');
	define('ERR_FOLDER_PATH_NOT_ALLOWED', 'Неверный путь для директории');
		//class manager
	define("ERR_FOLDER_NOT_FOUND", 'Невозможно найти выбранную директорию: ');
		//rename
	define('ERR_RENAME_FORMAT', 'Имя должно состоять только из букв, цифр, и подчеркиваний.'); //Please give it a name which only contain letters, digits, space, hyphen and underscore.
	define('ERR_RENAME_EXISTS', 'Такое имя существует');
	define('ERR_RENAME_FILE_NOT_EXISTS', 'Файл/директория не существует');
	define('ERR_RENAME_FAILED', 'Невозможно переименовать, попробуйте ещё раз.');
	define('ERR_RENAME_EMPTY', 'Введите имя');
	define("ERR_NO_CHANGES_MADE", 'Изменений не было!');
	define('ERR_RENAME_FILE_TYPE_NOT_PERMITED', 'У Вас недостаточно прав, чтобы изменить расширение.');
		//folder creation
	define('ERR_FOLDER_FORMAT', 'Имя должно состоять только из букв, цифр, и подчеркиваний.');
	define('ERR_FOLDER_EXISTS', 'Такое имя существует');
	define('ERR_FOLDER_CREATION_FAILED', 'Невозможно создать директорию, попробуйте ещё раз.');
	define('ERR_FOLDER_NAME_EMPTY', 'Введите имя');
	
		//file upload
	define("ERR_FILE_NAME_FORMAT", 'Имя должно состоять только из букв, цифр, и подчеркиваний.');
	define('ERR_FILE_NOT_UPLOADED', 'Файл для закачки не выбран');
	define('ERR_FILE_TYPE_NOT_ALLOWED', 'Нельзя закачивать файлы такого типа.');
	define('ERR_FILE_MOVE_FAILED', 'Невозможно переместить файл.');
	define('ERR_FILE_NOT_AVAILABLE', 'Файл недоступен');
	define('ERROR_FILE_TOO_BID', 'Файл слишком большой. (max: %s)');
	

	//Tips
	define('TIP_FOLDER_GO_DOWN', 'Зайти в директорию...');
	define("TIP_DOC_RENAME", 'Двойной клик - редактировать...');
	define('TIP_FOLDER_GO_UP', 'Выйти из директории...');
	define("TIP_SELECT_ALL", 'Отметить все');
	define("TIP_UNSELECT_ALL", 'Сбросить все');
	//WARNING
	define('WARNING_DELETE', 'Подтвердите удаление');
	//Preview
	define('PREVIEW_NOT_PREVIEW', 'Превью недоступно.');
	define('PREVIEW_OPEN_FAILED', 'Невозможно открыть файл.');
	define('PREVIEW_IMAGE_LOAD_FAILED', 'Невозможно загрузить файл');

	//Login
	define('LOGIN_PAGE_TITLE', 'Ajax File Manager Логин');
	define('LOGIN_FORM_TITLE', 'Логин');
	define('LOGIN_USERNAME', 'Имя:');
	define('LOGIN_PASSWORD', 'Пароль:');
	define('LOGIN_FAILED', 'Неверное имя/пароль.');
	
	
?>