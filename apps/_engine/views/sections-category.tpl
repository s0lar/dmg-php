﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_category">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>

            <div class="page-content">
                <div class="for-buyers">
                    {foreach item=menu from=$sub_menu}
                        <a class="for-buyers__elem" href="{$menu.path}">
                            <div class="for-buyers__title">{$menu.title}</div>
                            <div class="for-buyers__info">
                                {if !empty($menu.icon)}
                                    <div class="for-buyers__icon">
                                        <img src="{$menu.icon}">
                                    </div>
                                {/if}
                                <div class="for-buyers__more"><span>Подробнее</span>
                                    <svg class="icon icon-arrow-triangle ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                    </svg>
                                </div>
                            </div>
                        </a>
                    {/foreach}
                </div>
            </div>

        </div>

        <div class="feedback style_transparent">
            <div class="container">
                <div class="feedback__container">
                    <div class="feedback__left">
                        <div class="feedback__caption">
                            <div class="feedback__title">
                                Мы открыты к сотрудничеству! <br>
                                Отправьте заявку и наши менеджеры <br>
                                свяжутся с вами, чтобы уточнить <br>
                                все детали.
                            </div>
                        </div>
                    </div>
                    <div class="feedback__right">
                        {include file='_form.tpl' popup=false}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}