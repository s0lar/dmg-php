<?php

return array(
    # types of fields
    'fields'    => array(
        'title'   => 'string',
        'photo'   => 'gs_file',
        'content' => 'html',
        'sort'    => 'int',
    ),

    # labels of fields
    'ui'        => array(
        'title'   => 'Название',
        'photo'   => 'Фото',
        'content' => 'Подробное описание',
        'sort'    => 'Порядок',
    ),

    #
    'input_cfg' => array(
        'photo' => array('type' => 'image', 'mask' => 'article_photo'),
    ),

    # node configuration
    'node'      => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user',
    ),

    #
    'view'      => array(
        'mode'       => 'list',
        'fields'     => array('title', 'sort'),
        'orderby'    => ' `sort` DESC',
        'edit_field' => 'title',
        'limit'      => 150,
    ),

    # labels for actions
    'labels'    => array(
        'list'    => 'Статьи',
        'add'     => 'Добавить',
        'adding'  => 'Добавление',
        'edit'    => 'Редактировать',
        'editing' => 'Редактирование',
        'delete'  => 'Удалить',
    ),
);
