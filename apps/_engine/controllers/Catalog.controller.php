<?php

class Catalog_Controller extends ZObject_Controller
{
    public function action_index()
    {
        //  Get all Categories
        $categories = [];

        $res = Q('SELECT N.`path`, N.`parent_id` as `nodes_pid`, N.`id` as `nodes_id`, P.* FROM @@category1 as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="category1"
                        ORDER BY P.`sort` desc');

        while ($r = $res->each()) {
            $r['icon'] = $this->getFM($r['icon'], 'a');

            $categories[0][$r['nodes_id']] = $r;
        }

        $res = Q('SELECT N.`path`, N.`parent_id` as `nodes_pid`, N.`id` as `nodes_id`, P.* FROM @@category2 as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="category2"
                        ORDER BY P.`sort` desc');

        while ($r = $res->each()) {
            $r['icon'] = $this->getFM($r['icon'], 'a');

            $categories[$r['nodes_pid']][$r['nodes_id']] = $r;
        }


        $res = Q('SELECT * FROM @@sections WHERE id=?i ', [$this->node['object_id']])->row();
        RS('page', $res);

//        $page = Q('SELECT * FROM @@nodes WHERE `object_type`="category" ORDER BY `sort` LIMIT 1')->row();
        RS('categories', $categories);
        V('catalog');
    }

    /**
     * Страница результатов поиска
     */
    public function action_search()
    {
        //  Поисковая строка
        $search = $this->clearSearchString(
            _get('search')
        );

        //  Выбираем продукты и делаем постраничную разбивку
        $result = $this->search($search);

//        RS('products', $result['products']);
//        RS('products_count', count($result['products']));
//        RS('searchString', $search);

        $sm = SM();
        $sm->assign('products', $result['products']);
        $sm->assign('pg', $result['pg']);
        $sm->assign('search_link', '/catalog/:result?search=' . _get('search'));

        $this->jsonSuccess([
            '_search_dropdown' => $sm->fetch('_search_dropdown.tpl'),
        ]);
    }

    public function action_result()
    {
        //  Поисковая строка
        $search = $this->clearSearchString(
            _get('search')
        );

        //  Выбираем продукты и делаем постраничную разбивку
        $result = $this->search($search);

        RS('products', $result['products']);
        RS('catalog_nofilter', 1);
        V('searchResult');
    }

    /**
     * Чистим поисковую строку
     *
     * @param [string] $search
     *
     * @return string
     */
    private function clearSearchString($search)
    {
        //  Чистим поисковую фразу
        $search = strip_tags($search);
        $search = trim($search);

        return $search;
    }

    /**
     * Поиск в каталоге по заголовку
     *
     * @param [string] $search
     * @param [int] $limit
     *
     * @return array()
     */
    private function search($search)
    {
        //  Заменяем пробелы на %
        $search = preg_replace("/\s+/", "%", $search);

        //  Постраничная разбивка
        $sql = Qb('SELECT 1
                FROM `@@nodes` as N
				LEFT JOIN `@@product` as P
				ON N.`object_id` = P.`id`
				WHERE N.`object_type`=?s AND P.`title` LIKE ?s',
            ['product', "%{$search}%"]
        );

        $pg = PG($sql, 16);
        RS('pg', $pg);

        //  Поиск по каралогу
        $res = Q(
            'SELECT N.`path`, P.`id`, P.`title`, P.`cost_view`, 
                    P.`tech1`, P.`tech1_name`, P.`tech2`, P.`tech2_name`, P.`tech3`, P.`tech3_name`  
                FROM `@@nodes` as N
				LEFT JOIN `@@product` as P
				ON N.`object_id` = P.`id`
				WHERE N.`object_type`=?s AND P.`title` LIKE ?s LIMIT ?i, ?i',
            ['product', "%{$search}%", $pg['offset'], $pg['limit']]
        );

        while ($r = $res->each()) {
            $products[] = $r;
        }

        return [
            'search'   => $search,
            'products' => isset($products) ? $products : [],
            'pg'       => $pg,
        ];
    }

    /**
     * AJAX-поиск
     */
    public function action_search_post_json()
    {
        //  Ответ по-умолчанию
        $result = ['success' => true];

        //  Поисковая строка
        $search = $this->clearSearchString(
            _post('search')
        );

        //  Для поиска необходимо ввести минимум 2 символа
        if (mb_strlen($search) < 2) {
            exit(
            json_encode(
                [
                    'success'        => true,
                    'msg'            => 'Wrong length of search string',
                    '_search_slider' => '',
                ]
            )
            );
        }

        //  Рендерим код для отдачи через Ajax
        $sm = SM();
        $sm->assign(
            'search',
            $this->search(
                $search,
                10
            )
        );
        $sm->assign('search_string', _post('search'));

        $sm->assign(
            'allProductsLink',
            '/catalog/:search/-search/' . $search
        );

        //  HTML-код с результатами поиска
        $result['_search_slider'] = $sm->fetch('_search_slider.tpl');
//        $result['search'] = _post('search');

        exit(
        json_encode($result)
        );
    }
}
