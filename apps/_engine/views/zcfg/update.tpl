{include file='zobject/header.tpl' inline='true'}

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="#">Изменения записи в настройках</a></li>
	</ul>



	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		
		{* Add + Add message *}
		{if isset($smarty.get.msg)}
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">
					&times;
				</button>
				<h4>Запись успешно изменена</h4>
			</div>
		{/if}
		
		<fieldset class="group">
			<!-- <legend class="group-title">{$object.labels.adding}</legend> -->
			<form action="/zcfg/:update/-id/{$_item.id}/" method="post" class="ajax1" id="object-add">
				
				<div class="row-fluid border-bottom1" style="margin-bottom:15px">
					<label class="span5 f14  mb0" for="f_title" style="min-height:20px">Название поля для пользователя</label>
					<input type="text" name="title" id="f_title" class="input-text span6" placeholder="E-mail для уведомлений" value="{$_item.title}">
				</div>
				
				<div class="row-fluid border-bottom1" style="margin-bottom:15px">
					<label class="span5 f14  mb0" for="f_example" style="min-height:20px">Пример заполнения</label>
					<textarea name="example" id="f_example" class="input-text span6" placeholder="Добавьте e-mail'ы через запятую">{$_item.example}</textarea>
				</div>
				
				<div class="row-fluid border-bottom1" style="margin-bottom:15px">
					<label class="span5 f14  mb0" for="f_title" style="min-height:20px">Тип поля</label>
					<select name="view" class="input-text span6">
						{foreach item=i from=$view key=k}
							{if $_item.view == $k}
								<option value="{$k}" selected="selected">{$i}</option>
							{else}
								<option value="{$k}">{$i}</option>
							{/if}
						{/foreach}
					</select>
				</div>
				
				<div class="row-fluid border-bottom1" style="margin-bottom:15px">
					<label class="span5 f14  mb0" for="f_name" style="min-height:20px">Системное имя поля (ключ)</label>
					<input type="text" name="name" id="f_name" class="input-text span6" placeholder="feedback_email" value="{$_item.name}">
				</div>
				
				
				<div class="row-fluid border-bottom1" style="margin-bottom:15px">
					<label class="span5 f14  mb0" for="f_is_global" style="min-height:20px">Загружать глобально?</label>
					<input type="checkbox" name="is_global" id="f_is_global" {if $_item.is_global == 1}checked="checked"{/if} />
				</div>
				
				
				<div class="well well-panel well-panel-controll">
					<div class="btn-group">
						<input type="submit" name="" value="Изменить" class="btn btn-primary" />
						{*<input type="submit" name="apply" value="Изменить" class="btn" />*}
						
					</div>
					<a href="/zcfg/:edit_node" class="btn btn-small pull-right">Отмена</a>
				</div>
				
			</form>
		</fieldset>
		

	</div>
{include file='zobject/footer.tpl' inline='true'}