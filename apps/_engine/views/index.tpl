﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="hero">
        <div class="hero-slider js-heroSlider">
            <div class="hero-background js-heroBackgrounds"></div>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="hero-slider__image" style="background-image: url('/theme/dmg_markup/build/images/index-back-1-new.jpg')"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="hero-slider__image" style="background-image: url('/theme/dmg_markup/build/images/index-back-2-new.jpg')"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="hero-slider__image" style="background-image: url('/theme/dmg_markup/build/images/index-back-3-new.jpg')"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="hero-slider__image" style="background-image: url('/theme/dmg_markup/build/images/index-back-4-new.jpg')"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="hero-slider__image" style="background-image: url('/theme/dmg_markup/build/images/index-back-5-new.jpg')"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-video">
            <video id="video" width="100%" height="auto" muted="muted" autoplay="autoplay" loop="loop" preload="auto">
                <source src="/theme/dmg_markup/build/media/dmg-background.mp4">
            </video>
        </div>
        <div class="hero-section">
            <div class="container">
                <div class="hero-container">
                    <div class="hero-content is-animated">
                        <div class="hero-content__inner">
                            <div class="hero-title">Нержавеющая сталь <br>от ведущих производителей.</div>
                            <div class="hero-controls">
                                <div class="hero-controls__button">
                                    <a class="button style_border_green" href="/catalog/">
                                        <span class="button__text">
                                            Каталог продукции
                                            <svg class="icon icon-arrow-triangle ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                            </svg>
                                        </span>
                                    </a>
                                </div>
                                <div class="hero-controls__link"><a href="/f/dmg-catalog.pdf">Скачать каталог</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="hero-nav is-animated" data-delay="1000">
                        <div class="hero-nav__inner">
                            <div class="hero-nav__center">

                                {foreach item=category from=$menu_category[0] key=parent}
                                    <div class="hero-nav__elem">
                                        <a class="hero-nav__link js-heroNavLink" href="javascript:;">
                                            <span class="hero-nav__title">
                                                {$category.title}
                                            </span>
                                            <span class="hero-nav__icon">
                                                <img class="hidden-xs js-x2size" src="{$category.icon}">
                                                <img class="visible-xs" src="{$category.icon2}">
                                            </span>
                                        </a>

                                        {if !empty($menu_category[$category.nodes_id])}
                                            <div class="hero-nav__hover">
                                                <ul class="hero-nav__list">
                                                    {foreach item=sub_category from=$menu_category[$category.nodes_id]}
                                                        <li><a href="{$sub_category.path}">{$sub_category.title}</a></li>
                                                    {/foreach}
                                                </ul>
                                            </div>
                                        {/if}
                                    </div>
                                {/foreach}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-partner">
        <div class="container">
            <div class="hero-partner__left">
                <div class="hero-partner__slider js-heroPartnerSlider">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">

                            {foreach item=news from=$newsList}
                                <div class="swiper-slide">
                                    <div class="hero-partner__slide">
                                        {if !empty($news.photo)}
                                            <div class="hero-partner__image" style="background-image: url('{$news.photo}')"></div>
                                        {/if}
                                        <div class="hero-partner__desc">
                                            <p>{$news.anons}</p>
                                        </div>
                                        <div class="hero-partner__button">
                                            <a class="button style_green" href="{$news.path}">
                                                <span class="button__text">Подробнее</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}

                        </div>
                    </div>
                    <div class="hero-partner__nav">
                        <div class="slider-button js-heroPartnerSliderPrev">
                            <svg class="icon icon-arrow-triangle-big-left ">
                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle-big-left"></use>
                            </svg>
                        </div>
                        <div class="slider-button js-heroPartnerSliderNext">
                            <svg class="icon icon-arrow-triangle-big-right ">
                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle-big-right"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero-partner__right">
                <div class="hero-partner__logos">
                    <a class="partner"  href="http://www.inoxmim.net" target="_blank" style="background-image: url('/theme/dmg_markup/build/images/inoxmim_back.jpg')">
                        <div class="partner__image"><img src="/theme/dmg_markup/build/images/inoxmim.png"></div>
                    </a>
                    <a class="partner"  href="http://www.metall-prom.net" target="_blank" style="background-image: url('/theme/dmg_markup/build/images/metallprom_back.jpg')">
                        <div class="partner__image"><img src="/theme/dmg_markup/build/images/metallprom_white.png"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="hero-about">
        <div class="container">
            <div class="hero-about__left">
                <div class="hero-about__desc">
                    <h6>Наша миссия</h6>
                    <p>Защита, надежность и долговечность — наш атрибут качества. Мы сотрудничаем с ведущими мировыми поставщиками и помогаем Вам
                        создать главное — качественный продукт. </p>
                </div>
                <div class="hero-about__button"><a class="button style_border_green" href="/company/"><span class="button__text">О компании</span></a>
                </div>
            </div>
            <div class="hero-about__right">
                <div class="hero-about__image"><img class="hidden-xs" src="/theme/dmg_markup/build/images/company-map-huge-big.png"><img
                            class="visible-xs" src="/theme/dmg_markup/build/images/company-map-huge-m.png"></div>
                <div class="hero-about__advantages">
                    <div class="hero-about__advantage"><b>1996</b><span>Год основания</span></div>
                    <div class="hero-about__advantage"><b>3</b><span>Подразделения</span></div>
                    <div class="hero-about__advantage"><b>50 000<sup>+</sup></b><span>Позиций в каталоге</span></div>
                    <div class="hero-about__advantage"><b>5 000<sup>+</sup></b><span>Партнеров</span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="feedback style_gray">
        <div class="container">
            <div class="feedback__container">
                <div class="feedback__left">
                    <div class="feedback__caption">
                        <div class="feedback__title">
                            Мы открыты к сотрудничеству! <br>
                            Отправьте заявку и наши менеджеры <br>
                            свяжутся с вами, чтобы уточнить <br>
                            все детали.
                        </div>
                    </div>
                </div>
                <div class="feedback__right">
                    {include file='_form.tpl' popup=false}
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}