<?php
return array(
    # types of fields
    'fields' => array(
        'title' => 'string',
        'anons' => 'text',
        'city' => 'string',
        'phone' => 'text',
        'site' => 'string',
        'email' => 'text',
        'type' => array(
            'type' => 'list',
            'items' => array(
                'retail' => 'Розница',
                'online-store' => 'Интерне-магазин',
            ),
        ),
        'point' => 'map',
        'sort' => 'int',
    ),

    # labels of fields
    'ui' => array(
        'title' => 'Заголовок',
        'anons' => 'Описание',
        'city' => 'Город',
        'phone' => 'Телефоны (1 телефон на 1 строке)',
        'site' => 'Сайт',
        'email' => 'Email (1 email на 1 строке)',
        'type' => 'Тип',
        'point' => 'Координаты на карте',
        'sort' => 'Порядок',
    ),

    #
    'input_cfg' => array(
        'photo' => array('type' => 'image', 'mask' => 'series_photo'),
        'background' => array('type' => 'image', 'mask' => 'series_bg'),
    ),

    # node configuration
    'node' => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name' => '-auto',
    ),

    #   Views
    'view' => array(
        'mode' => 'list',
        'fields' => array('title', 'type', 'city', 'sort'),
        'orderby' => ' `sort` DESC ',
        'edit_field' => 'title',
        'limit' => 150,
    ),

    # labels for actions
    'labels' => array(
        'list' => 'Магазины',
        'add' => 'Добавить',
        'adding' => 'Добавление',
        'edit' => 'Редактировать',
        'editing' => 'Редактирование',
        'delete' => 'Удалить',
    ),
);
