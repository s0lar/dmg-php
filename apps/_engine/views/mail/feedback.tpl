{extends file='mail/layout.tpl'}

{block name=content}
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr><td class="offset-double" style="height: 60px;line-height: 60px;">&nbsp;</td></tr>
	<tr>
		<td>
			<div style="font-weight: bold; font-size: 24px;" class="title">Обратная связь с сайта <br /><a target="_blank" style="color:#0076ff" href='http://{$smarty.server.HTTP_HOST}'>{$smarty.server.HTTP_HOST}</a></div>
		</td>
	</tr>

	{* <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Страница:</span> {$post.page|stripslashes|default:'-'}</td></tr> *}

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Дата отправки:</span> {$date.date} {$date.time} </td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Имя:</span> {$post.name|stripslashes|default:'-'}</td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Телефон:</span> {$post.phone|stripslashes|default:'-'}</td></tr>

{*	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>*}
{*	<tr><td><span style="font-weight: bold;">E-mail:</span> {$post.email|stripslashes|default:'-'}</td></tr>*}

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Комментарий:</span> {$post.message|stripslashes|default:'-'}</td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>


</table>
{/block}