<?php

return [
    # types of fields
    'fields'    => [
        'title'    => 'string',
        'content'  => 'text',
        'icon'     => 'gs_file',
        'icon2'    => 'gs_file',
        'see_more' => [
            'type'     => 'mn2',
            'relation' => [
                'table' => 'category1',       //  Название таблицы
                'var'   => 'id',        //  Ключ для связи
                'value' => 'title',      //  Имя для отображения в CHECKBOX'е
                'sort'  => 'title',      //  Сортировка для отображения
            ],
        ],
        'sort'     => 'int',
    ],

    # labels of fields
    'ui'        => [
        'title'    => 'Заголовок',
        'content'  => 'Описание',
        'icon'     => 'Иконка',
        'icon2'    => 'Иконка (мобильная)',
        'see_more' => 'Смотрите так же',
        'sort'     => 'Порядок',
    ],

    #
    'input_cfg' => [
        'icon'  => ['type' => 'image', 'mask' => 'category1_icon'],
        'icon2' => ['type' => 'image', 'mask' => 'category1_icon'],
    ],

    # node configuration
    'node'      => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user',
    ],

    #
    /*
    'subtabs' => array(
    '0' => array(
    'title' => 'Русская версия',
    'fields_include' => array('title','anons','subtitle','desc','link','pic_main','pics','best','t_web','t_band','t_photo','visible','sort'),
    ),

    '1' => array(
    'title' => 'Английская версия',
    'fields_include' => array('title_en','anons_en','subtitle_en','desc_en'),
    ),
    ),
     */

    # labels for actions
    'labels'    => [
        'list'    => 'Категории',
        'add'     => 'Новая категория',
        'adding'  => 'Создание категории',
        'edit'    => 'Редактировать категорию',
        'editing' => 'Редактирование категории',
        'delete'  => 'Удалить категорию',
    ],
];
