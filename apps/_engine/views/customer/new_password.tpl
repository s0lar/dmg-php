﻿{extends file='_layout.tpl'}

{block name=content}

<div class="main">
    <div class="page-head single_head">
        <div class="container">
            <div data-aos="fade-up" data-aos-delay="500" data-aos-duration="400" data-aos-easing="ease-out-cubic" class="page-head__desc">
                <div class="page-head__title">Оформить заказ</div>
            </div>
        </div>
    </div>
    <div class="page-main">
        <div class="container">
            <div class="page-auth">
                <div class="page-auth__left">
                    <div class="page-auth__form">
                        <form action="" method="post" class="form">
                            <div class="medium-title">Восстановление пароля</div>

                            <div class="form-fieldset">
                                <div class="form-fieldset__input">
                                    <input name="password1" type="password" placeholder="Пароль" class="form-input">
                                    <div class="form-icon">
                                        <svg class="icon icon-password ">
                                            <use xlink:href="#icon-password"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>

                            <div class="form-fieldset">
                                <div class="form-fieldset__input">
                                    <input name="password2" type="password" placeholder="Повторите пароль" class="form-input">
                                    <div class="form-icon">
                                        <svg class="icon icon-password ">
                                            <use xlink:href="#icon-password"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>

                            {if isset($customer_error)}
                                <div>
                                    <p style="color:#f66">{$customer_error}</p>
                                </div>
                            {/if}

                            <div class="form-buttons">
                                <input type="hidden" name="csrf_auth" value="{$smarty.session.csrf_auth}" />
                                <button type="submit" class="button style_green size_m"><span class="button__text">Сменить пароль</span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="page-auth__right">
                    <div class="page-auth__info">
                        <div class="medium-title">Зарегистрироваться</div>
                        <div class="page-auth__info-desc">Заполните короткую форму с контактными данными и получите
                            доступ к личному кабинету, возможность отслеживать статус заказов и получать специальные
                            условия.</div>
                        <div class="page-auth__info-button"><a href="#" class="button style_border_grey size_m"><span
                                    class="button__text">Зарегистрироваться</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div><a href="#" class="wide-button">
        <div class="wide-button__title">Сделать заказ без регистрации</div>
    </a>
</div>

{/block}

{block name=footer}
{/block}