<?php
return [
    # types of fields
    'fields' => [
        'title'     => 'readonly_small',
        'date'      => 'string',
        'cart_html' => 'readonly_medium',
        'status'    => [
            'type'  => 'list',
            'items' => [
                0  => 'Не оплачен',
                10 => 'Оплачен',
            ],
        ],
        // 'cart_count' => 'text',
        // 'cart_total' => 'text',
        // 'person_html' => 'text',
        // 'customer' => array(
        //     'type' => 'pointer',
        //     'object' => array(
        //         'table' => 'customers', // Название таблицы
        //         'fields' => 'id,person', // Поля, которые будут выбраны
        //         'display' => '%person% %email%', // Что покажется пользоватедю в SELECT'е
        //     ),
        // ),
    ],

    # labels of fields
    'ui'     => [
        'title'     => 'Номер',
        'date'      => 'Дата',
        'status'    => 'Статус',
        'cart_html' => 'Информация',
        // 'cart_count' => 'Кол-во',
        // 'cart_total' => 'Заказ',
        // 'person_html' => 'Данные о покупателе',
        // 'customer' => 'Покупатель',
    ],

    #
    // 'input_cfg' => array(
    //     'menu' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'photo' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'background' => array('type' => 'image', 'mask' => 'series_bg'),
    // ),

    # node configuration
    'node'   => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-auto',
    ],

    #
    'view'   => [
        'mode'       => 'list',
        'fields'     => ['title', 'status', 'date'],
        'orderby'    => ' title DESC ',
        'edit_field' => 'title',
        'limit'      => 150,
    ],

    # labels for actions
    'labels' => [
        'list'    => 'Заказы',
        // 'add' => 'Новый',
        'adding'  => 'Создание',
        'edit'    => 'Редактировать',
        'editing' => 'Редактирование',
        'delete'  => 'Удалить',
    ],
];
