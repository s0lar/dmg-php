<?php

class Category2_Controller extends ZObject_Controller
{

    const VALID_FIELD = [
        'tech1',
        'tech2',
        'tech3',
        'tech4',
        'tech5',
        'tech6',
        'tech7',
        'tech8',
        'tech9',
        'tech10',
        'tech11',
        'tech12',
        'tech13',
        'tech14',
        'tech15',
        'tech16',
    ];

    const SORT_FIELD = [
        'title' => 'по имени',
        'cost'  => 'по стоимости',
    ];

    public function action_index()
    {
        $searchQuery = $this->prepareFilters();

        if (!empty($_GET['d'])) {
//            __($searchQuery);
//            exit;
        }

        $page         = Q('SELECT * FROM @@category2 WHERE id=?i ', [$this->node['object_id']])->row();
        $page['icon'] = $this->getFM($page['icon'], 'a');
        RS('page', $page);

        //  Постраничная разбивка
        $sql = Qb('SELECT 1 FROM @@product as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="product" AND N.parent_id = ?i' . $searchQuery,
            [$this->node['id']]
        );

        $sort = !empty(self::SORT_FIELD[_get('sort')]) ? _get('sort') : 'title';


        $pg = PG($sql, 30);
        RS('pg', $pg);

        $products = [];
        $res      = Q('SELECT N.`path`, N.`parent_id` as `nodes_pid`, N.`id` as `nodes_id`, P.* FROM @@product as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="product" AND N.parent_id = ?i ' . $searchQuery . '
                        ORDER BY P.' . $sort . '
                        LIMIT ?i, ?i',
            [
                $this->node['id'],
                $pg['offset'],
                $pg['limit'],
            ]);

        while ($r = $res->each()) {
//            $r['icon'] = $this->getFM($r['icon'], 'a');

            //  Show price
            if ((bool)$page['show_price'] !== true) {
                $r['cost_view'] = 'цена по запросу';
                $r['cost']      = null;
            }
            $products[] = $r;
        }

        //  todo - show only for auth users
//        if (empty($_SESSION['user'])) {
//            redirect('../');
//        }

        $catalogConfig = $this->getFilters();
        RS('catalog_config', $catalogConfig);
        RS('products', $products);
        RS('see_more', $this->getSeeMore());


        RS('sort_list', self::SORT_FIELD);
        RS('sort_links', $this->prepareSortLinks());

        RS('catalog_nofilter', empty($catalogConfig) ? 1 : 0);

        V('category2');
    }

    /**
     * Ajax count of products
     */
    public function action_filterCount()
    {
        $searchQuery = $this->prepareFilters();

        $sql = Q('SELECT count(1) as count FROM @@product as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="product" AND N.parent_id = ?i ' . $searchQuery,
            [$this->node['id']]
        );

        exit(json_encode([
            'success' => true,
            'count'   => 'Показать товаров: ' . $sql->row()['count'] ?? 0,
        ]));
    }

    public function prepareFilters()
    {
        $config  = $this->getFilters();
        $query   = '';
        $checked = [];
        foreach ($_GET as $field => $filter) {
            if (in_array($field, self::VALID_FIELD)) {
                foreach ($filter as $name => $value) {
//                    $result[$field] = [
//                        $field           => str_replace(['"', '\''], '', $name),
//                        $field . '_name' => $value,
//                    ];

                    //  Replace quotes
                    $name = str_replace(['"', '\''], '', $name);

                    if ($config[$field]['type'] == 'checkbox') {
                        $query .= Qb(" and (`{$field}_name` = ?s and `{$field}` in (?ls))", [
                            $name,
                            $value,
                        ]);
                    }

                    if ($config[$field]['type'] == 'range') {
                        $query .= Qb(" and (`{$field}_name` = ?s and cast(`{$field}` AS UNSIGNED) >= ?f  and cast(`{$field}` AS UNSIGNED) <= ?f)", [
                            $name,
                            $value['min'],
                            $value['max'],
                        ]);
                    }
                }

                //  Checked array
                $checked[$field] = array_values($filter)[0];
            }
        }

        RS('checked', $checked);

        return $query;
    }

    /**
     *
     */
    public function action_filters()
    {
        $filtersAll = [
            'tech1' => $this->filtersByField(1),
            'tech2' => $this->filtersByField(2),
            'tech3' => $this->filtersByField(3),
            'tech4' => $this->filtersByField(4),
            'tech5' => $this->filtersByField(5),
            'tech6' => $this->filtersByField(6),
            'tech7' => $this->filtersByField(7),
            'tech8' => $this->filtersByField(8),
            'tech9' => $this->filtersByField(9),
            'tech10' => $this->filtersByField(10),
            'tech11' => $this->filtersByField(11),
            'tech12' => $this->filtersByField(12),
            'tech13' => $this->filtersByField(13),
            'tech14' => $this->filtersByField(14),
            'tech15' => $this->filtersByField(15),
            'tech16' => $this->filtersByField(16),
        ];

        RS('filters_current', $this->getFilters());
        RS('filters_all', $filtersAll);
        V('zobject/filters');
    }

    /**
     *
     */
    public function action_filters_post()
    {
        $config = [];

        foreach ($_POST['use'] as $field => $_) {
            $name = $_POST['name'][$field];
            $type = $_POST['type'][$field] ?? 'checkbox';

            $config[$field] = [
                'name' => $name,
                'type' => $type,
                'data' => $this->getFilterData($field, $name, $type, $this->node['id']),
            ];
        }

        if (null === $this->getFilters()) {
            Q('insert into @@filters (filter, node_id, created_at) values (?s, ?i, now())',
                [serialize($config), $this->node['id'],]);
        } else {
            Q('update @@filters set filter=?s, updated_at=now() where node_id=?i',
                [serialize($config), $this->node['id'],]);
        }

        redirect($this->node['path'] . ':filters');
    }

    /**
     * @param string $field
     * @param string $name
     * @param string $type
     * @param int $nodeId
     * @return array
     */
    public function getFilterData(string $field, string $name, string $type, int $nodeId)
    {
        if ($type == 'checkbox') {
            $data = [];
            $res  = Q("select p.{$field} as `value` from dmg__product as p
                    left join dmg__nodes as n on n.object_id=p.id 
                    where n.object_type='product' and n.parent_id=?i and p.{$field}_name = ?s
                    group by p.{$field}
                    order by p.{$field}
                    ", [$nodeId, $name]);

            while ($r = $res->each()) {
                $data[$r['value']] = $r['value'];
            }
            return $data;
        }

        if ($type == 'range') {
            $data = [];
            $min = Q("select min( CAST(p.{$field} AS UNSIGNED) ) as `min` from dmg__product as p
                    left join dmg__nodes as n on n.object_id=p.id
                    where n.object_type='product' and n.parent_id=?i and p.{$field}_name = ?s

                    ", [$nodeId, $name])->row()['min'] ?? null;

            $max = Q("select max( CAST(p.{$field} AS UNSIGNED) ) as `max` from dmg__product as p
                    left join dmg__nodes as n on n.object_id=p.id
                    where n.object_type='product' and n.parent_id=?i and p.{$field}_name = ?s

                    ", [$nodeId, $name])->row()['max'] ?? null;

            $data['min'] = $min;
            $data['max'] = $max;

            return $data;
        }
    }

    /**
     * @return array
     */
    protected function prepareSortLinks()
    {
        $query  = [];
        $result = [];
        parse_str($_SERVER['QUERY_STRING'], $query);

        foreach (self::SORT_FIELD as $key => $_) {
            $query['sort'] = $key;
            $result[$key]  = http_build_query($query);
        }

        return $result;
    }

    /**
     * @param int $fieldNum
     * @return mixed
     */
    protected function filtersByField(int $fieldNum)
    {
        $res = Q("select n.parent_id, p.tech{$fieldNum}_name as tech from dmg__product as p
                    left join dmg__nodes as n on n.object_id=p.id 
                    where n.object_type='product' and n.parent_id=?i and p.tech{$fieldNum}_name <> ''
                    group by n.parent_id, p.tech{$fieldNum}_name
                    order by n.parent_id, p.tech{$fieldNum}_name", [$this->node['id']]);

        return $res->all();
    }

    /**
     * @return array|null
     */
    protected function getFilters(): ?array
    {
        $configCurrent = Q('SELECT `filter` FROM @@filters WHERE node_id=?i', [$this->node['id']])->row();

        if (!empty($configCurrent['filter'])) {
            return unserialize($configCurrent['filter']);
        }

        return null;
    }

    /**
     * @return array
     */
    protected function getSeeMore(): array
    {
        $currentCategory = Q('select C.see_more from @@category1 as C 
                                left join @@nodes as N on C.id = N.object_id 
                                where N.object_type="category1" and N.id=?i', [$this->node['parent_id']]
        )->row();

        //  Get ID's
        $seeMoreIds = explode('|', $currentCategory['see_more'] ?? '');

        $result = [];
        $res    = Q('select C.title, C.icon, C.icon2, N.path from @@nodes as N
                                inner join @@category1 as C on C.id = N.object_id 
                                where N.object_type="category1" and C.id in (?li)', [$seeMoreIds]
        );

        while ($r = $res->each()) {
            $r['icon']  = $this->getFM($r['icon'], 'a');
            $r['icon2'] = $this->getFM($r['icon2'], 'a');
            $result[]   = $r;
        }

        return $result;
    }
}
