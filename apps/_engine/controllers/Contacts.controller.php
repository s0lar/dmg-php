<?php

class Contacts_Controller extends ZObject_Controller
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_index()
    {
        $res = Q('SELECT * FROM @@sections WHERE id=?i ',
            [$this->node['object_id']])->row();
        RS('page', $res);

        RS('cls_body', '');
        V('contacts');
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Callback
    public function action_feedback()
    {
        $arr = ['success' => true];
        $errors = [];

        //    Check CSRF Token
        if (!(!empty($_POST['csrf_feedback'])
            && !empty($_SESSION['csrf_feedback'])
            && $_POST['csrf_feedback'] === $_SESSION['csrf_feedback'])
        ) {
            $errors['csrf'] = 'Ваша сессия устарела. Обновите страницу и попробуйте еще раз';
        }

        # Verify captcha
        $post_data = http_build_query(
            [
                'secret'   => reCaptcha_sec,
                'response' => $_POST['g-recaptcha-response'],
                'remoteip' => $_SERVER['REMOTE_ADDR'],
            ]
        );
        $opts = [
            'http' =>
                [
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $post_data,
                ],
        ];
        $context = stream_context_create($opts);
        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
        $result = json_decode($response);
        if (!$result->success) {
            throw new Exception('Gah! CAPTCHA verification failed. Please email me directly at: jstark at jonathanstark dot com', 1);
        }

//        __($post_data);
//        __($response);
//        __($result);
//        exit;

//        if ($response['success'] == false) {
//            $errors['agreement'] = 'Вы не прошли проверку капчи';
//        }

        if (empty($_POST['name'])) {
            $errors['name'] = 'Введите ваше имя';
        }

        if (!isset($_POST['agreement'])) {
            $errors['agreement'] = 'Необходимо согласие на обработку персональных данных';
        }

        if (empty($_POST['phone'])) {
            $errors['phone'] = 'Укажите телефон для связи';
        }
//
//        if (empty($_POST['email'])) {
//            $errors['email'] = 'Укажите email для связи';
//        }

        //    Strip HTML tags for secure form
        foreach ($_POST as $k => $v) {
            $_POST[$k] = strip_tags($v);
        }

        //    Есть ошибки - прерываем отправку
        if (!empty($errors)) {
            $arr['success'] = false;
            $arr['errors'] = $errors;

            exit(json_encode($arr));
        }

        //    Generate email
        $sm = SM();
        $sm->assign('date', $this->prepareDate(date("d.m.Y H:i")));
        $sm->assign('post', $_POST);
        $cont = $sm->fetch("mail/feedback.tpl");

        //    Include PHP mailer
        require_once SHARED_PATH . '/mmailer.php';
        $mm = mmailer();

        //    Sendto array
        $to = $this->getZcfg('email_feedback');
        $to = explode(",", $to['email_feedback']);
        foreach ($to as $key => $value) {
            $mm->addAddress(trim($value));
        }

        $branchEmail = $this->getContactsEmail(
            $_POST['city'] ?? 1
        );

        $mm->addAddress(trim($branchEmail));

        $allowed = [
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'application/msword',
            'application/doc',
            'application/rtf',
            'application/x-rtf',
            'text/richtext',
            'text/rtf',
            'application/zip',
            'application/vnd.oasis.opendocument.text',
            'application/vnd.sun.xml.writer',
        ];

        //  FILE
        if (!empty($_FILES) && !empty($_FILES['f'])) {
            if (1 or in_array($_FILES['f']['type'], $allowed)) {
                $mm->addStringAttachment($_FILES['f']['tmp_name'],
                    $_FILES['f']['name']);
            }
        }

        $mm->Subject = "Обратная связь " . date("d.m.Y H:i");
        $mm->Body = $cont;

        $rrr = $mm->send();

        exit(json_encode($arr));
    }
}
