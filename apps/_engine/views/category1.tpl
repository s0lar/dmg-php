﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_category">
        <div class="container">
            <div class="page-head">
                <div class="page-head__icon">
                    <img class="hidden-xs" src="{$page.icon}">
                    <img class="visible-xs" src="{$page.icon}">
                </div>
                <div class="page-head__top">
                    <div class="page-head__title">{$page.title}</div>
                    <a class="page-head__return mobile_is_show" href="../">
                        <svg class="icon icon-return ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-return"></use>
                        </svg>
                    </a>
                </div>

                {include file="blocks/bc.tpl"}
            </div>

            <div class="page-category">
                <div class="category">
                    <div class="category-list">
                        {foreach item=category from=$categories key=parent}
                            <a class="category-list__elem" href="{$category.path}">
                                <div class="category-list__title">{$category.title|stripslashes}</div>
                            </a>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}