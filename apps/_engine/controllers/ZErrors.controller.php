<?php

class ZErrors_Controller extends ZAny_Controller
{
		
	function action_401()
	{
		RSH('HTTP/1.0 401 Unauthorized');
		V('zobject/auth');
	}
	
	function action_403()
	{
		RSH('HTTP/1.1 403 Forbidden');
		V('errors/403');
	}
	
	function action_404()
	{
		RSH('HTTP/1.0 404 Not Found');
		V('errors/404');
	}

}

?>