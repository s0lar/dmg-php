<?php

class mn2_handler
{
	function prepare_view(&$value = null, $config = null)
	{
		//__($value, $config);
		$id = explode('|', $value);
		$value = array();
		
		$t = ($config['relation']['table']);
		$k = ($config['relation']['var']);
		$v = ($config['relation']['value']);
		$o = ($config['relation']['sort']);
		#
		foreach($id as $kk=>$vv)
			if( $vv != '' )
				$value['selected'][$vv] = $vv;
		#
		$res = Q("SELECT `{$k}`, `{$v}` FROM `@@{$t}` ORDER BY {$o}");
		
		while($r = $res->each()){
			$value['objects'][ $r{$k} ] = $r{$v};
		}
	}
}