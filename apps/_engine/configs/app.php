<?php
//    Project name
define('PR_NAME', 'D M G');

//    Emails list
define('mmm_DEV', 'pavel.sidorov@gmail.com');

define('reCaptcha_pub', '6LeO-qUZAAAAALb7IJJh39FRq9HNdLREdw9hvLb1');
define('reCaptcha_sec', '6LeO-qUZAAAAAFCKaE0oOk2LWpvcT5g7NAfPS5VC');

return [
    'views' => [
        'default' => 'Smarty_Templater',
        'html'    => 'Smarty_Templater',
        'json'    => 'JSON_Templater',
        'xml'     => 'XML_Templater',
    ],

    'templaters' => [
        'Smarty_Templater' => [
            'lib'          => 'shared:Smarty-3.1.34/libs',
            'template_dir' => 'app:views',
            'compile_dir'  => 'app:tmp/smarty_compiled',
            'plugins_dir'  => ['app:views/__plugins'],
        ],
    ],
];
