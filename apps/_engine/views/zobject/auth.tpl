<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>[CMS {$smarty.server.HTTP_HOST}] Управление сайтом</title>
		<link rel="shortcut icon" type="image/x-icon" href="/theme/n23/i/favicon-5.png" />
		
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="/backend/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/backend/css/backend.css" />
		<link 	type="text/css" 		href="/fm/js/fileuploader.css" rel="stylesheet">
		
		<!-- JS -->
		<script type="text/javascript"> var request = {$request|@json_encode}; </script>
        <script type="text/javascript" src="/backend/js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="/backend/js/cms.js"></script>
		
		
		<script type="text/javascript" src="/backend/bootstrap/js/bootstrap.min.js"></script>
		
	</head>
	<body>

<div id="wrap">
	{* Header panel *}
	<div class="header row-fluid">
		<div class="span3">
			<a href="/:edit" class="header__name ml20">{$smarty.const.PR_NAME}</a>
		</div>
		
	</div>
	
	<div style="width:450px;margin:0 auto;left:-225px;padding-top:50px">
		<h4>Авторизация</h4>
		<form action="{$request->url->path}:auth" method="post">
			<input name="login" placeholder="login" type="text" id="login" />
			<input name="pass"  placeholder="password" type="password" />
			<div><input type="submit" value="Войти" class="btn btn-primary" /></div>
		</form>
	</div>
	
{literal}
	<script type="text/javascript">
		$('document').ready(function(){
			$("#login").focus();
		});
	</script>
{/literal}
{include file='zobject/footer.tpl' inline='true'}