﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>


            <div class="page-content">
                <div class="for-buyers"><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Оплата. Доставка. Документы</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-delivery.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Услуги</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-metal.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Все о нержавеющей стали</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-info.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Вопрос-ответ</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-faq.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Глоссарий</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-glossary.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Реквизиты</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-details.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">Отраслевые решения</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-factory.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div></a><a class="for-buyers__elem" href="#">
                        <div class="for-buyers__title">ГОСТы</div>
                        <div class="for-buyers__info">
                            <div class="for-buyers__icon"><img src="/theme/dmg_markup/build/imagesicon-gost.png"></div>
                            <div class="for-buyers__more"><span>Подробнее</span>
                                <svg class="icon icon-arrow-triangle ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                </svg>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
{/block}

{block name=footer}
{/block}