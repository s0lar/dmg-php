<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.widget.php
 * Type:     function
 * Name:     widget
 * Purpose:  outputs a random magic answer
 * -------------------------------------------------------------
 */
function smarty_function_widget($params, &$smarty)
{
	$smarty->assign($params['assign'], 
		RQ($params['url'], array(
			'key' => $params['assign'],
			'script' => 'Widget_Script'
		))->dispatch()
	);
}
?> 