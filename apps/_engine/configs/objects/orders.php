<?php

return [
    # types of fields
    'fields' => [
        'title'          => 'string',
        'delivery'       => 'string',
        'pickup_address' => 'string',
        'city'           => 'string',
        'address'        => 'string',
        'date'           => 'string',
        'time'           => 'string',
        'name'           => 'string',
        'email'          => 'string',
        'phone'          => 'string',
        'comment'        => 'string',
        'html'           => 'text',
        'cart'           => 'text',
        'created_at'     => 'datetime',
    ],

    # labels of fields
    'ui'     => [
        'title'          => 'Номер заказа',
        'delivery'       => 'Тип',
        'pickup_address' => 'Самовывоз из',
        'city'           => 'Город',
        'address'        => 'Адрес',
        'date'           => 'Желаемая дата',
        'time'           => 'Желаемое время',
        'name'           => 'Имя',
        'email'          => 'Email',
        'phone'          => 'Телефон',
        'comment'        => 'Комментарий',
        'html'           => 'Заказ',
        'cart'           => 'Корзина',
        'created_at'     => 'Создан',
    ],

    #
    //    'input_cfg' => [
    //        'photo' => [
    //            'type' => 'image',
    //            'mask' => 'contacts',
    //        ],
    //    ],

    # node configuration
    'node'   => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user',
    ],

    # view
    'view'   => [
        'mode'       => 'list',
        'fields'     => ['title', 'delivery', 'name'],
        'orderby'    => ' `id` DESC ',
        'edit_field' => 'title',
        'limit'      => 150,
    ],

    # labels for actions
    'labels' => [
        'list'    => 'Филиалы',
        'add'     => 'Добавить филиал',
        'adding'  => 'Добавление филиала',
        'edit'    => 'Редактировать филиал',
        'editing' => 'Редактирование филиала',
        'delete'  => 'Удалить филиал',
    ],
];