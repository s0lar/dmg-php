<h4>Полный доступ к сайту</h4>
<p class="field">
	<label class="field-title">
		<input type="checkbox" name="root" {if isset($u.ac.root) && $u.ac.root == 1} checked="checked" {/if}  /> - Разрешить полный доступ
	</label>
</p>
<p>&nbsp;</p>


<h4>Доступ к пользователям</h4>
<p class="field">
	<label class="field-title">
		<input type="checkbox" name="users"  {if isset($u.ac.users) && $u.ac.users == 1} checked="checked" {/if} /> - Управление пользователями
	</label>
</p>
<p>&nbsp;</p>


<h4>Доступ к модулям</h4>
<p class="field">
	
	{foreach item=i from=$modules_list key=k}
	<label class="field-title">
		<input type="checkbox" name="zset[{$k}]" {if isset($u.ac.zset[$k])} checked="checked" {/if} /> - {$i}
	</label>
	
	{/foreach}
	
</p>
<p>&nbsp;</p>

<h4>Доступ к настройкам</h4>
<p class="field">
	<label class="field-title">
		<input type="checkbox" name="zcfg" {if isset($u.ac.zcfg) && $u.ac.zcfg == 1} checked="checked" {/if}  /> - Управление настройками
	</label>
</p>
<p>&nbsp;</p>


<h4>Доступ к страницам сайта</h4>
	
<div class="nodes-padding">
	<ul id="nodes_user" class="nodes_user">
		<li><div class="nodes-item-holder">
			<a href="/" class="folder icon-minus-sign" rel="1"></a><a id="nodes_user-tree-node-item-1" href="/:edit/">Root</a>
			<input type="checkbox" class="checkbox" name="user_nodes[1]" {if isset($u.ac.nodes.nodes[1])} checked="checked" {/if} />
		</div></li>
	</ul>
</div>

<script type="text/javascript"> var nodes_user_json = {$u.ac|@json_encode|default:''}; </script>
<p>&nbsp;</p>


<p>&nbsp;</p>
<p>&nbsp;</p>