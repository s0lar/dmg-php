{include file='zobject/header.tpl' inline='true'}

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="#">{$labels.editing}</a></li>
	</ul>



	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">

		{* Add + Add message *}
		{if isset($smarty.get.msg)}
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<p>Запись обновлена</p>
			</div>
		{/if}

		<fieldset class="group">
			<!-- <legend class="group-title">{$object.labels.editing}</legend> -->
			<form action="/zsettings/:edit/-m/{$module}/-id/{$id}" method="post" class="ajax1" id="object-edit">

				{foreach item=i key=k from=$object.ui}
					<div class="row-fluid border-bottom1 mb10" style="margin-bottom:15px">
							<label class="span3 f14  mb0" for="{$k}" style="min-height:20px">{$i}</label>

							{if $object.fields[$k] eq 'string'}
								<input type="text" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span6">

							{elseif $object.fields[$k] eq 'int' || $object.fields[$k] eq 'float'}
								<input type="text" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span3">

							{elseif $object.fields[$k] eq 'datetime'}
								<input type="text" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span3 datepick_datetime">

							{elseif $object.fields[$k] eq 'date'}
									<input type="text" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span3 datepick_date">

							{elseif $object.fields[$k] eq 'text'}
								<textarea name="{$k}" id="{$k}" class="input-textarea span12">{$object.data[$k]}</textarea>

							{elseif $object.fields[$k] eq 'html'}
							<div style="clear:both">
								<textarea name="{$k}" id="{$k}" class="input-textarea visual span12">{$object.data[$k]}</textarea>
								<label class="f11"><input type="checkbox" onclick="tinyMCE.execCommand('mceToggleEditor',false,'{$k}');" style="margin-top:-4px" /> &mdash; вкл/выкл редактор</label>
							</div>

							{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'list'}
								{include file='zobject/types-views/list.tpl' field=$k data=$object.data[$k] inline='true'}

							{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'pointer'}
								{include file='zobject/types-views/pointer.tpl' field=$k data=$object.data[$k] inline='true'}

							{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'gs_file'}
								{include file='zobject/types-views/file.tpl' field=$k data=$object.data[$k] inline='true'}

							{elseif $object.fields[$k] eq 'mn'}
								{foreach item=j from=$object.data[$k].object}
									<input type="checkbox" name="{$k}[{$j.id}]" id="{$k}-{$j.id}"{if $j.checked} checked{/if}><label for="{$k}-{$j.id}">{$j.display}</label>
								{/foreach}

							{elseif $object.fields[$k] eq 'gs_file'}
									{include file='zobject/types-views/file.tpl' field=$k _val=$object.data[$k] inline='true'}

							{elseif $object.fields[$k] eq 'checkbox'}
									<input type="checkbox" name="{$k}" id="{$k}" {if $object.data[$k] =='1'}checked="checked"{/if} />

							{elseif isset($object.fields[$k].type) && isset($object.fields[$k].type) && $object.fields[$k].type eq 'mn2'}
									{include file='zobject/types-views/mn2.tpl' field=$k data=$object.data[$k] inline='true'}

                            {elseif $object.fields[$k] eq 'readonly_small'}
                                <div class="input-text span6" style="border: 2px solid #ccc; padding: 7px 15px;margin-left:0;">
                                    {$object.data[$k]}
                                </div>

                            {elseif $object.fields[$k] eq 'readonly_medium'}
                                <div class="input-text span9" style="border: 2px solid #ccc; padding: 7px 15px;margin-left:0;">
                                    {$object.data[$k]}
                                </div>

                            {elseif $object.fields[$k] eq 'readonly_large'}
                                <div style="clear:both;padding:10px 20px;border:2px solid #ccc">
                                    {$object.data[$k]}
                                </div>

							{/if}

					</div>

				{/foreach}

				<div class="well well-panel well-panel-controll">
					<div class="btn-group">
						<input type="submit" value="Изменить" class="btn btn-primary" />
						<input type="submit" name="apply" value="Применить" class="btn" />
					</div>
					<a href="/zsettings/:edit_node/-m/{$module}/" class="btn btn-small pull-right">Отмена</a>
				</div>

			</form>
		</fieldset>

	</div>
{include file='zobject/footer.tpl' inline='true'}