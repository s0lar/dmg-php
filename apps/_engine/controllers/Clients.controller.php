<?php

class Clients_Controller extends ZObject_Controller
{
    public function action_index()
    {
        $res = Q('SELECT * FROM @@sections WHERE id=?i ', array($this->node['object_id']))->row();
        RS('page', $res);

        RS('sub_menu', $this->getSubMenu());
        V('sections-category');
    }

}
