
	<fieldset class="group">
		<!-- <legend class="group-title">{$object.labels.adding}</legend> -->
		<form action="{$request->url->path}:add.html?type={$smarty.get.type}" method="post" class="ajax1" id="object-add">

			{* Show subTabs ************************************************************************************************************************************}
			{if isset($object.subtabs) && !empty($object.subtabs)}
			<ul class="nav nav-tabs" id='subtabs'>
				{foreach item=i from=$object.subtabs key=k name=nn}
					<li class=' {if $smarty.foreach.nn.first}active{/if} '><a href="#subtabs_{$k}" data-toggle="tab">{$i.title}</a></li>
				{/foreach}
			</ul>
			{/if}

			{* Show subTabs ************************************************************************************************************************************}
			{if isset($object.subtabs) && !empty($object.subtabs)}

				<div class="tab-content" style='border:1px solid #cecece;padding:20px;margin-top:-21px;border-top:0'>
					{foreach item=ii from=$object.subtabs key=kk name=nn}
						<div class="tab-pane {if $smarty.foreach.nn.first}active{/if}" id="subtabs_{$kk}">

							{if $object.node.name eq '-user' && $smarty.foreach.nn.first}
								<div class="row-fluid border-bottom1 mb10" style="margin-bottom:15px">
									<label class="span3 f14  mb0" for="name" style="min-height:20px">Системное имя (латинские символы, цифры)</label>
									<input type="text" name="name" id="name" class="input-text span6" />
								</div>
							{/if}

							{foreach item=i key=k from=$object.ui}
								{if in_array( $k, $ii.fields_include )}

									{include file='zobject/blocks/add_object_fields.tpl'}

								{/if}
							{/foreach}

						</div>
					{/foreach}
				</div>

			{* Show fields ************************************************************************************************************************************}
			{else}
				{if $object.node.name eq '-user'}
					<div class="row-fluid border-bottom1 mb10" style="margin-bottom:15px">
						<label class="span3 f14  mb0" for="name" style="min-height:20px">Системное имя (латинские символы, цифры)</label>
						<input type="text" name="name" id="name" class="input-text span6" />
					</div>
				{/if}

				{foreach item=i key=k from=$object.ui}
					{include file='zobject/blocks/add_object_fields.tpl'}
				{/foreach}
			{/if}


			<div class="well well-panel well-panel-controll">
				<input type="submit" value="Сохранить" class="btn btn-primary" />
				<input type="submit" name="save" value="Применить" class="btn" />

				<a href="{$request->url->path}:edit" class="btn btn-small pull-right">Отмена</a>
			</div>

		</form>
	</fieldset>