<!doctype html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>[CMS {$smarty.server.HTTP_HOST}] Управление сайтом</title>
		<link rel="shortcut icon" type="image/x-icon" href="/theme/img/favicon.ico" />

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="/backend/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/backend/css/backend.css" />
		<link 	type="text/css" 		href="/fm/js/fileuploader.css" rel="stylesheet">

		<!-- JS -->
		<script type="text/javascript"> var request = {$request|@json_encode}; </script>
        <script type="text/javascript" src="/backend/js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="/backend/js/cms.js"></script>


		<script type="text/javascript" src="/fm/js/md5.js"></script>
		<script type="text/javascript" src="/fm/js/fileuploader.js"></script>
	    <script type="text/javascript" src="/fm/js/jquery.json-2.2.min.js"></script>
	    <script type="text/javascript" src="/fm/js/jquery.dragsort-0.5.1.min.js"></script>
		<script type="text/javascript" src="/fm/js/fm.js?v=2"></script>
		<script type="text/javascript" src="/backend/bootstrap/js/bootstrap.min.js"></script>
		{*<script type="text/javascript" src="/backend/js/jquery.nestable.js"></script>*}


		<!-- Calendar -->
		<script type="text/javascript" src="/backend/pickmeup/js/jquery.pickmeup.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/backend/pickmeup/css/pickmeup.min.css" />
		{literal}<script type="text/javascript">
			$(document).ready(function(){
				$('.datepick_date').pickmeup({
					format:			'Y-m-d',
					position		: 'right',
					hide_on_select	: true
				});

				$('.datepick_datetime').pickmeup({
					format:			'Y-m-d H:M:S',
					position		: 'right',
					hide_on_select	: true
				});

				//

			});
		</script>{/literal}

		<!-- Codemirror -->
		<script src="/backend/codemirror/lib/codemirror.js"></script>
		<script src="/backend/codemirror/mode/xml/xml.js"></script>
		<link rel="stylesheet" href="/backend/codemirror/lib/codemirror.css">

		{literal}
		<script type="text/javascript">
			$(document).ready(function(){

				$('.input-code').each(function(){
					//console.log( $(this) );
					CodeMirror.fromTextArea( $(this)[0] , {
						mode: "text/html",
						lineWrapping: true,
						tabSize: 2,
						lineNumbers: true
					});
				});
			});
		</script>
		<style type="text/css">
			.CodeMirror {border:2px solid #ccc;border-radius:2px;clear:both;width:100%;font-size:11px;}
			.CodeMirror-empty { outline: 1px solid #c22; }
			.CodeMirror-empty.CodeMirror-focused { outline: none; }
			.CodeMirror pre.CodeMirror-placeholder { color:#999; }

			/* auto height */
			.CodeMirror {height:auto;max-height:300px;}
			.CodeMirror-scroll {height:auto;max-height:300px;}
		</style>
		{/literal}

		{include file="zobject/blocks/tiny_mce.tpl"}

	</head>
	<body>

<div class="container1" id="wrap">

	{* Header panel *}
	<div class="header row-fluid">
		<div class="span3">
			<a href="/:edit" class="header__name ml20">{$smarty.const.PR_NAME}</a>
		</div>
		<div class="span9">
			<div class="btn-group">
				<a class="btn btn-small" href="{if strstr($request->url->path, '/zsettings')}/{else}{$request->url->path}{/if}" target="_blank"><span class="icon-share"></span> Просмотреть страницу</a>
				<a class="btn btn-small" href="/:user_list"><span class="icon-user"></span> Пользователи</a>
				<a class="btn btn-small" href="/zcfg/:edit_node"><span class="icon-cog"></span> Настройки</a>
				<a class="btn btn-small" href="/:sitemapxml"><span class="icon-cog"></span> Sitemap.XML</a>
			</div>
			<div class="btn-group pull-right mr20">
				<a class="btn btn-small disabled" href="#">{$smarty.session.user.login}</a>
				<a class="btn btn-small" href="/:logout"><span class="icon-off"></span> Выход</a>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<!-- SIDE -->
		<div class="span3">
			<div id="nodes-holder" class="nodes-holder">
				<p class="side_p_border"><b><i class="icon-align-left"></i> Структура сайта</b></p>
				<div class="nodes-padding">
					<ul id="nodes" class="nodes">
						<li><div class="nodes-item-holder"><a href="/" class="folder icon-minus-sign" rel="1"></a><a id="tree-node-item-1" href="/:edit/">Главная страница</a></div></li>
					</ul>
				</div>
			</div>


			{if isset($modules_list) && !empty($modules_list)}
				<p>&nbsp;</p><p>&nbsp;</p>
				<p class="side_p_border"><b><i class="icon-wrench"></i> Модули</b></p>
				{foreach item=i from=$modules_list key=k}
					<p class="side_p"><i class="icon-list-alt"></i> <a href="/zsettings/:edit_node/-m/{$k}/">{$i}</a></p>

				{/foreach}
			{/if}
		</div>

		<!-- Main -->
		<div class="span8">
				{*
					<ul class="bc">
						<li><a href="/:edit"><i class="icon-home"></i></a> &rarr; </li>

					{if isset($module)}
						<li>Редактирование модулей: {$modules_list->$module}</li>
					{else}
						{if isset($bc)}
							{foreach item=i from=$bc name=ii}
								{if !$smarty.foreach.ii.last}
									<li><a href="{$i.path}:edit">{$i.object_title}</a> &rarr; </li>
								{else}
									<li>{$i.object_title}</li>
								{/if}

							{/foreach}
						{/if}

					{/if}


					</ul>
				*}
