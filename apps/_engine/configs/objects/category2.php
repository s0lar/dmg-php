<?php

return [
    # types of fields
    'fields'    => [
        'title'       => 'string',
        'content'     => 'text',
        'icon'        => 'gs_file',
        '1c_group_id' => 'text',
        'show_price'  => 'checkbox',
        'sort'        => 'int',
    ],

    # labels of fields
    'ui'        => [
        'title'       => 'Заголовок',
        'content'     => 'Описание',
        'icon'        => 'Фоновая картинка в меню',
        '1c_group_id' => 'ID группы 1с',
        'show_price'  => 'Показывать цену',
        'sort'        => 'Порядок',
    ],

    #
    'input_cfg' => [
        'icon' => ['type' => 'image', 'mask' => 'category2_icon'],
    ],

    # node configuration
    'node'      => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user',
    ],

    #
    /*
    'subtabs' => array(
    '0' => array(
    'title' => 'Русская версия',
    'fields_include' => array('title','anons','subtitle','desc','link','pic_main','pics','best','t_web','t_band','t_photo','visible','sort'),
    ),

    '1' => array(
    'title' => 'Английская версия',
    'fields_include' => array('title_en','anons_en','subtitle_en','desc_en'),
    ),
    ),
     */

    # labels for actions
    'labels'    => [
        'list'    => 'Категории',
        'add'     => 'Новая категория',
        'adding'  => 'Создание категории',
        'edit'    => 'Редактировать категорию',
        'editing' => 'Редактирование категории',
        'delete'  => 'Удалить категорию',
    ],

    'actions' => [
        'filters' => 'Фильтры',
    ]
];
