<div style="height:1%;overflow:hidden;margin-bottom:20px">

    {foreach item=j from=$object.data[$k].objects key=kkey}

    <div style="width:50%;float:left">
        <input type="checkbox" name="{$k}[{$kkey}]" id="{$k}-{$kkey}" style="float:left;margin:6px 10px 0 0" {if isset($object.data[$k].selected[$kkey])}checked="checked"
            {/if} />
        <label for="{$k}-{$kkey}" style="margin-right:10px;display:block;padding-top:2px">{$j}</label>
    </div>

    {/foreach}

</div>