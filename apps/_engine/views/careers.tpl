﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-flex">
                <div class="page-flex__aside">
{*                    <div class="aside-select">*}
{*                        <div class="aside-select__control">*}
{*                            <div class="aside-select__button">*}
{*                                <div class="aside-select__title">{$title}</div>*}
{*                                <svg class="icon icon-sort-arrow-down ">*}
{*                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-sort-arrow-down"></use>*}
{*                                </svg>*}
{*                            </div>*}
{*                            <select class="aside-select__list js-asideSelectList">*}
{*                                <option value="1">Услуги</option>*}
{*                                <option value="2">Все о нержавеющей стали</option>*}
{*                                <option value="3">Справочники</option>*}
{*                                <option value="4">Вопрос-ответ</option>*}
{*                                <option value="5">Глоссарий</option>*}
{*                                <option value="6">Реквизиты</option>*}
{*                            </select>*}
{*                        </div>*}
{*                    </div>*}
{*                    <div class="aside-nav">*}
{*                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Услуги</a></div>*}
{*                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Все о нержавеющей стали</a></div>*}
{*                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Справочники</a></div>*}
{*                        <div class="aside-nav__elem is-active"><a class="aside-nav__link" href="#">Вопрос-ответ</a></div>*}
{*                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Глоссарий</a></div>*}
{*                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Реквизиты</a></div>*}
{*                    </div>*}
                </div>

                <div class="page-flex__section">
                    <div class="page-group">
                        <div class="vacancies">
                            <div class="faq">
                                {foreach item=career from=$careers}
                                    <div class="faq-elem">
                                        <div class="faq-elem__header js-faqToggle">
                                            <div class="faq-elem__title">{$career.title}</div>
                                            <div class="faq-elem__subtitle">{$career.salary}</div>
                                            <svg class="icon icon-dropdown ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                            </svg>
                                        </div>
                                        <div class="faq-elem__content">

                                            {$career.content}

                                            <div class="faq-elem__footer">
{*                                                <div class="faq-elem__button">*}
{*                                                    <a class="button style_green" href="#"><span class="button__text">Отправить резюме</span></a>*}
{*                                                </div>*}
                                                <div class="faq-elem__close js-faqClose">Свернуть</div>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}