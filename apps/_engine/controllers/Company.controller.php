<?php

class Company_Controller extends ZObject_Controller
{
    public function action_index()
    {
        $res = Q('SELECT * FROM @@sections WHERE id=?i ', array($this->node['object_id']))->row();

//        $res['gallery'] = $this->getFM($res['gallery']);
        RS('page', $res);


        if (preg_match("#\/company-details\/$#i", $this->node['path'])) {
            V('company-details');
        } else {
            RS('sub_menu', $this->getSubMenu());
            V('company');
        }
    }
}
