<?php
/*
 * X fw plugin
 * -------------------------------------------------------------
 * File:     modifier.action.php
 * Type:     modifier
 * Name:     action
 * Purpose:  apply action to url
 * -------------------------------------------------------------
 */
function smarty_modifier_action($url, $action)
{
	$buf = clone $url;
	$buf->action = $action;
    return $buf->build();
}
?>