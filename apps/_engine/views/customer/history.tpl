﻿{extends file='_layout.tpl'}

{block name=content}

<div class="main">
    <div class="page-head default_head">
        <div class="container">
            <div data-aos="fade-up" data-aos-delay="500" data-aos-duration="400" data-aos-easing="ease-out-cubic" class="page-head__desc">
                <div class="page-head__title">Личный кабинет</div>
            </div>
            <div data-aos="fade-up" data-aos-delay="600" data-aos-duration="400" data-aos-easing="ease-out-cubic" class="page-head__bottom">
                <div class="page-head__nav">
                    <div class="head-nav">
                        <div class="head-nav__elem"><a href="/customer/:customer_personal" class="head-nav__link">Контактные данные</a></div>
                        <div class="head-nav__elem is-active"><a href="/customer/" class="head-nav__link">история заказов <sup>{count($orders)}</sup></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-main">
        <div class="container">
            <div class="page-personal">
                <div class="order-history">

                    {foreach item=i from=$orders}
                        <div class="order-history__elem">
                            <div class="order-history__head js-orderHistoryToggle">
                                <div class="order-history__arrow">
                                    <svg class="icon icon-arrow-triangle-down "><use xlink:href="#icon-arrow-triangle-down"></use></svg>
                                </div>
                                <div class="order-history__left">
                                    <div class="order-history__number">Заказ {$i.title}</div>
                                    <div class="order-history__date">
                                        {$i.date|date_format:"%d/%m/%Y г."}
                                        <span>{$i.date|date_format:"%H:%M"}</span>
                                    </div>
                                    <div class="order-history__status">
                                        {$order_status[$i.status]}
                                    </div>
                                </div>
                                <div class="order-history__right">
                                    <div class="order-history__info">
                                        <div class="order-history__count">Товаров: {$i.cart_json.count}</div>
                                        <div class="order-history__price">Сумма: {$i.cart_json.total} руб.</div>
                                    </div>
                                    <div class="order-history__button">
                                        <a href="/order/:repeat/-id/{$i.id}/" class="button style_green size_s js-orderHistoryButton">
                                            <span class="button__text">Повторить весь заказ</span>
                                        </a>
                                    </div>
                                    <div class="order-history__more">
                                        <a href="javascript:;" class="button style_border_grey size_s">
                                            <span class="button__text">Содержимое заказа</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="order-history__body">
                                <div class="order-product">

                                    {foreach item=prod from=$i.cart_json.prods}
                                        <div class="order-product__elem">
                                            <a href="{$prod.info.path}" style="background-image: url('{$prod.info.photo}')" class="order-product__image"></a>
                                            <div class="order-product__info">
                                                <div class="order-product__title">{$prod.info.title}</div>
                                                <div class="order-product__control">
                                                    <div class="order-product__count">{$prod.count} шт.</div>
                                                    <div class="order-product__price">{$prod.cost} руб.</div>
                                                </div>
                                            </div>
                                            <div class="order-product__button">
                                                <a href="{$prod.info.path}" class="button style_border_grey size_s">
                                                    <span class="button__text">Подробнее</span>
                                                </a>
                                            </div>
                                        </div>

                                    {/foreach}

                                </div>
                                <div class="order-history__more">
                                    <a href="#" class="button style_border_grey size_s js-orderHistoryControlHide">
                                        <span class="button__text">Свернуть содержимое</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    {/foreach}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{/block}

{block name=footer}
{/block}