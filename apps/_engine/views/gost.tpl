﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-flex">
                <div class="page-flex__aside">
{*                    {include '_aside.tpl'}*}
                </div>

                <div class="page-flex__section">
                    <div class="page-group">
                        <div class="download">
                            <div class="download-list">

                                {foreach item=qa from=$gost}

                                        <a class="download-elem" href="{$qa.gost_file[0].fs[0]|default:'#'}">
                                            <div class="download-elem__header">
                                                <div class="download-elem__title">{$qa.title}</div>
                                                {if !empty($qa.gost_file[0].fs[0])}
                                                    <div class="download-elem__type">{$qa.gost_file[0].e}, {$qa.gost_file[0].s}</div>
                                                    <div class="download-elem__icon">
                                                        <svg class="icon icon-download ">
                                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-download"></use>
                                                        </svg>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="download-elem__content">
                                                {$qa.content}
                                            </div>

                                        </a>

                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}