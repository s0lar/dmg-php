<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
    <head>
    	<title>Доступ запрещен. Ошибка 403 / Access denied. Error 404</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        {literal}
        <style type="text/css">
        	*{border:0;padding:0;margin:0}
        	body{ font:100% Tahoma; color:#777 }
        	h2, p, hr {margin-bottom: 10px}
        	h2{ font-weight: normal }
        	hr { height:1px; color: #333; background: #333 }
        </style>
        {/literal}
	</head>
	<body>
		<div style="margin-left:100px;width:400px;padding-top:100px">
			<h2>Доступ запрещен.</h2>
			<p>Ошибка 403</p>
			<p>&nbsp;</p>
			
			<hr />

			<h2>Access denied.</h2>
			<p>Error 403</p>
			<p>&nbsp;</p>
			
		</div>
	</body>
</html>