<?php

return [
    # types of fields
    'fields'    => [
        'title'   => 'string',
        'content' => 'html',
        'icon'    => 'gs_file',
    ],

    # labels of fields
    'ui'        => [
        'title'   => 'Название страницы',
        'content' => 'Контент',
        'icon'    => 'Иконка',
    ],

    #
    'input_cfg' => [
        'icon'  => array('type' => 'image', 'mask' => 'category1_icon'),
    ],

    # node configuration
    'node'      => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user',
    ],

    # labels for actions
    'labels'    => [
        //'list' => 'Подразделы',
        'add'     => 'Новый раздел',
        'adding'  => 'Создание раздела',
        'edit'    => 'Редактировать раздел',
        'editing' => 'Редактирование раздела',
        'delete'  => 'Удалить раздел',
    ],
];
