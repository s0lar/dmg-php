{if isset($smarty.session.user.id) && isset($smarty.session.user.admin) && $smarty.session.user.admin == 1   &&   isset($smarty.session.seo_mode) && $smarty.session.seo_mode == 1}
<div style="font-size:12px;border-bottom:1px solid #ccc;background:#ffe;font:12px/120% Arial" id="seo_panel">
	{if isset($seo.id) && $seo.id > 0}
	<form action="./:seo_edit" method="post" style="width:1000px;margin:0 auto;padding:10px 0">
		<input type="hidden" name="id" value="{$seo.id}" />
	{else}
	<form action="./:seo_add" method="post" style="width:1000px;margin:0 auto;padding:10px 0">
	{/if}
			<input type="hidden" name="path" value="{$request->url->path}" />
			{*<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">Path</label>
				<input type="hidden" name="path1" value="{$request->url->path}" class="text span10" style="font-size:12px;border-radius:0" disabled='disabled' />
			</div>*}
			<!--<p><label style="font-size:11px">Name</label><input type="text" name="name" value="{$seo.name}" class="text count_letters" style="width:90%" /></p>-->
			<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">Meta title</label><br />
				<input type="text" name="title" value="{$seo.title|htmlspecialchars}" class="text span10 count_letters" style="font-size:12px;border-radius:0;width:70%" />
			</div>
			<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">Meta Keywords</label><br />
				<textarea name="key" class="text count_letters span10" style="height:100px;font:12px/120% Arial;border-radius:0;width:70%">{$seo.key|htmlspecialchars}</textarea>
			</div>
			<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">Meta Description</label><br />
				<textarea name="desc" class="text count_letters span10" style="height:100px;font:12px/120% Arial;border-radius:0;width:70%">{$seo.desc|htmlspecialchars}</textarea>
			</div>
			<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">H1</label><br />
				<input type="text" name="h1" value="{$seo.h1|htmlspecialchars}" class="text count_letters span10" style="font:12px/120% Arial;border-radius:0;width:70%" />
			</div>
			<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">Text</label><br />
				<textarea name="text" class="text count_letters span10" style="font:12px/120% Arial;height:100px;border-radius:0;width:70%">{$seo.text|htmlspecialchars}</textarea>
			</div>
			{*<div class="row" style="margin-bottom:15px">
				<label class="span2" style="font-size:11px;text-align:right">Text2 (выводится внизу, если текста много)</label>
				<textarea name="text2" class="text count_letters span10" style="font:12px/120% Arial;height:150px;border-radius:0">{$seo.text2|htmlspecialchars}</textarea>
			</div>*}
			<div class="row" style="margin-bottom:15px">
				<input type="submit" value="Сохранить" class="btn offset2"  />
			</div>
	</form>
</div>

{*
{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$(".count_letters").each(function(k, v){
			var $p = $(v).parent();
			var c = $(v).val().length;
			$p.find('.count_letters_res').html( c );
		});
		
		$(".count_letters").keydown(function(){
			var $p = $(this).parent();
			var c = $(this).val().length;
			$p.find('.count_letters_res').html( c );
		});
		
		$(".count_letters_res").css({'marginLeft':20, 'display':'inline-block'});
	});
</script>
{/literal}
*}
{/if}