<?php

class Category_Controller extends ZObject_Controller
{
    /**
     * Храним результаты выборки для фильтров
     */
    public $filterResult = null;

    public function action_index()
    {
        //  Нормализуем GET параметры.
        $this->normolizeGetParams();

        //  Подготавливаем запрос по фильтрам
        $query = $this->buildQuery();

        //  Считаем кол-во товаров и количество страниц
        $sql = Qb('SELECT 1 FROM @@product as P
                    LEFT JOIN @@nodes as N
                    ON P.id = N.object_id WHERE N.`object_type`=?s AND N.`path` LIKE ?s ' . $query, ['product', RQ()->url->path . '%']);
        $pg = PG($sql, 24);
        RS('pg', $pg);

        //  AJAX запрос для фильтра
        $url = RQ()->url;
        if ($url->view == 'json') {
            //  Возвращаем количество
            exit(
                json_encode([
                    'success' => true,
                    'tpl_link' => $this->tplLink($pg),
                ])
            );
        }

        //  Включаем оптовые цены
        $customer = new Customers();
        if ($customer->isAuth() && $customer->getCustomer()['opt'] == 1) {
            //  Оптовая цена
            $cost = ', P.`cost_opt` as `cost`';
        } else {
            //  Обычная цена
            $cost = ', P.`cost`';
        }

        //  Загружем найденые товары
        $res = Q('SELECT N.`path`, P.`id`, P.`title`, P.`art`, P.`material`, P.`series`, P.`cost_opt`, P.`new`, P.`top`, P.`photo` ' . $cost . ' FROM @@product as P
                    LEFT JOIN @@nodes as N
                    ON P.id = N.object_id
                    WHERE N.object_type="product" AND N.`path` LIKE ?s ' . $query . '
                    ORDER BY P.sort DESC
                    LIMIT ?i, ?i',
            array(
                RQ()->url->path . '%',
                $pg['offset'],
                $pg['limit'],
            )
        );
        $products = array();
        while ($r = $res->each()) {
            $r['photo'] = $this->getFM($r['photo']);
            $products[] = $r;
        }
        RS('products', $products);

        //  Загружаем информацию о категории
        $page = Q('SELECT * FROM @@category WHERE id=?i', array($this->node['object_id']))->row();
        $page['background'] = $this->getFM($page['background'], 'a');
        $page['menu'] = $this->getFM($page['menu'], 'a');
        RS('page', $page);

        //  Загружаем Серии для фильтра
        $series = $this->getSeries();
        RS('filter_series', $series);

        //  Загружаем Материалы для фильтра
        $material = $this->getMaterial();
        RS('filter_material', $material);

        //  Загружаем мин. и макс. стоимость товаров в категории
        $filter_cost = $this->getCostRange();
        RS('filter_cost', $filter_cost);
        RS('filter_cost_from', [
            'min' => isset($_GET['cost']['0']) ? $_GET['cost']['0'] : $filter_cost['min'],
            'max' => isset($_GET['cost']['1']) ? $_GET['cost']['1'] : $filter_cost['max'],
        ]);

        //  Если выбрана 1 серия, то персонализинуем страницу по Серии
        if (isset($_GET['series']) && count($_GET['series']) == 1) {
            foreach ($_GET['series'] as $v) {
                $page['background'] = $series[$v]['background'];
                $page['title'] .= ' серия ' . $series[$v]['title'];
            }
            RS('page', $page);
        }

        V('category');
    }

    /**
     * Загружаем Серии
     *
     * @return array()
     */
    private function getSeries()
    {
        $seriesFilter = [];
        $res = Q('SELECT p.`series` FROM `@@product` as p
                LEFT JOIN `@@nodes` as n
                ON p.id = n.object_id
                WHERE n.parent_id=?i
                GROUP BY p.`series`', [$this->node['id']]
        );

        while ($r = $res->each()) {
            $seriesFilter[] = $r['series'];
        }

        $series = [];
        $res = Q('SELECT id, title, background FROM @@series WHERE `category`=?i AND id IN (?li)', array($this->node['object_id'], $seriesFilter));
        while ($r = $res->each()) {
            $r['background'] = $this->getFM($r['background'], 'a');
            $series[$r['id']] = $r;
        }

        return $series;
    }

    /**
     * Загружаем Материалы
     *
     * @return array()
     */
    private function getMaterial()
    {
        $materialFilter = [];
        $res = Q('SELECT p.`material` FROM `@@product` as p
                LEFT JOIN `@@nodes` as n
                ON p.id = n.object_id
                WHERE n.parent_id=?i
                GROUP BY p.`material`', [$this->node['id']]
        );

        while ($r = $res->each()) {
            $materialFilter[] = $r['material'];
        }

        $material = [];
        $res = Q('SELECT * FROM @@material ORDER BY `sort` DESC');
        while ($r = $res->each()) {
            if (in_array($r['id'], $materialFilter)) {
                $material[$r['id']] = $r['title'];
            }
        }

        // $config = $this->config = import::config('app:configs/objects/product.php');
        // $material = @$config->fields['material']['items'];
        // unset($material[0]);

        //  Оставляем только найденные материалы
        // $material = array_filter($material, function ($key) use ($materialFilter) {
        //     return in_array($key, $materialFilter);
        // }, ARRAY_FILTER_USE_KEY);

        return $material;
    }

    /**
     * Прводим элементы массива к типу int и возвращаем склееную строку
     *
     * @param [type] $arr
     * @return string
     */
    private function intArray($arr)
    {
        foreach ($arr as $k => $v) {
            $arr[$k] = (int) $v;
        }

        return implode(',', $arr);
    }

    /**
     * Нормализуем GET параметры.
     *
     * /catalog/stulya/-material/2/-series/4;8/-page/2/
     * преобразуем в
     * /catalog/stulya/?material[]=2&series[]=4&series[]=8&page=2
     *
     * @return void
     */
    private function normolizeGetParams()
    {
        $filter_vars = ['cost', 'material', 'series'];
        foreach ($_GET as $k => $v) {
            if (in_array($k, $filter_vars) && !is_array($v)) {
                $_GET[$k] = explode(";", $v);
            }
        }
    }

    /**
     *
     */
    private function buildQuery()
    {
        //  Подставляем значения фильтра в запрос!
        $query = '';

        //  Фильтр по материалам
        if (isset($_GET['material'])) {
            $query .= ' AND P.`material` IN (' . $this->intArray($_GET['material']) . ')';
        }

        //  Фильтр по серии
        if (isset($_GET['series'])) {
            $query .= ' AND P.`series` IN (' . $this->intArray($_GET['series']) . ')';
        }

        //  Цена ОТ
        if (isset($_GET['cost'][0])) {
            $query .= ' AND P.`cost` >= ' . (int) $_GET['cost'][0];
        }

        //  Цена ДО
        if (isset($_GET['cost'][1])) {
            $query .= ' AND P.`cost` <= ' . (int) $_GET['cost'][1];
        }

        return $query;
    }

    /**
     * Генерируем HTML-код всплывающей кнопки количества найденых товаров
     *
     * @param [array] $pg
     * @return string
     */
    private function tplLink($pg)
    {
        $sm = SM();
        $sm->assign('pg', $pg);

        //  HTML-код с результатами поиска
        return $sm->fetch('_filter_show.tpl');
    }

    // public function action_cost()
    // {
    //     //  AUTH
    //     if (!$this->_checkAuth()) {
    //         redirect($this->node['path'] . ":auth");
    //     }

    //     V('zobject/list-cost');
    // }

    public function action_cost()
    {
        __(
            $this->getCostRange()
        );
        exit;
    }
    public function getCostRange()
    {
        return Q('SELECT min(p.`cost`) as `min`, max(p.`cost`) as `max`  FROM `@@product` as p
                LEFT JOIN `@@nodes` as n
                ON p.id = n.object_id
                WHERE n.parent_id=?i
                ', [$this->node['id']]
        )->row();
    }
}
