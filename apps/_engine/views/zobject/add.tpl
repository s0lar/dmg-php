{include file='zobject/header.tpl'}

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="{$request->url->path}:edit">{$object.labels.adding}</a></li>
	</ul>

	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">

		{include file='zobject/blocks/add_objects.tpl' inline='true'}

	</div>


    {*  *}
    {literal}
    <script type="text/javascript">
        var path_check = "{/literal}{$request->url->path}:check_path_exist{literal}";
        var path_translit = "{/literal}{$request->url->path}:translit_name{literal}";

        //console.log(path_check);
        //console.log(path_translit);

        $(document).ready(function(){

            //  Check path exist
            var check_path = function(){
                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    url: path_check,
                    data: {'path': $('#name').val()},
                    success: function (data) {
                        //  Delete error message
                        $('#name').parent().find('.error_message').remove();

                        //  Success
                        if (data.success) {
                            $('#name')
                                .removeClass('error')
                                .addClass('success');
                            //  Remove error message
                            $('#name').parent().find('.error_message').remove();
                        }

                        else if (data.error_message) {

                            $('#name')
                                .removeClass('success')
                                .addClass('error')
                                //  Remove error message
                                .parent().append('<div class="error_message">' + data.error_message + '</div>');
                        }
                        //console.log(data);
                    }
                });
            }


            //  Translit name
            var translit = function(){
                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    url: path_translit,
                    data: {'name': $('#title').val()},
                    success: function (data) {
                        //  Success
                        if (data.success && $('#name').val() == '') {
                            $('#name').val(data.path);
                            $('#name').change();
                        }
                        //console.log(data);
                    }
                });
            }

            //  Trigger on Change
            $('#name').change(check_path);

            //  Trigger on Change
            $('#title').change(translit);
        });
    </script>
    {/literal}

{include file='zobject/footer.tpl'}