﻿<div class="search-result__list">

    {foreach item=product from=$products}
{*        <a class="search-product" href="{$product.path}">*}
        <a class="search-product" href="/catalog/:result?search={$product.title}">
            <div class="search-product__info">
                <div class="search-product__title">{$product.title|stripcslashes}</div>
                <div class="search-product__char">
                    <span>Сталь: AISI304L</span><span>Состав: 03X18H11</span><span>Поверхность: 2B</span></div>
            </div>
            <div class="search-product__price">{$product.cost_view|stripcslashes}</div>
        </a>
    {/foreach}

</div>
<div class="search-result__footer">
    <a class="button style_border_white" href="{$search_link}">
        <span class="button__text">
            Все результаты: {$pg.count}
            <svg class="icon icon-arrow-triangle ">
                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
            </svg>
        </span>
    </a>
    <div class="search-result__close js-searchResultClose">Закрыть</div>
</div>