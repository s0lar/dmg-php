<?php

class News_Controller extends ZObject_Controller
{
    public function action_index()
    {
        //  Список новостей
        if ($this->node['object_type'] == 'sections') {
            RS('newsList', $this->newsList());
            V('newsList');
        }

        //  Подробная новость
        else {
            RS('newsItem', $this->newsItem());
            RS('newsList', $this->newsList());
            V('newsItem');
        }
    }

    /**
     * Получить список новостей
     */
    private function newsList()
    {
        $list = [];

        //  Постраничная разбивка
        $sql = Qb('SELECT 1 FROM @@news');
        $pg = PG($sql, 8);
        RS('pg', $pg);

        //  Загружем найденые товары
        $res = Q('SELECT N.`path`, P.`title`, P.`date`, P.`anons`, P.`photo` FROM @@news as P
                    LEFT JOIN @@nodes as N
                    ON P.id = N.object_id
                    WHERE N.object_type="news"
                    ORDER BY P.date DESC
                    LIMIT ?i, ?i',
            array(
                $pg['offset'],
                $pg['limit'],
            )
        );
        while ($r = $res->each()) {
            $r['photo'] = $this->getFM($r['photo'], 'a');
            $r['date'] = (new DateTime($r['date']))->format('d / m / Y');
            $r['shareLinkRaw'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $r['path'];
            $r['shareLink'] = urlencode($r['shareLinkRaw']);
            $r['shareTitle'] = urlencode($r['title']);
            $list[] = $r;
        }

        return $list;
    }

    /**
     * Получить подробную новость
     */
    private function newsItem()
    {
        $item = Q('SELECT * FROM @@news WHERE id=?i', [
            $this->node['object_id'],
        ])->row();

        $item['date'] = (new DateTime($item['date']))->format('d / m / Y');
        $item['photo'] = $this->getFM($item['photo'], 'a');
//        $item['gallery'] = $this->getFM($item['gallery']);
//        $item['gallery2'] = $this->getFM($item['gallery2']);

        return $item;
    }
}
