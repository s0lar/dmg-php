{include file='zobject/header.tpl' inline='true'}

	{include file='zobject/blocks/list_controll.tpl' inline='true'}
	
	<div class='' style='padding:20px;border:1px solid #ddd;margin-top:-1px'>
		
		<form action='{$request->url->path}:seo_save' method='post'>
			
			<div class='row-fluid'>
				<div class='span4'>Meta title</div>
				<div class='span7'><input type='text' name='title' value='{$seo.title|htmlspecialchars}' style='width:100%' /></div>
			</div>
			<div class='row-fluid'>
				<div class='span4'>Meta Keywords</div>
				<div class='span7'><textarea name='key'  style='width:100%'>{$seo.key|htmlspecialchars}</textarea></div>
			</div>
			<div class='row-fluid'>
				<div class='span4'>Meta Description</div>
				<div class='span7'><textarea name='desc'  style='width:100%'>{$seo.desc|htmlspecialchars}</textarea></div>
			</div>
			<div class='row-fluid'>
				<div class='span4'>H1</div>
				<div class='span7'><input type='text' name='h1' value='{$seo.h1|htmlspecialchars}'  style='width:100%' /></div>
			</div>
			<div class='row-fluid'>
				<div class='span4'>Text</div>
				<div class='span7'><textarea name='text' style='width:100%'>{$seo.text|htmlspecialchars}</textarea></div>
			</div>

			<div class='row-fluid'>
				<input type='hidden' name='node_id' value='{$node_id}' />
			</div>

			<div class='well well-panel well-panel-controll'>
				<input type='submit' value='Сохранить' class='btn btn-primary' />
				<a href='{$request->url->path}:edit' class='btn btn-small pull-right'>Отмена</a>
			</div>
		</form>


		
	</div>

{include file='zobject/footer.tpl' inline='true'}