﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_checkout">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">Заказ оформлен</div>
                </div>
                <div class="checkout-step">
                    <div class="checkout-step__elem is-active">
                        <div class="checkout-step__title">01. Состав заказа</div>
                    </div>
                    <div class="checkout-step__elem is-active">
                        <div class="checkout-step__title">02. Контакты и доставка</div>
                    </div>
                    <div class="checkout-step__elem is-active">
                        <div class="checkout-step__title">03. подтверждение</div>
                    </div>
                </div>
                <div class="checkout-counter">
                    <div class="checkout-counter__title">Подтверждение</div>
                    <div class="checkout-counter__number">03 / 03</div>
                </div>
            </div>
            <div class="checkout">
                <div class="checkout-flex">
                    <div class="checkout-flex__left">
                        <div class="checkout-success">
                            <div class="checkout-success__flex">
                                <div class="checkout-success__text">Спасибо за ваше обращение!<br><span class="hidden-xs">Наши менеджеры уже получили вашу заявку и&nbsp;свяжутся с&nbsp;вами в&nbsp;ближайшее время.</span></div>
                                <div class="checkout-success__icon"><img src="/theme/dmg_markup/build/images/tick-green.svg"></div>
                            </div>
                            <div class="checkout-success__message visible-xs">Наши менеджеры уже получили вашу заявку и&nbsp;свяжутся си&nbsp;вами ви&nbsp;ближайшее время.</div>
                        </div>
                        <div class="checkout-next"><a class="button style_green size_middle justify_space_between" href="/"><span class="button__text">На главную</span></a></div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}

    </div>
{/block}

{block name=footer}
{/block}