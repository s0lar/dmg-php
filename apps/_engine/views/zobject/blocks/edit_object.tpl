{if isset($smarty.get.msg) && $smarty.get.msg == 'saved'}
	<!-- <div style="text-align:center"><span style="padding:5px 10px;background:#555;color:#fff;margin-bottom:10px">Сохранено</span></div> -->
	<div class="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>Сохранено</strong>
	</div>

{elseif isset($smarty.get.msg) && $smarty.get.msg == 'added'}
	<!-- <div style="text-align:center"><span style="padding:5px 10px;background:#555;color:#fff;margin-bottom:10px">Сохранено</span></div> -->
	<div class="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong>Сохранено</strong>
	</div>
{/if}

<fieldset class="group">
	<!-- <legend class="group-title">{$object.labels.editing}</legend> -->
	<form action="{$request->url->path}:edit.html" method="post" class="ajax1" id="object-edit">


		{* Show subTabs ************************************************************************************************************************************}
		{if isset($object.subtabs) && !empty($object.subtabs)}

		<ul class="nav nav-tabs" id='subtabs'>
			{foreach item=i from=$object.subtabs key=k name=nn}
				<li class=' {if $smarty.foreach.nn.first}active{/if} '><a href="#subtabs_{$k}" data-toggle="tab">{$i.title}</a></li>
			{/foreach}
		</ul>

		{/if}



		{* Show subTabs ************************************************************************************************************************************}
		{if isset($object.subtabs) && !empty($object.subtabs)}

			<div class="tab-content" style='border:1px solid #cecece;padding:20px;margin-top:-21px;border-top:0'>
				{foreach item=ii from=$object.subtabs key=kk name=nn}
					<div class="tab-pane {if $smarty.foreach.nn.first}active{/if}" id="subtabs_{$kk}">
						
						{foreach item=i key=k from=$object.ui}
							{if in_array( $k, $ii.fields_include )}
							
								{include file='zobject/blocks/edit_object_fields.tpl'}

							{/if}
						{/foreach}

					</div>
				{/foreach}
			</div>

		{* Show fields ************************************************************************************************************************************}
		{else}
			{foreach item=i key=k from=$object.ui}
				{include file='zobject/blocks/edit_object_fields.tpl'}
			{/foreach}
		{/if}
		
					
		
		<div class="well well-panel well-panel-controll">
			{if isset($object.view) && $object.view == 'list'}<input type="submit" name="goparent" value="Сохранить" class="btn btn-primary" />{/if}
			<input type="submit" name="save" value="Применить" class="btn btn-primary1" />
			<a href="{$request->url->path}:edit" class="btn btn-small pull-right">Отмена</a>
		</div>
		
	</form>
</fieldset>
