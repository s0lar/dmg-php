<?php
@session_start();

class Zcfg_Controller extends ZObject_Controller
{
	var $cfg = null;
	
	var $view = array(
		'int' 		=> 'Число', 
		'float' 	=> 'Десятичное число', 
		'string' 	=> 'Строка', 
		'image' 	=> 'Фото', 
		'file' 		=> 'Файл', 
		'text' 		=> 'Текстовое поле', 
		'date' 		=> 'Дата', 
	);

	function __construct(){
		//parent::__construct(null);
		
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_editNode(){
		$res = Q("SELECT `id`, `title`, `view`, `value`, `is_global` FROM `@@zcfg` ");
		
		RS("_list", $res->all());
		V("zcfg/edit_node");
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_add(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		RS("view", $this->view);
		V("zcfg/add");
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_add_post(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		$id = Q('INSERT INTO `@@zcfg` SET `title`=?s, `example`=?s, `view`=?s, `name`=?s, `is_global`=?i', 
				array( strip_tags($_POST['title']), strip_tags($_POST['example']), strip_tags($_POST['view']), strip_tags($_POST['name']), isset($_POST['is_global']) ? '1' : '0' ));
		
		//	Apply
		if( isset($_POST['apply']) )
			redirect("/zcfg/:edit/-msg/good/-id/{$id}/");
		else
			redirect("/zcfg/:edit_node/-msg/good/-last/{$id}/");
	}

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_edit(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
		$data = Q("SELECT `id`, `title`, `example`, `view`, `value` FROM `@@zcfg` WHERE id = ?i", array( $id ))->row();
		
		RS("_item", $data);
		V("zcfg/edit");
	}

		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_edit_post(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
		
		$view = Q("SELECT `view` FROM @@zcfg WHERE `id`=?i", array($id))->row('view');
		
		$value = $this->prepare($_POST['value'], $view);
		
		// update data
		Q('UPDATE `@@zcfg` SET `value`=?s WHERE `id`=?i', array($value, $id));
		
		//	Apply
		if( isset($_POST['apply']) )
			redirect("/zcfg/:edit/-id/{$id}/");
		else
			redirect("/zcfg/:edit_node");
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_delete(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
		$res = Q("DELETE FROM `@@zcfg` WHERE id=?i LIMIT 1", array( $id ));
		redirect("/zcfg/:edit_node");
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_update(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
		$data = Q("SELECT `id`, `title`, `name`, `example`, `view`, `value`, `is_global` FROM `@@zcfg` WHERE id = ?i", array( $id ))->row();
		
		RS("view", $this->view);
		RS("_item", $data);
		V("zcfg/update");
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function action_update_post(){
		if(!$this->userCheckAccess("zcfg")) { redirect("/zcfg/:edit_node/-err/access"); }
		
		$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
		
		$sql = Q('UPDATE `@@zcfg` SET `title`=?s, `example`=?s, `view`=?s, `name`=?s, `is_global`=?i WHERE `id`=?i', 
				array( strip_tags($_POST['title']), strip_tags($_POST['example']), strip_tags($_POST['view']), strip_tags($_POST['name']), isset($_POST['is_global']) ? '1' : '0', $id ));
				
		redirect("/zcfg/:edit_node");
	}
	
	
//	
	function prepare($var, $view){
		switch ($view) {
			
			case 'int':
				$var = intval($var);
				break;
				
			case 'floatval':
				$var = intval($var);
				break;
			
			default:
				break;
		}
		
		return $var;
	}	
}

