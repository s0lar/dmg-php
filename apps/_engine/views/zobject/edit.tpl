{include file='zobject/header.tpl' inline='true'}
	
	{* Error message *}
	{if isset($smarty.get.err) && $smarty.get.err == 'access'}
		<div class="alert alert-block">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<p>Ошибка. У вас нет доступа к данному разделу</p>
		</div>
		
	{elseif isset($error_access) && $error_access == 1}
		<div class="alert alert-block">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<p>Ошибка. У вас нет доступа к данному разделу</p>
		</div>
		
	{/if}

	{include file='zobject/blocks/list_controll.tpl' inline='true'}
	
	
	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		<div class="well well-panel">
			{foreach item=i from=$object.subobjects}<a href="{$request->url->path}:add{$request->url->view}/-type/{$i.type}/" class="btn btn-mini btn-info"><span class="icon-plus-sign icon-white"></span> {$i.labels.add}</a> &nbsp;&nbsp;&nbsp;{/foreach}

			{foreach item=action_name from=$object.actions key=action}<a href="{$request->url->path}:{$action}" class="btn btn-mini btn-info">{$action_name}</a> &nbsp;&nbsp;&nbsp;{/foreach}
			
			<a href="{$request->url->path}:delete" onclick="return confirm('Удалить?')" class="delete-object btn btn-mini btn-danger pull-right"><span class="icon-remove-sign icon-white"></span> Удалить</a>
		</div>
		
		{include file='zobject/blocks/edit_object.tpl' inline='true'}
		
	</div>
{include file='zobject/footer.tpl' inline='true'}