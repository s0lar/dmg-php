﻿<form class="feedback__form  ajax"
      action="/contacts/:feedback"
      method="post"
      rel="feedback"
      id="feedback__form"
      data-reCAPTCHA_site_key="{$smarty.const.reCaptcha_pub}"
>
    <div class="form-field">
        <input name="name" class="form-input" type="text" placeholder="Представьтесь, пожалуйста">
        <div class="form-icon">
            <svg class="icon icon-form-user ">
                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-form-user"></use>
            </svg>

        </div>
    </div>
    <div class="form-field">
        <input name="phone" class="form-input" type="text" placeholder="Номер телефона">
        <div class="form-icon">
            <svg class="icon icon-form-phone ">
                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-form-phone"></use>
            </svg>
        </div>
    </div>
    <div class="form-field">
        <textarea name="message" class="form-input" placeholder="Комментарий"></textarea>
        <div class="form-icon">
            <svg class="icon icon-form-comment ">
                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-form-comment"></use>
            </svg>
        </div>
    </div>
    <div class="feedback__agree">
        <label class="checkbox">
            <input name="agreement" type="checkbox">
            <span class="checkbox__title">
                Я согласен с <a href="/personal/" target="_blank">политикой обработки персональных данных</a>.
            </span>
        </label>
    </div>
    <div class="feedback__submit">
        {*        <button class="button style_border_green justify_space_between js-modalLink" data-mfp-src="#modalSuccess">*}
        <button class="button style_border_green justify_space_between" id="form_submit">
            <span class="button__text">
                Отправить
                <svg class="icon icon-arrow-triangle ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                </svg>
            </span>
        </button>

        {if $popup === true}
            <a class="feedback__link hidden-xs js-feedbackModalClose" href="javascript:;">Закрыть</a>
        {/if}
        <a id="feedbackModalSuccess" class="js-modalLink" style="display: none" href="javascript:;"
           data-mfp-src="#modalSuccess"></a>

        <input type="hidden" name="city" value="1" class="js-formCityHidden">
        <input type="hidden" name="csrf_feedback" value="{$smarty.SESSION.csrf_feedback}"/>

        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value=""/>
    </div>
</form>

{if $env !== 'dev'}

{literal}
    <script>
        // grecaptcha.ready(function () {
        //     grecaptcha
        //         .execute(
        //             '6LeO-qUZAAAAALb7IJJh39FRq9HNdLREdw9hvLb1',
        //             {action: 'submit'}
        //         )
        //         .then(function (token) {
        //             // Verify the token on the server.
        //             $('#g-recaptcha-response').val(token);
        //         });
        // });
    </script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LeO-qUZAAAAALb7IJJh39FRq9HNdLREdw9hvLb1', {action: 'submit'}).then(function (token) {
                console.log('++++');
                console.log(token);
                document.getElementById("g-recaptcha-response").value = token;
                // document.getElementById("g-recaptcha-response").value=token;
            }).then(function (token) {
            });
        });
    </script>
{/literal}

{/if}