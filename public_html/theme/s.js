//  Update product total
var cartUpdateProduct = function (data) {
    for (id in data.prods) {
        $('.sjs-total-' + id).empty().html(data.prods[id].total_view)
    }
}

//  Remove product
var cartRemoveProduct = function (data, removeId) {
    $('.sjs-item-' + removeId).remove();
}

//  Update cart total & count
var cartUpdateTotal = function (data) {
    $('.sjs-cart-total').empty().html('Корзина: ' + data.total_view + ' руб.');
    $('.sjs-cart-count').empty().html(data['count']);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 	CALLBACKS
var callbacks = {

    /* Review forms callbacks */
    feedback_success: function (data, $form) {
        $('.js-feedbackModalClose').click();
        $('#feedbackModalSuccess').click();

        // $(document).trigger('modalFormSubscribeSuccessTrigger');
    },
    feedback_before: function ($form) {
    },
    feedback_error: function (data, $form) {
        $form.find(".is-error").each(function () {
            $(this).removeClass("is-error");
        });

        $.each(data.errors, function (field, text) {
            $form
                .find("input[name$='" + field + "']")
                .parent()
                .addClass("is-error");
        });
    },


    /* Review forms callbacks */
    filter_count_success: function (data, $target) {

        $tooltip = $("#id-filter-tooltip");
        $tooltip.find('.filter-tooltip__title').empty().append(data['count']);
        $tooltip.show();

        $tooltip.prependTo($target.parents('.filter-group__checkbox'));
    },
    filter_count_before: function ($form) {
        $("#id-filter-tooltip").hide();
    },
    filter_count_error: function (data, $form) {
        $("#id-filter-tooltip").hide();
    },

    //
    /* Review forms callbacks */
    cart_add_success: function (data, $target) {
        cartUpdateProduct(data.cart.cart);
        cartUpdateTotal(data.cart.cart);
        $('#modalAddedToCart2').find('.modal-cart').empty().append(data._cart_added);
        window.modalOpen('#modalAddedToCart2');
    },
    cart_add_before: function ($form) {
    },
    cart_add_error: function (data, $form) {
    },

    /* Review forms callbacks */
    cart_add2_success: function (data, $target) {
        cartUpdateProduct(data.cart.cart);
        cartUpdateTotal(data.cart.cart);
    },
    cart_add2_before: function ($form) {
    },
    cart_add2_error: function (data, $form) {
    },


    /* Review forms callbacks */
    cart_remove_success: function (data, $target) {
        cartRemoveProduct(data.cart.cart, data.product);
        cartUpdateTotal(data.cart.cart);
    },
    cart_remove_before: function ($form) {
    },
    cart_remove_error: function (data, $form) {
    },

    /* Review forms callbacks */
    checkout_order_success: function (data, $target) {
        window.location.replace("/order/:success");
    },
    checkout_order_before: function ($form) {
    },
    checkout_order_error: function (data, $form) {
        console.log(data);

        $p = $('#modalOrderErrors').find('.modal-text p');
        $p.empty();

        for (i in data.error) {
            $p.append(data.error[i] + '<br/>');
        }

        window.modalOpen('#modalOrderErrors');

        // alert(data);
    },

    /* Search forms callbacks */
    search_success: function (data, $target) {
        $('#search-result').empty().append(data['_search_dropdown'])
    },
    search_before: function ($form) {
    },
    search_error: function (data, $form) {
        console.log(data);

        $p = $('#modalOrderErrors').find('.modal-text p');
        $p.empty();

        for (i in data.error) {
            $p.append(data.error[i] + '<br/>');
        }

        window.modalOpen('#modalOrderErrors');

        // alert(data);
    },


}


var ajaxInit = function () {

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	Forms validate
    //$('form.validate').validate();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	Links ajax send
    $("a.ajax").click(function () {
        var callback_prefix = $(this).attr('rel').replace(/-/g, '_');
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: $(this).attr('href') + '.json',
            beforeSend: function () {
                callbacks[callback_prefix + '_before'] ? callbacks[callback_prefix + '_before']() : callbacks['default_before']();
            },
            success: function (data) {
                if (data) {
                    if (!data.success) callbacks[callback_prefix + '_error'] ? callbacks[callback_prefix + '_error'](data, $(this)) : callbacks['default_error'](data, $(this));
                    else callbacks[callback_prefix + '_success'] ? callbacks[callback_prefix + '_success'](data, $(this)) : callbacks['default_success'](data, $(this));
                }
            }
        });
        return false;
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	Forms ajax send
    $("form.ajax").submit(function (event) {
        event.preventDefault();
        //	Validation
        // if (!$(this).hasClass('xv-valid'))
        // 	return false;

        $form = $(this);
        var callback_prefix = $form.attr('rel').replace(/-/g, '_');

        console.log('callback_prefix', callback_prefix);


        $.ajax({
            type: $form.attr('method'),
            dataType: 'json',
            url: $form.attr('action'),
            data: $form.serialize(),
            beforeSend: function () {
                callbacks[callback_prefix + '_before'] ? callbacks[callback_prefix + '_before']($form) : callbacks['default_before']($form);
            },
            success: function (data) {
                if (data) {
                    if (!data.success)
                        callbacks[callback_prefix + '_error'] ? callbacks[callback_prefix + '_error'](data, $form) : callbacks['default_error'](data, $form);
                    else
                        callbacks[callback_prefix + '_success'] ? callbacks[callback_prefix + '_success'](data, $form) : callbacks['default_success'](data, $form);
                }
            }
        });

        return false;
    });


    $("#filterFormAjax input").change(function ($k, $v) {
        var $target = $(this);
        $form = $("#filterFormAjax");

        var callback_prefix = $form.attr('rel').replace(/-/g, '_');

        $.ajax({
            type: $form.attr('method'),
            dataType: 'json',
            url: $form.attr('action') + ':filter_count',
            data: $form.serialize(),
            beforeSend: function () {
                callbacks[callback_prefix + '_before'] ? callbacks[callback_prefix + '_before']($target) : callbacks['default_before']($target);
            },
            success: function (data) {
                if (data) {
                    if (!data.success)
                        callbacks[callback_prefix + '_error'] ? callbacks[callback_prefix + '_error'](data, $target) : callbacks['default_error'](data, $target);
                    else
                        callbacks[callback_prefix + '_success'] ? callbacks[callback_prefix + '_success'](data, $target) : callbacks['default_success'](data, $target);
                }
            }
        });
    });


    $('.dropdown-count').change(function () {
        $form = $(this).parents('form');
        $form.submit();
    });

    //
    $('.sort_select').change(function () {
        $(this).parents('form').submit();
    });

    //  Search form
    $searchForm = $('#header-search-form');
    $searchForm.find('.search-form__input').keydown(function () {
        var $target = $(this);
        if ($(this).val().length >= 3) {
            // $searchForm.submit();

            var callback_prefix = $searchForm.attr('rel').replace(/-/g, '_');

            $.ajax({
                type: $searchForm.attr('method'),
                dataType: 'json',
                url: '/catalog/:search',
                data: $searchForm.serialize(),
                beforeSend: function () {
                    callbacks[callback_prefix + '_before'] ? callbacks[callback_prefix + '_before']($target) : callbacks['default_before']($target);
                },
                success: function (data) {
                    if (data) {
                        if (!data.success)
                            callbacks[callback_prefix + '_error'] ? callbacks[callback_prefix + '_error'](data, $target) : callbacks['default_error'](data, $target);
                        else
                            callbacks[callback_prefix + '_success'] ? callbacks[callback_prefix + '_success'](data, $target) : callbacks['default_success'](data, $target);
                    }
                }
            });
        }
        else {
            $('#search-result').empty();
        }
    });

};

//
$(document).ready(ajaxInit);
