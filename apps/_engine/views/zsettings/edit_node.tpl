{include file='zobject/header.tpl' inline='true'}


{* Error message *}
{if isset($smarty.get.err) && $smarty.get.err == 'access'}
    <div class="alert alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>Ошибка. У вас нет доступа к данному разделу</p>
    </div>
{/if}


<ul class="nav nav-tabs" style="margin-bottom:0">
    <li class="active"><a href="#">{$labels.list}</a></li>
</ul>


<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">

    {if isset($labels.add)}
        <div class="well well-panel">
            <a href="/zsettings/:add/-m/{$module}/" class="btn btn-mini btn-info"><span
                        class="icon-plus-sign icon-white"></span> {$labels.add}</a> &nbsp;&nbsp;&nbsp;
        </div>
    {/if}

    <table class="table table-striped table-condensed">
        <tr>
            {foreach item=i from=$ui name=ii key=kk}
                <th{if $smarty.foreach.ii.first} class="span1"{/if}>
                    <a href="/zsettings/:edit_node/-m/{$module}/-order_field/{$kk}/-order_type/{if isset($smarty.get.order_type) && $smarty.get.order_type != "desc"}desc{else}asc{/if}">
                        {$i}

                        {if isset($smarty.get.order_field) && $smarty.get.order_field == $kk}
                            {if isset($smarty.get.order_type) && $smarty.get.order_type != "desc"}
                                &#9650;
                            {else}
                                &#9660;
                            {/if}
                        {/if}

                    </a>
                </th>
            {/foreach}
            <th class="span2"></th>
        </tr>

        {if (isset($_list) && !empty($_list))}
            {foreach item=i from=$_list}
                <tr {if isset($smarty.get.last) && $smarty.get.last == $i.id}class="added"{/if}>
                    {foreach item=ii from=$ui key=key}
                        <td>
                            {if isset($fields[$key]) && $fields[$key] == 'checkbox'}
                                {($i[$key] == 1) ? 'Да' : 'Нет' }
                            {else}
                                {$i[$key]}
                            {/if}
                        </td>
                    {/foreach}

                    <td style="text-align:right">
                        <a href="/zsettings/:edit/-m/{$module}/-id/{$i.id}/" class="btn btn-small"><span
                                    class="icon-edit"></span></a>
                        <a href="/zsettings/:delete/-m/{$module}/-id/{$i.id}/" class="btn btn-small btn-danger"
                           onclick="return confirm('Удалить?')"><span class="icon-remove icon-white"></span></a>
                    </td>
                </tr>
            {/foreach}

        {else}
            <tr>
                <td colspan="{$ui_count}" style="padding:30px 0;text-align:center">
                    Данных нет
                </td>
            </tr>
        {/if}

    </table>


</div>
{include file='zobject/footer.tpl' inline='true'}