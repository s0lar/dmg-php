{include file='zobject/header.tpl'}

{include file='zobject/blocks/list_controll.tpl' inline='true'}

<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">

    <div class="well well-panel">
        {foreach item=i from=$object.subobjects}
            <a href="{$request->url->path}:add?type={$smarty.get.type}" class="btn btn-mini btn-info">
                <span class="icon-plus-sign icon-white"></span>
                {$i.labels.add}
            </a>
            &nbsp;&nbsp;&nbsp;
        {/foreach}

    </div>

    <form method="post">
        <table class="table table-striped table-bordered">
            <tr>
                {foreach item=i from=$config->view.fields name=loop}
                    <th {if $smarty.foreach.loop.last} class="last"{/if}>{$config->ui[$i]}</th>
                {/foreach}

                <th class=""></th>
            </tr>

            {foreach item=i from=$items}
                <tr>
                    {foreach item=j from=$i key=k}
                        {if 'path' neq $k && 'id' neq $k}
                            <td>
                                {if $k eq $config->view.edit_field}
                                    <a href="{$i.path}:edit/">{$j|stripslashes}</a>
                                {else}

                                    {if isset($config->view.fields_edit) && in_array($k, $config->view.fields_edit)}
                                        <input name="{$k}[{$i.id}]" value="{$j|stripslashes}" class="input-text"
                                               style="width:100px;border:2px solid #ccc"/>
                                    {elseif isset($config->fields[$k]['type']) && $config->fields[$k]['type'] == 'list'}
                                        {$config->fields[$k]['items'][$j]|stripslashes}

                                    {elseif isset($config->fields[$k]['type']) && $config->fields[$k]['type'] == 'pointer'}
                                        {$ext_data[$k][$j]|stripslashes|default:'-- не указано --'}

                                    {else}
                                        {$j|stripslashes}
                                    {/if}

                                {/if}
                            </td>
                        {/if}
                    {/foreach}

                    <td>
                        <a href="{$request->url->path}:copy/-type/{$smarty.get.type}/-id/{$i.id}/" class="btn btn-small"
                           title="Создать копию"><span class="icon-repeat"></span></a>
                        <a href="{$i.path}:edit/" class="btn btn-small" title="Редактировать"><span
                                    class="icon-edit"></span></a>
                        <a href="{$i.path}:delete/" class="btn btn-small btn-danger"
                           onclick="return confirm('Удалить?')" title="Удалить"><span
                                    class="icon-remove icon-white"></span></a>
                    </td>

                </tr>
            {/foreach}
        </table>

        <input type="submit" class="btn btn-primary" value="Сохранить"/>
    </form>
</div>

{include file='zobject/footer.tpl'}
