{extends file='mail/layout.tpl'}

{block name=content}
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr><td class="offset-double" style="height: 60px;line-height: 60px;">&nbsp;</td></tr>
	<tr>
		<td>
			<div style="font-weight: bold; font-size: 24px;" class="title">Оформлен заказ на сайте <br /><a target="_blank" style="color:#0076ff" href='http://{$smarty.server.HTTP_HOST}'>{$smarty.const.PR_NAME}</a></div>
		</td>
	</tr>
    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>

    {$cart_html}

</table>
{/block}