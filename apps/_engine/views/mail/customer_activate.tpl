{extends file='mail/layout.tpl'}

{block name=content}
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr><td class="offset-double" style="height: 60px;line-height: 60px;">&nbsp;</td></tr>
	<tr>
		<td>
			<div style="font-weight: bold; font-size: 24px;" class="title">Активация учетной записи на сайте <br /><a target="_blank" style="color:#0076ff" href='http://{$smarty.server.HTTP_HOST}'>{$smarty.server.HTTP_HOST}</a></div>
		</td>
	</tr>

    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td>
        <span style="font-weight: bold;">Вы зарегистрировались на сайте <a href="http://{$smarty.server.HTTP_HOST}">{$smarty.const.PR_NAME}</a>. <br /></span>
        Для завершения регистрации, необходимо активировать учетную запись.
    </td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Для активации перейдите по ссылке <br /></span>
        <div style="width:400px;overflow:hidden;word-wrap:break-word;">
            <a target="_blank" style="color:#0076ff" href="http://{$actiovationUrl}">
                {$actiovationUrl}
            </a>
        </div>
    </td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Если Вы не регистрировались на сайте, то проигнорируйте это письмо</span></td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>


</table>
{/block}