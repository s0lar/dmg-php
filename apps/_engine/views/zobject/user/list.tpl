{include file='zobject/header.tpl'}	

	<fieldset class="group">
		<legend class="group-title">Пользователи</legend>
		
		{* Error message *}
		{if isset($smarty.get.err) && $smarty.get.err == 'access'}
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<p>Ошибка. У вас нет доступа к данному разделу</p>
			</div>
		{/if}
		
		
		<p><a class="btn btn-mini btn-info" href="/:user_add"><span class="icon-plus-sign icon-white"></span> Добавить нового пользователя</a><Br />&nbsp;</p>
		<table class="table">
			<tr>
				<th>Логин</th>
				<th>Изменен</th>
				<th></th>
			</tr>
			{foreach item=i from=$u_list}
			<tr>
				<td>{$i.login}</td>
				<td>{$i.updated}</td>
				<td>
					<div class="btn-group pull-right">
						<a href="/:user_edit/-id/{$i.id}/" class="btn btn-small">редактировать</a>
						<a href="/:user_del/-id/{$i.id}/"  class="btn btn-small btn-danger" onClick="return confirm('Confirm action?');">удалить</a>
					</div>
				</td>
			</tr>
			
			{/foreach}
		</table>
	</fieldset>
{include file='zobject/footer.tpl'}
