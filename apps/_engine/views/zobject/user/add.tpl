{include file='zobject/header.tpl'}	

	<fieldset class="group">
		<legend class="group-title">Добавление пользователя</legend>
		<form action="{$request->url->path}:user_add.html" method="post">
				<p class="field">
					<label class="field-title" for="login">Логин (латинские символы)</label>
					<input type="text" name="login" id="login" class="input-text" style="width:300px" />
					{if isset($smarty.get.err) && $smarty.get.err == 'empty_login'}&nbsp;&nbsp;&nbsp;<span style="color:red">Пустой логин</span>
					{elseif isset($smarty.get.err) && $smarty.get.err == 'login_exist'}&nbsp;&nbsp;&nbsp;<span style="color:red">Логин занят</span>
					{/if}
					<br />Например: <b>manager</b> или <b>user@mail.ru</b>
				</p>
				
				<p class="field">
					<label class="field-title" for="pass">Пароль</label>
					<input type="text" name="pass" id="pass" class="input-text" style="width:300px" /><br />
				</p>
				
				<p class="field">
					<label class="field-title" for="pass2">Повторите пароль</label>
					<input type="text" name="pass2" id="pass2" class="input-text" style="width:300px" /><br />
				</p>
				
				
				{include file="zobject/user/access_rules.tpl"}
				
				{*
				<p class="field">
					<label class="field-title">
						<input type="checkbox" name="admin" />
						Доступ в панель управления сайтом
					</label>
				</p>
				*}
				
				{*<p class="field">
					<label class="field-title">
						<input type="checkbox" name="dealer" />
						Доступ ко всем дилерам
					</label>
				</p>*}
				
			
			<div class="btn-group">
				<input type="submit" value="Сохранить" class="btn btn-primary"/>
				<input type="submit" name="save" value="Применить" class="btn btn-primary1" />
			</div>
			
		</form>
	</fieldset>
{include file='zobject/footer.tpl'}
