<?php

class Category1_Controller extends ZObject_Controller
{
    public function action_index()
    {
        $category = Q('SELECT * FROM @@category1 where id=?i', [$this->node['object_id']])->row();

//        $category['icon'] = $this->getFM($category['icon'], 'a');
        $category['icon'] = $this->getFM($category['icon2'], 'a');
        RS('page', $category);


        //  Get all Categories
        $categories = [];

        $res = Q('SELECT N.`path`, P.* FROM @@category2 as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="category2" and N.parent_id = ?i
                        ORDER BY P.`sort` desc', [$this->node['id']]);

        while ($r = $res->each()) {
            $r['icon'] = $this->getFM($r['icon'], 'a');

            $categories[$r['id']] = $r;
        }

        RS('categories', $categories);
        V('category1');
    }
}
