<?php

return [
    # types of fields
    'fields'    => [
        'title'     => 'string',
        '1c_id'     => 'string',
        'cost'      => 'float',
        'cost_view' => 'string',

        'tech1_name' => 'string',
        'tech1'      => 'string',
        'tech2_name' => 'string',
        'tech2'      => 'string',
        'tech3_name' => 'string',
        'tech3'      => 'string',
        'tech4_name' => 'string',
        'tech4'      => 'string',
        'tech5_name' => 'string',
        'tech5'      => 'string',
        'tech6_name' => 'string',
        'tech6'      => 'string',
        'tech7_name' => 'string',
        'tech7'      => 'string',
        'tech8_name' => 'string',
        'tech8'      => 'string',
        'tech9_name' => 'string',
        'tech9'      => 'string',
        'tech10_name' => 'string',
        'tech10'      => 'string',
        'tech11_name' => 'string',
        'tech11'      => 'string',
        'tech12_name' => 'string',
        'tech12'      => 'string',
        'tech13_name' => 'string',
        'tech13'      => 'string',
        'tech14_name' => 'string',
        'tech14'      => 'string',
        'tech15_name' => 'string',
        'tech15'      => 'string',
        'tech16_name' => 'string',
        'tech16'      => 'string',

    ],

    # labels of fields
    'ui'        => [
        'title'     => 'Название',
        '1c_id'     => 'Код 1с',
        'cost'      => 'Цена',
        'cost_view' => 'Цена отображения',

        'tech1_name' => 'Название 1',
        'tech1'      => 'Значение 1',
        'tech2_name' => 'Название 2',
        'tech2'      => 'Значение 2',
        'tech3_name' => 'Название 3',
        'tech3'      => 'Значение 3',
        'tech4_name' => 'Название 4',
        'tech4'      => 'Значение 4',
        'tech5_name' => 'Название 5',
        'tech5'      => 'Значение 5',
        'tech6_name' => 'Название 6',
        'tech6'      => 'Значение 6',
        'tech7_name' => 'Название 7',
        'tech7'      => 'Значение 7',
        'tech8_name' => 'Название 8',
        'tech8'      => 'Значение 8',
        'tech9_name' => 'Название 9',
        'tech9'      => 'Значение 9',
        'tech10_name' => 'Название 10',
        'tech10'      => 'Значение 10',
        'tech11_name' => 'Название 11',
        'tech11'      => 'Значение 11',
        'tech12_name' => 'Название 12',
        'tech12'      => 'Значение 12',
        'tech13_name' => 'Название 13',
        'tech13'      => 'Значение 13',
        'tech14_name' => 'Название 14',
        'tech14'      => 'Значение 14',
        'tech15_name' => 'Название 15',
        'tech15'      => 'Значение 15',
        'tech16_name' => 'Название 16',
        'tech16'      => 'Значение 16',
    ],

    #
    'input_cfg' => [
        'photo'         => ['type' => 'image', 'mask' => 'product_photo'],
        'gallery'       => ['type' => 'image', 'mask' => 'product_gallery'],
        'gallery2'      => ['type' => 'image', 'mask' => 'product_gallery'],
        'video_preview' => ['type' => 'image', 'mask' => 'video_preview'],
        'prod_files'    => ['type' => 'file', 'mask' => 'product_files'],
    ],

    # node configuration
    'node'      => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user',
    ],

    // #
    // 'subtabs' => array(
    //     '0' => array(
    //         'title' => 'Карточка товара в категории',
    //         'fields_include' => array('title', 'art', 'material', 'series', 'cost', 'new', 'top', 'sort'),
    //     ),
    //     '1' => array(
    //         'title' => 'Карточка товара',
    //         'fields_include' => array('prod_photo', 'prod_photo_colors', 'prod_warranty'),
    //     ),
    //     '2' => array(
    //         'title' => 'Главные характеристики',
    //         'fields_include' => array(
    //             'prod_option_1', 'prod_option_2', 'prod_option_3', 'prod_option_4',
    //         ),
    //     ),
    //     '3' => array(
    //         'title' => 'Фото галерея',
    //         'fields_include' => array('prod_gallery'),
    //     ),
    //     '4' => array(
    //         'title' => 'Технические характеристики',
    //         'fields_include' => array('prod_desc_img', 'prod_desc_text', 'prod_desc_table', 'prod_files'),
    //     ),
    //     '5' => array(
    //         'title' => 'Кривая света',
    //         'fields_include' => array('prod_light_img', 'prod_light_angle', 'prod_light_text'),
    //     ),
    //     '6' => array(
    //         'title' => 'Смотрите так же',
    //         'fields_include' => array('prod_more'),
    //     ),
    // ),

    #
    'view'      => [
        'mode'       => 'list',
        'fields'     => [
            'title', 'cost',
            'tech1_name', 'tech1',
            'tech2_name', 'tech2',
            'tech3_name', 'tech3',
            'tech4_name', 'tech4',
        ],
        //        'fields_edit' => ['cost'],
        'orderby'    => ' `id` DESC ',
        'edit_field' => 'title',
        'limit'      => 150,
    ],

    # labels for actions
    'labels'    => [
        'list'    => 'Товары',
        'add'     => 'Новый товар',
        'adding'  => 'Создание товара',
        'edit'    => 'Редактировать товар',
        'editing' => 'Редактирование товара',
        'delete'  => 'Удалить товар',
    ],
];
