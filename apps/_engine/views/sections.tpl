﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-article page_textpage">
                <div class="page-article__section">
                    <div class="page-article__content">
                        <article class="article">
                            {$page.content|default:''}

                        </article>
                    </div>
                </div>
                <div class="page-article__aside">
                    <div class="aside-nav">
                        {if !empty($sub_menu)}
                            {foreach item=menu from=$sub_menu}
                                <div class="aside-nav__elem {if $menu.path == $node.path} is-active {/if}">
                                    <a class="aside-nav__link" href="{$menu.path}">{$menu.title|stripcslashes}</a>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}