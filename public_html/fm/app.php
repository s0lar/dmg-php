<?php

//
define('APP_PATH', realpath(__DIR__));
define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
define('SHARED_PATH', realpath('../../apps/shared'));

//    includes
include APP_PATH.'/lib/uploader.class.php';
include APP_PATH.'/lib/helpers.php';
require SHARED_PATH.'/Q3i/Q.php';
require APP_PATH.'/lib/IM/im.php';

//     Get config
$config = require APP_PATH.'/cfg.php';
$dsn    = require APP_PATH.'/cfg.php';

// create connection to database using dsn from config
if (strstr($_SERVER['HTTP_HOST'], ".lo")) {
    QF($config['dsn_local'])->connect()->alias('default');
} else {
    QF($config['dsn'])->connect()->alias('default');
}

//$allowedExtensions = NULL;
/*
//    Exist
if( !file_exists( $default['upload_dir'] ) )
exit( json_encode(array( 'error' => 'Dir is not exist' )) );

//    Writeble
if( !is_writable( $default['upload_dir'] ) )
exit( json_encode(array( 'error' => 'Dir is not writeble' )) );
 */

//     Add files
if (isset($_GET['act']) && $_GET['act'] == 'add' && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && $_SERVER['REQUEST_METHOD'] == 'POST') {
    //     Get type of upload (file || image)
    $type = $_GET['type'];

    //    Check config type
    if (!isset($config[$type])) {
        exit(json_encode(array('error' => 'Wrong upload type')));
    }

    //     Get file mask
    $mask = isset($_GET['mask']) ? $_GET['mask'] : 0;
    //    Check config mask
    if (!isset($config[$type][$mask])) {
        exit(json_encode(array('error' => 'Wrong file mask')));
    }

    $file['name'] = $_GET['qqfile'];
    $file['ext']  = getExt($_GET['qqfile']);
    $file['key']  = $_GET['key'];

    //    Check file group key
    if ($file['key'] == '') {
        $file['key'] = md5(time().rand().'my solt fsaf9as7fda9sfdsafd');
    }

    //exit( json_encode( array('error' =>'Wrong key') ) );

    //    Check file ext
    if ($file['ext'] === false) {
        exit(json_encode(array('error' => 'Wrong file extenseion')));
    }

    //    Check allowed ext
    if (!checkAllowExt($file['ext'], $config[$type][$mask]['allow'])) {
        exit(json_encode(array('error' => 'File is not allowed')));
    }

    //    Check disallowed ext IF "allow" is empty
    if ($config[$type][$mask]['allow'] == '' && !checkDisallowExt($file['ext'], $config[$type][$mask]['disallow'])) {
        exit(json_encode(array('error' => 'File is disallowed')));
    }

    validate_install();

    //    Check allowed count of upload files
    if ($config[$type][$mask]['count'] > 0) {
        $c = Q("SELECT COUNT(id) as `count` FROM @@gs WHERE `key`=?s", array($file['key']))->row('count');
        if ($config[$type][$mask]['count'] <= $c) {
            exit(json_encode(array('error' => 'Exceeded limit of upload files')));
        }

    }

    //    upload file
    $uploader = new qqFileUploader();

    // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
    $result = $uploader->handleUpload(ROOT_PATH.'/'.$config['app']['storage_dir']);

    //    Get uploaded file info
    $file = array_merge($file, $uploader->getFileInfo());

    //    Get default file storege
    $file['path'] = '/'.$config['app']['storage_dir'];

    //    Get default storege prefix
    $file['path'] .= $config[$type][$mask]['prefix_dir'].'/';

    //    Generate path
    $file['path'] .= genPath();

    //    Insert record into database
    $file['id'] = Q('INSERT INTO @@gs(`name`, `ext`, `size`, `mime`, `created`, `key`, `path`) VALUES(?s, ?s, ?i, ?s, NOW(), ?s, ?s)', array(
        $file['name'],
        $file['ext'],
        $file['size'],
        get_mime($file['ext']),
        $file['key'],
        $file['path'],
    ));

    //    Encode short file name
    $file['name_id'] = base58_encode($file['id']);

    // mkdir for file
    if (!mkdir($concurrentDirectory = ROOT_PATH.$file['path'], 0777, true) && !is_dir($concurrentDirectory)) {
        throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
    }

    //
    if ($type == 'image') {
        $file['files'] = upload_image($file, $config[$type][$mask]);
    } elseif ($type == 'file') {
        $file['files'] = upload_file($file, $config[$type][$mask]);
    }

    //    Delete tmp file
    @unlink($file['md5_path'].$file['md5_name']);

    exit(json_encode(array(
        'success' => true,
        'type'    => $type,
        'key'     => $file['key'],
        'files'   => array(
            'id' => $file['id'],
            'n'  => $file['name'],
            's'  => formatSizeUnits($file['size']),
            'e'  => $file['ext'],
            't'  => '',
            'fs' => $file['files'],
        ),
    )));

} //     Delete files
elseif (isset($_GET['act']) && $_GET['act'] == 'del' && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    //     Get type of upload (file || image)
    $type = $_GET['type'];
    //    Check config type
    if (!isset($config[$type])) {
        exit(json_encode(array('error' => 'Delete. Wrong upload type')));
    }

    //     Get type of upload (file || image)
    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
    //    Check config type
    if ($id < 1) {
        exit(json_encode(array('error' => 'Delete. Wrong file id')));
    }

    $res = Q("SELECT * FROM @@gs WHERE id=?i", array($id));
    if ($res->numRows() < 1) {
        ;
    } //exit( json_encode( array('error' =>'Delete. Mysql record not found') ) );

    $r     = $res->row();
    $flist = glob(ROOT_PATH.$r['path'].base58_encode($id)."*");
    if (!empty($flist)) {
        foreach ($flist as $filename) {
            //echo "$filename size " . filesize($filename) . "\n";
            unlink($filename);
        }
    }

    Q("DELETE FROM @@gs WHERE id=?i", array($id));

    exit(json_encode(
        array(
            'success' => true,
            'id'      => $id,
        )
    ));
} else {
    exit(json_encode(array('error' => 'Direct access denied')));
}

///////////////////////////////////////////////////////////////////////////////////////////
//     HELPERS

function upload_file($file, $config)
{
    $dest = $file['path'].$file['name_id'].'.'.$file['ext'];
    copy($file['md5_path'].$file['md5_name'], ROOT_PATH.$dest);
    chmod(ROOT_PATH.$dest, 0644);

    return array($dest);
}

function upload_image($file, $config)
{
    $thubms = array();
    //    Create thumbs
    foreach ($config['thubms'] as $k => $v) {
        $thubms[$k] = $file['path'].$file['name_id']."_{$k}.".$file['ext'];

        copy($file['md5_path'].$file['md5_name'], ROOT_PATH.$thubms[$k]);
        chmod(ROOT_PATH.$thubms[$k], 0644);

        if (isset($v['method']) && $v['method'] == 'crop') {
            $res = im_crop(ROOT_PATH.$thubms[$k], $v['width'], $v['height'], false);
        } elseif (isset($v['method']) && $v['method'] == 'resize') {
            $res = im_resize(ROOT_PATH.$thubms[$k], $v['width'], $v['height'], false);
        } elseif (isset($v['method']) && $v['method'] == 'no_change') {
            ;
            //$res = im_resize(ROOT_PATH . $thubms[$k], $v['width'], $v['height'], false);
            //__($res);
        } elseif (isset($v['method'])) {
            exit(json_encode(array('error' => 'Wrong image method')));
        }

    }

    return $thubms;
}

function __($s, $exit = 0)
{
    echo "<pre>";
    print_r($s);
    echo "</pre>";
    if ($exit) {
        exit;
    }

}

function genPath()
{
    $a = md5(rand());

    return $a[0].'/'.$a[1].'/';
}

function checkAllowExt($ext, $allowed)
{
    if ($allowed == '') {
        return true;
    }

    $tmp = explode(',', $allowed);
    foreach ($tmp as $v) {
        if ($ext == trim($v)) {
            return true;
        }

    }

    return false;
}

function checkDisallowExt($ext, $disallowed)
{
    if ($disallowed == '') {
        return true;
    }

    $tmp = explode(',', $disallowed);
    foreach ($tmp as $v) {
        if ($ext == trim($v)) {
            return false;
        }

    }

    return true;
}

function getExt($file)
{
    $tmp = explode(".", $file);
    if (count($tmp) > 1) {
        return strtolower($tmp[count($tmp) - 1]);
    } else {
        false;
    }

}

function validate_install()
{
    Q('CREATE TABLE IF NOT EXISTS `@@gs` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `path` varchar(255) DEFAULT NULL,
	  `name` varchar(255) NOT NULL,
	  `ext` varchar(255) NOT NULL,
	  `size` int(11) NOT NULL,
	  `mime` varchar(255) NOT NULL,
	  `created` datetime NOT NULL,
	  `key` varchar(32) DEFAULT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;');
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 1).' Гб';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 1).' Мб';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 1).' Кб';
    } elseif ($bytes > 1) {
        $bytes = $bytes.' б';
    } elseif ($bytes == 1) {
        $bytes = $bytes.' б';
    } else {
        $bytes = '0 б';
    }

    return $bytes;
}
