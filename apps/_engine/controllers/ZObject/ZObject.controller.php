<?php

@session_start();
define('ZOBJECT_PATH', realpath(dirname(__FILE__)));
include_once SHARED_PATH . '/helpers.php';

class ZObject_Controller extends ZAny_Controller
{
    const ADD_MODE = 1;
    const EDIT_MODE = 2;

    const FIELD_STRING = 'string';
    const FIELD_TEXT = 'text';
    const FIELD_HTML = 'html';
    const FIELD_LIST = 'list';
    const FIELD_POINTER = 'pointer';
    const FIELD_MN = 'mn';
    const FIELD_MN2 = 'mn2';
    const FIELD_FLOAT = 'float';
    const FIELD_INT = 'int';
    const FIELD_DATE = 'date';
    const FIELD_TIME = 'time';
    const FIELD_DATETIME = 'datetime';
    const FIELD_CHECKBOX = 'checkbox';
    const FIELD_FILE = 'gs_file';

    private $sql_types = [
        'string'   => 'VARCHAR(512) NOT NULL',
        'text'     => 'TEXT NOT NULL',
        'code'     => 'LONGTEXT NOT NULL',
        'html'     => 'LONGTEXT NOT NULL',
        'list'     => 'VARCHAR(512) NOT NULL',
        'gs_file'  => 'TEXT NOT NULL',
        'pointer'  => 'VARCHAR(512) NOT NULL',
        'mn'       => 'VARCHAR(512) NOT NULL',
        'mn2'      => 'VARCHAR(512) NOT NULL',
        'float'    => 'FLOAT NOT NULL',
        'int'      => 'INT NOT NULL',
        'date'     => 'DATE NOT NULL',
        'time'     => 'TIME NOT NULL',
        'datetime' => 'DATETIME NOT NULL',
        'checkbox' => 'INT NOT NULL',
    ];

    public $config;
    public $object_type;

    /**
     * Init current object
     *
     * Detect object type and load configuration for it
     *
     * @param XRequest $request
     * @param array $node
     */
    public function __construct($node)
    {
        $this->before_construct();
        parent::__construct($node);

        // get object type
        $this->object_type = strtolower($this->node['object_type']);

        // load configuration for current object
        $this->config = import::config('app:configs/objects/' . $this->object_type . '.php');

        //    FrontEnd - load SEO
        if (BACKEND === false) {
            //
        } else {
            //    Zsettings modules
            $modules_list = import::config('app:configs/zsettings/__zsettings.php');
            RS("modules_list", $modules_list);
        }

        $this->after_construct();
    }

    //
    public function before_construct()
    {
    }

    //
    public function after_construct()
    {
    }

    /**
     * Prepare object type - cut all disallowed symbols
     *
     */
    public function _checkAuth()
    {
        //     User not authorized
        if (!(isset($_SESSION['user']['id']) && $_SESSION['user']['id'] > 0 && $_SESSION['user']['admin'] == 1)) {
            return false;
        }
        return true;
    }

    public function action_logout()
    {
        @session_start();
        unset($_SESSION['user']);
        unset($_SESSION['d_order']);
        unset($_SESSION['dealer_order']);
        $rq = RQ();
        header("Location: " . $rq->url->path);
        exit();
    }

    /*
    function action_info()
    {
    if(!$this->_checkAuth()) { redirect($this->node['path'].":auth"); }         //     AUTH
    V("zobject/info");
    }
     */

    /**
     * Load child nodes for current path
     *
     * @return XResponse
     */
    public function action_loadChildNodes()
    {
        $childs = [];

        $subobjects = $this->_getSubobjects();
        if (count($subobjects)) {
            foreach ($subobjects as $object => $count) {
                if ('list' !== ($view_mode = @import::config('app:configs/objects/' . $object . '.php')->view['mode'])) {
                    unset($subobjects[$object]);
                    continue;
                }

                $childs['list-' . mt_rand()] = [
                    'object'       => $object,
                    'object_title' => import::config('app:configs/objects/' . $object . '.php')->labels['list'],
                    'type'         => 'list',
                    'path'         => $this->node['path'],
                    'pid'          => $this->node['id'],
                ];

                $subobjects[$object] = $object;
            }
        }

        if (count($subobjects)) {
            $subobjects_count = Q(
                'SELECT object_type, COUNT(id) AS count
										FROM @@nodes
									WHERE
										parent_id = ?i
										AND object_type IN("' . implode('","', $subobjects) . '")
									GROUP BY object_type',
                [$this->node['id']]
            )->all('object_type');
            foreach ($childs as &$item) {
                $item['count'] = (int)@$subobjects_count[$item['object']]['count'];
            }
        }

        // get childs for current node
        $ids = [];
        $res = Q(
            'SELECT * FROM @@nodes WHERE parent_id = ?i' . (count($subobjects) ? ' AND object_type NOT IN("' . implode('","',
                    $subobjects) . '")' : '') . " ORDER BY `sort` DESC",
            [$this->node['id']]
        );

        while ($row = $res->each()) {
            $ids[] = $row['id'];
            $row['type'] = count($this->_getSubobjects($row['object_type'], $row['path'])) ? 'folder' : 'file';
            $row['object_title'] = stripcslashes($row['object_title']);
            $childs[$row['id']] = $row;
            //__($row);
        }

        // detect have_childs for each child node
        /*if (count($ids))
        {
        $res = Q('SELECT DISTINCT parent_id FROM @@nodes WHERE parent_id IN ('.implode(',', $ids).')');

        while ($row = $res->each())
        $childs[$row['parent_id']]['type'] = 'folder';
        }*/

        RS('items', $childs);
    }

    public function action_listObjects()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $objects_type = $this->_prepareObjectType($_GET['type']);

        if (false === ($config = import::config('app:configs/objects/' . $objects_type . '.php'))) {
            exit;
        }

        $fields = @$config->view['fields'];
        $objects_ids = Q('SELECT path, object_id FROM @@nodes WHERE parent_id = ?i AND object_type = ?s',
            [$this->node['id'], $objects_type])->all('object_id');

        $objects = [];
        if (count($objects_ids)) {
            $_ssql = Qb(
                'SELECT id, ' . (count($fields) ? '`' . implode('`, `',
                        $fields) . '`' : '*') . ' FROM @@' . $objects_type . ' WHERE id IN(' . implode(',',
                    array_keys($objects_ids)) . ')'
                . (isset($config->view['orderby']) ? ' ORDER BY ' . $config->view['orderby'] : '')
            );

            $res = Q($_ssql);
            while ($row = $res->each()) {
                $row['path'] = $objects_ids[$row['id']]['path'];
                $objects[] = $row;
            }
        }

        //
        $ext_data = [];
        foreach ($fields as $v) {
            //    Pointer
            if (is_array($config->fields[$v]) && isset($config->fields[$v]['type']) && $config->fields[$v]['type'] == 'pointer' && !isset($ext_data[$v])) {
                $ext_data[$v] = [];

                $config->fields[$v]['object']['fields'] = explode(',', $config->fields[$v]['object']['fields']);
                $id = $config->fields[$v]['object']['fields'][0];
                $title = $config->fields[$v]['object']['fields'][1];

                //
                foreach ($config->fields[$v]['object']['fields'] as &$f) {
                    $f = trim($f);
                    if ($f == '*') {
                        continue;
                    }

                    $f = '`' . $f . '`';
                }

                //$res = Q('SELECT id, ' . $config->fields[$v]['object']['fields'] . ' FROM @@' . $config->fields[$v]['object']['table'] . ' ');
                $res = Q('SELECT ' . implode(', ',
                        $config->fields[$v]['object']['fields']) . ' FROM @@' . $config->fields[$v]['object']['table']);

                while ($r = $res->each()) {
                    $ext_data[$v][$r[$id]] = $r[$title];
                }
            }
        }

        RS('ext_data', $ext_data);
        RS('config', $config);
        RS('items', $objects);

        // load allowed objects for creating as childs
        if (count($subobjects = $this->_getSubobjects())) {
            // load configuration of allowed objects
            foreach ($subobjects as $object => $count) {
                $subobjects[$object] = [
                    'labels' => import::config('app:configs/objects/' . $object . '.php')->labels,
                    'type'   => $object,
                ];
            }
        }
        RS('object', [
            'subobjects' => $subobjects,
            'labels'     => $this->config->labels,
        ]);

        V('zobject/list');
    }

    public function action_listObjects_post()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $objects_type = $this->_prepareObjectType($_GET['type']);

        if (false === ($config = import::config('app:configs/objects/' . $objects_type . '.php'))) {
            exit;
        }

        //  Получаем поля для измениня
        $fields = array_keys($_POST);

        //  Сохраняем значения
        foreach ($_POST[$fields[0]] as $id => $value) {
            //  Собираем поля в запрос
            $sql = "UPDATE `@@{$objects_type}` SET ";
            foreach ($fields as $field) {
                $sql .= " `{$field}` = '" . $_POST[$field][$id] . "',";
            }

            //  Удаляем последнюю запятую
            $sql = substr($sql, 0, -1);

            //  ID
            $sql .= " WHERE `id` = {$id}";

            //  Выполняем запрос
            $sql = Q($sql);
        }

        redirect(
            $this->node['path'] . ":list-objects/-type/{$objects_type}"
        );
    }

    /**
     * Build breadcrumbs for current node
     *
     * @return array
     */
    public function action_breadcrumbs()
    {
        //return Q('SELECT path, object_title FROM @@nodes WHERE id IN('.$this->node['idpath'].') ORDER BY id ASC')->all();
    }

    /**
     * Get allowed subobjects for current object
     *
     * @return array
     */
    protected function _getSubobjects($object_type = null, $path = null)
    {
        $object_type = is_null($object_type) ? $this->node['object_type'] : $object_type;
        $path = is_null($path) ? $this->node['path'] : $path;

        $subobjects = [];

        if (!($config = @import::config('app:configs/objects/__subobjects.php')->$object_type)) {
            return $subobjects;
        }

        foreach ($config as $path_re => $objects) {
            if (!preg_match($path_re, $path)) {
                continue;
            }

            foreach ($objects as $object => $count) {
                if (0 === $count) {
                    unset($subobjects[$object]);
                    continue;
                }

                $subobjects[$object] = $count;
            }
        }

        return $subobjects;
    }

    /**
     * Prepare ui fields
     *
     * @return array
     */
    protected function _prepareUI($data = [], $object_config = null)
    {
        if (!$object_config) {
            $object_config = $this->config;
        }

        foreach ($object_config->fields as $field => $config) {
            $type = $config;
            if (is_array($config)) {
                $type = $config['type'];
            }

            $handler_class = $type . '_handler';

            if (!import::from($handler_class) && !class_exists($handler_class)) {
                $handler_class = 'default_handler';
            }

            $handler = new $handler_class($field);
            $handler->prepare_view($data[$field], $config);

            // if( $config == 'int' ){
            //     __($handler_class);
            // }
        }
        return $data;
    }

    /**
     * Prepare data fields
     *
     * @param int $mode - ADD_MODE or EDIT_MODE
     * @return array
     */
    protected function _prepareValues($mode, $object_config = null)
    {
        $prepared = [];

        if (!$object_config) {
            $object_config = $this->config;
        }

        foreach ($object_config->fields as $field => $type) {
            if (ZObject_Controller::FIELD_STRING == $type || ZObject_Controller::FIELD_TEXT == $type) {
                $type = 's';
                $prepared['values'][$field] = $_POST[$field];
            } elseif (ZObject_Controller::FIELD_FLOAT == $type) {
                $type = 'f';
                $prepared['values'][$field] = $_POST[$field];
            } elseif (ZObject_Controller::FIELD_LIST == $type) {
                $field_config = $object_config->{'fields-configs'}[$field];
                if (!array_key_exists($_POST[$field], $field_config['items'])) {
                    $_POST[$field] = null;
                }

                //$prepared['fields'][$field] = '"'.$field.'" = ?'.$field_config['type'];
                $type = $field_config['type'];
                $prepared['values'][$field] = $_POST[$field];
            } elseif (ZObject_Controller::FIELD_POINTER == $type) {
                $field_config = $object_config->{'fields-configs'}[$field];

                $buf = Q('SELECT id FROM `' . $field_config['object']['table'] . '`')->all('id');

                if (!array_key_exists($_POST[$field], $buf)) {
                    $_POST[$field] = null;
                }

                $type = 'i';
                $prepared['values'][$field] = $_POST[$field];
            } elseif (ZObject_Controller::FIELD_MN == $type) {
                $field_config = $object_config->{'fields-configs'}[$field];

                $res = Q('SELECT id FROM `' . $field_config['object']['table'] . '`');
                $relations = [];

                while ($row = $res->each()) {
                    if (!isset($_POST[$field][$row['id']])) {
                        continue;
                    }

                    $relations[] = $row['id'];
                }

                $prepared['many-to-many'][$field] = [
                    'queries'  => [
                        'delete' => 'DELETE FROM `@@' . $field_config['relation']['table'] . '` WHERE `' . $field_config['relation']['key1'] . '` = ?i',
                        'insert' => 'INSERT INTO `@@' . $field_config['relation']['table'] . '` VALUES (?i, ?i)',
                    ],
                    'selected' => $relations,
                ];

                continue;
            } /*s0lar*/
            elseif (ZObject_Controller::FIELD_CHECKBOX == $type) {
                $type = 'i';
                $prepared['values'][$field] = isset($_POST[$field]) ? '1' : '0';
            } elseif (ZObject_Controller::FIELD_INT == $type) {
                $type = 'i';
                $prepared['values'][$field] = isset($_POST[$field]) ? intval($_POST[$field]) : 0;
            } elseif (ZObject_Controller::FIELD_FILE == $type) {
                $type = 's';
                $prepared['values'][$field] = $_POST[$field];
            } /* s0lar
             * MN2
             *  */
            elseif (is_array($type) && ZObject_Controller::FIELD_MN2 == @$type['type']) {
                //__($_POST[$field]);
                //exit("123");
                $tmp = '';
                if (!empty($_POST[$field])) {
                    $tmp = '|';
                    foreach ($_POST[$field] as $_kk => $_vv) {
                        $tmp .= $_kk . '|';
                    }
                }

                $type = 's';
                $prepared['values'][$field] = $tmp;
            } else {
                $type = 's';
                $prepared['values'][$field] = $_POST[$field];
            }

            $prepared['fields'][$field] = '`' . $field . '` = ?' . $type;
        }

        return $prepared;
    }

    /**
     * Update many-to-many relations for object
     *
     * @param int $object_id
     * @param array $relations
     * @return void
     */
    protected function _updateManyToManyRelations($object_id, $relations)
    {
        // update many-to-many
        foreach ($relations as $field => $params) {
            // remove old relations
            Q($params['queries']['delete'], [$object_id]);

            // prepare new relations
            $buf = [];
            foreach ($params['selected'] as $v) {
                $buf[] = [$object_id, $v];
            }

            // insert new relations
            Q($params['queries']['insert'], $buf);
        }
    }

    /**
     * Prepare object type - cut all disallowed symbols
     *
     * @param string $type
     * @return string
     */
    protected function _prepareObjectType($type)
    {
        $type = strtolower($type);
        return preg_replace('/[^\w\d\-_]/', '', $type);
    }

    public function before_add()
    {
    }

    /**
     * Add form for new object
     *
     * @return XResponse
     */
    public function action_add()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        if (!$this->userCheckAccess()) {
            redirect($this->node['path'] . ":edit/-err/access");
        }

        $this->before_add();

        $object_type = $this->_prepareObjectType($_GET['type']);

        // exit if configuration for new object not exists
        if (false === ($object_config = import::config('app:configs/objects/' . $object_type . '.php'))) {
            exit;
        }

        RS('object', [
            'ui'        => $object_config->ui,
            'fields'    => $object_config->fields,
            'node'      => $object_config->node,
            'labels'    => $object_config->labels,
            'data'      => $this->_prepareUI([], $object_config),
            //'data' => $this->_prepareUI(),
            'input_cfg' => isset($object_config->input_cfg) ? $object_config->input_cfg : false,
            'subtabs'   => isset($object_config->subtabs) ? $object_config->subtabs : false,
        ]);

        $this->after_add();

        V('zobject/add');
    }

    public function after_add()
    {
    }

    public function before_add_post()
    {
    }

    /**
     * Add new object to current path
     *
     * @return XResponse
     */
    public function action_add_post()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        if (!$this->userCheckAccess()) {
            redirect($this->node['path'] . ":edit/-err/access");
        }

        $this->before_add_post();

        $node = [];
        $node['object_type'] = strtolower($_GET['type']);

        // exit if configuration for new object not exists
        if (false === ($object_config = import::config('app:configs/objects/' . $node['object_type'] . '.php'))) {
            exit;
        }

        $prepared = $this->_prepareValues(ZObject_Controller::ADD_MODE, $object_config);

        // add object by add_function and build params for inserting query in to nodes
        //$sql = Qb('INSERT INTO `@@'.$node['object_type'].'` SET '.implode(', ', $prepared['fields']), $prepared['values']);
        //__( $sql );
        //exit;
        $node['object_id'] = Q('INSERT INTO `@@' . $node['object_type'] . '` SET ' . implode(', ', $prepared['fields']),
            $prepared['values']);

        $node['parent_id'] = $this->node['id'];
        $node['object_title'] = $_POST[$object_config->node['object_title']];

        // update relations
        if (isset($prepared['many-to-many'])) {
            $this->_updateManyToManyRelations($node['object_id'], $prepared['many-to-many']);
        }

        // build node name
        if ($object_config->node['name'] == '-user') {
            $node['name'] = translit($_POST['name']); // if name defined by user

            if (empty($node['name'])) {
                if (isset($_POST['title']) && !empty($_POST['title'])) {
                    $node['name'] = translit($_POST['title']);
                } else {
                    $node['name'] = $node['object_type'] . '-' . $node['object_id'];
                }
            }

            if (empty($node['name'])) {
                exit("'Системное имя' - не может быть пустым! Заполните это поле");
            }
        } elseif (isset($object_config->fields[$object_config->node['name']])) {
            $node['name'] = $_POST[$object_config->node['name']];
        } // if name defined by user throughout object field

        else { // else name generated as "object_type-object_id"
            $node['name'] = $node['object_type'] . '-' . $node['object_id'];
        }

        //$node['name'] = $node['object_id'];

        $node['path'] = $this->node['path'] . $node['name'] . '/';

        if (empty($node['object_title'])) {
            exit("Не все поля заполнены!");
        }

        // insert new node
        $node['id'] = Q('INSERT INTO @@nodes(parent_id, name, path, object_type, object_id, object_title, created, updated)
							VALUES(?i, ?s, ?s, ?s, ?i, ?s, NOW(), NOW()) ', [
            $node['parent_id'],
            $node['name'], $node['path'],
            $node['object_type'], $node['object_id'], $node['object_title'],
        ]);

        $url = RQ()->url;
        //__(("Location: " . $url->path . "/:list-objects/-type/".$node['object_type']."/" ));__( $object_config );__( $node );__($_POST);exit;

        $this->gen_sitemap();

        $this->after_add_post($node);

        if (isset($_POST['save'])) {
            header("Location: " . $node['path'] . ":edit");
        } elseif (isset($object_config->view['mode']) && $object_config->view['mode'] == 'list') {
            header("Location: " . $url->path . ":list-objects/-type/" . $node['object_type'] . "/");
        } else {
            //RS('result', 'success');
            //RS('path', $node['path']);
            header("Location: " . $url->path . "-msg/added/" . ":edit");
        }
        exit;
    }

    public function after_add_post($node = [])
    {
    }

    public function before_edit()
    {
    }

    /**
     * Show edit form for current object
     *
     * @return XResponse
     */
    public function action_edit()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        if (!$this->userCheckAccess()) {
            RS("error_access", 1);
        }

        $this->before_edit();

        // load data of current object
        $data = Q('SELECT * FROM `@@' . $this->object_type . '` WHERE id = ' . $this->node['object_id'])->row();

        foreach ($data as $k => $v) {
            $data[$k] = stripslashes($data[$k]);
        }

        // prepare values
        $data = $this->_prepareUI($data);

        // load allowed objects for creating as childs
        if (count($subobjects = $this->_getSubobjects())) {
            // load configuration of allowed objects
            foreach ($subobjects as $object => $count) {
                $subobjects[$object] = [
                    'labels' => import::config('app:configs/objects/' . $object . '.php')->labels,
                    'type'   => $object,
                ];
            }
        }

        $this->after_edit();

        RS('object', [
            'ui'         => $this->config->ui,
            'fields'     => $this->config->fields,
            'node'       => $this->config->node,
            'data'       => $data,
            'subobjects' => $subobjects,
            'labels'     => $this->config->labels,
            'view'       => isset($this->config->view['mode']) ? $this->config->view['mode'] : '',
            'input_cfg'  => isset($this->config->input_cfg) ? $this->config->input_cfg : false,
            'subtabs'    => isset($this->config->subtabs) ? $this->config->subtabs : false,
            'actions'    => $this->config->actions ?? false,
        ]);
        V('zobject/edit');
    }

    public function after_edit()
    {
    }

    public function before_edit_post()
    {
    }

    /**
     * Edit current object
     *
     * @return XResponse
     */
    public function action_edit_post()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        if (!$this->userCheckAccess()) {
            redirect($this->node['path'] . ":edit/-err/access");
        }

        $this->before_edit_post();

        $fields = [];
        $update = [];

        // prepare values
        $prepared = $this->_prepareValues(ZObject_Controller::EDIT_MODE);
        $title = $prepared['values'][$this->config->node['object_title']];

        // update data
        Q('UPDATE `@@' . $this->object_type . '` SET ' . implode(', ',
                $prepared['fields']) . ' WHERE id = ' . $this->node['object_id'], $prepared['values']);

        // update many-to-many
        if (isset($prepared['many-to-many'])) {
            $this->_updateManyToManyRelations($this->node['object_id'], $prepared['many-to-many']);
        }

        // update node title
        Q('UPDATE @@nodes SET object_title = ?s, updated=NOW() WHERE id = ' . $this->node['id'], [$title]);

        RS('result', 'success');
        RS('id', $this->node['id']);
        RS('title', $title);
        $url = RQ()->url;
        $this->gen_sitemap();
        //
        $this->after_edit_post($this->node);

        if (isset($_POST['goparent'])) {
            array_pop($url->segments);
            //header("Location: /" . implode('/', $url->segments) . "/-msg/saved/" . ":" . $url->action );
            //__("Location: /" . implode('/', $url->segments) . "/:list-objects/-type/".$this->node['object_type']."/" );exit;
            header("Location: /" . implode('/',
                    $url->segments) . "/:list-objects/-type/" . $this->node['object_type'] . "/");
            //header("Location: " . $url->path . ":list-objects/-type/".$node['object_type']."/" );
        } else {
            header("Location: " . $url->path . "-msg/saved/" . ":" . $url->action);
        }
        exit;
    }

    public function after_edit_post($node = [])
    {
    }

    public function before_delete()
    {
    }

    /**
     * Delete current object
     */
    /**
     * Delete current object
     */
    public function action_delete()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        if (!$this->userCheckAccess()) {
            redirect($this->node['path'] . ":edit/-err/access");
        }

        $this->before_delete();

        //    Check lock path
        if (isset($this->node['lock']) && $this->node['lock'] == 1) {
            //    If unlock key isset then force delete
            if (isset($_GET['unlock']) && $_GET['unlock'] == 1) {
                $res = Q('SELECT id, object_type, object_id FROM @@nodes WHERE `path` LIKE \'' . $this->node['path'] . '%\'');
            } //    Redirect
            else {
                header("Location: " . $this->node['path'] . './:edit/?locked=1');
                exit();
            }
        } // load all childs
        else {
            $res = Q('SELECT id, object_type, object_id FROM @@nodes WHERE `lock`=0 AND `path` LIKE \'' . $this->node['path'] . '%\'');
        }

        $objects = [];
        $nodes = [];
        while ($row = $res->each()) {
            $objects[$row['object_type']][] = $row['object_id'];
            $nodes[] = $row['id'];
        }

        if (!empty($objects)) {
            foreach ($objects as $object => $ids) {
                Q('DELETE FROM `@@' . $object . '` WHERE id IN(' . implode(',', $ids) . ')');
            }
        }

        if (!empty($nodes)) {
            Q('DELETE FROM `@@nodes` WHERE id IN(' . implode(',', $nodes) . ')');
        }

        $this->after_delete();

        if (isset($this->config->view['mode']) && $this->config->view['mode'] == 'list') {
            $url = RQ()->url;
            array_pop($url->segments);
            header("Location: /" . implode('/',
                    $url->segments) . "/:list-objects/-type/" . $this->node['object_type'] . "/");
        } else {
            header("Location: " . $this->node['path'] . '../:edit/');
        }
        exit();
    }

    public function after_delete()
    {
    }

    /*s0lar*/
    /**
     * Shows configuration of object
     *
     * @return XResponse
     */
    public function action_cfg()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        //$object_type = $this->node['object_type'];
        $object_type = strtolower($_GET['type']);
        // exit if configuration for new object not exists

        $object_config = import::config('app:configs/zsettings/' . $object_type . '.php');

        if (false === $object_config) {
            $object_config = import::config('app:configs/objects/' . $object_type . '.php');
        }

        if (false === $object_config) {
            exit("ZObject config not found");
        }

        try {
            $res = Q("SELECT * FROM @@{$object_type} WHERE 1 LIMIT 1");
        } catch (Exception $e) {
            //    Table does not exist
            if ($e->getCode() == 1146) {
                RS('error', '1146');
            }
        }
        RS('object', [
            'ui'     => $object_config->ui,
            'fields' => $object_config->fields,
            'node'   => $object_config->node,
            'labels' => $object_config->labels,
            //'data'         => $this->_prepareUI(),
            'table'  => $object_type,
        ]);
        V('zobject/cfg');
    }

    /**
     * Shows configuration of object
     *
     * @return XResponse
     */
    public function action_createtable()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $object_type = strtolower($_GET['type']);
        $object_config = import::config('app:configs/zsettings/' . $object_type . '.php');
        if (false === $object_config) {
            $object_config = import::config('app:configs/objects/' . $object_type . '.php');
        }

        if (false === $object_config) {
            exit("ZObject config not found");
        }

        //    Create table
        $sql = "";
        $sql .= "CREATE TABLE IF NOT EXISTS `@@{$object_type}` ( \n ";
        $sql .= "`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ";
        foreach ($object_config->fields as $k => $v) {
            if (is_array($v) && isset($v['type'])) {
                $sql .= ", \n`{$k}` " . $this->sql_types[$v['type']] . "";
            } else {
                $sql .= ", \n`{$k}` " . $this->sql_types[$v] . "";
            }
        }

        $sql .= ") \nENGINE = MyISAM DEFAULT CHARSET=utf8 ;";
        Q($sql);

        header("Location: ../../../:cfg/-type/" . $_GET['type']);
        exit;
    }

    /**
     * Edit node
     *
     */
    public function action_editNode_post()
    {
        foreach ($_POST['node'] as $k => $v) {
            Q("UPDATE @@nodes SET `sort`=?i WHERE id=?i", [$_POST['sort'][$k], $k]);
        }
        header("Location: " . $this->node['path'] . ":edit_node");
        exit;
    }

    /**
     * Edit node POST
     *
     */
    public function action_editNode()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        //__($this->node);
        $arr[] = $this->node;

        $res = Q("SELECT * FROM @@nodes WHERE parent_id=?i ORDER BY `sort`", [$this->node['id']]);
        while ($r = $res->each()) {
            $arr[] = $r;
        }

        RS("nodes", $arr);

        // load allowed objects for creating as childs
        if (count($subobjects = $this->_getSubobjects())) {
            // load configuration of allowed objects
            foreach ($subobjects as $object => $count) {
                $subobjects[$object] = [
                    'labels' => import::config('app:configs/objects/' . $object . '.php')->labels,
                    'type'   => $object,
                ];
            }
        }
        RS('object', [
            'subobjects' => $subobjects,
            'labels'     => $this->config->labels,
        ]);

        V('zobject/nodes_edit');
    }

    /**
     * Edit system POST
     * edit_system_post
     */
    public function action_editSystem_post()
    {
        //    We can't change root node
        if ($this->node['path'] == '/') {
            header("Location: " . $this->node['path'] . ":edit_system/-err/root");
            exit;
        }

        //    System name
        $name = _post('system_name');

        //    Is empty system name?
        if ($name === false || empty($name)) {
            header("Location: " . $this->node['path'] . ":edit_system/-err/empty_system_name");
            exit;
        }

        //    Only allowed symbols
        $tran = translit($name);
        if ($tran != $name) {
            header("Location: " . $this->node['path'] . ":edit_system/-err/not_allowed_symbols");
            exit;
        }

        //    Get new path
        $old_path = $this->node['path'];
        $new_path = explode('/', $this->node['path']);
        $new_path[count($new_path) - 2] = $_POST['system_name'];
        $new_path = implode('/', $new_path);

        //    If path not changed
        if ($old_path == $new_path) {
            header("Location: " . $this->node['path'] . ":edit_system/-err/path_not_changed");
            exit;
        }

        //    Is new path exist?
        $res = Q('SELECT 1 FROM `@@nodes` WHERE `path`=?s', [$new_path]);
        if ($res->numRows() > 0) {
            header("Location: " . $this->node['path'] . ":edit_system/-err/path_already_exists");
            exit;
        }

        //    Update name in nodes
        Q('UPDATE `@@nodes` SET `name`=?s WHERE `id`=?i', [$name, $this->node['id']]);

        //    Change path and all parents
        //Q('UPDATE `@@nodes` SET `path` = REPLACE(`path`, ?s, ?s) WHERE `path` LIKE (?s)', array( $old_path, $new_path, $old_path . "%" ));
        $res = Q('SELECT `id`, `path` FROM `@@nodes` WHERE `path` LIKE ?s', [$old_path . "%"]);
        $arr = [];
        while ($r = $res->each()) {
            Q(
                'UPDATE `@@nodes` SET `path` = ?s WHERE `id`=?i',
                [
                    preg_replace('#^' . $old_path . '*#', $new_path, $r['path']),
                    $r['id'],
                ]
            );
        }

        //    Redirect
        header("Location: " . $new_path . ":edit_system/-msg/updates");
        exit;
    }

    /**
     * Edit node
     * edit_system
     */
    public function action_editSystem()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $subobject = @import::config('app:configs/objects/__subobjects.php');

        //    Get system name
        $system_path = explode('/', $this->node['path']);
        foreach ($system_path as $k => $v) {
            if (empty($v)) {
                unset($system_path[$k]);
            }
        }
        $system_path = array_pop($system_path);
        RS('system_name', $system_path);
        RS('node', $this->node);

        RS('object', [
            'labels' => $this->config->labels,
        ]);

        V('zobject/edit_system');
    }

    /**
     * Move node POST
     * edit_movenode_post
     */
    public function action_movenode_post()
    {
        $subobject = @import::config('app:configs/objects/__subobjects.php');

        if ($_POST['user_nodes'] && !empty($_POST['user_nodes'])) {
            $id = $_POST['user_nodes'];
            $id = array_keys($id);
            $id = $id[0];

            //
            //__($id);__($this->node);

            $res = Q('SELECT `id`,`path` FROM `@@nodes` WHERE `id`=?i ', [$id])->row();

            //__( $res );

            //    Update name in nodes
            $old_path = $this->node['path'];
            $new_path = $res['path'] . $this->node['name'] . '/';

            //__($new_path);exit;
            Q('UPDATE `@@nodes` SET `parent_id`=?i, `path`=?s WHERE `id`=?i',
                [$res['id'], $new_path, $this->node['id']]);
            //__($sql);exit;

            //    Change path and all parents
            //Q('UPDATE `@@nodes` SET `path` = REPLACE(`path`, ?s, ?s) WHERE `path` LIKE (?s)', array( $old_path, $new_path, $old_path . "%" ));
            $res = Q('SELECT `id`, `path` FROM `@@nodes` WHERE `path` LIKE ?s', [$old_path . "%"]);
            $arr = [];
            while ($r = $res->each()) {
                Q(
                    'UPDATE `@@nodes` SET `path` = ?s WHERE `id`=?i',
                    [
                        preg_replace('#^' . $old_path . '*#', $new_path, $r['path']),
                        $r['id'],
                    ]
                );
                //__($sql);
            }
        }

        //    Redirect
        header("Location: " . $new_path . ":edit_system/-msg/movenode");
        exit;
    }

    /*
     * seo actions
     *
     * */

    public function action_seoOn()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        if (isset($_SESSION['seo_mode']) && $_SESSION['seo_mode'] == 1) {
            $_SESSION['seo_mode'] = 0;
        } else {
            $_SESSION['seo_mode'] = 1;
        }

        header("Location: ./");
        exit;
    }

    //    Seo
    public function action_seoEdit()
    {
        $seo = Q('SELECT * FROM @@seo WHERE node_id=?i', [$this->node['id']])->row();

        RS('node_id', $this->node['id']);
        RS('seo', $seo);

        // load allowed objects for creating as childs
        if (count($subobjects = $this->_getSubobjects())) {
            // load configuration of allowed objects
            foreach ($subobjects as $object => $count) {
                $subobjects[$object] = [
                    'labels' => import::config('app:configs/objects/' . $object . '.php')->labels,
                    'type'   => $object,
                ];
            }
        }
        RS('object', [
            'subobjects' => $subobjects,
            'labels'     => $this->config->labels,
        ]);

        V('zobject/seo/seo_edit');
    }

    //    Seo
    public function action_seoSave()
    {
        Q(
            'INSERT INTO `@@seo` (`node_id`,`title`,`h1`,`key`,`desc`,`text`,`created`) VALUES(?i, ?s, ?s, ?s, ?s, ?s, NOW() )
			ON DUPLICATE KEY UPDATE
				`title`=VALUES(`title`),
				`h1`=VALUES(`h1`),
				`key`=VALUES(`key`),
				`desc`=VALUES(`desc`),
				`text`=VALUES(`text`),
				`updated`=NOW()',
            //
            [$_POST['node_id'], $_POST['title'], $_POST['h1'], $_POST['key'], $_POST['desc'], $_POST['text']]
        );

        redirect(RQ()->url->path . ':seo_edit/-msg/saved');
    }

    public function action_seoAdd_post()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $p = [];
        foreach ($_POST as $k => $v) {
            $p[$k] = strip_tags($v, '<p><a><b><strong><i><em><ul><li><ol><h1><h2><h3><h4><h5><h6><span>');
        }

        Q("INSERT INTO `@@seo` SET `path`=?s, `name`=?s, `title`=?s, `h1`=?s, `key`=?s, `desc`=?s, `text`=?s, `text2`=?s ",
            [_post('path'), @$p['name'], @$p['title'], @$p['h1'], @$p['key'], @$p['desc'], @$p['text'], @$p['text2']]);

        header("Location: ./");
        exit;
    }

    public function action_seoEdit_post()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $p = [];
        foreach ($_POST as $k => $v) {
            $p[$k] = strip_tags($v, '<p><a><b><strong><i><em><ul><li><ol><h1><h2><h3><h4><h5><h6><span>');
        }

        Q("UPDATE `@@seo` SET `name`=?s, `title`=?s, `h1`=?s, `key`=?s, `desc`=?s, `text`=?s, `text2`=?s WHERE `id`=?i LIMIT 1",
            [@$p['name'], @$p['title'], @$p['h1'], @$p['key'], @$p['desc'], @$p['text'], @$p['text2'], $_POST['id']]);

        header("Location: ./");
        exit;
    }

    //
    public function action_settings()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $group = isset($_GET['group']) ? intval($_GET['group']) : 0;
        $config = import::config('app:configs/settings.php');

        //__($config);

        $res = Q("SELECT * FROM @@settings ORDER BY `group`, `sort`");
        $set = [];

        while ($r = $res->each()) {
            $set[$r['group']][] = $r;
        }

        RS("set_type", $config->type);
        RS("group", $group);
        RS("settings", $set);
        V("zobject/settings");
    }

    public function action_settings_post()
    {
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        } //     AUTH

        $group = isset($_GET['group']) ? intval($_GET['group']) : 0;

        foreach ($_POST['var'] as $id => $v) {
            //Q("UPDATE @@settings SET `var`=?s, `type`=?s, `group`=?s, `sort`=?s, `value`=?s WHERE id=?i", array( $_POST['var'][$id], $_POST['type'][$id], $_POST['group'][$id], $_POST['sort'][$id], $_POST['value'][$id], $id ));
            Q("UPDATE @@settings SET `var`=?s, `type`=?s, `sort`=?s, `value`=?s WHERE id=?i",
                [$_POST['var'][$id], $_POST['type'][$id], $_POST['sort'][$id], $_POST['value'][$id], $id]);
        }
        header("Location: /:settings/-group/{$group}/");
        exit;
    }

    //
    public function check_settings()
    {
    }

    public function gen_sitemap()
    {
        require SHARED_PATH . '/sitemap.php';

        $sitemap = new Sitemap();
        //$res = Q("SELECT * FROM @@nodes WHERE `path` NOT LIKE '/orders/%' AND `path` NOT LIKE '/bin/%'  ");
        $res = Q('SELECT `path`, `updated`, `sitemap_changefreq`, `sitemap_priority` FROM @@nodes WHERE `sitemap_enable`=1 ORDER BY `path` ');
        while ($r = $res->each()) {
            if ($r['path'] == '/import/') {
                continue;
            }

            $sitemap->addItem(new SitemapItem(
                'http://' . $_SERVER['HTTP_HOST'] . $r['path'],
                strtotime($r['updated']),
                $r['sitemap_changefreq'],
                $r['sitemap_priority']
            ));
        }
        $sitemap->generate('sitemap.xml');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    XML
    public function action_sitemapxml()
    {
        $pg = PG('SELECT 1 FROM `@@nodes`', 250);
        RS('pg', $pg);

        $res = Q('SELECT `id`, `object_title` as `title`, `path`, `updated`, `sitemap_changefreq`, `sitemap_priority`, `sitemap_enable`
                FROM `@@nodes` ORDER BY `path`  LIMIT ?i, ?i', [$pg['offset'], $pg['limit']]);

        RS('_list', $res->all());
        V("zobject/sitemapxml");
    }

    public function action_sitemapxml_post()
    {
        //Q("UPDATE `iva__nodes` SET `sitemap_changefreq`='monthly' WHERE 1");exit;

        foreach ($_POST['sitemap_changefreq'] as $key => $value) {
            $sql = Qb(
                'UPDATE `@@nodes` SET `sitemap_changefreq`=?s, `sitemap_priority`=?s, `sitemap_enable`=?s WHERE `id`=?i',
                [
                    $_POST['sitemap_changefreq'][$key],
                    $_POST['sitemap_priority'][$key],
                    isset($_POST['sitemap_enable'][$key]) ? 1 : 0,
                    $key,
                ]
            );
            Q($sql);
        }

        $this->gen_sitemap();

        redirect(RQ()->url->raw);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Users section

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Auth action
    public function action_auth()
    {
        V("zobject/auth");
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Login action
    public function action_auth_post()
    {
        $url = RQ()->url;

        if (!isset($_POST['login'])) {
            redirect("{$url->path}:auth?err=notset_login");
        }

        if (empty($_POST['login'])) {
            redirect("{$url->path}:auth?err=empty_login");
        }

        if (!isset($_POST['pass'])) {
            redirect("{$url->path}:auth?err=notset_pass");
        }

        if (empty($_POST['pass'])) {
            redirect("{$url->path}:auth?err=empty_pass");
        }

        Users::login($_POST['login'], $_POST['pass']);
        redirect("{$url->path}:edit");
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Edit check access
    public function userCheckAccess($type = 'nodes')
    {
        $uid = $_SESSION['user']['id'];

        $access = Q("SELECT `access` FROM @@users WHERE `id`=?i", [$uid])->row('access');
        if (empty($access)) {
            return false;
        }

        $access = unserialize($access);

        //$rq = RQ();

        if (isset($access['root']) && $access['root'] == 1) {
            return true;
        }

        if ($type == 'users') {
            if (isset($access['users']) && $access['users'] == 1) {
                return true;
            } else {
                return false;
            }
        } elseif ($type == 'zset') {
            if (empty($access['zset'])) {
                return false;
            }

            if (isset($access['zset'][_get("m")])) {
                return true;
            } else {
                return false;
            }
        } elseif ($type == 'zcfg') {
            if (isset($access['zcfg']) && $access['zcfg'] == 1) {
                return true;
            } else {
                return false;
            }
        } elseif ($type == 'zcfg') {
            exit("zcfg");
            if (empty($access['zset'])) {
                return false;
            }

            if (isset($access['zset'][_get("m")])) {
                return true;
            } else {
                return false;
            }
        } elseif ($type == 'nodes') {
            if ($access['nodes']['nodes'] == 0 && $access['nodes']['lists'] == 0) {
                return false;
            }

            $nid = $this->node['id'];
            $pid = $this->node['parent_id'];

            if (isset($access['nodes']['nodes'][$nid])) {
                return true;
            } elseif (isset($access['nodes']['lists'][$pid]) && $access['nodes']['lists'][$pid] == $this->node['object_type']) {
                return true;
            } else {
                return false;
            }
        }

        // __($rq);
        // __($this->node);

        // __($access);
        // __($uid);
        // __($_SESSION);
        // exit;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Add user action
    public function userAccessArr()
    {
        $arr = [
            'root'  => 1,
            'nodes' => [],
            'users' => 0,
            'zset'  => [],
            'zcfg'  => 0,
        ];
        return $arr;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Add user action
    public function action_userAdd()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        if (!$this->userCheckAccess("users")) {
            redirect("/:user_list/-err/access");
        }

        V("zobject/user/add");
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Add user action post
    public function action_userAdd_post()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        if (!$this->userCheckAccess("users")) {
            redirect("/:user_list/-err/access");
        }

        if (empty($_POST['login'])) {
            redirect("/:user_add/-err/empty_login");
        }

        $u = Users::getUser($_POST['login']);
        if (!empty($u)) {
            redirect("/:user_add/-err/login_exist");
        }

        if (empty($_POST['pass'])) {
            redirect("/:user_add/-err/empty_pass");
        }

        if ($_POST['pass'] != $_POST['pass2']) {
            redirect("/:user_add/-err/pass2");
        }

        $admin = 1;

        $u = new Users();
        $user = $u->addUser($_POST['login'], $_POST['pass'], $admin);

        if (isset($_POST['dealer'])) {
            Q("UPDATE @@users SET dealer=1 WHERE id=?i", [$user['id']]);
        }

        //    Update access
        $access = $this->userAccessArr();
        $access['root'] = isset($_POST['root']) ? 1 : 0;
        $access['zcfg'] = isset($_POST['zcfg']) ? 1 : 0;
        $access['zset'] = isset($_POST['zset']) ? $_POST['zset'] : 0;
        $access['users'] = isset($_POST['users']) ? 1 : 0;
        $access['nodes']['nodes'] = isset($_POST['user_nodes']) ? $_POST['user_nodes'] : 0;
        $access['nodes']['lists'] = isset($_POST['user_nodes_list']) ? $_POST['user_nodes_list'] : 0;
        $access = serialize($access);
        Q("UPDATE @@users SET access=?s WHERE id=?i", [$access, $user['id']]);

        //     Send mail
        include_once SHARED_PATH . '/helpers.php';
        include_once SHARED_PATH . '/mmail.class.php';

        $msg = "<p>Добавлен новый пользователь<br />";
        $msg .= "User: " . $_POST['login'] . "<br />";
        $msg .= "Pass: " . $_POST['pass'] . "<br />";
        $msg .= "</p>";
        $m = MMM(
            "Новый пользователь на сайте " . $_SERVER['HTTP_HOST'],
            //array('pavel.sidorov@gmail.com'),
            ['pavel.sidorov@gmail.com'],
            'BonoCMS:Users <noreply@' . $_SERVER['HTTP_HOST'] . '>',
            iconv('utf-8', 'windows-1251', $msg)
        );

        if (isset($_POST['save'])) {
            redirect("/:user_edit/-id/" . $user['id'] . "/");
        } else {
            redirect("/:user_list");
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Edit user action
    public function action_userEdit()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        if (!$this->userCheckAccess("users")) {
            redirect("/:user_list/-err/access");
        }

        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        if (!$id) {
            redirect("/:user_list/-err/wrong_id");
        }

        $u = Q("SELECT id, login, admin, dealer, access FROM @@users WHERE id=?i", [$_GET['id']])->row();
        $u['ac'] = unserialize($u['access']);
        RS("u", $u);
        V("zobject/user/edit");
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Edit user action post
    public function action_userEdit_post()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        if (!$this->userCheckAccess("users")) {
            redirect("/:user_list/-err/access");
        }

        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
        if (!$id) {
            redirect("/:user_list/-err/wrong_id");
        }

        if (empty($_POST['login'])) {
            redirect("/:user_edit/-id/{$id}/-err/empty_login/");
        }

        $u = Users::getUser($_POST['login']);

        if (!empty($u) && $id != $u['id']) {
            $_SESSION['wrong_login'] = $_POST['login'];
            redirect("/:user_edit/-id/{$id}/-err/login_exist");
        }

        if (!empty($_POST['pass']) && $_POST['pass'] != $_POST['pass2']) {
            redirect("/:user_edit/-id/{$id}/-err/pass2");
        }

        $admin = 1;

        $u = new Users();
        $u->editUser($id, $_POST['login'], $_POST['pass'], $admin);

        //    Update access
        $access = $this->userAccessArr();
        $access['root'] = isset($_POST['root']) ? 1 : 0;
        $access['zcfg'] = isset($_POST['zcfg']) ? 1 : 0;
        $access['zset'] = isset($_POST['zset']) ? $_POST['zset'] : 0;
        $access['users'] = isset($_POST['users']) ? 1 : 0;
        $access['nodes']['nodes'] = isset($_POST['user_nodes']) ? $_POST['user_nodes'] : 0;
        $access['nodes']['lists'] = isset($_POST['user_nodes_list']) ? $_POST['user_nodes_list'] : 0;
        $access = serialize($access);
        Q("UPDATE @@users SET access=?s WHERE id=?i", [$access, $id]);

        //     Send mail
        include_once SHARED_PATH . '/helpers.php';
        include_once SHARED_PATH . '/mmail.class.php';

        $msg = "<p>Изменен пользователь<br />";
        $msg .= "User: " . $_POST['login'] . "<br />";
        if (!empty($_POST['pass'])) {
            $msg .= "Pass: " . $_POST['pass'] . "<br />";
        }

        $msg .= "</p>";
        $m = MMM(
            "Новый пароль на сайте " . $_SERVER['HTTP_HOST'],
            //array('pavel.sidorov@gmail.com'),
            ['pavel.sidorov@gmail.com'],
            'BonoCMS:Users <noreply@' . $_SERVER['HTTP_HOST'] . '>',
            iconv('utf-8', 'windows-1251', $msg)
        );

        if (isset($_POST['save'])) {
            redirect("/:user_edit/-id/{$id}/");
        } else {
            redirect("/:user_list");
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Del user action
    public function action_userDel()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        if (!$this->userCheckAccess("users")) {
            redirect("/:user_list/-err/access");
        }

        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        if (!$id) {
            redirect("/:user_list/-err/wrong_id");
        }

        $r = Q("SELECT id, login FROM @@users WHERE id=?i", [$_GET['id']])->row();

        //     Send mail
        include_once SHARED_PATH . '/helpers.php';
        include_once SHARED_PATH . '/mmail.class.php';

        $msg = "<p>Добавлен новый пользователь<br />";
        $msg .= "User: " . $r['login'] . "<br />";
        $msg .= "</p>";
        $m = MMM(
            "Удален пользователь на сайте " . $_SERVER['HTTP_HOST'],
            //array('pavel.sidorov@gmail.com'),
            ['pavel.sidorov@gmail.com'],
            'BonoCMS:Users <noreply@' . $_SERVER['HTTP_HOST'] . '>',
            iconv('utf-8', 'windows-1251', $msg)
        );

        Q("DELETE FROM @@users WHERE id=?i LIMIT 1", [$id]);
        Q("ALTER TABLE @@users AUTO_INCREMENT=1;");
        redirect("/:user_list");
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    Users list
    public function action_userList()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        $res = Q("SELECT id, login, updated, admin FROM @@users ORDER BY id");
        RS("u_list", $res->all());
        V("zobject/user/list");
    }

    /**
     * Check path exist
     * :check_path_exist
     *
     * @return array
     */
    public function action_checkPathExist()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        $result = [
            'success' => true,
        ];

        $path = _get('path');

        // Проверка на пустой путь
        if (empty($path)) {
            $result['success'] = false;
            $result['error_message'] = 'Значение не может быть пустым';
        } // Проверка на существующий путь
        elseif (!empty(
        Q('SELECT `id` FROM `@@nodes` WHERE `path`=?s', [$this->node['path'] . $path . '/'])->all()
        )) {
            $result['success'] = false;
            $result['error_message'] = 'Страница "' . $path . '" уже существует. ';
        }

        exit(json_encode($result));
    }

    /**
     * Translit name to path
     * :translit_name
     *
     * @return void
     * @throws XException
     */
    public function action_translitName()
    {
        if (!(isset($_SESSION['user']['root']) && $_SESSION['user']['root'] == 1)) {
            throw new XException('access denied', 403);
        }

        $result = [
            'success' => true,
        ];

        $name = _get('name');

        $result['path'] = translit($name);

        exit(json_encode($result));
    }


    /**
     * Show edit form for current object
     *
     * @return XResponse
     */
    public function action_copy()
    {
        //  AUTH
        if (!$this->_checkAuth()) {
            redirect($this->node['path'] . ":auth");
        }

        //  Access
        if (!$this->userCheckAccess()) {
            RS("error_access", 1);
        }

        $this->before_edit();

        $object_type = $this->_prepareObjectType($_GET['type']);

        // exit if configuration for new object not exists
        if (false === ($object_config = import::config('app:configs/objects/' . $object_type . '.php'))) {
            exit;
        }


        // load data of current object
        $data = Q('SELECT * FROM @@' . _get('type') . ' WHERE id = ?i', [_get('id')])->row();

        foreach ($data as $k => $v) {
            $data[$k] = stripslashes($data[$k]);
        }

        // prepare values
        $data = $this->_prepareUI($data, $object_config);

        if (isset($data['title'])) {
            $data['title'] = 'КОПИЯ!!! - ' . $data['title'];
        }

        // load allowed objects for creating as childs
        if (count($subobjects = $this->_getSubobjects())) {
            // load configuration of allowed objects
            foreach ($subobjects as $object => $count) {
                $subobjects[$object] = [
                    'labels' => import::config('app:configs/objects/' . $object . '.php')->labels,
                    'type'   => $object,
                ];
            }
        }

        $this->after_edit();

        RS('object', [
            'ui'         => $object_config->ui,
            'fields'     => $object_config->fields,
            'node'       => $object_config->node,
            'labels'     => $object_config->labels,
            'data'       => $data,
            'subobjects' => $subobjects,
            'view'       => isset($this->config->view['mode']) ? $this->config->view['mode'] : '',
            'input_cfg'  => isset($object_config->input_cfg) ? $object_config->input_cfg : false,
            'subtabs'    => isset($object_config->subtabs) ? $object_config->subtabs : false,
        ]);

        $this->after_add();

        V('zobject/add');
    }
}
