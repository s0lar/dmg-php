<?php

class Router extends XAny_Router
{
    public $node;

    public $auth_actions = array(
        'add', 'edit', 'delete', 'cfg', 'createtable', 'seoOn', 'seoAdd', 'seoEdit',
        'user_list', 'user_add', 'user_edit', 'user_del', 'load-child-nodes', 'edit_node', 'list-objects',
    );

    public function _findController()
    {
        $rq = RQ();
        define("HOST", RQ()->url->host['domain']);

        //    Check auth
        if (in_array($rq->url->action, $this->auth_actions)) {
            //    Define BACKEND
            define('BACKEND', true);

            @session_start();
            if (Users::checkAuth() == false) {
                throw new XException('auth', 401);
            }
        } else {
            //    Define BACKEND
            define('BACKEND', false);
        }

        //    Admin URL
        if ($rq->url->path == '/admin' || $rq->url->path == '/admin/') {
            header("Location: /:edit");
            exit;
        }

        //    Zsettings routing
        if (strstr($rq->url->path, '/zsettings/')) {
            $this->controller = 'Zsettings_Controller';
        }

        //    Zcgf routing
        elseif (strstr($rq->url->path, '/zcfg/')) {
            $this->controller = 'Zcfg_Controller';
        }

        //    ZObject & other controllers
        else {
            $this->node = Q('SELECT * FROM `@@nodes` WHERE `path` = ?s', array($rq->url->path))->row();

            //    404 - check
            if (empty($this->node)) {
                throw new XException('not found', 404);
            }

            /* Redirect */
            if (isset($this->node['redirect']) && !empty($this->node['redirect']) && $rq->url->action == 'index') {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: ' . $this->node['redirect']);
                exit;
            }

            if (!empty($this->node['controller'])) {
                $this->controller = $this->node['controller'];
            } else {
                $this->controller = ucfirst($this->node['object_type']);
                //$this->controller = preg_replace('/(-|_)(\w)/e', 'strtoupper("$2")', $this->controller).'_Controller';
                $this->controller = preg_replace_callback('/(-|_)(\w)/', function ($m) {
                    return strtoupper($m[2]);
                }, $this->controller) . '_Controller';
                //$action = 'action_'.preg_replace_callback("/(-|_)(\w)/", function($m) { return strtoupper($m[2]); }, $request->url->action);
            }

            if (!import::from($this->controller)) {
                $this->controller = 'ZObject_Controller';
            }
        }
    }
}
