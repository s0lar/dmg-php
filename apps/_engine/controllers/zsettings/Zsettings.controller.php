<?php
@session_start();

class Zsettings_Controller extends ZObject_Controller
{
    public $cfg = null;
    public $module = null;

    public function __construct()
    {
        parent::__construct(null);

        $modules_list = import::config('app:configs/zsettings/__zsettings.php');
        RS("modules_list", $modules_list);

        if (RQ()->url->action == 'auth' && RQ()->method == 'post') {
            redirect("/:edit");
        }

        //    Get module name
        $this->module = isset($_GET['m']) ? strip_tags($_GET['m']) : null;
        if ($this->module === null) {
            exit("Zsettings - module not defined!");
        }

        //    Load config
        $this->cfg = import::config('app:configs/zsettings/'.$this->module.'.php');
        if ($this->cfg === false) {
            exit("Zsettings - config not found!");
        }

        RS("module", $this->module);
        RS("labels", $this->cfg->labels);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_editNode()
    {
        //    Prepare fields to select
        $fields = implode("`,`", $this->cfg->view['fields']);
        $order = '';
        $limit = '';

        //    Order by
        if ($this->cfg->view['orderby'] != '') {
            $order = "ORDER BY ".$this->cfg->view['orderby'];
        }

        //  User order
        if (in_array(_get('order_field'), $this->cfg->view['fields']) || _get('order_field') == "id") {
            $order = sprintf(
                "ORDER BY `%s` %s",
                _get('order_field'),
                (_get('order_type') == "desc") ? " DESC" : " ASC"
            );
        }

        //    Get UI & special field
        $ui['id'] = "#ID";
        $sub_objects = array();
        foreach ($this->cfg->view['fields'] as $v) {
            $ui[$v] = $this->cfg->ui[$v];

            //    Find special field
            if (is_array($this->cfg->fields[$v]) && isset($this->cfg->fields[$v]['type']) && !isset($sub_objects[$v])) {

                //    POINTER /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if ($this->cfg->fields[$v]['type'] == 'pointer') {
                    preg_match_all('/(%(\w+)%)/', $this->cfg->fields[$v]['object']['display'], $display_vars, PREG_SET_ORDER);
                    $_fl = '';

                    //    Prepare fields to query
                    foreach ($display_vars as $vv) {
                        $_fl .= '`'.$vv['2'].'`,';
                    }

                    $res = Q(" SELECT {$_fl}`id` FROM `@@".$this->cfg->fields[$v]['object']['table']."` ");

                    while ($r = $res->each()) {
                        $_str = $this->cfg->fields[$v]['object']['display'];
                        //    Parse template string
                        foreach ($r as $kkk => $vvv) {
                            $_str = str_replace("%{$kkk}%", $vvv, $_str);
                        }

                        $sub_objects[$v][$r['id']] = $_str;

                    }
                }

                //    LIST /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if ($this->cfg->fields[$v]['type'] == 'list') {
                    foreach ($this->cfg->fields[$v]['items'] as $kkk => $vvv) {
                        $sub_objects[$v][$kkk] = $vvv;
                    }
                }

            }
        }

        //    Get table data
        $_list = array();
        $res = Q("SELECT `id`,`{$fields}` FROM `@@{$this->module}` {$order}");
        while ($r = $res->each()) {
            foreach ($r as $k => $v) {
                if (isset($sub_objects[$k]) && isset($sub_objects[$k][$v])) {
                    $r[$k] = $sub_objects[$k][$v];
                }

            }
            $_list[] = $r;
        }

        //
        RS("_list", $_list);
        RS("sub_objects", $sub_objects);
        RS("ui", $ui);
        RS("fields", $this->cfg->fields);
        RS("ui_count", count($ui) + 1);
        V("zsettings/edit_node");
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_add()
    {
        if (!$this->userCheckAccess("zset")) {
            redirect("/zsettings/:edit_node/-m/"._get('m')."/-err/access");
        }

        RS(
            'object',
            array(
                'ui'        => $this->cfg->ui,
                'fields'    => $this->cfg->fields,
                'labels'    => $this->cfg->labels,
                'data'      => $this->_prepareUI(array(), $this->cfg),
                'input_cfg' => isset($this->cfg->input_cfg) ? $this->cfg->input_cfg : false,
            )
        );
        V("zsettings/add");
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_add_post()
    {
        if (!$this->userCheckAccess("zset")) {
            redirect("/zsettings/:edit_node/-m/"._get('m')."/-err/access");
        }

        $prepared = $this->_prepareValues(ZObject_Controller::ADD_MODE, $this->cfg);
        $id = Q('INSERT INTO `@@'.$this->module.'` SET '.implode(', ', $prepared['fields']), $prepared['values']);

        //    Add + add
        if (isset($_POST['add_add'])) {
            redirect("/zsettings/:add/-m/{$this->module}/-msg/good/-last/{$id}/");
        } elseif (isset($_POST['apply'])) {
            redirect("/zsettings/:edit/-m/{$this->module}/-msg/good/-id/{$id}/");
        } else {
            redirect("/zsettings/:edit_node/-m/{$this->module}/-msg/good/-last/{$id}/");
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_edit()
    {
        if (!$this->userCheckAccess("zset")) {
            redirect("/zsettings/:edit_node/-m/"._get('m')."/-err/access");
        }

        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

        // load data of current object
        $data = Q("SELECT * FROM `@@".$this->module."` WHERE id = {$id}")->row();

        foreach ($data as $k => $v) {
            $data[$k] = stripslashes($data[$k]);
        }

        // prepare values
        $data = $this->_prepareUI($data, $this->cfg);

        RS("id", $id);
        RS(
            'object',
            array(
                'ui'        => $this->cfg->ui,
                'fields'    => $this->cfg->fields,
                'data'      => $data,
                'labels'    => $this->cfg->labels,
                'input_cfg' => isset($this->cfg->input_cfg) ? $this->cfg->input_cfg : false,
            )
        );

        V("zsettings/edit");
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_edit_post()
    {
        if (!$this->userCheckAccess("zset")) {
            redirect("/zsettings/:edit_node/-m/"._get('m')."/-err/access");
        }

        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

        // prepare values
        $prepared = $this->_prepareValues(ZObject_Controller::EDIT_MODE, $this->cfg);

        // update data
        Q('UPDATE `@@'.$this->module.'` SET '.implode(', ', $prepared['fields']).' WHERE id = '.$id, $prepared['values']);

        if (isset($_POST['apply'])) {
            redirect("/zsettings/:edit/-m/{$this->module}/-msg/good/-id/{$id}/");
        } else {
            redirect("/zsettings/:edit_node/-m/{$this->module}/");
        }

    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_delete()
    {
        if (!$this->userCheckAccess("zset")) {
            redirect("/zsettings/:edit_node/-m/"._get('m')."/-err/access");
        }

        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

        $imgs = array();

        //    Find images to delete
        foreach ($this->cfg->fields as $k => $v) {
            if ($v == 'gs_file') {
                $imgs[] = $k;
            }

        }

        if (!empty($imgs)) {
            $res = Q("SELECT `".implode("`,`", $imgs)."` FROM @@{$this->module} WHERE id=?i ", array($id))->row();
            if (!empty($res)) {
                foreach ($res as $v) {
                    $tmp = json_decode($v);

                    //
                    if (isset($tmp->files) && !empty($tmp->files)) {
                        foreach ($tmp->files as $vv) {
                            if (isset($vv->fs) && !empty($vv->fs)) {
                                foreach ($vv->fs as $vvv) {
                                    @unlink($_SERVER['DOCUMENT_ROOT'].$vvv);
                                }
                            }
                        }
                    }
                }
            }
        }
        $res = Q("DELETE FROM @@{$this->module} WHERE id=?i LIMIT 1", array($id));
        //
        redirect("/zsettings/:edit_node/-m/{$this->module}/");
    }

}
