<?php

class pointer_handler
{
	function prepare_view(&$value = null, $config = null)
	{
		$config['object']['fields'] = explode(',', $config['object']['fields']);
		foreach ($config['object']['fields'] as &$f)
		{
			$f = trim($f);
			if ($f == '*')
				continue;
			
			$f = '`'.$f.'`';
		}
		
		$res = Q('SELECT '.implode(', ', $config['object']['fields']).' FROM @@'.$config['object']['table']);
		
		$display = $config['object']['display'];
		preg_match_all('/(%(\w+)%)/', $display, $display_vars, PREG_SET_ORDER);
		$n = count($display_vars);
		
		$value = array(
			'selected' => $value,
			'items' => array()
		);
		while ($row = $res->each())
		{
			$row['display'] = $display;
			
			for ($i=0; $i<$n; $i++)
				$row['display'] = preg_replace('#'.$display_vars[$i][0].'#', $row[$display_vars[$i][2]], $row['display']);
			
			$value['items'][$row['id']] = $row;
		}
	}
}
