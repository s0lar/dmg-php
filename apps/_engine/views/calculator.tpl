﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_calculator">
        <div class="container">
            <div class="calculator-panel js-calculatorPanel">
                <div class="calculator-panel__icon">
                    <svg class="icon icon-weight ">
                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-weight"></use>
                    </svg><span>Расчет массы</span>
                </div>
                <div class="calculator-panel__weight js-calculatorWeight">12 345 кг</div>
                <div class="calculator-panel__info js-calculatorInfo">
                    <svg class="icon icon-info ">
                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-info"></use>
                    </svg>
                </div>
            </div>
            <div class="page-head__top visible-xs">
                <div class="page-head__title">Калькулятор металла</div>
            </div>
            <div class="calculator-head">
                <script>
                    var calculatorDMG = [
                        {
                            type: 'sheet',
                            fields: ['A', 'B', 'S']
                        },
                        {
                            type: 'circle',
                            fields: ['D', 'L']
                        },
                        {
                            type: 'square',
                            fields: ['A', 'L']
                        },
                        {
                            type: 'equilateral_corner',
                            fields: ['A', 'S', 'L']
                        },
                        {
                            type: 'unequal_angle',
                            fields: ['A', 'B', 'S', 'L']
                        },
                        {
                            type: 'pipe_round',
                            fields: ['D', 'S', 'L']
                        },
                        {
                            type: 'pipe_square',
                            fields: ['A', 'S', 'L']
                        },
                        {
                            type: 'pipe_rectangular',
                            fields: ['A', 'B', 'S', 'L']
                        }
                    ];

                </script>
                <div class="calculator-nav">
                    <div class="calculator-nav__elem is-active"><a class="calculator-nav__link js-calculatorNavLink" href="javascript:;" data-type="sheet">Лист</a></div>
                    <div class="calculator-nav__elem"><a class="calculator-nav__link js-calculatorNavLink" href="javascript:;" data-type="circle">Круг</a></div>
                    <div class="calculator-nav__elem"><a class="calculator-nav__link js-calculatorNavLink" href="javascript:;" data-type="square">Квадрат</a></div>
                    <div class="calculator-nav__elem"><a class="calculator-nav__link js-calculatorNavLink" href="javascript:;" data-type="equilateral_corner">Уголок</a></div>
                    <div class="calculator-nav__elem"><a class="calculator-nav__link js-calculatorNavLink" href="javascript:;" data-type="pipe_round">Труба</a></div>
                </div>
            </div>
            <div class="calculator-content">
                <div class="calculator-top">
                    <ul class="calculator-top__nav" data-subtype="equilateral_corner">
                        <li class="is-active"><a class="js-calculatorNavLink is_subnav_link" href="javascript:;" data-type="equilateral_corner">Равнополочный</a></li>
                        <li><a class="js-calculatorNavLink is_subnav_link" href="javascript:;" data-type="unequal_angle">Неравнополочный</a></li>
                    </ul>
                    <ul class="calculator-top__nav" data-subtype="pipe_round">
                        <li class="is-active"><a class="js-calculatorNavLink is_subnav_link" href="javascript:;" data-type="pipe_round">Круглая</a></li>
                        <li><a class="js-calculatorNavLink is_subnav_link" href="javascript:;" data-type="pipe_square">Квадратная</a></li>
                        <li><a class="js-calculatorNavLink is_subnav_link" href="javascript:;" data-type="pipe_rectangular">Прямоугольная</a></li>
                    </ul>
                </div>
                <div class="calculator-mob">
                    <label class="calculator-radio">
                        <input class="js-calculatorNavRadio" type="radio" name="calc_type" data-type="sheet" checked>
                        <div class="calculator-radio__box">
                            <div class="calculator-radio__title">Лист</div>
                        </div>
                    </label>
                    <label class="calculator-radio">
                        <input class="js-calculatorNavRadio" type="radio" name="calc_type" data-type="circle">
                        <div class="calculator-radio__box">
                            <div class="calculator-radio__title">Круг</div>
                        </div>
                    </label>
                    <label class="calculator-radio">
                        <input class="js-calculatorNavRadio" type="radio" name="calc_type" data-type="square">
                        <div class="calculator-radio__box">
                            <div class="calculator-radio__title">Квадрат</div>
                        </div>
                    </label>
                    <label class="calculator-radio">
                        <input class="js-calculatorNavRadio" type="radio" name="calc_type" data-type="equilateral_corner">
                        <div class="calculator-radio__box">
                            <div class="calculator-radio__title">Уголок</div>
                            <ul class="calculator-radio__list">
                                <li><a class="js-calculatorNavLink" data-type="equilateral_corner" href="javascript:;">Равнополочный<img src="/theme/dmg_markup/build/images/corner.svg"></a></li>
                                <li><a class="js-calculatorNavLink" data-type="unequal_angle" href="javascript:;">Неравнополочный<img src="/theme/dmg_markup/build/images/corner-2.svg"></a></li>
                            </ul>
                        </div>
                    </label>
                    <label class="calculator-radio">
                        <input class="js-calculatorNavRadio" type="radio" name="calc_type" data-type="pipe_round">
                        <div class="calculator-radio__box">
                            <div class="calculator-radio__title">Труба</div>
                            <ul class="calculator-radio__list">
                                <li><a class="js-calculatorNavLink" data-type="pipe_round" href="javascript:;">Круглая<img src="/theme/dmg_markup/build/images/pipe.svg"></a></li>
                                <li><a class="js-calculatorNavLink" data-type="pipe_square" href="javascript:;">Квадратная<img src="/theme/dmg_markup/build/images/pipe-2.svg"></a></li>
                                <li><a class="js-calculatorNavLink" data-type="pipe_rectangular" href="javascript:;">Прямоугольная<img src="/theme/dmg_markup/build/images/pipe-3.svg"></a></li>
                            </ul>
                        </div>
                    </label>
                </div>
                <div class="calculator-main">
                    <div class="calculator-section">
                        <div class="calculator-image js-calculatorImage is-active" data-type="sheet" style="background-image: url('/theme/dmg_markup/build/images/calc-8-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="circle" style="background-image: url('/theme/dmg_markup/build/images/calc-7-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="square" style="background-image: url('/theme/dmg_markup/build/images/calc-6-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="equilateral_corner" style="background-image: url('/theme/dmg_markup/build/images/calc-5-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="unequal_angle" style="background-image: url('/theme/dmg_markup/build/images/calc-4-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="pipe_round" style="background-image: url('/theme/dmg_markup/build/images/calc-3-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="pipe_square" style="background-image: url('/theme/dmg_markup/build/images/calc-2-size.jpg')"></div>
                        <div class="calculator-image js-calculatorImage" data-type="pipe_rectangular" style="background-image: url('/theme/dmg_markup/build/images/calc-1-size.jpg')"></div>
                        <form class="calculator-form js-calculatorForm" action="#">
                            <div class="calc-fieldset">
                                <div class="calc-label">Марка стали</div>
                                <div class="calc-field">
                                    <select class="calc-select js-select" data-calc-name="ro">
                                        <option value="7850">AISI 304</option>
                                        <option value="7850">AISI 304 L</option>
                                        <option value="7850">AISI 310 S</option>
                                        <option value="7850">AISI 316</option>
                                        <option value="7850">AISI 316 L</option>
                                        <option value="7850">AISI 316Ti</option>
                                        <option value="7850">AISI 321</option>
                                        <option value="7850">08Х18Н12Т</option>
                                        <option value="7850">10Х23Н18</option>
                                        <option value="7700">AISI 430</option>
                                        <option value="7900">08Х18Н10Т</option>
                                        <option value="7900">12Х18Н10Т</option>
                                    </select>
                                </div>
                            </div>
                            <div class="calc-fieldset" data-field="A">
                                <div class="calc-label">Сторона А, мм</div>
                                <div class="calc-field js-count">
                                    <div class="calc-field__tip">A</div>
                                    <input class="calc-input js-countInput" type="tel" name="A" value="" placeholder="0" data-calc-name="A">
                                    <div class="calc-count">
                                        <div class="calc-count__button button_minus js-countMinus"></div>
                                        <div class="calc-count__button button_plus js-countPlus"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calc-fieldset" data-field="B">
                                <div class="calc-label">Сторона B, мм</div>
                                <div class="calc-field js-count">
                                    <div class="calc-field__tip">B</div>
                                    <input class="calc-input js-countInput" type="tel" name="B" value="" placeholder="0" data-calc-name="B">
                                    <div class="calc-count">
                                        <div class="calc-count__button button_minus js-countMinus"></div>
                                        <div class="calc-count__button button_plus js-countPlus"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calc-fieldset" data-field="D">
                                <div class="calc-label">Диаметр наружный, D, мм</div>
                                <div class="calc-field js-count">
                                    <div class="calc-field__tip">D</div>
                                    <input class="calc-input js-countInput" type="tel" name="D" value="" placeholder="0" data-calc-name="D">
                                    <div class="calc-count">
                                        <div class="calc-count__button button_minus js-countMinus"></div>
                                        <div class="calc-count__button button_plus js-countPlus"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calc-fieldset" data-field="S">
                                <div class="calc-label">Толщина стенки S, мм</div>
                                <div class="calc-field js-count">
                                    <div class="calc-field__tip">S</div>
                                    <input class="calc-input js-countInput" type="tel" name="S" value="" placeholder="0" data-calc-name="S">
                                    <div class="calc-count">
                                        <div class="calc-count__button button_minus js-countMinus"></div>
                                        <div class="calc-count__button button_plus js-countPlus"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calc-fieldset" data-field="L">
                                <div class="calc-label">Длина трубы, м</div>
                                <div class="calc-field js-count">
                                    <div class="calc-field__tip">L</div>
                                    <input class="calc-input js-countInput" type="tel" name="L" value="" placeholder="0" data-calc-name="L">
                                    <div class="calc-count">
                                        <div class="calc-count__button button_minus js-countMinus"></div>
                                        <div class="calc-count__button button_plus js-countPlus"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="calc-button visible-xs">
                                <button class="button style_green size_middle justify_space_between js-calculatorButton" disabled>
                                    <span class="button__text">
                                        Рассчитать массу
                                        <svg class="icon icon-arrow-triangle ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="calculator-aside js-calculatorAside">
                        <div class="calculator-aside__center">
                            <div class="calculator-aside__head">
                                <div class="calculator-aside__label">Расчет массы</div>
                                <div class="calculator-aside__title js-calculatorWeight">12 345 кг</div>
                            </div>
                            <div class="calculator-aside__char">
                                <p>Параметры: <br><span class="js-calculatorChar">15×10×1 мм, длина 11 м.<span></p>
                                <p>Плотность материала <span class="js-calculatorRo">7850</span> кг/м³</p>
                                <p>Расчёт производился по формуле: <br><span class="js-calculatorFormula"></span></p>
                            </div>
                            <div class="calculator-aside__button hidden-xs">
                                <a class="button style_green size_middle justify_space_between" href="/contacts/">
                                    <span class="button__text">
                                        Отправить заявку
                                        <svg class="icon icon-arrow-triangle ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                        </svg>
                                    </span>
                                </a>
                            </div>
                            <div class="calculator-aside__button visible-xs"><a class="button style_green" href="/contacts/"><span class="button__text">Оформить заказ</span></a></div>
                        </div>
                        <div class="calculator-aside__close js-calculatorAsideClose">Свернуть</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}