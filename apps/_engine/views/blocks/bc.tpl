{strip}
    <div class="page-head__breadcrumb">
        <div class="breadcrumb">
            <div class="breadcrumb__elem"><a class="breadcrumb__link" href="/">Главная</a></div>


            {foreach item=item from=$bc name=i}
                <div class="breadcrumb__elem">
                    <a class="breadcrumb__link" href="{$item.path}">{$item.object_title}</a>
                </div>
            {/foreach}

        </div>
    </div>
{/strip}