{if !empty($cart) && $cart.count > 0}
    <a id="id-header-cart__control" class="header-cart__control js-cartControl" href="/order/">
        <div class="header-cart__icon">
            <div class="header-cart__number sjs-cart-count">{$cart.count}</div>
            <svg class="icon icon-cart ">
                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-cart"></use>
            </svg>
        </div>
        <div class="header-cart__title sjs-cart-total">Корзина: {$cart.total_view} руб.</div>
    </a>
{else}
    <a id="id-header-cart__control" class="header-cart__control style_cart_empty" href="javascript:;">
        <div class="header-cart__icon">
            <svg class="icon icon-cart ">
                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-cart"></use>
            </svg>
        </div>
        <div class="header-cart__title sjs-cart-total">Корзина пуста</div>
    </a>
{/if}