<?php

class Faq_Controller extends ZObject_Controller
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_index()
    {
        if ($this->node['object_type'] === 'faq') {
            redirect('../');
        }

        $res = Q('SELECT P.* FROM @@faq as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="faq" and N.parent_id=?i
                        ORDER BY `sort` desc', [$this->node['id']])->all();
        RS('faq', $res);

        RS('cls_body', '');
        V('faq');
    }
}
