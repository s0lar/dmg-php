/* helpers */

/* ready */

$(document).ready(function(){
	
/* Maybe old function #######################################################################################################*/
	$(function(){
		//	Drag & drop sort
		$('.gsItems').dragsort({
			dragSelector: 'div.gsItemDs',
			dragSelectorExclude: 'a,input',
			dragEnd: callbacks.updateSort,
			dragBetween: false,
			placeHolderTemplate: '<li class="gsItem"><div class="gsItemPlaceHolder"></div></li>'
		});
		
		//	gstore plugin
		$('.gstore').gstore({
			dispatcher: '/gs/add/*/',
			ui: { src: '/gs/ui/ui.swf', width: 101, height: 24 },
			callbacks: {
				init: callbacks.init,
				uploadStart: 'callbacks.uploadStart',
				uploadSuccess: 'callbacks.uploadSuccess',
				uploadError: 'callbacks.uploadError',
				allComplete: 'callbacks.allComplete'
			}
		});
	});
/*#######################################################################################################*/

/* tinyMCE #######################################################################################################*/
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		editor_selector : "visual",
		plugins : "flash,style,simplebrowser,table,save,advimage,advlink,iespell,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,gentable",
		//plugin_simplebrowser_browselinkurl : '/res/shared/tiny_mce3/plugins/simplebrowser/browser.html?Connector=connectors/php/connector.php',
		//plugin_simplebrowser_browseimageurl : '/res/shared/tiny_mce3/plugins/simplebrowser/browser.html?Type=Image&Connector=connectors/php/connector.php',
		//plugin_simplebrowser_browseflashurl : '/res/shared/tiny_mce3/plugins/simplebrowser/browser.html?Type=Flash&Connector=connectors/php/connector.php',
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,images",
		//plugins : "filemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontsizeselect,help",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image1,images,cleanup,code,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,fullscreen",
		theme_advanced_buttons4 : "template",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_path_location : "bottom",
		//content_css : "/css/w.css",
		plugin_insertdate_dateFormat : "%Y-%m-%d",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		//extended_valid_elements : "hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
		//	Allow all tags
		valid_elements : '*[*]',
		file_browser_callback : "fileBrowserCallBack",
		theme_advanced_resize_horizontal : true,
		theme_advanced_resizing : true,
		convert_newlines_to_brs: false,
		force_br_newlines: true,
		force_p_newlines: false,
		language : "ru",
		relative_urls : false,
		convert_urls : false,
		width : '100%',
		height : '300px',
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//	templates
		/*
		template_templates : [
				////////
				,{ title:"Подавители", 	src:"/res/shared/tiny_mce3/plugins/template/tpl_gsm_podaviteli.htm", 	description:"" }
		],
		*/
		entity_encoding : "raw"
	});


/*#######################################################################################################*/
});
