<?php

class Glossary_Controller extends ZObject_Controller
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_index()
    {
        if ($this->node['object_type'] === 'glossary') {
            redirect('../');
        }

        $res = Q('SELECT P.* FROM @@glossary as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="glossary" and N.parent_id=?i
                        ORDER BY `sort` desc', [$this->node['id']])->all();
        RS('glossary', $res);

        RS('cls_body', '');
        V('glossary');
    }
}
