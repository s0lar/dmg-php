﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="main">
        <div class="page-head default_head">
            <div class="container">
                <div data-aos="fade-up" data-aos-delay="500" data-aos-duration="400" data-aos-easing="ease-out-cubic"
                     class="page-head__desc">
                    <div class="page-head__title">Личный кабинет</div>
                </div>
                <div data-aos="fade-up" data-aos-delay="600" data-aos-duration="400" data-aos-easing="ease-out-cubic"
                     class="page-head__bottom">
                    <div class="page-head__nav">
                        <div class="head-nav">
                            <div class="head-nav__elem is-active"><a href="/customer/:customer_personal"
                                                                     class="head-nav__link">Контактные данные</a></div>
                            <div class="head-nav__elem"><a href="/customer/" class="head-nav__link">история заказов
                                    <sup>{count($orders)}</sup></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-main">
            <div class="container">
                <div class="page-personal">

                    <form action="/customer/:customer_personal" class="form-account  save-changes" method="post">

                        <div class="form-tab">
                            <div class="form-tab__nav">
                                <label class="form-tab__elem">
                                    <input type="radio" name="type" {if $customer.type == 0}checked{/if} value="0" class="js-formTabRadio">
                                    <span class="form-tab__title">
                                        <span class="hidden-xs">Физическое лицо</span>
                                        <span class="visible-xs">Физ. лицо</span>
                                    </span>
                                </label>
                                <label class="form-tab__elem">
                                    <input type="radio" name="type" {if $customer.type == 1}checked{/if} value="1" class="js-formTabRadio">
                                    <span class="form-tab__title">
                                        <span class="hidden-xs">Юридическое лицо</span>
                                        <span class="visible-xs">Юр. лицо</span>
                                    </span>
                                </label>
                            </div>
                        </div>

                        <div class="form-account__group">
                            <div class="form-account__aside">
                                <div class="form-account__title">Данные</div>
                            </div>
                            <div class="form-account__section">
                                <div {if $customer.type == 0}style="display: none;"{/if} class="form-account__fields js-formTabLegalFields">
                                    <div class="form-fieldset">
                                        <div class="form-fieldset__input">
                                            <input name="title" type="text" placeholder="Название компании"
                                                   class="form-input" value="{$customer.title|default:''}"/>
                                        </div>
                                    </div>
                                    <div class="form-fieldset">
                                        <div class="form-fieldset__input">
                                            <input name="addr" type="text" placeholder="Юридический адрес"
                                                   class="form-input" value="{$customer.addr|default:''}"/>
                                        </div>
                                    </div>
                                    <div class="form-fieldset fieldset_flex">
                                        <div class="form-fieldset__input">
                                            <input name="inn" type="text" placeholder="ИНН" class="form-input"
                                                   value="{$customer.inn|default:''}"/>
                                        </div>
                                        <div class="form-fieldset__input">
                                            <input name="kpp" type="text" placeholder="КПП" class="form-input"
                                                   value="{$customer.kpp|default:''}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-fieldset fieldset_margin">
                                    <div class="form-fieldset__input">
                                        <input name="person" type="text" placeholder="Контактное лицо *"
                                               class="form-input" value="{$customer.person|default:''}"/>
                                    </div>
                                </div>
                                <div class="form-fieldset fieldset_flex">
                                    <div class="form-fieldset__input">
                                        <input name="email" type="email" placeholder="E-Mail *" class="form-input"
                                               value="{$customer.email|default:''}" readonly/>
                                        {if isset($customer_error_code) && $customer_error_code == 1 && isset($customer_error)}
                                            <div>
                                                <p style="color:#f66">{$customer_error}</p>
                                            </div>
                                        {/if}
                                    </div>
                                    <div class="form-fieldset__input input_col_3">
                                        <input name="phone" type="tel" placeholder="Телефон" class="form-input"
                                               value="{$customer.phone|default:''}"/>
                                    </div>
                                    <div class="form-fieldset__input input_col_3">
                                        <input name="fax" type="tel" placeholder="Факс" class="form-input"
                                               value="{$customer.fax|default:''}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-account__group">
                            <div class="form-account__aside">
                                <div class="form-account__title">город</div>
                            </div>
                            <div class="form-account__section">
                                <div class="form-fieldset">
                                    <div class="form-fieldset__input">
                                        <input name="city" type="text" placeholder="Введите город" class="form-input"
                                               value="{$customer.city|default:''}"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-account__group">
                            <div class="form-account__aside"></div>
                            <div class="form-account__section">
                                <div class="form-account__submit">
                                    <button type="submit" class="button style_green"><span class="button__text">сохранить</span>
                                    </button>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}