<?php

class Docs_Controller extends ZObject_Controller
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_index()
    {
        if ($this->node['object_type'] === 'gost') {
            redirect('../');
        }


        $docs = [];
        $res = Q('SELECT * FROM @@docs ORDER BY sort DESC');
        while ($r = $res->each()) {
            $r['preview'] = $this->getFM($r['preview'], 'a');
            $r['files'] = $this->getFM($r['files'], '0');
            $docs[$r['group']][] = $r;
        }

        $groups = [];
        $res = Q('SELECT * FROM @@group ORDER BY sort DESC');
        while ($r = $res->each()) {
            if (!empty($docs[$r['id']])) {
                $groups[$r['id']] = [
                    'title' => $r['title'],
                    'files' => $docs[$r['id']],
                ];
            }
        }

        RS('page', '');
        RS('groups', $groups);

        V('docs');
    }
}
