<?php
return array(
    # types of fields
    'fields' => array(
        'title' => 'string',
        'content1' => 'code',
        'content2' => 'code',
    ),

    # labels of fields
    'ui' => array(
        'title' => 'Название страницы',
        'content1' => 'Контент 1',
        'content2' => 'Контент 2',
    ),

    # node configuration
    'node' => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name' => '-user',
    ),

    # labels for actions
    'labels' => array(
        //'list' => 'Подразделы',
        'add' => 'Новый раздел',
        'adding' => 'Создание раздела',
        'edit' => 'Редактировать раздел',
        'editing' => 'Редактирование раздела',
        'delete' => 'Удалить раздел',
    ),
);
