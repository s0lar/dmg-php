﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-flex">

                <div class="page-flex__aside">
                    <div class="subscribe">
                        <div class="subscribe__text">Подпишитесь на&nbsp;наши аккаунты в&nbsp;социальных сетях, чтобы быть в&nbsp;курсе всех новостей компании &laquo;Дарья-Металл-Групп&raquo;</div>
                        <div class="subscribe__social">
                            <div class="social">
                                <div class="social__elem">
                                    <a class="social__link" href="{$cfg.link_instagram|default:'#'}">
                                        <svg class="icon icon-instagram ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-instagram"></use>
                                        </svg>
                                    </a>
                                </div>
                                <div class="social__elem">
                                    <a class="social__link" href="{$cfg.link_whatsapp|default:'#'}">
                                        <svg class="icon icon-whatsapp ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-whatsapp"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="page-flex__section">
                    <div class="page-group">
                        <div class="news-section">
                            <div class="news-list">

                                {foreach item=news from=$newsList}
                                    <div class="news-tile  {if empty($news.photo)} style_no_image {/if}">
                                        {if !empty($news.photo)}
                                        <div class="news-tile__image">
                                            <div class="news-tile__cover" style="background-image: url('{$news.photo}')"></div>
                                        </div>
                                        {/if}
                                        <div class="news-tile__info">
                                            <div class="news-tile__desc">{$news.anons}</div>
                                            <div class="news-tile__controls">
                                                <div class="news-tile__button"><a class="button style_green" href="{$news.path}"><span class="button__text">Подробнее</span></a></div>
                                                <div class="news-tile__date">{$news.date}</div>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}