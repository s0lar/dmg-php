﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-flex">

                {include file="_aside.tpl"}

                <div class="page-flex__section">
                    <div class="page-group">
                        <div class="article-list">
                            <div class="article-grid">

                                {foreach item=article from=$articlesList}
                                    <div class="article-grid__col">
                                        <a class="article-tile" href="{$article.path}">
                                            <div class="article-tile__image" style="background-image: url('{$article.photo}')"></div>
                                            <div class="article-tile__info">
                                                <div class="article-tile__title">{$article.title}</div>
                                                <div class="article-tile__button">
                                                    <button class="button style_green"><span class="button__text">Подробнее</span></button>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                {/foreach}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}