﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">Вопрос-ответ</div>
                </div>
                <div class="page-head__breadcrumb">
                    <div class="breadcrumb">
                        <div class="breadcrumb__elem"><a class="breadcrumb__link" href="#">Главная</a></div>
                        <div class="breadcrumb__elem"><a class="breadcrumb__link" href="#">Вопрос-ответ</a></div>
                    </div>
                </div>
            </div>
            <div class="page-flex">
                <div class="page-flex__aside">
                    <div class="aside-select">
                        <div class="aside-select__control">
                            <div class="aside-select__button">
                                <div class="aside-select__title">Вопрос-ответ</div>
                                <svg class="icon icon-sort-arrow-down ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-sort-arrow-down"></use>
                                </svg>
                            </div>
                            <select class="aside-select__list js-asideSelectList">
                                <option value="1">Услуги</option>
                                <option value="2">Все о нержавеющей стали</option>
                                <option value="3">Справочники</option>
                                <option value="4">Вопрос-ответ</option>
                                <option value="5">Глоссарий</option>
                                <option value="6">Реквизиты</option>
                            </select>
                        </div>
                    </div>
                    <div class="aside-nav">
                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Услуги</a></div>
                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Все о нержавеющей стали</a></div>
                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Справочники</a></div>
                        <div class="aside-nav__elem is-active"><a class="aside-nav__link" href="#">Вопрос-ответ</a></div>
                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Глоссарий</a></div>
                        <div class="aside-nav__elem"><a class="aside-nav__link" href="#">Реквизиты</a></div>
                    </div>
                </div>
                <div class="page-flex__section">
                    <div class="page-group">
                        <div class="faq">
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                            <div class="faq-elem">
                                <div class="faq-elem__header js-faqToggle">
                                    <div class="faq-elem__title">Какие существуют способы оплаты для частных лиц?</div>
                                    <svg class="icon icon-dropdown ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                    </svg>
                                </div>
                                <div class="faq-elem__content">
                                    <p>Независимо от общей суммы приобретаемого товара и способа оплаты, реализация штучного товара производится кратно 1 шт либо неделимой упаковке (например, пачка электродов 0,7 кг в вакуумной упаковке), а также любой длинны, квадратуры или веса для товара с соответствующими единицами измерения.</p>
                                    <p>Адрес и график работы розничного магазина: г. Краснодар, ул. Старокубанская, 2, пн-пт с 8:00 до 17:00, тел.: +7 (861) 227-01-78.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-group">
                        <div class="requisites">
                            <div class="requisites__top">
                                <div class="requisites__title">Реквизиты компании</div>
                            </div>
                            <div class="requisites__main">
                                <table class="requisites__table">
                                    <tr>
                                        <td>Полное наименование</td>
                                        <td>Общество с ограниченной ответственностью «Дарья-Металл-Групп»</td>
                                    </tr>
                                    <tr>
                                        <td>Сокращенное наименование</td>
                                        <td>ООО «Дарья-Металл-Групп»</td>
                                    </tr>
                                    <tr>
                                        <td>ИНН</td>
                                        <td>2312096615</td>
                                    </tr>
                                    <tr>
                                        <td>КПП</td>
                                        <td>230901001</td>
                                    </tr>
                                    <tr>
                                        <td>ОГРН</td>
                                        <td>1032307157410</td>
                                    </tr>
                                    <tr>
                                        <td>ОКПО</td>
                                        <td>26410178</td>
                                    </tr>
                                    <tr>
                                        <td>Юридический адрес</td>
                                        <td>350011, Краснодарский край, г. Краснодар, ул. Старокубанская, дом No 2</td>
                                    </tr>
                                    <tr>
                                        <td>Фактический адрес</td>
                                        <td>350011, Краснодарский край, г. Краснодар, ул. Старокубанская, дом No 2</td>
                                    </tr>
                                    <tr>
                                        <td>Почтовый адрес</td>
                                        <td>350011, Краснодарский край, г. Краснодар, а/я 888</td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td>8 (861) 227-06-35, 8 (861) 227-01-78</td>
                                    </tr>
                                    <tr>
                                        <td>E-mail</td>
                                        <td>mail@d-m-g.ru</td>
                                    </tr>
                                    <tr>
                                        <td>Сайт</td>
                                        <td>http://www.d-m-g.ru/</td>
                                    </tr>
                                    <tr>
                                        <td>График работы</td>
                                        <td>ПН—ПТ: с 8:00 до 17:00, СБ—ВС: выходные</td>
                                    </tr>
                                </table>
                                <table class="requisites__table">
                                    <tr>
                                        <td>Банковские реквизиты</td>
                                        <td>
                                            <p>ФИЛИАЛ ПАО БАНК ВТБ В Г.РОСТОВЕ-НА-ДОНУ</p>
                                            <p>Р.счет 40702810203300002436 </p>
                                            <p>БИК 046015999</p>
                                            <p>К.счет 30101810300000000999</p>
                                            <p>БАНК АО БАНК ЗЕНИТ СОЧИ</p>
                                            <p>Р.счет 40702810430010000133 </p>
                                            <p>БИК 040396717</p>
                                            <p>К.счет 30101810400000000717</p>
                                        </td>
                                    </tr>
                                </table>
                                <table class="requisites__table">
                                    <tr>
                                        <td>Директор</td>
                                        <td>Булимов Сергей Геннадиевич (на основании устава)</td>
                                    </tr>
                                    <tr>
                                        <td>Главный бухгалтер</td>
                                        <td>Батищева Людмила Ивановна</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="requisites__button"><a class="button style_border_green size_middle justify_space_between" href="#"><span class="button__text">Скачать реквизиты
												<svg class="icon icon-download ">
													<use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-download"></use>
												</svg></span></a></div>
                        </div>
                    </div>
                    <div class="page-group">
                        <div class="article-list">
                            <div class="article-grid">
                                <div class="article-grid__col"><a class="article-tile" href="#">
                                        <div class="article-tile__image" style="background-image: url('/theme/dmg_markup/build/images/catalog_image.jpg')"></div>
                                        <div class="article-tile__info">
                                            <div class="article-tile__title">Переработка рыбы и морепродуктов</div>
                                            <div class="article-tile__button">
                                                <button class="button style_green"><span class="button__text">Подробнее</span></button>
                                            </div>
                                        </div></a></div>
                                <div class="article-grid__col"><a class="article-tile" href="#">
                                        <div class="article-tile__image" style="background-image: url('/theme/dmg_markup/build/images/catalog_image.jpg')"></div>
                                        <div class="article-tile__info">
                                            <div class="article-tile__title">Переработка рыбы и морепродуктов</div>
                                            <div class="article-tile__button">
                                                <button class="button style_green"><span class="button__text">Подробнее</span></button>
                                            </div>
                                        </div></a></div>
                                <div class="article-grid__col"><a class="article-tile" href="#">
                                        <div class="article-tile__image" style="background-image: url('/theme/dmg_markup/build/images/catalog_image.jpg')"></div>
                                        <div class="article-tile__info">
                                            <div class="article-tile__title">Переработка рыбы и морепродуктов</div>
                                            <div class="article-tile__button">
                                                <button class="button style_green"><span class="button__text">Подробнее</span></button>
                                            </div>
                                        </div></a></div>
                                <div class="article-grid__col"><a class="article-tile" href="#">
                                        <div class="article-tile__image" style="background-image: url('/theme/dmg_markup/build/images/catalog_image.jpg')"></div>
                                        <div class="article-tile__info">
                                            <div class="article-tile__title">Переработка рыбы и морепродуктов</div>
                                            <div class="article-tile__button">
                                                <button class="button style_green"><span class="button__text">Подробнее</span></button>
                                            </div>
                                        </div></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="page-group">
                        <div class="news-list">
                            <div class="news-tile">
                                <div class="news-tile__image">
                                    <div class="news-tile__cover" style="background-image: url('/theme/dmg_markup/build/images/d_logo.png')"></div>
                                </div>
                                <div class="news-tile__info">
                                    <div class="news-tile__desc">ООО «Дарья-Металл-Групп» и InoxMIMgrup принимают участие в выставке АГРОПРОДМАШ 2018. Мы приглашаем Вас посетить с 8 по 12 октября 2018 г. наш стенд FG010 в павильоне «Форум».</div>
                                    <div class="news-tile__controls">
                                        <div class="news-tile__button"><a class="button style_green" href="#"><span class="button__text">Подробнее</span></a></div>
                                        <div class="news-tile__date">20 / 06 / 2019</div>
                                    </div>
                                </div>
                            </div>
                            <div class="news-tile style_no_image">
                                <div class="news-tile__info">
                                    <div class="news-tile__desc">ООО «Дарья-Металл-Групп» и InoxMIMgrup принимают участие в выставке АГРОПРОДМАШ 2018. Мы приглашаем Вас посетить с 8 по 12 октября 2018 г. наш стенд FG010 в павильоне «Форум».</div>
                                    <div class="news-tile__controls">
                                        <div class="news-tile__button"><a class="button style_green" href="#"><span class="button__text">Подробнее</span></a></div>
                                        <div class="news-tile__date">20 / 06 / 2019</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-group">
                        <div class="download">
                            <div class="download-all"><a class="button style_border_green size_middle justify_space_between" href="#"><span class="button__text">Загрузить все файлы
												<svg class="icon icon-download ">
													<use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-download"></use>
												</svg></span></a></div>
                            <div class="download-list"><a class="download-elem" href="#">
                                    <div class="download-elem__header">
                                        <div class="download-elem__title">ГОСТ 19904-90 Прокат листовой холоднокатанный</div>
                                        <div class="download-elem__type">PDF, 350 Кб</div>
                                        <div class="download-elem__icon">
                                            <svg class="icon icon-download ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-download"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="download-elem__content">
                                        <p>Настоящий стандарт распространяется на листовой холоднокатаный прокат шириной 500мм и более, изготовляемый в листах, толщиной от 0,35 до 5,00мм, рулонах толщиной от 0,35 до 3,50мм.</p>
                                    </div></a><a class="download-elem" href="#">
                                    <div class="download-elem__header">
                                        <div class="download-elem__title">ГОСТ 5632-72Стали высоколегированные и сплавы коррозийно-стойкие, жаростойкие и жаропрочные</div>
                                        <div class="download-elem__type">PDF, 350 Кб</div>
                                        <div class="download-elem__icon">
                                            <svg class="icon icon-download ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-download"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="download-elem__content">
                                        <p>Настоящий стандарт распространяется на деформируемые стали и сплавы на железоникелевые и сплавы на никелевых основах, предназначенные для работы в коррозионно-активных средах и при высоких температурах. К высоколегированным сталям условно отнесены сплавы, массовая доля железа в которых более 45%, а суммарная массовая доля легирующих элементов не менее 10%, считая по верхнему пределу, при массовой доле одного их элементов не менее 8% по нижнему пределу. К сплавам на железоникелевой основе отнесены сплавы, основная структура которых является твердым раствором хрома и других легирующих элементов в железоникелевой основе (сумма никеля и железа более 65% при приблизительном отношении никеля к железу 1:1,5)</p>
                                    </div></a></div>
                        </div>
                    </div>
                    <div class="page-group">
                        <div class="vacancies">
                            <div class="faq">
                                <div class="faq-elem">
                                    <div class="faq-elem__header js-faqToggle">
                                        <div class="faq-elem__title">Менеджер по продажам</div>
                                        <div class="faq-elem__subtitle">от 50 000 руб./мес</div>
                                        <svg class="icon icon-dropdown ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                        </svg>
                                    </div>
                                    <div class="faq-elem__content">
                                        <p>Требуемый опыт работы: 1–3 года <br>Полная занятость, полный день</p>
                                        <p><b>Требования:</b></p>
                                        <ul>
                                            <li>Высшее/неоконченное высшее образование (менеджмент, маркетинг, экономика, психология, управление, прикладная информатика, перерабатывающие технологии, плодоовощеводство, инженерные специальности);</li>
                                            <li>Высокие коммуникативные способности, высокий уровень эрудированности, обучаемость;</li>
                                            <li>Инициативность, стрессоустойчивость, пунктуальность, аналитический склад ума, грамотная речь, целеустремленность, уверенный пользователь ПК.</li>
                                        </ul>
                                        <p><b>Обязанности:</b></p>
                                        <ul>
                                            <li>Выполнение плановых качественных и количественных показателей работы;</li>
                                            <li>Управление отношениями с контрагентами;</li>
                                            <li>Оформление и заключение договоров с контрагентами;</li>
                                            <li>Контроль выполнения договорных обязательств и условий работы с контрагентами;</li>
                                            <li>Документооборот;</li>
                                            <li>Контроль выполнения поставок товаров партнерам в соответствии с заказами в рамках имеющихся планов и 		      заключенных договоров;</li>
                                            <li>Рекламационная работа;</li>
                                            <li>Участие в профильных выставках.</li>
                                        </ul>
                                        <p><b>Условия:</b></p>
                                        <ul>
                                            <li>График работы: с 8:00 до 17:00; 5/2;</li>
                                            <li>Оформление согласно ТК РФ;</li>
                                            <li>Официальная заработная плата;</li>
                                            <li>Интересная работа в команде профессионалов;</li>
                                            <li>Корпоративное обучение;</li>
                                            <li>Возможность личностного, профессионального и карьерного роста;</li>
                                            <li>Конкурентный уровень заработной платы.</li>
                                        </ul>
                                        <p><b>Работа в нашей компании это:</b></p>
                                        <ul>
                                            <li>Достойный заработок – мы высоко ценим профессионализм наших сотрудников</li>
                                            <li>Социальные гарантии – оформление по трудовой книжке, индексируемая и своевременная заработная плата, отпуск 28 календарных дней, больничные в соответствии с трудовым кодексом.</li>
                                            <li>Обучение и развитие – сотрудникам компании предоставляется возможность регулярно посещать тренинги и семинары, совершенствовать свои знания, навыки и умения.</li>
                                            <li>Доброжелательный коллектив и атмосфера – наши сотрудники - это большая сплоченная команда единомышленников. Мы работаем в теплой дружеской атмосфере, где каждый при необходимости готов помочь и поддержать друг друга.</li>
                                        </ul>
                                        <p>Мы ищем амбициозных, коммуникабельных и активных, нацеленных на результат и желающих зарабатывать!</p>
                                        <div class="faq-elem__footer">
                                            <div class="faq-elem__button"><a class="button style_green" href="#"><span class="button__text">Отправить резюме</span></a></div>
                                            <div class="faq-elem__close js-faqClose">Свернуть</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}