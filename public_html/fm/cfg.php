<?php

/**
 * image method ( crop / resize / no_change )
 */
return [

    //    MYSQL config
    'dsn'       => 'mysqli://webmaster:A*054SDgasdasd79@127.0.0.1:3306/dmg_php?encoding=utf8&table_prefix=dmg__',
    'dsn_local' => 'mysqli://root:123@127.0.0.1:3306/dmg-php?encoding=utf8&table_prefix=dmg__',

    //     Defaults
    'app'       => [
        'storage_dir' => 'f/',
    ],

    //    Files config
    'file'      => [
        '0' => [
            'allow'      => '',
            'disallow'   => '',
            'count'      => 0,
            'prefix_dir' => 'default_files',
        ],

        'gost' => [
            'allow'      => 'pdf,doc,docx,xls,xlsx',
            'disallow'   => '',
            'count'      => 0,
            'prefix_dir' => 'pdf',
        ],

        'docs_files' => [
            'allow'      => 'jpg,png,jpeg,pdf,doc,docx,xls,xlsx',
            'disallow'   => '',
            'count'      => 0,
            'prefix_dir' => 'docs',
        ],

    ],

    //    Images config
    'image'     => [
        '0' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'default',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width' => 0,
                ],
            ],
        ],

        'svg' => [
            'allow'      => 'svg,jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'category',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 200,
                    'height' => 1000,
                    'method' => 'no_change',
                ],
            ],
        ],

        'article_photo' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'article',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 340,
                    'height' => 340,
                    'method' => 'crop',
                ],
            ],
        ],

        'category1_icon' => [
            'allow'      => 'jpg,gif,png,svg',
            'disallow'   => '',
            'prefix_dir' => 'category1',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 340,
                    'height' => 340,
                    'method' => 'no_change',
                ],
            ],
        ],

        'category2_icon' => [
            'allow'      => 'jpg,gif,png,svg',
            'disallow'   => '',
            'prefix_dir' => 'category1',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 340,
                    'height' => 340,
                    'method' => 'no_change',
                ],
            ],
        ],


        'product_photo' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'prod',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 400,
                    'height' => 400,
                    'method' => 'resize',
                ],
                'b' => [
                    'width'  => 982,
                    'height' => 982,
                    'method' => 'resize',
                ],
            ],
        ],

        'product_gallery' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'prod',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 1140,
                    'height' => 760,
                    'method' => 'resize',
                ],
            ],
        ],

        'docs_preview' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'prod',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 300,
                    'height' => 1000,
                    'method' => 'resize',
                ]
            ],
        ],

        'series_photo' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'series',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 200,
                    'height' => 1000,
                    'method' => 'no_change',
                ],
            ],
        ],

        'series_bg' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'series',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 200,
                    'height' => 1000,
                    'method' => 'no_change',
                ],
            ],
        ],

        'video_preview' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'video_preview',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 200,
                    'height' => 1000,
                    'method' => 'no_change',
                ],
            ],
        ],

        'news_photo' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'news',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 750,
                    'height' => 750,
                    'method' => 'resize',
                ],
            ],
        ],

        'news_gallery' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'news',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 1140,
                    'height' => 760,
                    'method' => 'resize',
                ],
            ],
        ],

        'news_gallery2' => [
            'allow'      => 'jpg,gif,png',
            'disallow'   => '',
            'prefix_dir' => 'news',
            'count'      => 0,
            'thubms'     => [
                'a' => [
                    'width'  => 300,
                    'height' => 300,
                    'method' => 'resize',
                ],
                'b' => [
                    'width'  => 300,
                    'height' => 300,
                    'method' => 'no_change',
                ],
            ],
        ],
    ],
];
