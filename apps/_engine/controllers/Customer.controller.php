<?php

class Customer_Controller extends ZObject_Controller
{
    /**
     * Главная страница покупателя
     * История заказов
     * /customer/
     */
    public function action_index()
    {
        $customer = new Customers();

        //  Если не авторизован/не активен, редиректим на авторизацию
        if (!$customer->isAuth()) {
            redirect('/customer/:customer_auth');
        }

        //  Текущий покупатель
        $currentCustomer = $customer->getCustomer();

        //  Выбираем заказы
        $orders = [];
        $res    = Q(
            'SELECT `id`, `title`, `date`, `status`, `cart_json` FROM @@orders WHERE `customer`=?i ORDER BY `id` DESC',
            [
                $currentCustomer['id'],
            ]
        );

        while ($r = $res->each()) {
            $r['cart_json'] = json_decode($r['cart_json'], true);
            $orders[]       = $r;
        }
        RS('orders', $orders);

        //  Статусы
        $config = $this->config = import::config('app:configs/zsettings/orders.php');
        $status = @$config->fields['status']['items'];
        RS('order_status', $status);

        RS('withoutPreloader', true);
        V('customer/history');
    }

    /**
     * Авторизация.
     * /customer/:customer_auth
     */
    public function action_customerAuth()
    {
        $error    = '';
        $customer = new Customers();

        //  Если уже авторизованы, редиректим на гланую
        if ($customer->isAuth()) {
            redirect('/customer/');
        }

        //  Пробуем авторизоваться
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            try {
                $customer->doAuth(
                    _post('email'),
                    _post('password')
                );

                //  Если указан backUrl, то редиректим туда
                if (isset($_SESSION['backUrl'])) {
                    $url = $_SESSION['backUrl'];
                    unset($_SESSION['backUrl']);
                    redirect($url);
                } //  На главную покупателя
                else {
                    redirect('/customer/');
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                RS('customer_error', $error);
            }
        }

        RS('withoutPreloader', true);
        V('customer/auth');
    }

    /**
     * Выход
     * /customer/:customer_logout
     */
    public function action_customerLogout()
    {
        $customer = new Customers();
        $customer->doLogout();

        redirect('/customer/');
    }

    /**
     * Генерируем ссылку для восстановления пароля
     * /customer/:customer_reset
     */
    public function action_customerReset()
    {
        $customer = new Customers();

        //  Пробуем сбросить пароль
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            try {
                //  Ищем пользователя по email
                $customer->doReset(
                    _post('email')
                );

                //  Склеиваем ссылку для восстановления
                $resetUrl = $_SERVER['HTTP_HOST']."/customer/:customer_new_password?code=".$customer->getCustomer()['reset_url'];

                //  Отправляем письмо со ссылко для сброса пароля
                $sm = SM();
                $sm->assign('date', $this->prepareDate(date("d.m.Y H:i")));
                $sm->assign('resetUrl', $resetUrl);
                $cont = $sm->fetch("mail/customer_reset.tpl");

                //    Include PHP mailer
                require_once SHARED_PATH.'/mmailer.php';
                $mm = mmailer();
                $mm->addAddress($customer->getCustomer()['email']);

                $mm->Subject = "Восстановление пароля";
                $mm->Body    = $cont;

                $rrr = $mm->send();

                redirect('/customer/:customer_reset_send');
            } catch (Exception $e) {
                $error = $e->getMessage();
                RS('customer_error', $error);
            }
        }

        RS('withoutPreloader', true);
        V('customer/reset');
    }

    /**
     * Ссылка для восстановления пароля отправлена
     * /customer/:customer_reset_send
     */
    public function action_customerResetSend()
    {
        RS('withoutPreloader', true);
        V('customer/reset_send');
    }

    /**
     * Смена пароля
     * /customer/:customer_new_password
     */
    public function action_customerNewPassword()
    {
        $customer = new Customers();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            try {
                //  Создаем новый пароль для покупателя
                $customer->doNewPassword(
                    _get('code'),
                    _post('password1'),
                    _post('password2')
                );

                //  Успешно сменили пароль
                redirect('/customer/:customer_reset_success');
            } catch (Exception $e) {
                $error = $e->getMessage();
                RS('customer_error', $error);
            }
        }

        RS('withoutPreloader', true);
        V('customer/new_password');
    }

    /**
     * Смена пароля прощла успешно
     * /customer/:customer_reset_success
     */
    public function action_customerResetSuccess()
    {
        RS('withoutPreloader', true);
        V('customer/reset_success');
    }

    /**
     * Регистрация
     * /customer/:customer_registry
     */
    public function action_customerRegistry()
    {
        $error    = null;
        $customer = new Customers();

        //  Регистрируем пользователя
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            //  Сохранение в сиссию данных
            $_SESSION['post'] = $_POST;

            try {
                //  Обязательные поля для заполнения
                if (_post('type') == 1 && !_post('title')) {
                    throw new Exception('Укажите название компании', 3);
                }
                if (_post('type') == 1 && !_post('inn')) {
                    throw new Exception('Укажите ИНН', 4);
                }
                if (_post('type') == 1 && !_post('city')) {
                    throw new Exception('Укажите город', 5);
                }

                //  Регистрируем нового покупателя
                $newCustomer = $customer->doRegistry(
                    _post('email'),
                    _post('password1'),
                    _post('password2')
                );

                //  Сохраняем персональные данные покупателя
                $customer->doUpdate(
                    $newCustomer['id'],
                    $_POST
                );

                // $newCustomer = $customer->loadCustomer(5);

                //  Склеиваем ссылку для активации аккаунта
                $actiovationUrl = $_SERVER['HTTP_HOST']."/customer/:customer_activate?code=".$newCustomer['actiovation_url'];

                //  Если нужно вернуться к заказу после активации аккаунта
                if (isset($_SESSION['backUrl'])) {
                    $actiovationUrl .= '&backToOrder=1';
                }

                //  Рендерим письмо
                $sm = SM();
                $sm->assign('date', $this->prepareDate(date("d.m.Y H:i")));
                $sm->assign('customer', $newCustomer);
                $sm->assign('actiovationUrl', $actiovationUrl);
                $cont = $sm->fetch("mail/customer_activate.tpl");

                //    Include PHP mailer
                require_once SHARED_PATH.'/mmailer.php';
                $mm          = mmailer();
                $mm->Subject = "Подтверждение регистрации";
                $mm->addAddress($newCustomer['email']);
                $mm->Body = $cont;
                $rrr      = $mm->send();

                //  Удаляем данные из сессии
                unset($_SESSION['post']);

                //  Сохраняем в сессии пользователя
                $customer->loadCustomer($newCustomer['id'], true);

                redirect('/customer/:customer_activation_waiting');

            } catch (CustomerException $e) {
                RS('customer_error', $e->getMessage());
                RS('customer_error_code', $e->getCode());
            } catch (Exception $e) {
                RS('customer_error', $e->getMessage());
                RS('customer_error_code', $e->getCode());
            }
        }

        RS('withoutPreloader', true);
        V('customer/registry');
    }

    /**
     * Активация аккаунта
     * /customer/:customer_activate
     */
    public function action_customerActivate()
    {
        $customer = new Customers();

        try {
            //  Пробуем активировать пользователя
            $activatedCustomer = $customer->doActivation(
                _get('code')
            );

            //  Если покупатель регистрировался при заказе
            //  то возвращаем его обратно к заказу
            //  и сразу авторизуем в системе
            if (_get('backToOrder')) {
                //  Авторизуем покупателя
                $customer->setCustomer($activatedCustomer);

                //  Редирект на заказ
                redirect('/order/:step2');
            } else {
                //  Редирект на сообщение об успешной регистрации
                redirect('/customer/:customer_activation_success');
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            RS('customer_error', $error);
        }

        RS('withoutPreloader', true);
        V('customer/activation');
    }

    /**
     * Активация аккаунта сообщение об отправленом письме
     * /customer/:customer_activation_waiting
     */
    public function action_customerActivationWaiting()
    {
        V('customer/activation_waiting');
    }

    /**
     * Активация аккаунта завершена
     * /customer/:customer_activation_success
     */
    public function action_customerActivationSuccess()
    {
        V('customer/activation_success');
    }

    /**
     * Авторизация.
     * /customer/:customer_personal
     */
    public function action_customerPersonal()
    {
        $customer = new Customers();

        //  Если не авторизован/не активен, редиректим на авторизацию
        if (!$customer->isAuth()) {
            redirect('/customer/:customer_auth');
        }

        //  Текущий покупатель
        $currentCustomer = $customer->getCustomer();

        //  Сохраняем в сессии пользователя
        $currentCustomer = $customer->loadCustomer($currentCustomer['id'], true);

        try {
            //  Сохраняем персональные данные покупателя
            $r = $customer->doUpdate(
                $currentCustomer['id'],
                $_POST
            );

            //  Сохраняем в сессии пользователя
            $currentCustomer = $customer->loadCustomer($currentCustomer['id'], true);

        } catch (Exception $e) {
            RS('customer_error', $e->getMessage());
            RS('customer_error_code', $e->getCode());
        }

        //  Выбираем заказы, количество заказов
        $orders = [];
        $res    = Q(
            'SELECT `id` FROM @@orders WHERE `customer`=?i ORDER BY `id` DESC',
            [
                $currentCustomer['id'],
            ]
        );

        while ($r = $res->each()) {
            $orders[] = $r;
        }
        RS('orders', $orders);
        RS('customer', $currentCustomer);
        V('customer/personal');
    }

}
