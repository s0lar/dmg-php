<?php
return array(
    # types of fields
    'fields' => array(
        'type'     => array(
            'type'  => 'list',
            'items' => array(
                0 => 'Физлицо',
                1 => 'Юрлицо',
            ),
        ),
        'title'    => 'string',
        'addr'     => 'string',
        'inn'      => 'string',
        'kpp'      => 'string',
        'person'   => 'string',
        'email'    => 'string',
        'phone'    => 'string',
        'fax'      => 'string',
        'city'     => 'string',
        'discount' => 'string',
        'active'   => 'checkbox',
        'opt'      => 'checkbox',
    ),

    # labels of fields
    'ui'     => array(
        'type'     => 'Тип организации',
        'title'    => 'Название компании',
        'addr'     => 'Юридический адрес',
        'inn'      => 'ИНН',
        'kpp'      => 'КПП',
        'person'   => 'Контактное лицо',
        'email'    => 'E-Mail',
        'phone'    => 'Телефон',
        'fax'      => 'Факс',
        'city'     => 'Город',
        'discount' => 'Скидка %',
        'active'   => 'Активен',
        'opt'      => 'Оптовые цены',
    ),

    #
    // 'input_cfg' => array(
    //     'menu' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'photo' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'background' => array('type' => 'image', 'mask' => 'series_bg'),
    // ),

    # node configuration
    'node'   => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-auto',
    ),

    #
    'view'   => array(
        'mode'       => 'list',
        'fields'     => array('type', 'title', 'person', 'email', 'phone', 'city', 'opt'),
        'orderby'    => ' id DESC ',
        'edit_field' => 'title',
        'limit'      => 150,
    ),

    # labels for actions
    'labels' => array(
        'list'    => 'Покупатели',
        'add'     => 'Новый',
        'adding'  => 'Создание',
        'edit'    => 'Редактировать',
        'editing' => 'Редактирование',
        'delete'  => 'Удалить',
    ),
);
