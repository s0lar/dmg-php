<?php

if (php_sapi_name() !== 'cli') {
    exit('not cli');
}

//  Imports
$config = require_once __DIR__ . '/../apps/_engine/configs/cfg.php';
require_once __DIR__ . '/../apps/shared/Q3i/Q.php';
require_once __DIR__ . '/../apps/shared/helpers.php';
require_once __DIR__ . '/helpers.php';
require_once __DIR__ . '/class/Logger.php';
require_once __DIR__ . '/class/Abstract1cXml.php';
require_once __DIR__ . '/class/Import1cXml.php';
require_once __DIR__ . '/class/Offers1cXml.php';

//  Base connection
QF($config['db']['dsn'])->connect()->alias('default');

$fileImport = __DIR__ . '/xml/import.xml';
$fileOffers = __DIR__ . '/xml/offers.xml';


try {
    $logger   = new Logger();
    $logger->info('Start import');
    $imported = 'imported-' . date('Y.m.d_H:i:s') . '.xml';

    //  Check files
    if (!file_exists($fileImport)) {
        throw new Exception('File not found: ' . $fileImport);
    }
    if (!file_exists($fileOffers)) {
        throw new Exception('File not found: ' . $fileOffers);
    }

    //  Clear exists products
    Q('TRUNCATE TABLE @@product');
    Q('DELETE FROM @@nodes WHERE object_type="product"');

    //  Imports.XML
    $import1c = new Import1cXml(
        simplexml_load_file($fileImport),
        $logger
    );
    $import1c->execute();
    rename($fileImport, $fileImport . $imported);

    //  Offers.XML
    $offers1c = new Offers1cXml(
        simplexml_load_file($fileOffers),
        $logger
    );
    $offers1c->execute();
    rename($fileOffers, $fileOffers . $imported);

} catch (\Throwable $exception) {
    $logger->error($exception->getMessage());
}


//  Close sql connection
Q()->disconnect();
$logger->info('Close MySQL connection');
$logger->info('End import');
exit;