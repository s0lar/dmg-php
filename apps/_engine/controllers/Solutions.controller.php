<?php

class Solutions_Controller extends ZObject_Controller
{
    public function action_index()
    {
        if ($this->node['object_type'] == 'sections') {
            RS('articlesList', $this->getList());
            V('articlesList');
        } else {
            RS('articlesItem', $this->getItem());
            RS('articlesMenu', $this->getArticlesMenu());
            V('articlesItem');
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getList()
    {
        $list = [];

        //  Постраничная разбивка
        $sql = Qb('SELECT 1 FROM @@solutions ORDER BY `sort` DESC');
        $pg  = PG($sql, 8);
        RS('pg', $pg);

        //  Загружем найденые товары
        $res = Q('SELECT N.`path`, P.* FROM @@solutions as P
                    LEFT JOIN @@nodes as N
                    ON P.id = N.object_id
                    WHERE N.object_type="solutions"
                    ORDER BY P.sort DESC
                    LIMIT ?i, ?i', [
                $pg['offset'],
                $pg['limit'],
            ]
        );
        while ($r = $res->each()) {
            $r['photo'] = $this->getFM($r['photo'], 'a');
            $list[]     = $r;
        }

        return $list;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function getItem()
    {
        $item = Q('SELECT * FROM @@solutions WHERE id=?i', [
            $this->node['object_id'],
        ])->row();

        $item['photo'] = $this->getFM($item['photo'], 'a');

        return $item;
    }

    /**
     * @return mixed
     */
    private function getArticlesMenu()
    {
        return Q('SELECT N.`path`, P.title FROM @@solutions as P
                    LEFT JOIN @@nodes as N
                    ON P.id = N.object_id
                    WHERE N.object_type="solutions"
                    ORDER BY P.sort DESC'
        )->all();
    }

}

