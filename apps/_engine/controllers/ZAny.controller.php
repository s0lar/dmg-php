<?php

@session_start();

class ZAny_Controller
{
    public $node; // hold data from @@nodes about current node
    public $bc;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function __construct($node)
    {
        $this->node = $node;
        $url        = RQ()->url;

        //    FRONT END
        if (BACKEND === false) {
            //    SEO
            $u            = "http://" . $url->host['raw'] . $url->path;
            $seo          = Q("SELECT `id`, `title`, `h1`, `key`, `desc`, `text`, `text2` FROM @@seo WHERE `node_id`=?i",
                [$this->node['id'] ?? null])->row();
            $seo['title'] = stripcslashes(@$seo['title']);
            $seo['h1']    = stripcslashes(@$seo['h1']);
            $seo['key']   = stripcslashes(@$seo['key']);
            $seo['desc']  = stripcslashes(@$seo['desc']);
            $seo['text']  = stripcslashes(@$seo['text']);
            $seo['text2'] = stripcslashes(@$seo['text2']);
            RS("seo", $seo);

            RS("title", strip_tags($this->node['object_title'] ?? ''));
            RS("node", $this->node);

            //    CSRF
            $this->CSRF('csrf_feedback');
            $this->CSRF('csrf_auth');
            
//
//            //  Share
//            $shareLink = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $url->path;
//            RS('shareLink', urlencode($shareLink));
//            RS('shareLinkRaw', $shareLink);
//            RS('shareTitle', urlencode($this->node['object_title']));

            //  Show popup only once
            if (!isset($_SESSION['show_popup'])) {
                $_SESSION['show_popup'] = 1;
            } else {
                $_SESSION['show_popup'] = 0;
            }

            //  Cart
            $c = new Cart();
            RS("cart", $c->cart);
            RS("bc", $this->getBC());
            RS("menu_category", $this->getCategory());
            RS("menu", $this->getMenu());
            RS("contacts", $this->getContacts());
            RS('sub_menu', $this->getParentMenu());
            RS('cfg', $this->getZcfg());
            RS('authorized', !empty($_SESSION['user']));
            RS('authorized', true);
            //
        } else {
            RS("bc", $this->getBC());
        }

        RS("env", $this->getEnv());
    }

    /**
     * @return string
     */
    public function getEnv()
    {
        $result = preg_match('#(\.lo)$#', $_SERVER['HTTP_HOST']);
        return ($result === 1) ? 'dev' : 'prod';
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    CSRF
    public function CSRF($name)
    {
        if (!isset($_SESSION[$name])) {
            $_SESSION[$name] = md5('*SALT*' . rand());
        }

        return $_SESSION[$name];
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * @return mixed
     */
    public function getSubMenu()
    {
        $menu = [];
        $res  = Q('SELECT N.path, N.object_title as title, S.icon FROM @@nodes as N 
            LEFT JOIN @@sections as S
            ON S.id = N.object_id
            WHERE N.object_type="sections" and N.parent_id=?i
            ORDER BY N.sort', [
            $this->node['id'],
        ]);

        while ($r = $res->each()) {
            $r['icon'] = $this->getFM($r['icon'], 'a');
            $menu[]    = $r;
        }

        return $menu;
    }

    /**
     * @return mixed
     */
    public function getParentMenu()
    {
        return Q('SELECT path, object_title as title FROM @@nodes WHERE parent_id=?i ORDER BY sort', [
            $this->node['parent_id'] ?? null,
        ])->all();
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        $menu = [];

        $res = Q('SELECT id, parent_id, path, object_title as title FROM @@nodes WHERE in_cat=1 ORDER BY sort', [
            $this->node['id'] ?? null,
        ]);

        while ($r = $res->each()) {
            $menu[$r['parent_id']][$r['id']] = $r;
        }

        return $menu;
    }

    public function getContacts()
    {
        $contacts = [];
        $res      = Q('SELECT N.`path`, N.`name` as `id`, P.title as city, P.id as cid, P.addr, P.addr_full, P.phone, P.lat_lon, P.link, P.email, P.description FROM @@contacts as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="contacts"
                        ORDER BY P.sort DESC');

        while ($r = $res->each()) {
            $r['phone']    = explode("\r\n", $r['phone']);
            $r['location'] = explode(',', $r['lat_lon']);
            $contacts[]    = $r;
        }

        return $contacts;
    }

    public function getCategory()
    {
        //  Get all Categories
        $category = [];
        $res      = Q('SELECT N.`path`, N.`parent_id` as `nodes_pid`, N.`id` as `nodes_id`, P.`id`, P.`title`, P.`icon`, P.`icon2` FROM @@category1 as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="category1"
                        ORDER BY P.`sort` desc');

        while ($r = $res->each()) {
            $r['icon']  = $this->getFM($r['icon'], 'a');
            $r['icon2'] = $this->getFM($r['icon2'], 'a');

            $category[0][$r['id']] = $r;
        }

        //  Category
        $res = Q('SELECT N.`path`, N.`parent_id` as `nodes_pid`, N.`id` as `nodes_id`, P.`id`, P.`title`, P.`icon` FROM @@category2 as P
                        LEFT JOIN @@nodes as N
                        ON P.id = N.object_id
                        WHERE N.object_type="category2"
                        ORDER BY P.`sort` desc');

        while ($r = $res->each()) {
            $r['icon'] = $this->getFM($r['icon'], 'a');

            $category[$r['nodes_pid']][$r['id']] = $r;
        }

        return $category;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getZcfg($name = '')
    {
        $arr = [];

        if ($name == '') {
            $res = Q("SELECT `name`, `value` FROM @@zcfg WHERE `is_global`=1");
        } else {
            $res = Q("SELECT `name`, `value` FROM @@zcfg WHERE `name`=?s", [$name]);
        }

        while ($r = $res->each()) {
            $arr[$r['name']] = $r['value'];
        }

        return $arr;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Default action of any controller
     *
     * @return
     */
    public function action_index()
    {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getBC()
    {
        $url  = RQ()->url;
        $res  = [];
        $tmp  = [];
        $path = '/';
        foreach ($url->segments as $k => $v) {
            $path    .= $v . '/';
            $tmp[$k] = $path;
        }

        $sql = Qb("SELECT id, parent_id, name, path, object_title FROM @@nodes WHERE path IN (?ls) ", [$tmp]);
        $res = Q($sql);

        $arr = [];
        while ($r = $res->each()) {
            $r['object_title'] = stripcslashes($r['object_title']);
            $arr[]             = $r;
        }

        return $arr;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getFM($json, $thumb = '')
    {
        if ($json == '') {
            return $json;
        }

        $arr = json_decode(stripslashes($json), true);
        if (empty($arr)) {
            return [];
        }

        if ($thumb != '') {
            if (isset($arr['files'][0]['fs'][$thumb])) {
                return $arr['files'][0]['fs'][$thumb];
            } else {
                return false;
            }
        }

        return isset($arr['files']) ? $arr['files'] : [];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getMonthName($m)
    {
        $_m = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];

        //
        return $_m[$m];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function prepareDate($d, $type = 1)
    {
        $_m = [
            '01' => 'Января',
            '02' => 'Февраля',
            '03' => 'Марта',
            '04' => 'Апреля',
            '05' => 'Мая',
            '06' => 'Июня',
            '07' => 'Июля',
            '08' => 'Августа',
            '09' => 'Сентября',
            '10' => 'Октября',
            '11' => 'Ноября',
            '12' => 'Декабря',
        ];

        $_mm = [
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря',
        ];

        $_w = [
            '1' => 'Пн',
            '2' => 'Вт',
            '3' => 'Ср',
            '4' => 'Чт',
            '5' => 'Пт',
            '6' => 'Сб',
            '7' => 'Вс',
        ];

        $_ww = [
            '1' => 'Понедельник',
            '2' => 'Вторник',
            '3' => 'Среда',
            '4' => 'Четверг',
            '5' => 'Пятница',
            '6' => 'Суббота',
            '7' => 'Воскресенье',
        ];

        $time = strtotime($d);
        $ret  = [];

        if ($type == 2) {
            //    Format: Пятница, 30 мая 2014
            $ret = $_ww[date("N", $time)] . ", " . date("d", $time) . " " . $_mm[date("m", $time)] . " " . date("Y",
                    $time);
        } else {
            //    Format: 1 января 9:00
            $ret['date'] = date("d", $time) . " " . $_mm[date("m", $time)];
            $ret['time'] = date("H:i", $time);
            $ret['year'] = date("Y", $time);
        }

        return $ret;
    }

    /**
     * @param $data
     */
    protected function jsonSuccess($data)
    {
        $this->jsonResponse(array_merge(
            ['success' => true],
            $data
        ));
    }

    /**
     * @param $data
     */
    protected function jsonError($data)
    {
        $this->jsonResponse(array_merge(
            ['success' => false],
            ['error' => $data]
        ));
    }

    /**
     * @param $data
     */
    protected function jsonResponse($data)
    {
        exit(json_encode($data));
    }


    public function getContactsEmail(int $id)
    {
        $res = Q('SELECT email FROM @@contacts WHERE id=?i', [$id])->row();

        return $res['email'] ?? '';
    }
}
