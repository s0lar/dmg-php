<?php
/*
 * X fw plugin
 * -------------------------------------------------------------
 * File:     modifier.view.php
 * Type:     modifier
 * Name:     view
 * Purpose:  apply view to url
 * -------------------------------------------------------------
 */
function smarty_modifier_view($url, $view)
{
	$buf = clone $url;
	$buf->view = $view;
    return $buf->build();
}
?>