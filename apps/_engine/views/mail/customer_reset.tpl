{extends file='mail/layout.tpl'}

{block name=content}
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr><td class="offset-double" style="height: 60px;line-height: 60px;">&nbsp;</td></tr>
	<tr>
		<td>
			<div style="font-weight: bold; font-size: 24px;" class="title">Восстановление пароля для сайта <br /><a target="_blank" style="color:#0076ff" href='http://{$smarty.server.HTTP_HOST}'>{$smarty.server.HTTP_HOST}</a></div>
		</td>
	</tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Запрошена ссылка для восстановления пароля. Перейдите по ссылке, чтоыб задать новый пароль<br /></span>
        <div style="width:400px;overflow:hidden;word-wrap:break-word;">
            <a target="_blank" style="color:#0076ff" href="http://{$resetUrl}">
                {$resetUrl}
            </a>
        </div>
    </td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
	<tr><td><span style="font-weight: bold;">Если Вы не запрашивали восстановление пароля &mdash; проигнорируйте это письмо</span></td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>


</table>
{/block}