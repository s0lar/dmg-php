{include file='zobject/header.tpl' inline='true'}

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="#">{$labels.adding}</a></li>
	</ul>



	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		
		{* Add + Add message *}
		{if isset($smarty.get.msg)}
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">
					&times;
				</button>
				<h4>Запись успешно добавлена</h4>
				Чтобы просмотреть добавленную запись, перейдите по <a title="Откроется в новой вкладке" target="_blank" href="/zsettings/:edit/-m/{$module}/-id/{$smarty.get.last}/">ссылке</a>.
			</div>
		{/if}
		
		<fieldset class="group">
			<!-- <legend class="group-title">{$object.labels.adding}</legend> -->
			<form action="/zsettings/:add/-m/{$module}/" method="post" class="ajax1" id="object-add">
				
				{foreach item=i key=k from=$object.ui}
					<div class="row-fluid border-bottom1 mb10" style="margin-bottom:15px">
						<label class="span3 f14  mb0" for="{$k}" style="min-height:20px">{$i}</label>
						
						{if $object.fields[$k] eq 'string'}
							<input type="text" name="{$k}" id="{$k}" class="input-text span6">
						
						{elseif $object.fields[$k] eq 'int' || $object.fields[$k] eq 'float'}
								<input type="text" name="{$k}" id="{$k}" class="input-text span3">

						{elseif $object.fields[$k] eq 'datetime'}
							<input type="text" name="{$k}" id="{$k}" value="{date('Y-m-d H:i:s')}" class="input-text span3 datepick_datetime">
							
						{elseif $object.fields[$k] eq 'date'}
							<input type="text" name="{$k}" id="{$k}" value="{date('Y-m-d')}" class="input-text span3 datepick_date">
						
						{elseif $object.fields[$k] eq 'text'}
							<textarea name="{$k}" id="{$k}" class="input-textarea span12"></textarea>
						
						{elseif $object.fields[$k] eq 'html'}
						<div style="clear:both">
							<textarea name="{$k}" id="{$k}" class="input-textarea span12 visual"></textarea>
							{*<label><input type="checkbox" onclick="tinymce.execCommand('mceToggleEditor',false,'{$k}');" /> &mdash; включить редактор</label>*}
						</div>
						
						{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'pointer'}
							{include file='zobject/types-views/pointer.tpl' field=$k data=$object.data[$k] inline='true'}
						
						{elseif $object.fields[$k] eq 'mn'}
							{foreach item=j from=$object.data[$k].object}
								<input type="checkbox" name="{$k}[{$j.id}]" id="{$k}-{$j.id}"><label for="{$k}-{$j.id}">{$j.display}</label>
							{/foreach}
							
						{elseif $object.fields[$k] eq 'gs_file'}
							{include file='zobject/types-views/file.tpl' field=$k inline='true'}
						
						{elseif $object.fields[$k] eq 'checkbox'}
							<input type="checkbox" name="{$k}" id="{$k}" />
	
						
						{elseif isset($object.fields[$k].type) && isset($object.fields[$k].type) && $object.fields[$k].type eq 'list'}
							{include file='zobject/types-views/list.tpl' field=$k data=$object.data[$k] inline='true'}					
						
						
						{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'mn2'}
							{include file='zobject/types-views/mn2.tpl' field=$k data=$object.data[$k] inline='true'}					
						
						{/if}
					</div>
				{/foreach}
				
				
				
				<div class="well well-panel well-panel-controll">
					<div class="btn-group">
						<input type="submit" name="" value="Добавить" class="btn btn-primary" />
						<input type="submit" name="apply" value="Применить" class="btn" />
						<input type="submit" name="add_add" value="Добавить + добавить" class="btn" />
					</div>
					<a href="/zsettings/:edit_node/-m/{$module}/" class="btn btn-small pull-right">Отмена</a>
				</div>
				
			</form>
		</fieldset>
		

	</div>
{include file='zobject/footer.tpl' inline='true'}