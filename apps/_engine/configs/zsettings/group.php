<?php

return [
    # types of fields
    'fields' => [
        'title' => 'string',
        'sort'  => 'int',
    ],

    # labels of fields
    'ui'     => [
        'title' => 'Название',
        'sort'  => 'Порядок',
    ],

    #
    // 'input_cfg' => array(
    //     'menu' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'photo' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'background' => array('type' => 'image', 'mask' => 'series_bg'),
    // ),

    # node configuration
    'node'   => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-auto',
    ],

    #
    'view'   => [
        'mode'       => 'list',
        'fields'     => ['title', 'sort'],
        'orderby'    => ' sort DESC ',
        'edit_field' => 'title',
        'limit'      => 150,
    ],

    # labels for actions
    'labels' => [
        'list'    => 'Группы файлов',
        'add'     => 'Новый',
        'adding'  => 'Создание',
        'edit'    => 'Редактировать',
        'editing' => 'Редактирование',
        'delete'  => 'Удалить',
    ],
];
