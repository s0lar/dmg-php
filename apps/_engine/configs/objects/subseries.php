<?php
return array(
    # types of fields
    'fields' => array(
        'title' => 'string',
        'anons' => 'text',
        'desc' => 'code',
        'background' => 'gs_file',
        'photo' => 'gs_file',
        'category' => array(
            'type' => 'pointer',
            'object' => array(
                'table' => 'category',
                'fields' => 'id,title',
                'display' => '%title%',
            ),
        ),
        'sort' => 'int',
    ),

    # labels of fields
    'ui' => array(
        'title' => 'Заголовок',
        'category' => 'Категория',
        'anons' => 'Описание',
        'desc' => 'Характеристики',
        'photo' => 'Фото',
        'background' => 'Фоновая картинка',

        'sort' => 'Порядок',
    ),

    #
    'input_cfg' => array(
        'photo' => array('type' => 'image', 'mask' => 'series_photo'),
        'background' => array('type' => 'image', 'mask' => 'series_bg'),
    ),

    # node configuration
    'node' => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name' => '-auto',
    ),

    #
    'view' => array(
        'mode' => 'list',
        'fields' => array('title', 'sort'),
        'orderby' => ' `sort` DESC ',
        'edit_field' => 'title',
        'limit' => 150,
    ),

    # labels for actions
    'labels' => array(
        'list' => 'Категории',
        'add' => 'Добавить',
        'adding' => 'Добавление',
        'edit' => 'Редактировать',
        'editing' => 'Редактирование',
        'delete' => 'Удалить',
    ),
);
