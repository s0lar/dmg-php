<div class="modal-cart">
    <div class="modal-cart__top">
        <div class="modal-cart__title">{$product.info.title}</div>
        <div class="modal-cart__char">
            {if !empty($product.info.tech1)}
                <div class="modal-cart__char-el"><span>{$product.info.tech1_name|stripcslashes}: </span><span>{$product.info.tech1|stripcslashes}</span></div>
            {/if}
            {if !empty($product.info.tech2)}
                <div class="modal-cart__char-el"><span>{$product.info.tech2_name|stripcslashes}: </span><span>{$product.info.tech2|stripcslashes}</span></div>
            {/if}
            {if !empty($product.info.tech3)}
                <div class="modal-cart__char-el"><span>{$product.info.tech3_name|stripcslashes}: </span><span>{$product.info.tech3|stripcslashes}</span></div>
            {/if}
        </div>
    </div>
    <div class="modal-cart__main">
        <div class="modal-cart__price">{$product.info.cost_view}</div>
        <div class="modal-cart__count">{$product.count}</div>
        <div class="modal-cart__total">{$product.total_view} руб</div>
    </div>
    <div class="modal-cart__footer">
        <a class="button style_white" href="/order/">
            <span class="button__text">Оформить заказ</span>
        </a>
        <button class="button style_dark_green js-modalClose hidden-xs"><span class="button__text">Закрыть окно</span>
        </button>
    </div>
</div>