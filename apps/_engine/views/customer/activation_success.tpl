﻿{extends file='_layout.tpl'}

{block name=content}

<div class="main">
    <div class="page-head single_head">
        <div class="container">
            <div data-aos="fade-up" data-aos-delay="500" data-aos-duration="400" data-aos-easing="ease-out-cubic" class="page-head__desc">
                <div class="page-head__title">Регистрация</div>
            </div>
        </div>
    </div>
    <div class="page-main">
        <div class="container">
            <div class="page-auth">
                <div class="page-auth__left">
                    <div class="page-auth__form">
                        <div class="medium-title">Активация учетной записи</div>
                        <p>Вы успешно активировали учетную запись.</p>
                        <div class="form-buttons">
                            <a href="/customer/" class="button style_border_grey size_m">
                                <span class="button__text">Авторизоваться</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{/block}

{block name=footer}
{/block}