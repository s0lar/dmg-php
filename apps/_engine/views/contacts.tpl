﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_contact">
        <div class="contact">
            <div class="container">
                <div class="contact-container">
                    <div class="page-head">
                        <div class="page-head__top">
                            <div class="page-head__title">Контактная информация</div>
                        </div>
                    </div>
                    <div class="contact-tab">
                        <div class="tab">
                            <div class="tab-nav hidden-xs">
                                {foreach item=contact from=$contacts name=n}
                                    <div class="tab-nav__elem {if $smarty.foreach.n.first} is-active {/if}">
                                        <a class="tab-nav__link js-tabLink js-contactTabControl" href="#{$contact.id}">{$contact.city}</a>
                                    </div>
                                {/foreach}
                            </div>

                            <div class="tab-select visible-xs">
                                <div class="sort-select__control js-sortSelectControl">
                                    <div class="sort-select__button">
                                        <div class="sort-select__title js-sortSelectTitle">{$contacts[0].city}</div>
                                        <svg class="icon icon-sort-arrow-down ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-sort-arrow-down"></use>
                                        </svg>
                                    </div>
                                    <select class="sort-select__list js-sortSelectList js-tabSelect js-contactTabSelect">
                                        {foreach item=contact from=$contacts name=n}
                                            <option value="#{$contact.id}">{$contact.city}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>

                            <div class="tab-content">

                                {foreach item=contact from=$contacts name=n}
                                    <div id="{$contact.id}">
                                        <div class="contact-info">
                                            <div class="contact-info__col">
                                                <div class="contact-info__label">Тел.:</div>
                                                <div class="contact-info__phones">
                                                    {foreach item=number from=$contact.phone}
                                                        <div class="contact-info__phone">{$number}</div>
                                                    {/foreach}
                                                </div>
                                            </div>
                                            <div class="contact-info__col">
                                                <div class="contact-info__label">Адрес:</div>
                                                <div class="contact-info__address">{$contact.addr_full}</div>
                                            </div>
                                        </div>
                                        <div class="contact-text">
                                            {$contact.description}
                                        </div>

                                        <div class="contact-footer">
                                            <a class="button style_border_green justify_space_between" href="mailto:{$contact.email}">
                                                <span class="button__text">
                                                    {$contact.email}
                                                    <svg class="icon icon-mail ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-mail"></use>
                                                    </svg>
                                                </span>
                                            </a>
                                            <div class="hidden-xs">
                                                <a class="contact-footer__link js-feedbackModalLink" href="javascript:;">
                                                    Отправить заявку
                                                </a>
                                            </div>
                                            <div class="visible-xs">
                                                <a class="button style_green justify_space_between" href="javascript:;" data-scroll-to="#feedback">
                                                    <span class="button__text">
                                                        Отправить заявку
                                                        <svg class="icon icon-arrow-triangle ">
                                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                                        </svg>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="contact-social">
                                                <div class="social style_no_border">
                                                    <div class="social__elem">
                                                        <a class="social__link" href="{$cfg.link_instagram|default:'#'}">
                                                            <svg class="icon icon-instagram ">
                                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-instagram"></use>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                    <div class="social__elem">
                                                        <a class="social__link" href="{$cfg.link_whatsapp|default:'#'}">
                                                            <svg class="icon icon-whatsapp ">
                                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-whatsapp"></use>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-map">
                <div class="map" id="contactMap" data-pin="/theme/dmg_markup/build/images/pin.png" data-center="45.003652,39.034153"></div>
                <div class="contact-map__button"><a class="button style_border_green js-contactLocationButton"
                            href="https://yandex.ru/maps/-/CGHqAT~i" target="_blank"><span class="button__text">Проложить маршрут</span></a></div>
                <script>
                    // var contactMarkers = [
                    //     {
                    //         id: 'krasnodar',
                    //         city: 'Краснодар',
                    //         location: [45.003888,39.034171],
                    //         link: "https://yandex.ru/maps/-/CGHqAT~i"
                    //     },
                    //     {
                    //         id: 'rostov',
                    //         city: 'Ростов-на-Дону',
                    //         location: [47.198458,39.617951],
                    //         link: "https://yandex.ru/maps/-/CGHqEUNX"
                    //     },
                    //     {
                    //         id: 'pyatigorsk',
                    //         city: 'Пятигорск',
                    //         location: [44.058493,43.005445],
                    //         link: "https://yandex.ru/maps/-/CGHqEF2E"
                    //     },
                    //     {
                    //         id: 'simferopol',
                    //         city: 'Симферополь',
                    //         location: [44.927182,34.115953],
                    //         link: "https://yandex.ru/maps/-/CGHqEN~b"
                    //     }
                    // ];

                    var contactMarkers = {json_encode($contacts)};

                </script>
            </div>
        </div>
        <div class="feedback feedback_modal" id="feedback">
            <div class="container">
                <div class="feedback__container">
                    <div class="feedback__left">
                        <div class="feedback__caption">
                            <div class="feedback__title">
                                Мы открыты к сотрудничеству! <br>
                                Отправьте заявку и наши менеджеры <br>
                                свяжутся с вами, чтобы уточнить <br>
                                все детали.
                            </div>
                        </div>
                    </div>
                    <div class="feedback__right">
                        {include file="_form.tpl" popup=true}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}