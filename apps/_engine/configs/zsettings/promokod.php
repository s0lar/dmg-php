<?php
return array(
    # types of fields
    'fields' => array(
        'title' => 'string',
        'date' => 'date',
        'discount' => 'int',
        'unit' => array(
            'type' => 'list',
            'items' => array(
                'persent' => '% от суммы',
                'fixed' => 'руб.',
            ),
        ),
    ),

    # labels of fields
    'ui' => array(
        'title' => 'Промокод',
        'date' => 'Дата окончания',
        'discount' => 'Скидка',
        'unit' => 'Еденица скидки',
    ),

    #
    // 'input_cfg' => array(
    //     'menu' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'photo' => array('type' => 'image', 'mask' => 'series_photo'),
    //     'background' => array('type' => 'image', 'mask' => 'series_bg'),
    // ),

    # node configuration
    'node' => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name' => '-auto',
    ),

    #
    'view' => array(
        'mode' => 'list',
        'fields' => array('title', 'date', 'discount', 'unit'),
        'orderby' => ' id DESC ',
        'edit_field' => 'title',
        'limit' => 150,
    ),

    # labels for actions
    'labels' => array(
        'list' => 'Промокоды',
        'add' => 'Новый',
        'adding' => 'Создание',
        'edit' => 'Редактировать',
        'editing' => 'Редактирование',
        'delete' => 'Удалить',
    ),
);
