<?php

class XML_Templater extends Any_Templater
{
	public $buf = '<?xml version="1.0" encoding="utf-8"?>';
	
	function init()
	{
		if (!is_array($this->response->headers))
			$this->response->headers[] = 'Content-Type: text/xml';
		else
			$this->response->headers = array_merge(array('Content-Type: text/xml'), $this->response->headers); 
			
		return $this;
	}
	
	function display()
	{		
		//return $this->array2xml($this->response->body);
		return new ArrayToXML($this->response->body);
	}
	
	function array2xml($array)
	{
		if (!is_array($array) && !is_object($array))
			return $this->buf;
			
		foreach ($array as $k => $v)
		{
			$opentag = $k;
			$closetag = $k;
			if (is_numeric($k))
			{
				$opentag = 'item id="'.$k.'"';
				$closetag = 'item';
			}
			
			if (is_null($v))
			{
				$this->buf .= '<'.$opentag.'/>';
				continue;
			}
			
			$this->buf .= '<'.$opentag.'>';
			
			if (is_array($v) || is_object($v))
				$this->array2xml($v);
			elseif (is_string($v))
				$this->buf .= '<![CDATA['.$v.']]>';
			else
				$this->buf .= $v;
				
			$this->buf .= '</'.$closetag.'>';
		}
		
		return $this->buf;
	}
}

class ArrayToXML 
{
    private $dom;
	
    public function __construct($array) 
	{
        $this->dom = new DOMDocument("1.0", "UTF8");
        $root = $this->dom->createElement('response');
        foreach ($array as $key => $value) 
		{
            $node = $this->createNode($key, $value);
            if ($node != null)
                $root->appendChild($node);
        }
        $this->dom->appendChild($root);
    }
	
    private function createNode($key, $value) 
	{
        $node = null;
		if (false !== ($pos = strpos($key, 'attr_')))
		{
			$node = $this->dom->createAttribute(substr($key, $pos+5));
    		$node->appendChild($this->dom->createTextNode($value)); 
		}		
        elseif (is_string($value) || is_numeric($value) || is_bool($value) || $value == null) 
		{
			if ( is_numeric($key) ) 
			{						         	
            	$node = $this->dom->createElement('item');
				$node->setAttribute('id', $key);
			}
			else
				$node = $this->dom->createElement($key);
			
            if ($value !== null)
				$node->appendChild($this->dom->createCDATASection($value));
        } 
		else 
		{
        	if ( is_numeric($key) ) 
			{						         	
            	$node = $this->dom->createElement('item');
				$node->setAttribute('id', $key);
			}
			else
				$node = $this->dom->createElement($key);
				
            if ($value != null) 
			{
                foreach ($value as $key => $value) 
				{
                    $sub = $this->createNode($key, $value);
                    if ($sub != null)
                        $node->appendChild($sub);
                }
            }
        }
        return $node;
    }
	
    public function __toString() 
	{
        return $this->dom->saveXML();
    }
}

?>