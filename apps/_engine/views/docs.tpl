﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-flex">
                <div class="page-flex__aside">
                    <div class="aside-nav">
                        {if !empty($sub_menu)}
                            {foreach item=menu from=$sub_menu}
                                <div class="aside-nav__elem {if $menu.path == $node.path} is-active {/if}">
                                    <a class="aside-nav__link" href="{$menu.path}">{$menu.title|stripcslashes}</a>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
                <div class="page-flex__section">
                    <div class="document">

                        {foreach item=item from=$groups}
                            <div class="document-elem">
                                <div class="document-elem__header js-toggle">
                                    <div class="document-elem__title">
                                        <div class="default-title size_small">{$item.title}</div>
                                    </div>
                                    <div class="document-elem__arrow">
                                        <svg class="icon icon-down ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-down"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="document-elem__content">
                                    <div class="document-grid">
                                        {foreach item=file from=$item.files}
                                            <div class="document-grid__col">
                                                <div class="document-tile">
                                                    <div class="document-tile__image">
                                                        <a class="document-tile__cover js-modalImage"
                                                           href="{$file.preview}"
                                                           style="background-image: url('{$file.preview}')"></a>
                                                        <div class="document-tile__icon">
                                                            <svg class="icon icon-zoom ">
                                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-zoom"></use>
                                                            </svg>
                                                        </div>
                                                        <div class="document-tile__download">
                                                            <a class="document-tile__link" href="{$file.files}">
                                                                Скачать
                                                                <svg class="icon icon-m-download ">
                                                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-download"></use>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <a class="document-tile__title" href="{$file.files}">
                                                        {$file.title|stripcslashes}
                                                    </a>
                                                </div>
                                            </div>
                                        {/foreach}

                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}