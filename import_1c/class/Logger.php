<?php


class Logger
{
    const
        TYPE_ERROR = 'error',
        TYPE_INFO = 'info';

    const TYPES = [
        self::TYPE_ERROR,
        self::TYPE_INFO,
    ];

    /**
     * @param string $type
     * @param string $message
     * @param array $params
     */
    public function log(string $type, string $message, array $params = [])
    {
        if (!in_array($type, self::TYPES)) {
            $type = self::TYPE_INFO;
        }

        printf(
            "[%s] %s: %s %s \n",
            date('Y-m-d H:i:s'),
            $type,
            $message,
            !empty($params) ? json_encode($params) : ''
        );
    }

    /**
     * @param string $message
     * @param array $params
     */
    public function info(string $message, array $params = [])
    {
        $this->log(self::TYPE_INFO, $message, $params);
    }

    /**
     * @param string $message
     * @param array $params
     */
    public function error(string $message, array $params = [])
    {
        $this->log(self::TYPE_ERROR, $message, $params);
    }


}