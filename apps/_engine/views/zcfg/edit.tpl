{include file='zobject/header.tpl' inline='true'}

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="#">Редактирование настроек</a></li>
	</ul>



	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		
		{* Add + Add message *}
		{if isset($smarty.get.msg)}
			<div class="alert alert-block">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<p>Настройка успешно добавлена</p>
			</div>
		{/if}

		<fieldset class="group">
			<!-- <legend class="group-title">{$object.labels.editing}</legend> -->
			<form action="/zcfg/:edit/-id/{$_item.id}/" method="post" class="ajax1" id="object-edit">
				
				<div class="row-fluid border-bottom1" style="margin-bottom:15px">
					<label class="span5 f14  mb0" for="f_value" style="min-height:20px">{$_item.title}
					{if !empty($_item.example)}
						<div style="padding:5px 0;font-size:12px">{$_item.example}</div>
					{/if}
					</label>
				
				
							
					{if $_item.view eq 'string'}
						<input type="text" name="value" id="f_value" value="{$_item.value|escape}" class="input-text span6">
					
					{elseif $_item.view eq 'int' || $_item.view eq 'float'}
						<input type="text" name="value" id="f_value" value="{$_item.value|escape}" class="input-text span3">

					{elseif $_item.view eq 'datetime'}
						<input type="text" name="value" id="f_value" value="{$_item.value|escape}" class="input-text span3 datepick_datetime">
						
					{elseif $_item.view eq 'date'}
						<input type="text" name="value" id="f_value" value="{$_item.value|escape}" class="input-text span3 datepick_date">
						
					{elseif $_item.view eq 'text'}
						<textarea name="value" id="f_value" class="input-textarea span12">{$_item.value|escape}</textarea>
						
					{elseif $_item.view eq 'html'}
						<div style="clear:both">
							<textarea name="value" id="f_value" class="input-textarea visual span12">{$_item.value|escape}</textarea>
							<label class="f11"><input type="checkbox" onclick="tinyMCE.execCommand('mceToggleEditor',false,'{$k}');" style="margin-top:-4px" /> &mdash; вкл/выкл редактор</label>
						</div>
						
					{elseif isset($_item.view.type) && $_item.view.type eq 'gs_file'}
						{include file='zobject/types-views/file.tpl' field=$k data=$object.data[$k] inline='true'}
							
					{elseif $_item.view eq 'checkbox'}
						<input type="checkbox" name="value" id="f_value" {if $_item.value =='1'}checked="checked"{/if} />
							
					{/if}
					
					
				
				<div class="well well-panel well-panel-controll">
					<div class="btn-group">
						<input type="submit" value="Изменить" class="btn btn-primary" />
						<input type="submit" name="apply" value="Применить" class="btn" />
					</div>
					<a href="/zcfg/:edit_node" class="btn btn-small pull-right">Отмена</a>
				</div>
				
			</form>
		</fieldset>

	</div>
{include file='zobject/footer.tpl' inline='true'}