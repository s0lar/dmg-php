﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-flex">
                <div class="page-flex__aside">
{*                    {include '_aside.tpl'}*}
                </div>

                <div class="page-flex__section">
                    <div class="page-group">
                        <div class="vacancies">
                            <div class="faq">
                                {foreach item=qa from=$faq}
                                    <div class="faq-elem">
                                        <div class="faq-elem__header js-faqToggle">
                                            <div class="faq-elem__title">{$qa.title}</div>
                                            <svg class="icon icon-dropdown ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-dropdown"></use>
                                            </svg>
                                        </div>
                                        <div class="faq-elem__content">

                                            {$qa.content}

                                            <div class="faq-elem__footer">
{*                                                <div class="faq-elem__button">*}
{*                                                    <a class="button style_green" href="#"><span class="button__text">Отправить резюме</span></a>*}
{*                                                </div>*}
                                                <div class="faq-elem__close js-faqClose">Свернуть</div>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}