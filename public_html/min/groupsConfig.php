<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'css' => array(
        '//theme/css/style.min.css', 
        '//theme/bower_components/slick.js/slick/slick.css', 
        '//theme/bower_components/slick.js/slick/slick-theme.css', 
    ),

    'js' => array(
        "//theme/bower_components/jquery/dist/jquery.js",
        "//theme/bower_components/slick.js/slick/slick.min.js",
        "//theme/js/build/plugins.js",
        "//theme/js/build/script.js",
        "//theme/js/s.js",
	),
);