<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>

{*    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>*}
    <tr><td><div style="font-weight: bold; font-size: 24px;" class="title">Заказ № {$order.title}</div></td></tr>
    <tr><td><span style="font-weight: bold;">Дата отправки:</span> {$order.created}</td></tr>

    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
    <tr><td><div style="font-weight: bold; font-size: 24px;" class="title">Данные о заказчике</div></td></tr>

    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
    <tr><td><span style="font-weight: bold;">Имя:</span> {$order.name}</td></tr>
    <tr><td><span style="font-weight: bold;">Телефон:</span> {$order.phone}</td></tr>
    <tr><td><span style="font-weight: bold;">Email:</span> {$order.email}</td></tr>

	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
    <tr><td><div style="font-weight: bold; font-size: 24px;" class="title">{$order.delivery}</div></td></tr>

    {if $order.deliveryType == delivery}
        <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
        <tr><td><span style="font-weight: bold;">Город:</span> {$order.city}</td></tr>
        <tr><td><span style="font-weight: bold;">Адрес:</span> {$order.address}</td></tr>
        <tr><td><span style="font-weight: bold;">Желаемая дата и время:</span> {$order.date} {$order.time}</td></tr>
    {else}
        <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
        <tr><td><span style="font-weight: bold;">Пункт самовывоза:</span> {$order.pickupAddress}</td></tr>
        <tr><td><span style="font-weight: bold;">Желаемая дата и время:</span> {$order.date} {$order.time}</td></tr>
    {/if}


    {* Корзина *}
	<tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
    <tr><td><div style="font-weight: bold; font-size: 24px;" class="title">Заказ</div></td></tr>

    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>
    <tr><td>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td><span style="font-weight: bold;">Название</span></td>
                <td><span style="font-weight: bold;">Цена</span></td>
                <td><span style="font-weight: bold;">Кол-во</span></td>
                <td><span style="font-weight: bold;">Итого</span></td>
            </tr>
            {foreach item=i from=$order.cart.prods}
                <tr><td colspan="4" class="offset" style="height: 10px;line-height: 10px;">&nbsp;</td></tr>
                <tr>
                    <td>
{*                        <a target="_blank" style="font-weight: bold;color:#0076ff" href='http://{$smarty.server.HTTP_HOST}{$i.info.path}'>*}
                            {$i.info.title}
{*                        </a>*}
                    </td>
                    <td>{$i.cost}</td>
                    <td>{$i.count}</td>
                    <td>{$i.total}</td>
                </tr>
            {/foreach}

            <tr><td colspan="4" class="offset" style="height: 10px;line-height: 10px;">&nbsp;</td></tr>
            <tr>
                <td><span style="font-weight: bold;"></span></td>
                <td><span style="font-weight: bold;"></span></td>
                <td><span style="font-weight: bold;">{$order.cart.count}</span></td>
                <td><span style="font-weight: bold;">{$order.cart.total}</span></td>
            </tr>

        </table>
    </td></tr>
    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>

    <tr><td><span style="font-weight: bold;">Коментарий к заказу:</span> {$order.comment}</td></tr>
    <tr><td class="offset" style="height: 30px;line-height: 30px;">&nbsp;</td></tr>

</table>