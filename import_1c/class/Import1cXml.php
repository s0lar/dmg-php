<?php


class Import1cXml extends Abstract1cXml
{
    const XPATH_PRODUCT = '/КоммерческаяИнформация/Каталог/Товары/Товар';
    const XPATH_GROUP_ID = 'Группы/Ид';


    public function execute()
    {
        $groups = $this->getGroupsByNodesId();

        foreach ($this->xml->xpath(self::XPATH_PRODUCT) as $product) {
            $id1c = $product->{self::XPATH_ID}->__toString();
            $title = $product->{self::XPATH_TITLE}->__toString();
            $groupId = $product->xpath('Группы/Ид')[0]->__toString();

            if (empty($groups[$groupId])) {
                continue;
            }

            $productId = $this->addProduct($id1c, $title);

            if ($productId > 0)
            {
                $this->addNode($groups[$groupId], $productId, $title);
            }
        }
    }

    /**
     * @param string $id1c
     * @param string $title
     * @return int
     */
    public function addProduct(string $id1c, string $title): int
    {
        $productId = Q('INSERT IGNORE INTO @@product (`1c_id`, `title`, `created_at`) VALUES (?s, ?s, NOW())',
            [
                $id1c,
                $title,
            ]);

        if ($productId > 0) {
            $this->logger->info('Add new product', [
                'id'    => $productId,
                '1c_id' => $id1c,
            ]);
        }

        return $productId;
    }

    /**
     * @param array $parentNode
     * @param int $productId
     * @param string $title
     * @return int
     */
    public function addNode(array $parentNode, int $productId, string $title): int
    {
        $name = translit($title) . '--' . $productId;
        $path = $parentNode['path'] . "{$name}/";

        $res = Q('INSERT INTO @@nodes SET 
                    `parent_id`=?i, 
                    `name`=?s,
                    `path`=?s,
                    `object_id`=?i,
                    `object_title`=?s,
                    `object_type`="product",
                    `created`=NOW(),
                    `updated`=NOW()', [
            $parentNode['id'],
            $name,
            $path,
            $productId,
            $title,
        ]);

        return $res;
    }

    /**
     * @return array
     */
    public function getGroupsByNodesId(): array
    {
        $groups = [];
        $res = Q('select n.id, n.path, n.object_type, c.1c_group_id from @@category2 as c
                left join @@nodes as n on c.id = n.object_id
                where c.1c_group_id <> "" and n.object_type="category2"');

        while ($r = $res->each()) {
            $groupsId = explode("\r\n", $r['1c_group_id']);

            foreach ($groupsId as $id) {
                if (empty($groups[$id])) {
                    unset($r['1c_group_id']);
                    $groups[$id] = $r;
                }
            }
        }

        return $groups;
    }
}