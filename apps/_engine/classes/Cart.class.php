<?php

$__class__cart = null;

function cart()
{
    global $__class__cart;
    if ($__class__cart === null) {
        $__class__cart = new Cart();
    }

    return $__class__cart;
}

class Cart
{
    public $cart = [
        'prods' => [],
        'count' => 0,
        'total' => 0,
        'promo' => null,
    ];

    //
    public function __construct()
    {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = $this->cart;
        } else {
            $this->cart = $_SESSION['cart'];
        }

        return $this;
    }

    public function count()
    {
        return $this->cart['count'];
    }

    public function total()
    {
        return $this->cart['total'];
    }

    public function prods($id = null)
    {
        if ($id != null) {
            return isset($this->cart['prods'][$id]) ? $this->cart['prods'][$id] : false;
        } else {
            return $this->cart['prods'];
        }
    }

    public function save()
    {
        $_SESSION['cart'] = $this->cart;
        return $this;
    }

    public function clear($id = 0)
    {
        //    Delete 1 item
        if ($id > 0) {
            if (isset($this->cart['prods'][$id])) {
                unset($this->cart['prods'][$id]);
                $this->recount();
            }
        } //    Clear all cart
        else {
            $this->cart['prods'] = [];
            $this->cart['count'] = 0;
            $this->cart['total'] = 0;
            $this->save();
        }

        return $this;
    }

    public function recount()
    {
        $count = $total = 0;
        foreach ($this->cart['prods'] as $k => $v) {
            //    Delete empty rows
            if (!isset($v['count']) || $v['count'] < 1) {
                unset($this->cart['prods'][$k]);
                continue;
            }

            //    Calculate prod total
            $this->cart['prods'][$k]['total'] = $v['count'] * $v['cost'];
            $this->cart['prods'][$k]['total_view'] = number_format($this->cart['prods'][$k]['total'], 1, ',', ' ');

            //    Calculate cart count & total
            //$count += $v['count'];
            $total += $v['total'];
        }

        //    Save cart count & total
        $this->cart['count'] = count($this->cart['prods']);
        $this->cart['total'] = $total;
        $this->cart['total_view'] = number_format($this->cart['total'], 1, ',', ' ');

//        //  Promokod
//        if (!empty($this->cart['promo'])) {
//            $promo = $this->cart['promo'];
//
//            //  Промокод % от суммы
//            if ($promo['unit'] == 'persent') {
//                $this->cart['total'] = $total - (($promo['discount'] * $total) / 100);
//            }
//
//            //  Промокод % от суммы
//            elseif ($promo['unit'] == 'fixed') {
//                $this->cart['total'] = $total - $promo['discount'];
//            }
//        }

        $this->save();
    }

    //    Add
    public function add($id, $count = 1, $cost = null, $info = [])
    {
        if (!isset($this->cart['prods'][$id])) {
            $this->cart['prods'][$id] = [];
        }

        if (!is_null($cost)) {
            $this->cart['prods'][$id]['cost'] = $cost;
        }

        $this->cart['prods'][$id]['count'] = $count;
        $this->cart['prods'][$id]['total'] = $count * $this->cart['prods'][$id]['cost'];

        if (!empty($info)) {
            $this->cart['prods'][$id]['info'] = $info;
        }

        $this->recount();

        return $this;
    }

    //    isEmpty
    public function isEmpty()
    {
        return empty($this->cart['prods']);
    }

    //    Promokod
    public function addPromokod($name, $discount, $unit)
    {
        $this->cart['promo'] = [
            'name'     => $name,
            'discount' => $discount,
            'unit'     => $unit,
        ];

        $this->recount();
    }
}
