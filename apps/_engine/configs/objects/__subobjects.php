<?php

# which object can be created as a child
return [
    'sections' => [
        '#.*#' => ['sections' => '*' /*'catalog' => '*',*/],

        '#^/contacts/.*#' => [
            'sections' => 0,
            'contacts' => '*',
        ],

        '#^/company/docs/.*#' => [
            'sections' => 0,
            'docs' => '*',
        ],

        '#^/company/careers/.*#' => [
            'sections' => 0,
            'careers' => '*',
        ],


        '#^/clients/faq/.*#' => [
            'sections' => 0,
            'faq' => '*',
        ],

        '#^/clients/glossary/.*#' => [
            'sections' => 0,
            'glossary' => '*',
        ],

        '#^/clients/gost/.*#' => [
            'sections' => 0,
            'gost' => '*',
        ],

        '#^/clients/stainless-steel/.*#' => [
            'sections' => 0,
            'articles' => '*',
        ],

        '#^/clients/solutions/.*#' => [
            'sections' => 0,
            'solutions' => '*',
        ],

        '#^/suppliers/glossary/.*#' => [
            'sections' => 0,
            'glossary' => '*',
        ],

        '#^/suppliers/faq/.*#' => [
            'sections' => 0,
            'faq' => '*',
        ],

        '#^/catalog/.*#' => [
            'sections' => 0,
            'category1' => '*',
        ],

        '#^/news/.*#' => [
            'sections' => 0,
            'news' => '*',
        ],

    ],

    'articles' => [
        '#.*#' => ['sections' => 0],
    ],

    'solutions' => [
        '#.*#' => ['sections' => 0],
    ],

    'contacts' => [
        '#.*#' => ['sections' => 0],
    ],

    'careers' => [
        '#.*#' => ['sections' => 0],
    ],

    'faq' => [
        '#.*#' => ['sections' => 0],
    ],

    'category1' => [
        '#.*#' => ['category2' => '*', 'category1' => 0, 'sections' => 0],
    ],

    'category2' => [
        '#.*#' => ['product' => '*', 'category2' => 0, 'category1' => 0, 'sections' => 0],
    ],

//    'category3' => [
//        '#.*#' => ['product' => '*', 'category3' => 0, 'category2' => 0, 'category1' => 0, 'sections' => 0],
//    ],

    'docssections' => [
        '#.*#' => ['docs' => '*', 'sections' => 0],
    ],
];
