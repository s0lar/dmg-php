<?php

class Customers
{
    //  Название перемонной сессии
    private $session = 'customer';

    /**
     * Авторизация
     *
     * @param $email
     * @param $password
     *
     * @return $this
     * @throws Exception
     */
    public function doAuth($email, $password)
    {
        //  Ищем покупателя
        $customer = Q('SELECT * FROM @@customers WHERE `email` = ?s LIMIT 1', [$email, $email])->row();

        //  Найден ли пользователь
        if (empty($customer)) {
            throw new Exception('Покупатель не найден');
        }

        //  Проверяем правильность пароля
        if (!password_verify($password, $customer['password'])) {
            throw new Exception('Пароль не верный');
        }

        //  Проверяем правильность пароля
        if (!$customer['active']) {
            throw new Exception('Пользователь заблокирован. Обратитесь к администратору');
        }

        //  Сохраняем покупателя в сессию
        $this->setCustomer($customer);

        return $this;
    }

    /**
     * Регистрируем пользователя в сессии
     *
     * @param array $customer
     *
     * @return $this
     */
    public function setCustomer($customer)
    {
        unset($customer['password']);
        $_SESSION[$this->session] = $customer;

        return $this;
    }

    /**
     * Регистрация
     *
     * @param $email
     * @param $password1
     * @param $password2
     *
     * @return mixed
     * @throws Exception
     */
    public function doRegistry($email, $password1, $password2)
    {
        //  Проверка e-mail
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new CustomerException('E-mail не задан', '1');
        }

        //  Ищем совпадение по Логину
        $customer = Q('SELECT * FROM @@customers WHERE `email` = ?s LIMIT 1', [$email])->row();
        if (!empty($customer)) {
            throw new CustomerException('Аккаунт с данным email уже зарегистрирован', '1');
        }

        //  Проверка пароля на сложность
        if (!preg_match('/^[\S]{6,32}$/', $password1)) {
            throw new CustomerException('Пароль должен быть не менее 6 символов и содержать латинские буквы и цифры', '2');
        }

        //  Проверка на совпадение паролей
        if ($password1 !== $password2) {
            throw new CustomerException('Пароли не совпадают', '2');
        }

        //  Генерируем новую ссылку для восстановления пароля
        $actiovation_url = generateRandomString(64);

        //  Сохраняем ссылку для восстановления
        $customerId = Q(
            'INSERT  @@customers SET `email`=?s, `password`=?s, `actiovation_url`=?s',
            [
                $email,
                password_hash($password1, PASSWORD_BCRYPT),
                $actiovation_url,
            ]
        );

        //  Возвращаем данные о созданном пользователе
        return $this->loadCustomer($customerId);
    }

    /**
     * Загрущить пользователя
     *
     * @param      $id
     * @param bool $save
     *
     * @return mixed
     */
    public function loadCustomer($id, $save = false)
    {
        $customer = Q('SELECT * FROM @@customers WHERE id=?i', [$id])->row();

        if (!empty($customer)) {
            unset($customer['password']);
        }

        if ($save) {
            $this->setCustomer($customer);
        }

        return $customer;
    }

    /**
     * Обновить информацию о пользователе
     *
     * @param $id
     * @param $post
     *
     * @return mixed
     */
    public function doUpdate($id, $post)
    {
        //  Массив полей, которе можно сохранить
        $allow = [
            'title',
            'addr',
            'inn',
            'kpp',
            'person',
            'phone',
            'fax',
            'city',
            'type',
        ];

        //  Массив данных для запроса
        $args = [
            'fields' => [],
            'values' => [],
        ];

        //  Собираем данные для запроса
        foreach ($post as $key => $value) {
            if (in_array($key, $allow)) {
                $args['fields'][] = "`{$key}` = ?s";
                $args['values'][] = htmlspecialchars(
                    strip_tags($value)
                );
            }
        }

        //  Выполняем сохранения
        $result = Q(
            'UPDATE @@customers SET '.implode(', ', $args['fields']).' WHERE `id`=?i',
            array_merge(
                $args['values'],
                [$id]
            )
        );

        //  Возвращаем количество измененных строк
        return $result;
    }

    /**
     * Восстановление пароля
     *
     * @param $email
     *
     * @return $this
     * @throws CustomerException
     */
    public function doReset($email)
    {
        //  Ищем покупателя
        $customer = Q('SELECT * FROM @@customers WHERE `email` = ?s LIMIT 1', [$email])->row();

        //  Найден ли пользователь
        if (empty($customer)) {
            throw new CustomerException('E-mail не найден');
        }

        //  Генерируем новую ссылку для восстановления пароля
        $customer['reset_url'] = generateRandomString(64);

        //  Сохраняем ссылку для восстановления
        Q(
            'UPDATE @@customers SET `reset_url`=?s WHERE `id`=?i',
            [
                $customer['reset_url'],
                $customer['id'],
            ]
        );

        //  Сохраняем покупателя в сессию
        $this->setCustomer($customer);

        return $this;
    }

    /**
     * Смена пароля
     *
     * @param $code
     * @param $password1
     * @param $password2
     *
     * @return $this
     * @throws class
     */
    public function doNewPassword($code, $password1, $password2)
    {
        //  Проверяем код
        if (empty($code)) {
            throw new CustomerException('Не правильная ссылка для восстановления пароля');
        }

        //  Проверка пароля на сложность
        if (!preg_match('/^[\S]{6,32}$/', $password1)) {
            throw new CustomerException('Пароль должен быть не менее 6 символов и содержать латинские буквы и цифры');
        }

        if ($password1 !== $password2) {
            throw new CustomerException('Пароли не совпадают');
        }

        //  Ищем покупателя
        $customer = Q('SELECT * FROM @@customers WHERE `reset_url` = ?s LIMIT 1', [$code])->row();

        //  Найден ли запись о пользователе?
        if (empty($customer)) {
            throw new CustomerException('Ссылка для восстановления устарела');
        }

        //  Сохраняем новый пароль
        Q(
            'UPDATE @@customers SET `password`=?s WHERE `id`=?i',
            [
                password_hash($password1, PASSWORD_BCRYPT),
                $customer['id'],
            ]
        );

        //  Сохраняем покупателя в сессию
        $this->setCustomer($customer);

        return $this;
    }

    /**
     * Активируем покупателя
     *
     * @param $code
     *
     * @return mixed
     * @throws class
     */
    public function doActivation($code)
    {
        //  Проверяем код
        if (empty($code)) {
            throw new CustomerException('Не правильная ссылка для активации');
        }

        //  Ищем покупателя
        $customer = Q('SELECT * FROM @@customers WHERE `actiovation_url` = ?s LIMIT 1', [$code])->row();

        //  Найден ли запись о пользователе?
        if (empty($customer)) {
            throw new CustomerException('Ссылка для активации устарела');
        }

        //  Активируем пользователя
        //$result = Q('UPDATE @@customers SET `active`=1, `actiovation_url`="" WHERE `id`=?i',
        $result = Q(
            'UPDATE @@customers SET `active`=1 WHERE `id`=?i',
            [$customer['id']]
        );

        //  Возвращаем обновленные данные о покупателе
        return $this->loadCustomer($customer['id']);
    }

    /**
     * Выход
     */
    public function doLogout()
    {
        unset($_SESSION[$this->session]);
    }

    /**
     * Авторизован и активен ли пользователь?
     *
     * @return bool
     */
    public function isAuth()
    {
        $customer = $this->getCustomer();

        return isset($customer['active']) ? $customer['active'] : false;
    }

    /**
     * Получаем информацию о покупателе из сессии
     *
     * @param null $key
     *
     * @return mixed|null
     */
    public function getCustomer($key = null)
    {
        if ($key !== null) {
            return isset($_SESSION[$this->session][$key]) ? $_SESSION[$this->session][$key] : null;
        } else {
            return isset($_SESSION[$this->session]) ? $_SESSION[$this->session] : null;
        }
    }
}
