<?php
return array(
    # types of fields
    'fields' => array(
        'title' => 'string',
        'date' => 'datetime',
        'anons' => 'text',
        'photo' => 'gs_file',
        'content' => 'html',
//        'gallery' => 'gs_file',
//        'gallery2' => 'gs_file',
    ),

    # labels of fields
    'ui' => array(
        'title' => 'Название',
        'date' => 'Дата',
        'anons' => 'Анонс',
        'photo' => 'Превью',
        'content' => 'Подробное описание',
//        'gallery' => 'Слайдер',
//        'gallery2' => 'Галерея',
    ),

    #
    'input_cfg' => array(
        'photo' => array('type' => 'image', 'mask' => 'news_photo'),
        'gallery' => array('type' => 'image', 'mask' => 'news_gallery'),
        'gallery2' => array('type' => 'image', 'mask' => 'news_gallery2'),
    ),

    # node configuration
    'node' => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name' => '-user',
    ),

    #
    'view' => array(
        'mode' => 'list',
        'fields' => array('title', 'date'),
        'orderby' => ' `date` DESC',
        'edit_field' => 'title',
        'limit' => 150,
    ),

    # labels for actions
    'labels' => array(
        'list' => 'Новости',
        'add' => 'Добавить новость',
        'adding' => 'Создание',
        'edit' => 'Редактировать',
        'editing' => 'Редактирование',
        'delete' => 'Удалить',
    ),
);
