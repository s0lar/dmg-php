<div class="row-fluid border-bottom1 mb10" style="margin-bottom:15px">
		<label class="span3 struct_label" for="{$k}" style="min-height:20px">{$i}</label>

		{if $object.fields[$k] eq 'string'}
			<input type="text" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span6">

		{elseif $object.fields[$k] eq 'int' || $object.fields[$k] eq 'float'}
			<input type="text" name="{$k}" id="{$k}" value="{$object.data[$k]}" class="input-text span4">

		{elseif $object.fields[$k] eq 'datetime'}
			<input type="datetime" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span3 datepick_datetime">

		{elseif $object.fields[$k] eq 'date'}
			<input type="date" name="{$k}" id="{$k}" value="{$object.data[$k]|escape}" class="input-text span3 datepick_date">

		{elseif $object.fields[$k] eq 'text'}
			<textarea name="{$k}" id="{$k}" class="input-textarea span12">{$object.data[$k]}</textarea>

		{elseif $object.fields[$k] eq 'code'}
			<textarea name="{$k}" id="{$k}" class="input-textarea input-code span12">{$object.data[$k]|htmlspecialchars}</textarea>

		{elseif $object.fields[$k] eq 'html'}
		<div style="clear:both">
			<textarea name="{$k}" id="{$k}" class="input-textarea span12 visual">{$object.data[$k]}</textarea>
			{*<label><input type="checkbox" onclick="tinymce.execCommand('mceToggleEditor',false,'{$k}');" /> &mdash; включить редактор</label>*}
		</div>

		{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'pointer'}
			{include file='zobject/types-views/pointer.tpl' field=$k data=$object.data[$k] inline='true'}

		{elseif $object.fields[$k] eq 'mn'}
            {foreach item=j from=$object.data[$k].object}
                <input type="checkbox" name="{$k}[{$j.id}]" id="{$k}-{$j.id}"{if $j.checked} checked{/if}><label for="{$k}-{$j.id}">{$j.display}</label>
            {/foreach}
{*******}
		{elseif $object.fields[$k] eq 'gs_file'}
			{include file='zobject/types-views/file.tpl' field=$k inline='true'}


		{elseif $object.fields[$k] eq 'checkbox'}
		    <input type="checkbox" name="{$k}" id="{$k}" {if $object.data[$k] =='1'}checked="checked"{/if} />


		{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'list'}
			{include file='zobject/types-views/list.tpl' field=$k data=$object.data[$k] inline='true'}

		{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'list_mn'}
			{include file='zobject/types-views/list_mn.tpl' field=$k data=$object.data[$k] inline='true'}

		{elseif isset($object.fields[$k].type) && $object.fields[$k].type eq 'mn2'}
		{include file='zobject/types-views/mn2.tpl' field=$k data=$object.data[$k] inline='true'}


		{elseif $object.fields[$k] eq 'map'}
			<input type="text" name="{$k}" id="{$k}" class="input-text span6"> <a style="margin-top:-10px;" href="/backend/maps.test.htm" target="_blank" class="btn">Яндекс-карта</a>

		{/if}
	</div>