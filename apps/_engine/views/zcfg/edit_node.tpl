{include file='zobject/header.tpl' inline='true'}

	<ul class="nav nav-tabs" style="margin-bottom:0">
		<li class="active"><a href="#">Конфигурация сайта</a></li>
	</ul>
	
	
	
	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		
		<div class="well well-panel">
			<a href="/zcfg/:add" class="btn btn-mini btn-info"><span class="icon-plus-sign icon-white"></span> Добавить запись (для администратора сайта)</a> &nbsp;&nbsp;&nbsp;
			
		</div>
		
		<table class="table table-striped table-condensed">
			<tr>
				<th class="span3">Название</th>
				{*<th class="span2">Тип</th>*}
				<th class="span5">Значение</th>
				<th class="span2"></th>
			</tr>
			
			{if (isset($_list) && !empty($_list))}
				{foreach item=i from=$_list}
				<tr {if isset($smarty.get.last) && $smarty.get.last == $i.id}class="added"{/if}>
					<td>{$i.title|stripslashes}</td>
					{*<td>{$i.view|stripslashes}</td>*}
					<td>{$i.value|stripslashes}</td>
					
					<td style="text-align:right">
						<a href="/zcfg/:edit/-id/{$i.id}/" class="btn btn-small"><span class="icon-edit"></span></a>
						<a href="/zcfg/:update/-id/{$i.id}/" class="btn btn-small btn-danger" onclick="return confirm('Изменить настройки (только для администратора сайта)?')"><span class="icon-pencil icon-white"></span></a>
						<a href="/zcfg/:delete/-id/{$i.id}/" class="btn btn-small btn-danger" onclick="return confirm('Удалить запись  (только для администратора сайта)?')"><span class="icon-remove icon-white"></span></a>
					</td>
				</tr>
				
				{/foreach}
				
			{else}
				<tr>
					<td colspan="{$ui_count}" style="padding:30px 0;text-align:center">
						Данных нет
					</td>
				</tr>
			
			{/if}
			
		</table>
		
		
	</div>
{include file='zobject/footer.tpl' inline='true'}