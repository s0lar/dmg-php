﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_category">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-category">
                <div class="category">
                    <div class="category-nav">

                        {foreach item=category from=$categories[0] key=parent}
                            <div class="category-nav__group">
                                <div class="category-nav__header">
                                    <div class="category-nav__title">{$category.title}</div>
                                </div>

                                {if !empty($categories[$parent])}
                                    <ul class="category-nav__list">

                                        {foreach item=sub_category from=$categories[$parent]}
                                            <li><a href="{$sub_category.path}">{$sub_category.title}</a></li>
                                        {/foreach}
                                    </ul>
                                {/if}

                            </div>
                        {/foreach}


                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Листовой прокат</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Лист</a></li>*}
                        {*                                <li><a href="#">Рулон</a></li>*}
                        {*                                <li><a href="#">Штритц</a></li>*}
                        {*                                <li><a href="#">Рифленый лист</a></li>*}
                        {*                                <li><a href="#">Перфорированный лист</a></li>*}
                        {*                                <li><a href="#">Декоративный лист</a></li>*}
                        {*                                <li><a href="#">Полоса</a></li>*}
                        {*                                <li><a href="#">Просечно-вытяжной лист</a></li>*}
                        {*                            </ul>*}
                        {*                        </div>*}
                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Сортовой прокат</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Круг</a></li>*}
                        {*                                <li><a href="#">Двутавр</a></li>*}
                        {*                                <li><a href="#">Уголок</a></li>*}
                        {*                                <li><a href="#">Сетка</a></li>*}
                        {*                                <li><a href="#">Шестигранник</a></li>*}
                        {*                                <li><a href="#">Швеллер</a></li>*}
                        {*                                <li><a href="#">Квадрат</a></li>*}
                        {*                                <li><a href="#">Тавр</a></li>*}
                        {*                            </ul>*}
                        {*                        </div>*}
                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Арматура</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Пневмоприводы</a></li>*}
                        {*                                <li><a href="#">Затворы дисковые</a></li>*}
                        {*                                <li><a href="#">Краны шаровые</a></li>*}
                        {*                                <li><a href="#">Муфты молочные</a></li>*}
                        {*                                <li><a href="#">Ключи гаечные</a></li>*}
                        {*                                <li><a href="#">Кламповые соединения</a></li>*}
                        {*                                <li><a href="#">Отводы</a></li>*}
                        {*                                <li><a href="#">Переходы</a></li>*}
                        {*                                <li><a href="#">Тройники</a></li>*}
                        {*                                <li><a href="#">Крестовины</a></li>*}
                        {*                                <li><a href="#">Заглушки</a></li>*}
                        {*                                <li><a href="#">Крепление трубопроводное</a></li>*}
                        {*                                <li><a href="#">Отбортовка</a></li>*}
                        {*                                <li><a href="#">Фланцы</a></li>*}
                        {*                                <li><a href="#">Ниппели приварные резьбовые</a></li>*}
                        {*                                <li><a href="#">Контргайки</a></li>*}
                        {*                                <li><a href="#">Муфты бочонок резьбовые</a></li>*}
                        {*                                <li><a href="#">Сгоны резьбовые</a></li>*}
                        {*                                <li><a href="#">Ерш резьбовой</a></li>*}
                        {*                                <li><a href="#">Отводы резьбовыее</a></li>*}
                        {*                                <li><a href="#">Тройники резьбовые</a></li>*}
                        {*                                <li><a href="#">Муфты резьбовые</a></li>*}
                        {*                                <li><a href="#">Футорки резьбовые</a></li>*}
                        {*                                <li><a href="#">Ниппели приварные резьбовые</a></li>*}
                        {*                                <li><a href="#">Гайки заглушки</a></li>*}
                        {*                                <li><a href="#">Диоптры гаечные</a></li>*}
                        {*                                <li><a href="#">Диоптры трубные</a></li>*}
                        {*                                <li><a href="#">Клапаны дыхательные</a></li>*}
                        {*                                <li><a href="#">Клапаны обратные</a></li>*}
                        {*                                <li><a href="#">Клапаны перепускные</a></li>*}
                        {*                                <li><a href="#">Фильтры угловые</a></li>*}
                        {*                                <li><a href="#">Фильтры грязеулавливающие</a></li>*}
                        {*                                <li><a href="#">Головки моющие</a></li>*}
                        {*                                <li><a href="#">Люки нержавеющие</a></li>*}
                        {*                                <li><a href="#">Держатели уровнемера</a></li>*}
                        {*                                <li><a href="#">Трубы уровнемера</a></li>*}
                        {*                                <li><a href="#">Пробоотборники</a></li>*}
                        {*                            </ul>*}
                        {*                            <div class="category-nav__more">*}
                        {*                                <div class="category-nav__link">Показать все</div>*}
                        {*                            </div>*}
                        {*                        </div>*}
                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Крепеж</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Болт</a></li>*}
                        {*                                <li><a href="#">Винт</a></li>*}
                        {*                                <li><a href="#">Гайка</a></li>*}
                        {*                                <li><a href="#">Шайба</a></li>*}
                        {*                                <li><a href="#">Заклепка</a></li>*}
                        {*                                <li><a href="#">Кольцо</a></li>*}
                        {*                                <li><a href="#">Хомут</a></li>*}
                        {*                                <li><a href="#">Шплинт</a></li>*}
                        {*                                <li><a href="#">Саморез</a></li>*}
                        {*                                <li><a href="#">Анкер</a></li>*}
                        {*                                <li><a href="#">Шпилька</a></li>*}
                        {*                                <li><a href="#">Штифт</a></li>*}
                        {*                                <li><a href="#">Такелаж</a></li>*}
                        {*                                <li><a href="#">Цепь</a></li>*}
                        {*                                <li><a href="#">Трос</a></li>*}
                        {*                            </ul>*}
                        {*                        </div>*}
                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Сварочные материалы</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Электроды</a></li>*}
                        {*                                <li><a href="#">Проволока сварочная</a></li>*}
                        {*                                <li><a href="#">Краги сварочные</a></li>*}
                        {*                                <li><a href="#">Маска сварочная</a></li>*}
                        {*                                <li><a href="#">Травильные средства</a></li>*}
                        {*                            </ul>*}
                        {*                        </div>*}
                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Абразивные материалы</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Диск отрезной для резки</a></li>*}
                        {*                                <li><a href="#">Паста для резки металла</a></li>*}
                        {*                                <li><a href="#">Диск шлифовальный лепестковый торцевой</a></li>*}
                        {*                                <li><a href="#">Диск шлифовальный лепестковый радиальный</a></li>*}
                        {*                                <li><a href="#">Диск шлифовальный зачистной</a></li>*}
                        {*                                <li><a href="#">Рулон для сатинации</a></li>*}
                        {*                                <li><a href="#">Валик для сатинации</a></li>*}
                        {*                                <li><a href="#">Диск гофрированный для сатинации</a></li>*}
                        {*                            </ul>*}
                        {*                        </div>*}
                        {*                        <div class="category-nav__group">*}
                        {*                            <div class="category-nav__header">*}
                        {*                                <div class="category-nav__title">Оборудование</div>*}
                        {*                            </div>*}
                        {*                            <ul class="category-nav__list">*}
                        {*                                <li><a href="#">Центробежные насосы</a></li>*}
                        {*                                <li><a href="#">Кулачковые насосы</a></li>*}
                        {*                                <li><a href="#">Импеллерные насосные</a></li>*}
                        {*                                <li><a href="#">Винтонвые насосы</a></li>*}
                        {*                                <li><a href="#">Пропеллерные мешалки</a></li>*}
                        {*                                <li><a href="#">Мешалки лопастного типа</a></li>*}
                        {*                                <li><a href="#">Мешалки турбинного типа</a></li>*}
                        {*                            </ul>*}
                        {*                        </div>*}
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}

    </div>
{/block}

{block name=footer}
{/block}