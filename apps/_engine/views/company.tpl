﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_company">
        <div class="company js-companySlider">
            <div class="swiper-pagination"></div>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="company-slide company_first_slide">
                            <div class="company-slide__header">
                                <div class="page-head__top">
                                    <div class="page-head__title">О компании</div>
                                </div>
                                <div class="company-slide__title">ООО «Дарья-Металл-Групп» с 1996 года специализируется
                                    на поставках продукции из нержавеющей стали ведущих мировых производителей. На
                                    протяжении многих лет наша компания является стабильным и динамично развивающимся
                                    предприятием.
                                </div>
                            </div>
                            <div class="company-slide__image" data-swiper-parallax="100"
                                 style="background-image: url('/theme/dmg_markup/build/images/index-back-1.jpg')"></div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="company-slide company_slide_statistic">
                            <div class="company-slide__background" data-swiper-parallax="100"
                                 style="background-image: url('/theme/dmg_markup/build/images/index-back-2.jpg')"></div>
                            <div class="company-slide__grid">
                                <div class="company-slide__left">
                                    <div class="statistic-grid">
                                        <div class="statistic-elem">
                                            <div class="statistic-elem__title">1996</div>
                                            <p>Год основания</p>
                                        </div>
                                        <div class="statistic-elem">
                                            <div class="statistic-elem__title">3</div>
                                            <p>Подразделения</p>
                                        </div>
                                        <div class="statistic-elem">
                                            <div class="statistic-elem__title">50 000<sup>+</sup></div>
                                            <p>Позиций в каталоге</p>
                                        </div>
                                        <div class="statistic-elem">
                                            <div class="statistic-elem__title">5 000<sup>+</sup></div>
                                            <p>Партнеров</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="company-slide__right">
                                    <div class="company-slide__title">Защита, надежность и долговечность – наши атрибуты
                                        качества. Мы сотрудничаем с ведущими мировыми поставщиками и помогаем клиенту
                                        создать главное – качественный продукт.
                                    </div>
                                    <div class="company-slide__title">Мы не просто поставляем нержавеющую сталь, мы даем
                                        реальную возможность воплощения идей.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="company-slide company_about_slide">
                            <div class="company-slide__background" data-swiper-parallax="100"
                                 style="background-image: url('/theme/dmg_markup/build/images/index-back-3.jpg')"></div>
                            <div class="company-slide__map"><img
                                        src="/theme/dmg_markup/build/images/company-map-big.jpg"></div>
                            <div class="company-slide__about">
                                <div class="company-slide__image visible-xs"
                                     style="background-image: url('/theme/dmg_markup/build/images/index-back-3.jpg')"></div>
                                <div class="company-slide__title">
                                    Успешная деятельность компании
                                    способствовала появлению подразделений
                                    в&nbsp;Ростове-на-Дону, Пятигорске и Симферополе,
                                    что позволило расширить нашу географию продаж.

                                </div>
                                <div class="company-slide__title">
                                    Благодаря высокому качеству поставляемого нержавеющего металлопроката и слаженной
                                    работе коллектива, компания зарекомендовала
                                    себя универсальным поставщиком и надежным партнёром по бизнесу. 

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="company-slide company_advantage_slide">
                            <div class="company-slide__background" data-swiper-parallax="100"
                                 style="background-image: url('/theme/dmg_markup/build/images/index-back-4.jpg')"></div>
                            <div class="company-slide__grid">
                                <div class="company-slide__right">
                                    <div class="company-slide__advantage">
                                        <div class="advantage">
                                            <div class="advantage-elem">
                                                <div class="advantage-title">Гарантия</div>
                                                <p>Мы тщательно отбираем поставщиков стали, чтобы гарантированно
                                                    поставлять качественный материал.</p>
                                            </div>
                                            <div class="advantage-elem">
                                                <div class="advantage-title">Клиенториентированность</div>
                                                <p>Мы учитываем пожелания каждого нашего клиента. Доверительные
                                                    взаимоотношения – неотъемлемый принцип работы нашей компании.</p>
                                            </div>
                                            <div class="advantage-elem">
                                                <div class="advantage-title">Ответственность</div>
                                                <p>Наша компания ответственна перед своим работником, поставщиком,
                                                    клиентом и обществом.</p>
                                            </div>
                                            <div class="advantage-elem">
                                                <div class="advantage-title">Профессионализм</div>
                                                <p>Мы постоянно развиваем компетентность и профессионализм наших
                                                    сотрудников. Слаженная работа в командее делает нас эффективнее.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="company-slide__left">
                                    <div class="company-slide__info">
                                        <div class="company-slide__title">
                                            Мы&nbsp;всегда открыты
                                            к&nbsp;взаимовыгодному сотрудничеству
                                            с&nbsp;поставщиками
                                            и&nbsp;заказчиками!
                                        </div>
                                        <div class="company-slide__button">
                                            <a class="button style_border_green justify_space_between" href="/contacts/">
                                                <span class="button__text">
                                                    Отправить запрос
                                                    <svg class="icon icon-arrow-triangle ">
                                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}

{block name=footer}
{/block}