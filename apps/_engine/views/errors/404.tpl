{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_category">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">Ошибка 404</div>
                </div>
            </div>

            <div class="page-content">
                <div class="for-buyers">
                    <h3>Что-то пошло не так, данной страницы не существует.</h3>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>

                    <p>
                        <a class="button style_green" href="/">
                            <span class="button__text">На главную</span>
                        </a>
                    </p>
                </div>
            </div>

        </div>

    </div>
{/block}
