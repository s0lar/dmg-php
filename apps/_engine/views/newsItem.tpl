﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">{$title}</div>
                </div>

                {include file="blocks/bc.tpl"}
            </div>
            <div class="page-article">
                <div class="page-article__section">
                    <div class="page-article__top">
                        <div class="page-article__date"><span class="hidden-xs">Дата публикации: </span>{$newsItem.date}</div>
                        <div class="page-article__share">Поделиться:
                            <div class="social">
                                <div class="social__elem">
                                    <a class="social__link" href="#">
                                        <svg class="icon icon-tw ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-tw"></use>
                                        </svg>
                                    </a>
                                </div>
                                <div class="social__elem">
                                    <a class="social__link" href="#">
                                        <svg class="icon icon-vk ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-vk"></use>
                                        </svg>
                                    </a>
                                </div>
                                <div class="social__elem">
                                    <a class="social__link" href="#">
                                        <svg class="icon icon-facebook ">
                                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-facebook"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="copy-button">
                                <svg class="icon icon-copy-link ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-copy-link"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="page-article__content">
                        <div class="page-article__image visible-xs" style="background-image: url('{$newsItem.photo}')"></div>
                        <article class="article">
                            {$newsItem.content}
                        </article>
                    </div>
                    <div class="page-article__button"><a class="button style_border_gray" href="../"><span class="button__text">Назад к списку новостей</span></a></div>
                </div>
                <div class="page-article__aside">
                    <div class="page-article__image" style="background-image: url('{$newsItem.photo}')"></div>
                    <div class="news-feed">
                        {foreach item=news from=$newsList}
                            <a class="news-feed__elem" href="{$news.path}">
                                <div class="news-feed__title">{$news.title}</div>
                                <div class="news-feed__date">{$news.date}</div>
                            </a>
                        {/foreach}

                        <p>&nbsp;</p>
                </div>
            </div>
        </div>

            {include file='_footer_form.tpl'}
    </div>
{/block}

{block name=footer}
{/block}