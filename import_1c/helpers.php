<?php

/**
 * @return array
 */
function getCurrentGroups()
{
    $groups = [];
    $res = Q('select id, 1c_group_id from @@category2 where 1c_group_id <> ""');

    while ($r = $res->each()) {
        $groupsId = explode("\n", $r['1c_group_id']);

        foreach ($groupsId as $id) {
            if (empty($groups[$id])) {
                $groups[$id] = $r['id'];
            }
        }
    }

    return $groups;
}

/**
 * Debug
 */
function __()
{
    $args = func_get_args();
    $nargs = func_num_args();
    $trace = debug_backtrace();
    $caller = array_shift($trace);

    $key = $caller['file'] . ':' . $caller['line'];

    for ($i = 0; $i < $nargs; $i++) {
        echo print_r($args[$i], 1), "\n";
    }
}

function _log()
{
    $args = func_get_args();
    $nargs = func_num_args();

    for ($i = 0; $i < $nargs; $i++) {
        echo
        date('[Y-m-d H:i:s] '),
        print_r(
            $args[$i], 1
        ), "\n";
    }
}
