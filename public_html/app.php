<?php
$t0 = microtime(true);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
date_default_timezone_set('Europe/Moscow');
define('APP_PATH', realpath('../apps/_engine')); 			// set application path
define('SHARED_PATH', realpath('../apps/shared')); 		// shared libs path, optional

// include Xfw
include '../apps/X2/X2.php';

// load configuration for classes scanner
// app: prefix mean APP_PATH/
// import::scan('app:configs/import.yml');
import::scan('app:configs/import.php');

// create new request for current REQUEST_URI
// method - GET or POST
// script - main application script
RQ($_SERVER['REQUEST_URI'], array(
                'key' => 'main',
                'method' => $_SERVER['REQUEST_METHOD'],
                'script' => 'Application_Script'
        )
)->dispatch();  // and run dispatch() method

$t1 = microtime(true);
//echo "<!--\n\nload time: ".($t1 - $t0)."-->";
exit;