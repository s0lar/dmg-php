<?php

return array(
    # types of fields
    'fields'    => array(
        'title'   => 'string',
        'content' => 'html',
        'gost_file' => 'gs_file',
        'visible' => 'checkbox',
        'sort'    => 'int',
    ),

    # labels of fields
    'ui'        => array(
        'title'   => 'Название страницы',
        'content' => 'Контент',
        'gost_file' => 'Файл',
        'visible' => 'Показывать на сайте',
        'sort'    => 'Порядок',
    ),

    #
    'input_cfg' => array(
        'gost_file' => array('type' => 'file', 'mask' => 'product_files'),
    ),

    # node configuration
    'node'      => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-auto',
    ),

    # view
    'view'      => [
        'mode'       => 'list',
        'fields'     => ['title', 'visible', 'sort'],
        'orderby'    => ' `sort` DESC ',
        'edit_field' => 'title',
        'limit'      => 150
    ],

    # labels for actions
    'labels'    => array(
        'list'    => 'ГОСТы',
        'add'     => 'Добавить',
        'adding'  => 'Добавить',
        'edit'    => 'Редактировать',
        'editing' => 'Редактирование',
        'delete'  => 'Удалить',
    ),
);
