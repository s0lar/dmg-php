﻿<div class="feedback style_gray_with_border">
    <div class="container">
        <div class="feedback__container">
            <div class="feedback__left">
                <div class="feedback__caption">
                    <div class="feedback__title">
                        Мы открыты к сотрудничеству! <br>
                        Отправьте заявку и наши менеджеры <br>
                        свяжутся с вами, чтобы уточнить <br>
                        все детали.
                    </div>
                </div>
            </div>
            <div class="feedback__right">
                {include file='_form.tpl' popup=false}
            </div>
        </div>
    </div>
</div>