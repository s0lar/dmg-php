{foreach from=$cart.prods item=i key=id}
    <div class="cart-product  sjs-item-{$id}">
        <div class="cart-product__header">
            <span class="cart-product__title">
                {$i.info.title}
            </span>
            <div class="cart-product__char">
                {if !empty($i.info.tech1)}<span>{$i.info.tech1_name}: {$i.info.tech1}</span>{/if}
                {if !empty($i.info.tech2)}<span>{$i.info.tech2_name}: {$i.info.tech2}</span>{/if}
                {if !empty($i.info.tech3)}<span>{$i.info.tech3_name}: {$i.info.tech3}</span>{/if}
            </div>
            <a href="/order/:remove/-id/{$id}" class="cart-product__del   ajax" rel="cart_remove">
                <svg class="icon icon-delete ">
                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-delete"></use>
                </svg>
            </a>
        </div>
        <form action="/order/:order" class="ajax" rel="cart_add2">
            <div class="cart-product__footer">
                <div class="cart-product__price">{$i.info.cost_view}</div>
                <div class="cart-product__count">
                    <div class="count js-count">
                        <div class="count-button js-countMinus">
                            <svg class="icon icon-minus ">
                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-minus"></use>
                            </svg>
                        </div>
                        <input class="count-input js-countInput dropdown-count   sjs-count-{$id}" name="count" type="text" value="{$i.count}">
                        <input type="hidden" value="{$id}" name="id">
                        <div class="count-button js-countPlus">
                            <svg class="icon icon-plus ">
                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-plus"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="cart-product__total">
                    <span class="sjs-total-{$id}">{$i.total_view}</span>
                    руб.
                </div>
            </div>
        </form>
    </div>
{/foreach}