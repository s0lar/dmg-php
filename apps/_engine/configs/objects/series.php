<?php
return array(
    # types of fields
    'fields' => array(
        'title' => 'string',
        'category_name' => 'string',
        'category' => array(
            'type' => 'pointer',
            'object' => array(
                'table' => 'category',
                'fields' => 'id,title',
                'display' => '%title%',
            ),
        ),
        'anons' => 'text',
        'desc' => 'code',
        'background' => 'gs_file',
        'photo' => 'gs_file',
        'menu' => 'gs_file',
        'sort' => 'int',
    ),

    # labels of fields
    'ui' => array(
        'title' => 'Заголовок',
        'category_name' => 'Название категории',
        'category' => 'Категория',
        'anons' => 'Описание',
        'desc' => 'Характеристики',
        'background' => 'Фон',
        'photo' => 'Фото',
        'menu' => 'Фото в меню',
        'sort' => 'Порядок',
    ),

    #
    'input_cfg' => array(
        'menu' => array('type' => 'image', 'mask' => 'series_photo'),
        'photo' => array('type' => 'image', 'mask' => 'series_photo'),
        'background' => array('type' => 'image', 'mask' => 'series_bg'),
    ),

    # node configuration
    'node' => array(
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name' => '-auto',
    ),

    #
    'view' => array(
        'mode' => 'list',
        'fields' => array('title', 'category_name', 'sort'),
        'orderby' => ' title, `sort` DESC ',
        'edit_field' => 'title',
        'limit' => 150,
    ),

    # labels for actions
    'labels' => array(
        'list' => 'Серии',
        'add' => 'Новая серия',
        'adding' => 'Создание серии',
        'edit' => 'Редактировать серию',
        'editing' => 'Редактирование серии',
        'delete' => 'Удалить серию',
    ),
);
