{* В корзине есть товары *}
{*{if isset($cart) && isset($cart.prods) && !empty($cart.prods)}*}
<div class="cart-dropdown" id="cart-dropdown-container">
    <div class="cart-close js-cartClose">
        <svg class="icon icon-close ">
            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
        </svg>
    </div>
    <div class="cart-dropdown__container">
        <div class="cart-dropdown__list">
            {include file="_cart_list.tpl"}

        </div>
        <div class="cart-dropdown__footer">
            <a class="button style_border_green" href="/order/">
                    <span class="button__text">
                        Оформить заказ
                        <svg class="icon icon-arrow-triangle ">
                            <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                        </svg>
                    </span>
            </a>
            <div class="cart-dropdown__close js-cartClose">Закрыть</div>
        </div>
    </div>
</div>
{* Корзина пуста *}
{*{else}*}

{*    -*}

{*{/if}*}