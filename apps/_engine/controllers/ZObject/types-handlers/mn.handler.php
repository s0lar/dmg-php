<?php

class mn_handler
{
	function prepare_view(&$value = null, $config = null)
	{
		$id = $value;
		$value = array();
		
		$value['relation'] = Q('SELECT * FROM `@@'.$config['relation']['table'].'` WHERE `'.$config['relation']['key1'].'` = '.$id)->all($config['relation']['key2']);
		$res = Q('SELECT `'.implode('`, `', $config['object']['fields']).'` FROM `#'.$config['object']['table'].'`');
		
		$display = $config['object']['display'];
		preg_match_all('/(%(\w+)%)/', $display, $display_vars, PREG_SET_ORDER);
		$n = count($display_vars);
		
		while ($row = $res->each())
		{
			$row['checked'] = isset($value['relation'][$row['id']]);
			
			$row['display'] = $display;
			
			for ($i=0; $i<$n; $i++)
				$row['display'] = preg_replace('#'.$display_vars[$i][0].'#', $row[$display_vars[$i][2]], $row['display']);
			
			$value['object'][] = $row;
		}
	}
}