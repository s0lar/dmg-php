<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Email template</title>
	{literal}<style type="text/css">

		.ReadMsgBody{width:100%;}
		.ExternalClass{width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
		table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
		img{-ms-interpolation-mode:bicubic;}

		body{margin:0; padding:0;}
		img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
		table{border-collapse:collapse !important}
		body{height:100% !important; margin:0; padding:0; width:100% !important;}

		@media only screen and (max-width: 650px) {

			table[class="content_wrap"] {
				width: 100% !important;
				font-size: 14px !important;
				line-height: 20px !important;
			}

			.photo{
				height:auto !important;
				width:100% !important;
			}

			.offset {
				height: 25px !important;
				line-height: 25px !important;
			}

			.offset-double {
				height: 25px !important;
				line-height: 25px !important;
			}

			.offset-side {
				width: 25px !important;
			}

			.offset-logo-top {
				height: 0 !important;
				line-height: 0 !important;
			}
			.offset-logo-bottom {
				height: 20px !important;
				line-height: 20px !important;
			}

			.title {
				font-size: 18px !important;
			}

			.logo img {
				width: 50%;
				height: auto;
			}

		}
	</style>{/literal}
</head>

<body bgcolor="#eeeeee" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing:antialiased;width:100% !important;background-color:#eeeeee;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-text-size-adjust:none;">
	<table width="100%" cellpadding="20" cellspacing="0" border="0" style="font-family: Arial, sans-serif;font-size:16px;line-height:24px;color:#444;">
		<tr>
			<td>
				<table width="610" cellpadding="0" cellspacing="0" border="0" align="center" class="content_wrap">
					<tr>
						<td align="center">
							<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="offset-logo-top" style="height: 15px;line-height: 15px;">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										<a href="http://{$smarty.server.HTTP_HOST}/">
											<img src="http://{$smarty.server.HTTP_HOST}/theme/mail-logo.png" width="158" height="41" alt="" border="0" />
										</a>
									</td>
								</tr>
								<tr>
									<td class="offset-logo-bottom" style="height: 35px;line-height: 35px;">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#ffffff" style="border-width:1px;border-style:solid;border-color:#E2E3E7;">
								<tr>
									<td valign="top">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td class="offset-side" style="width: 70px;">&nbsp;</td>
												<td valign="top">
													{block name=content}{/block}

												</td>
												<td class="offset-side" style="width: 70px;">&nbsp;</td>
											</tr>
										</table>
										{*
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td valign="top">
													<img class="photo" src="/theme/mail/photo.jpg" width="609" height="406" alt="" style="max-width: 610px;" />
												</td>
											</tr>
										</table>
										*}
										{*
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td class="offset-side" style="width: 70px;">&nbsp;</td>
												<td valign="top">
													<table width="100%" cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td class="offset" style="height:40px;line-height:40px;">&nbsp;</td>
														</tr>
														<tr>
															<td>
																<div style="height:1px;background:#E2E3E7;"></div>
															</td>
														</tr>
														<tr>
															<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
														</tr>
														<tr>
															<td>
																<span style="font-weight: bold;">Наименование заказанного товара, может быть очень длинным и занимать две строки.</span>
															</td>
														</tr>
														<tr>
															<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
														</tr>
														<tr>
															<td>
																<span style="font-weight: bold;">Цена:</span> 5000<br />
																<span style="font-weight: bold;">Количество:</span> 1
															</td>
														</tr>
														<tr>
															<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
														</tr>
														<tr>
															<td>
																<div style="height:1px;background:#E2E3E7;"></div>
															</td>
														</tr>
														<tr>
															<td class="offset" style="height:40px;line-height:40px;">&nbsp;</td>
														</tr>
														<tr>
															<td>
																<table width="100%" cellpadding="15" cellspacing="0" border="0" style="font-size: 14px;line-height: 20px;">
																	<tr>
																		<td align="center" style="width: 50%;border-width:1px;border-style: solid;border-color: #E2E3E7;">
																			Столбец 1
																		</td>
																		<td align="center" style="width: 50%;border-width:1px;border-style: solid;border-color: #E2E3E7;">
																			Столбец 2
																		</td>
																	</tr>
																	<tr>
																		<td align="center" style="width: 50%;border-width:1px;border-style: solid;border-color: #E2E3E7;">
																			Столбец 1<br /> две строки
																		</td>
																		<td align="center" style="width: 50%;border-width:1px;border-style: solid;border-color: #E2E3E7;">
																			Столбец 2
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr>
															<td class="offset" style="height:40px;line-height:40px;">&nbsp;</td>
														</tr>
													</table>
												</td>
												<td class="offset-side" style="width: 70px;">&nbsp;</td>
											</tr>
										</table>
										*}
										<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ECEDF0">
											<tr>
												<td class="offset-side" style="width: 70px;">&nbsp;</td>
												<td valign="top">
													<table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-size: 13px;">
														<tr>
															<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
														</tr>
														<tr>
															<td>
																Письмо создано автоматически, отвечать на него не нужно.
															</td>
														</tr>
														<tr>
															<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
														</tr>
													</table>
												</td>
												<td class="offset-side" style="width: 70px;">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="font-size: 13px;">
								<tr>
									<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										<span style="font-weight: bold;text-transform: uppercase;">{$smarty.const.PR_NAME}</span>
									</td>
								</tr>
								<tr>
									<td class="offset" style="height:30px;line-height:30px;">&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
									{*
										<span style="font-weight: bold;">тел.:</span> 8 (861) 200-05-18, 8 (918) 277-00-07<br />
										<span style="font-weight: bold;">mail:</span> <a style="color:#444444" href="mailto:info@iva-group.ru">info@iva-group.ru</a><br />
									*}
										<span style="font-weight: bold;">web:</span> <a style="color:#444444" href="http://{$smarty.server.HTTP_HOST}">{$smarty.server.HTTP_HOST}</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>