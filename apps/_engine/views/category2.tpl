﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="catalog">
        {if !empty($page.icon)}
            <div class="catalog-image" style="background-image: url('{$page.icon}')"></div>
        {/if}
        <div class="container">
            <div class="catalog-head">
                <div class="catalog-head__left">
                    <div class="catalog-head__icon">
                        {*                        <img class="hidden-xs" src="{$page.icon}">*}
                        {*                        <img class="visible-xs" src="{$page.icon}">*}
                    </div>
                    <div class="catalog-head__content">
                        <div class="catalog-head__top">
                            <div class="catalog-head__title">{$page.title}</div>

{*                            <a class="page-head__return mobile_is_show" href="../">*}
{*                                <svg class="icon icon-return ">*}
{*                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-return"></use>*}
{*                                </svg>*}
{*                            </a>*}
                        </div>

                        {include file="blocks/bc.tpl"}
                    </div>

                </div>

                {*                <div class="catalog-head__right">*}
                {*                    <div class="checkbox-switch js-checkboxSwitch">*}
                {*                        <input class="js-checkboxSwitchInput" type="checkbox">*}
                {*                        <span class="checkbox-switch__label js-checkboxSwitchLabel"*}
                {*                              data-checked="false">Цена за метр</span>*}
                {*                        <div class="checkbox-switch__slider js-checkboxSwitchSlider">*}
                {*                            <div class="checkbox-switch__circle"></div>*}
                {*                        </div>*}
                {*                        <span class="checkbox-switch__label js-checkboxSwitchLabel"*}
                {*                              data-checked="true">Цена за кг</span>*}
                {*                    </div>*}
                {*                </div>*}
            </div>
            <div class="catalog-content {if $catalog_nofilter} catalog_nofilter {/if}">
                <div class="catalog-aside">
                    <div class="catalog-aside__view">
                        <div class="catalog-view">
                            <div class="catalog-view__control is-active js-catalogViewControl" data-view="list">
                                <svg class="icon icon-tile ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-tile"></use>
                                </svg>
                            </div>
                            <div class="catalog-view__control js-catalogViewControl" data-view="table">
                                <svg class="icon icon-list ">
                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-list"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="catalog-aside__sort">
                        <div class="sort-select">
                            <div class="sort-select__label">Сортировать:</div>
                            <div class="sort-select__control js-sortSelectControl">
                                <div class="sort-select__button">
                                    {if !empty($smarty.get.sort)}
                                        <div class="sort-select__title js-sortSelectTitle">{$sort_list[$smarty.get.sort]|default:'по имени'}</div>
                                    {else}
                                        <div class="sort-select__title js-sortSelectTitle">по имени</div>
                                    {/if}
                                    <svg class="icon icon-m-menu-dropdown-down ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-menu-dropdown-down"></use>
                                    </svg>
                                </div>
                                <form action="{$request->url->raw}">
                                    <select class="sort-select__list js-sortSelectList  sort_select" name="sort">
                                        {foreach item=sort from=$sort_list key=field}
                                            <option value="{$field}" {if !empty($smarty.get.sort) && $smarty.get.sort == $field} selected="selected" {/if}>{$sort}</option>
                                        {/foreach}
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>

                    {if !$catalog_nofilter}
                        <div class="filter">
                            <div class="filter-header js-filterToggle">
                                <div class="filter-header__title">Фильтр</div>
                                <div class="filter-header__icon">
                                    <svg class="icon icon-m-filter ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-filter"></use>
                                    </svg>
                                </div>
                            </div>

                            <div class="filter-content">
                                <form action="{$request->url->path}" class="filter-form" method="get"
                                      id="filterFormAjax" rel="filter_count">

                                    {*                                    <input type="submit" class="btn mfp-close-btn-in">*}

                                    {foreach item=$filter from=$catalog_config key=input_name}
                                        <div class="filter-group">
                                            <div class="filter-group__header is-active js-filterToggle">
                                                <div class="filter-group__title">{$filter.name}</div>
                                                <svg class="icon icon-m-menu-dropdown-down ">
                                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-m-menu-dropdown-down"></use>
                                                </svg>
                                            </div>

                                            <div class="filter-group__content" style="display: block">

                                                {if $filter.type == 'checkbox'}
                                                    <div class="filter-group__checkbox">

                                                        {foreach item=$input from=$filter.data key=value}
                                                            <div class="filter-checkbox">
                                                                <label class="checkbox">
                                                                    <input type="checkbox"
                                                                           name="{$input_name}['{$filter.name}'][{$value}]"
                                                                           value="{$value}"
                                                                            {if !empty($checked[$input_name][$value])}
                                                                                checked="checked"
                                                                            {/if}
                                                                    >
                                                                    <span class="checkbox__title">{$input}</span>
                                                                </label>
                                                            </div>
                                                        {/foreach}

                                                        <div class="filter-tooltip" id="id-filter-tooltip"
                                                             style="display: none">
                                                            {*                                                            <a class="filter-tooltip__title" href="#">*}
                                                            <button type="submit" class="filter-tooltip__title">
                                                                Показать товаров: 0
                                                            </button>
                                                            {*                                                            </a>*}
                                                            <div class="filter-tooltip__close">
                                                                <svg class="icon icon-close ">
                                                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                                                                </svg>
                                                            </div>
                                                        </div>

                                                    </div>
                                                {/if}

                                                {if $filter.type == 'range'}
                                                    <div class="filter-inputs">
                                                        <div class="filter-field">
                                                            <input class="filter-input"
                                                                   name="{$input_name}['{$filter.name}'][min]"
                                                                   type="text"
                                                                   value="{$checked[$input_name]['min']|default:$filter.data.min}">
                                                            <div class="filter-field__label">от</div>
                                                        </div>
                                                        <div class="filter-field">
                                                            <input class="filter-input"
                                                                   name="{$input_name}['{$filter.name}'][max]"
                                                                   type="text"
                                                                   value="{$checked[$input_name]['max']|default:$filter.data.max}">
                                                            <div class="filter-field__label">до</div>
                                                        </div>
                                                    </div>
                                                {/if}

                                            </div>
                                        </div>
                                    {/foreach}

                                </form>
                                <div class="filter-footer">
                                    <div class="filter-footer__control js-filterClose">Свернуть</div>
                                </div>
                            </div>
                        </div>
                    {/if}
                </div>

                <div class="catalog-section">
                    <div class="catalog-panel">
                        <div class="catalog-sort">
                            <div class="sort-list">
                                {foreach item=sort from=$sort_list key=field name=n}
                                    <div class="sort-list__elem
                                            {if (empty($smarty.get.sort) && $smarty.foreach.n.first) || (!empty($smarty.get.sort) && $smarty.get.sort == $field)}
                                            is-active
                                            {/if}
                                    ">
                                        <a class="sort-list__link" href="{$request->url->path}?{$sort_links[$field]|default:''}">
                                            {$sort}

                                            {if (empty($smarty.get.sort) && $smarty.foreach.n.first) || (!empty($smarty.get.sort) && $smarty.get.sort == $field)}
                                                <svg class="icon icon-sort-arrow-down ">
                                                    <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-sort-arrow-up"></use>
                                                </svg>
                                            {/if}
                                        </a>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>

                    <div class="catalog-products">
                        <div class="product-list is_product_list_grid">

                            {foreach item=product from=$products}
                                <div class="product-block">
                                    <a class="product-block__title js-modalLink" href="javascript:;"
                                       data-mfp-src="#modalProduct-{$product.id}">
                                        {$product.title|stripcslashes}
                                    </a>
                                    <div class="product-block__main">
                                        <div class="product-block__preview">
                                            <svg class="icon icon-nophoto ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-nophoto"></use>
                                            </svg>
                                        </div>
                                        <a class="product-block__price js-modalLink" href="javascript:;"
                                           data-mfp-src="#modalProduct-{$product.id}">
                                            {$product.cost_view|stripcslashes}
                                        </a>
                                    </div>

                                    <div class="product-block__char">
                                        <table>
                                            {if !empty($product.tech1)}
                                                <tr>
                                                    <td>{$product.tech1_name|stripcslashes}</td>
                                                    <td>{$product.tech1|stripcslashes}</td>
                                                </tr>
                                            {/if}
                                            {if !empty($product.tech2)}
                                                <tr>
                                                    <td>{$product.tech2_name|stripcslashes}</td>
                                                    <td>{$product.tech2|stripcslashes}</td>
                                                </tr>
                                            {/if}
                                            {if !empty($product.tech3)}
                                                <tr>
                                                    <td>{$product.tech3_name|stripcslashes}</td>
                                                    <td>{$product.tech3|stripcslashes}</td>
                                                </tr>
                                            {/if}
                                        </table>
                                    </div>
                                </div>
                            {/foreach}

                        </div>
                    </div>

                    {include file="blocks/pg.tpl"}


                    <div class="see-more">
                        {if !empty($see_more)}
                            <div class="see-more__header">
                                <div class="see-more__icon"><img src="/theme/dmg_markup/build/images/warning.svg"></div>
                                <div class="see-more__title">Смотрите также</div>
                            </div>
                            <div class="see-more__content">
                                <div class="category-grid">
                                    {foreach item=more from=$see_more}
                                        <div class="category-grid__col">
                                            <a class="category-tile" href="{$more.path}">
                                                <div class="category-tile__title">{$more.title|stripcslashes}</div>
                                                <div class="category-tile__icon">
                                                    {if !empty($more.icon2)}<img class="hidden-xs" src="{$more.icon2}">{/if}
                                                    {if !empty($more.icon)}<img class="visible-xs" src="{$more.icon2}">{/if}
                                                </div>
                                            </a>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        {/if}
                    </div>

                    <div class="catalog-desc">Компания «Дарья-Металл-Групп» - официальный дистрибьютор испанской
                        компании InoxMIMgrup в России. Оборудование является эффективным решением для заказчиков с
                        малыми и значительными производственными объемами обработки сырья в пищевой, фармацевтической,
                        косметической сферах. InoxMIMgrup - европейский производитель насосов, мешалок, эмульгаторов и
                        микронайзеров для взбалтывания, перемешивания, перекачивания рабочей среды. Поставки уже
                        начались.
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file='_footer_form.tpl'}
{/block}


{block name=modal_products}

    {foreach item=product from=$products}
        <div class="modal modal_size_small mfp-hide" id="modalProduct-{$product.id}">
            <div class="modal-block">
                <div class="modal-close js-modalClose">
                    <svg class="icon icon-close ">
                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-close"></use>
                    </svg>
                </div>
                <div class="modal-product">
                    <div class="modal-product__center">
                        <div class="modal-product__title">{$product.title|stripcslashes}</div>

                        <form action="/order/:order" class="ajax" rel="cart_add">
                            <div class="modal-product__main">
                                <div class="modal-product__count">
                                    <div class="modal-product__price">{$product.cost_view|stripcslashes}</div>
                                    <div class="count js-count">
                                        <div class="count-button js-countMinus">
                                            <svg class="icon icon-minus ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-minus"></use>
                                            </svg>
                                        </div>
                                        <input class="count-input js-countInput" type="text" name="count"
                                               value="{$cart.prods[$product.id]['count']|default:1}"
                                        >
                                        <input type="hidden" value="{$product.id}" name="id">
                                        <div class="count-button js-countPlus">
                                            <svg class="icon icon-plus ">
                                                <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-plus"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-product__cart">
                                    <button class="button style_green">
                                        <span class="button__text">Добавить в корзину</span>
                                    </button>
                                    <div class="modal-product__total">
                                        Итого:
                                        {if !empty($cart.prods[$product.id])}
                                            {$cart.prods[$product.id].total_view}
                                        {else}
                                            {$product.cost_view|stripcslashes}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="modal-product__table">
                            <table>
                                {if !empty($product.tech1)}
                                    <tr>
                                        <td>{$product.tech1_name|stripcslashes}</td>
                                        <td>{$product.tech1|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech2)}
                                    <tr>
                                        <td>{$product.tech2_name|stripcslashes}</td>
                                        <td>{$product.tech2|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech3)}
                                    <tr>
                                        <td>{$product.tech3_name|stripcslashes}</td>
                                        <td>{$product.tech3|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech4)}
                                    <tr>
                                        <td>{$product.tech4_name|stripcslashes}</td>
                                        <td>{$product.tech4|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech5)}
                                    <tr>
                                        <td>{$product.tech5_name|stripcslashes}</td>
                                        <td>{$product.tech5|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech6)}
                                    <tr>
                                        <td>{$product.tech6_name|stripcslashes}</td>
                                        <td>{$product.tech6|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech7)}
                                    <tr>
                                        <td>{$product.tech7_name|stripcslashes}</td>
                                        <td>{$product.tech7|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech8)}
                                    <tr>
                                        <td>{$product.tech8_name|stripcslashes}</td>
                                        <td>{$product.tech8|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech9)}
                                    <tr>
                                        <td>{$product.tech9_name|stripcslashes}</td>
                                        <td>{$product.tech9|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech10)}
                                    <tr>
                                        <td>{$product.tech10_name|stripcslashes}</td>
                                        <td>{$product.tech10|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech11)}
                                    <tr>
                                        <td>{$product.tech11_name|stripcslashes}</td>
                                        <td>{$product.tech11|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech12)}
                                    <tr>
                                        <td>{$product.tech12_name|stripcslashes}</td>
                                        <td>{$product.tech12|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech13)}
                                    <tr>
                                        <td>{$product.tech13_name|stripcslashes}</td>
                                        <td>{$product.tech13|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech14)}
                                    <tr>
                                        <td>{$product.tech14_name|stripcslashes}</td>
                                        <td>{$product.tech14|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech15)}
                                    <tr>
                                        <td>{$product.tech15_name|stripcslashes}</td>
                                        <td>{$product.tech15|stripcslashes}</td>
                                    </tr>
                                {/if}
                                {if !empty($product.tech16)}
                                    <tr>
                                        <td>{$product.tech16_name|stripcslashes}</td>
                                        <td>{$product.tech16|stripcslashes}</td>
                                    </tr>
                                {/if}
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-cancel js-modalClose">
                    <div class="modal-cancel__title">Закрыть</div>
                </div>
            </div>
        </div>
    {/foreach}

{/block}


{block name=footer}
{/block}

