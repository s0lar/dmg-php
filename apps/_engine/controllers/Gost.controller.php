<?php

class Gost_Controller extends ZObject_Controller
{

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function action_index()
    {
        if ($this->node['object_type'] === 'gost') {
            redirect('../');
        }

        $list = [];
        $res = Q('SELECT * FROM @@gost WHERE visible=1 ORDER BY sort DESC');

        while ($r = $res->each()) {
            $r['gost_file'] = $this->getFM($r['gost_file']);
            $list[] = $r;
        }

        RS('gost', $list);

        RS('cls_body', '');
        V('gost');
    }
}
