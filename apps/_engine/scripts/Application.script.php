<?php

import::from('shared:Q3i/Q.php');

class Application_Script extends XAny_Script
{
    public $config;

    public function init()
    {
        // load application configuration
        //$this->config = import::config('app:configs/app.yml');

        $this->config = import::config('app:configs/app.php');
        $dbDsn        = import::config('app:configs/cfg.php');

        if (empty($dbDsn) || empty($dbDsn->db['dsn'])) {
            throw new Exception('Need to setup configs/cfg');
        }

        //  Connect to Database
        QF($dbDsn->db['dsn'])->connect()->alias('default');
    }

    public function run()
    {
        // create new XURL object with current url
        $request      = RQ();
        $request->url = XURL($request->url)
            // set allowed views for parser (ex: html, json, xml, etc)
            ->views(array_keys($this->config->views))
            // parse url
            ->parse();

        // create new Router class with current request (XRequest object)
        $router = new Router();

        //if( isset($_GET['dd']) ) __($router->route());

        try {
            $router->route(); // try to find route
        } catch (XException $e) // 403, 404 errors
        {
            switch ($e->getCode()) {
                case 401: // path not found
                    $router->controller = 'ZErrors_Controller';
                    $router->method     = 'action_401';
                    break;
                case 403: // access denied
                    exit("1");
                    $router->controller = 'ZErrors_Controller';
                    $router->method     = 'action_403';
                    break;
                case 404: // path not found
                    $router->controller = 'ZErrors_Controller';
                    $router->method     = 'action_404';
                    break;
            }
        }

        // create new controller with current request object and node date
        // __($router->node);exit;
        $controller = new $router->controller($router->node);
        try {
            if ($request->url->view !== 'json') {
                RS('request', $request);
            }

            // call controller's method
            $res = $controller->{$router->method}();

            // get templater class name from config
            // if urls's view not defined - use default
            $templater_class = $this->config->views[$request->url->view ? $request->url->view : 'default'];

            // try to load templater config
            $templater_config = isset($this->config->templaters[$templater_class]) ? $this->config->templaters[$templater_class] : null;

            // create templater
            $templater = new $templater_class($templater_config);
        } catch (XException $e) // 302 - redirect
        {
            switch ($e->getCode()) {
                case 401: // path not found
                    $router->controller = 'ZErrors_Controller';
                    $router->method     = 'action_401';
                    $controller         = new $router->controller($router->node);
                    break;

                case 403: // access denied
                    $router->controller = 'ZErrors_Controller';
                    $router->method     = 'action_403';
                    $controller         = new $router->controller($router->node);
                    break;

                case 404: // path not found
                    $router->controller = 'ZErrors_Controller';
                    $router->method     = 'action_404';
                    $controller         = new $router->controller($router->node);
                    break;

                case 301:
                case 302:
                    RSH('location: '.$e->getMessage(), $e->getCode());
                    $templater = new Any_Templater();
                    break;
            }

            //    s0lar
            if ($request->url->view != 'json') {
                RS('request', $request);
            }

            // call controller's method
            $res = $controller->{$router->method}();

            // get templater class name from config
            // if urls's view not defined - use default
            $templater_class = $this->config->views[$request->url->view ? $request->url->view : 'default'];

            // try to load templater config
            $templater_config = isset($this->config->templaters[$templater_class]) ? $this->config->templaters[$templater_class] : null;

            // create templater
            $templater = new $templater_class($templater_config);

        }

        // init templater, print headers and display result
        echo $templater->init()->headers()->display();

        return $res;
        //__debug();
    }

    public function close()
    {
        // disconnect from database
        Q()->disconnect();
    }
}
