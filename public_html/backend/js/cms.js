/* UTILS */
function __() {try {for(var i=0; i<arguments.length; i++) {console.log(arguments[i]);}}catch (e) {}}

/* CMS */
var cms = {
	global: {
		init: function() {
			//cms.workspace.init();
			cms.nodes.init();
			cms.forms.init();
			cms.tabs.init();
			cms.nodes_user.init();
		}
	},
	workspace: {
		$nodes_holder: null,
		heights: 0,
		init: function() {
			cms.workspace.$nodes_holder = $("#nodes-holder");
			cms.workspace.heights = 2+$(".header").height() + $(".redline").height() + $(".greyline").height() + $(".footer").height();
			
			cms.workspace.resize();
			
			window.onresize = function(e) {
				/*window.clearTimeout(resizeTimeoutId);
				resizeTimeoutId = window.setTimeout('resizeWindow();', 2);*/
				cms.workspace.resize();
			};
		},
		resize: function() {
			current_height = $(document).height() - cms.workspace.heights;
			cms.workspace.$nodes_holder.css({
				'height': current_height
			});		
		}
	},
	nodes: {
		$tree: null,
		init: function() {
			cms.nodes.$tree = $("#nodes"); 
			$("#nodes .folder").live("click", cms.nodes.folderClick);
			
			cms.nodes.$tree.find("a.folder").each(function(){
				var href = $(this).attr("href").replace('http://'+document.domain,'');							
				if (href.charAt(href.length-1) != '/') href = href+'/';

				if (request.url.path.indexOf(href) == 0) {
					$(this).addClass("opened");
					$("#tree-node-item-"+$(this).attr("rel")).addClass("opened");
					$(this).click();
				}
			});
		},
		folderClick: function(e) {
			var $a = $(this);
			var key = $a.attr("rel");
			
			if (cms.nodes.$tree.data("state-"+key) != null ) {
				
				if (cms.nodes.$tree.data("state-"+key) == true) {
					$a.removeClass('opened');
					$("#tree-node-item-"+key).removeClass("item-opened");
					$("#tree-node-"+key).slideUp();
				}
				else {
					$a.addClass('opened');
					$("#tree-node-item-"+key).addClass("item-opened");
					$("#tree-node-"+key).slideDown();
				}
					
				cms.nodes.$tree.data("state-"+key, !cms.nodes.$tree.data("state-"+key));
				return false;
			}

			$a.addClass('icon-minus-sign');
			$("#tree-node-item-"+key).addClass("item-opened");
			
			$('<ul id="tree-node-'+key+'"><li>Загрузка...</li></ul>').appendTo($a.parent().parent());
			
			$.get($a.attr("href")+":load-child-nodes.json", {}, function(data) {				
				$("#tree-node-"+key+" li").remove();
				
				var buf = '';
				for (i in data.items) {					
					buf += '<li><div class="nodes-item-holder">';
					if ("folder" == data.items[i].type)
						buf += '<a class="folder icon-plus-sign" href="'+data.items[i].path+'" rel="'+i+'"></a>';
					else if ("list" == data.items[i].type)
						buf += '<a class="list icon-th-list" node:path="'+data.items[i].path+'" rel="'+i+'"></a>';					
					else
						buf += '<a class="leaf icon-list-alt" node:path="'+data.items[i].path+'" rel="'+i+'"></a>';
						
					buf += '<a id="tree-node-item-'+i+'" href="'+data.items[i].path+':';
					if ("list" == data.items[i].type)
						buf += 'list-objects/-type/'+data.items[i].object;
					else
						buf += 'edit';
						
					buf += '/">'+data.items[i].object_title+("list" == data.items[i].type ? ' ('+data.items[i].count+')':'')+'</a>\
					</div></li>';
				}
				$("#tree-node-"+key).append(buf);
				
				$("#tree-node-"+key+" a.folder").each(function() {
					var href = $(this).attr("href").replace('http://'+document.domain, '');

					if (request.url.path == '/') {
						return;
					}
					
					if (request.url.path.indexOf(href) == 0) {
						$(this).addClass("item-opened");
						$("#tree-node-item-"+$(this).attr("rel")).addClass("opened");
						$(this).click();
					}
				});
				
				/*$("#tree-node-"+key+" a.list").each(function() {
					var href = $(this).attr("node:path");
					
					if (request.url.path.indexOf(href) == 0) {
						$("#tree-node-item-"+$(this).attr("rel")).addClass("item-opened");
					}
				});*/
				
				$("#tree-node-"+key+" a.leaf").each(function() {
					var href = $(this).attr("node:path");

					if (request.url.path == href) {
						$("#tree-node-item-"+$(this).attr("rel")).addClass("item-current");						
					}
				});

				cms.nodes.$tree.data("state-"+key, !cms.nodes.$tree.data("state-"+key));
			}, "json");
			
			return false;

		}
	},
	
	nodes_user: {
		$tree: null,
		init: function() {
			//console.log(nodes_user_json);
			
			cms.nodes_user.$tree = $("#nodes_user"); 
			$("#nodes_user .folder").live("click", cms.nodes_user.folderClick);
			
			cms.nodes_user.$tree.find("a.folder").each(function(){
				var href = $(this).attr("href").replace('http://'+document.domain,'');							
				if (href.charAt(href.length-1) != '/') href = href+'/';

				if (request.url.path.indexOf(href) == 0) {
					$(this).addClass("opened");
					$("#tree-node-item-"+$(this).attr("rel")).addClass("opened");
					$(this).click();
				}
			});
		},
		folderClick: function(e) {
			var $a = $(this);
			var key = $a.attr("rel");
			
			if (cms.nodes_user.$tree.data("state-"+key) != null ) {
				
				if (cms.nodes_user.$tree.data("state-"+key) == true) {
					$a.removeClass('opened');
					$("#nodes_user-tree-node-item-"+key).removeClass("item-opened");
					$("#nodes_user-tree-node-"+key).slideUp();
				}
				else {
					$a.addClass('opened');
					$("#nodes_user-tree-node-item-"+key).addClass("item-opened");
					$("#nodes_user-tree-node-"+key).slideDown();
				}
					
				cms.nodes_user.$tree.data("state-"+key, !cms.nodes_user.$tree.data("state-"+key));
				return false;
			}

			$a.addClass('icon-minus-sign');
			$("#nodes_user-tree-node-item-"+key).addClass("item-opened");
			
			$('<ul id="nodes_user-tree-node-'+key+'"><li>Загрузка...</li></ul>').appendTo($a.parent().parent());
			
			$.get($a.attr("href")+":load-child-nodes.json", {}, function(data) {				
				$("#nodes_user-tree-node-"+key+" li").remove();
				
				var buf = '';
				for (i in data.items) {					
					buf += '<li><div class="nodes-item-holder">';
					if ("folder" == data.items[i].type)
						buf += '<a class="folder icon-plus-sign" href="'+data.items[i].path+'" rel="'+i+'"></a>';
					else if ("list" == data.items[i].type)
						buf += '<a class="list icon-th-list" node:path="'+data.items[i].path+'" rel="'+i+'"></a>';					
					else
						buf += '<a class="leaf icon-list-alt" node:path="'+data.items[i].path+'" rel="'+i+'"></a>';
						
					buf += '<a id="nodes_user-tree-node-item-'+i+'" href="'+data.items[i].path+':';
					if ("list" == data.items[i].type)
						buf += 'list-objects/-type/'+data.items[i].object;
					else
						buf += 'edit';
						
					buf += '/">'+data.items[i].object_title+("list" == data.items[i].type ? ' ('+data.items[i].count+')':'')+'</a>';
					
					if ("list" == data.items[i].type){
						/*
						if( nodes_user_json !== null && nodes_user_json.nodes.lists[ data.items[i].pid ] !== undefined && nodes_user_json.nodes.lists[ data.items[i].pid ] == data.items[i].object )
							buf += '<input type="checkbox" class="checkbox" name="user_nodes_list['+data.items[i].pid+']" value="'+data.items[i].object+'" checked="checked" />';
						else
							buf += '<input type="checkbox" class="checkbox" name="user_nodes_list['+data.items[i].pid+']" value="'+data.items[i].object+'" />';
						*/
						buf += '<input type="checkbox" class="checkbox" disabled style="opacity:0" />';
					}
					else{
						if( nodes_user_json !== null && nodes_user_json.nodes.nodes[ data.items[i].id ] !== undefined )
							buf += '<input type="checkbox" class="checkbox" name="user_nodes['+data.items[i].id+']" checked="checked" />';
						else
							buf += '<input type="checkbox" class="checkbox" name="user_nodes['+data.items[i].id+']" />';
					}
						
					buf += '</div></li>';
				}
				$("#nodes_user-tree-node-"+key).append(buf);
				
				$("#nodes_user-tree-node-"+key+" a.folder").each(function() {
					var href = $(this).attr("href").replace('http://'+document.domain, '');

					if (request.url.path == '/') {
						return;
					}
					
					if (request.url.path.indexOf(href) == 0) {
						$(this).addClass("item-opened");
						$("#nodes_user-tree-node-item-"+$(this).attr("rel")).addClass("opened");
						$(this).click();
					}
				});
				
				/*$("#tree-node-"+key+" a.list").each(function() {
					var href = $(this).attr("node:path");
					
					if (request.url.path.indexOf(href) == 0) {
						$("#tree-node-item-"+$(this).attr("rel")).addClass("item-opened");
					}
				});*/
				
				$("#nodes_user-tree-node-"+key+" a.leaf").each(function() {
					var href = $(this).attr("node:path");

					if (request.url.path == href) {
						$("#nodes_user-tree-node-item-"+$(this).attr("rel")).addClass("item-current");						
					}
				});

				cms.nodes_user.$tree.data("state-"+key, !cms.nodes_user.$tree.data("state-"+key));
			}, "json");
			
			return false;

		}
	},
	
	
	tabs: {
		$tabs_holder: null,
		init: function() {
			
			$(".tabs a").live("click", function() {
				$(".tabs a").removeClass("current");
				$(".tab").hide();
				$("#tab-"+$(this).attr("rel")).show();
				$(this).addClass("current");
				
				return false;
			});
			
			cms.tabs.$tabs_holder = $(".tabs");
			var i = 0;
			$(".tab").each(function() {
				cms.tabs.$tabs_holder.append('<a href="" rel="'+i+'">'+$(this).attr("title")+'</a>');
				$(this).attr("id", "tab-"+i);
				$(this).hide();
				
				i++;
			});
			
			$(".tabs a:eq(0)").click();
		}
	},
	forms: {
		init: function() {
			$("form.ajax").live("submit", function() {
				var $form = $(this);
				
				/*if (!site.utils.checkForm($form)) 
					return false;*/
				
				$.ajax({
					type: $form.attr("method"),
					url: $form.attr("action"),
					dataType: "json",
					data: $form.serialize(),
					beforeSend: function() {
						$form.find(":input").attr("disabled", "disabled");
					},
					success: function(data) {
						var prefix = $form.attr("id").replace(/-/, '_'), 
							success = prefix + '__success', 
							failed = prefix + '__failed';
						
						success = cms.callbacks[success] ? success : "__success";
						failed = cms.callbacks[failed] ? failed : "__failed";
						
						if ('failed' == data.result)
							cms.callbacks[failed](data, $form);
						else if ('success' == data.result)
							cms.callbacks[success](data, $form);						
					},
					error: function() {
					}
				});
				
				return false;
			});
		}
	},
	callbacks: {
		__success: function(data, $form) {},
		__failed: function(data, $form) {},
		
		object_add__success: function(data, $form) {
			alert("Object added");
			$form.find(":input").removeAttr("disabled");
			location.href = data.path+":edit/";
			//$form.attr("action", data.path+":edit.json");			
		},
		
		object_edit__success: function(data, $form) {
			$form.find(":input").removeAttr("disabled");
			$("#tree-node-item-"+data.id).text(data.title);
			alert("Object updated");
		}
	}
};

$(document).ready(cms.global.init);