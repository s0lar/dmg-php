{if isset($pg) && !empty($pg) && $pg.pages > 1}

    <div class="pager">
        <div class="pager-nav">
            {section name=i start=$pg.start loop=$pg.loop}
                <div class="pager-nav__elem  {if $pg.curr == $smarty.section.i.index}is-active{/if}">
                    <a class="pager-nav__link" href="{$pg.path}&page={$smarty.section.i.index}">
                        {$smarty.section.i.index}
                    </a>
                </div>
            {/section}
        </div>

{*        <div class="pager-select">*}
{*            <div class="pager-select__label">Показывать:</div>*}
{*            <div class="pager-control">*}
{*                <div class="pager-control__title js-pagerControl"><span>50 на страницу</span>*}
{*                    <svg class="icon icon-city-dropdown-down ">*}
{*                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-city-dropdown-down"></use>*}
{*                    </svg>*}
{*                </div>*}
{*                <div class="pager-control__dropdown">*}
{*                    <div class="pager-control__elem">*}
{*                        <a class="pager-control__link" href="#">30 на страницу</a>*}
{*                    </div>*}
{*                    <div class="pager-control__elem">*}
{*                        <a class="pager-control__link" href="#">60 на страницу</a>*}
{*                    </div>*}
{*                    <div class="pager-control__elem">*}
{*                        <a class="pager-control__link" href="#">90 настраницу</a>*}
{*                    </div>*}
{*                    <div class="pager-control__elem">*}
{*                        <a class="pager-control__link" href="#">Показать все</a>*}
{*                    </div>*}
{*                </div>*}
{*            </div>*}
{*        </div>*}
    </div>
{/if}
