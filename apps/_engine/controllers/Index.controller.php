<?php

class Index_Controller extends ZObject_Controller
{
    public function action_index()
    {
        $res = Q('SELECT * FROM @@sections WHERE id=?i ', array($this->node['object_id']))->row();

        RS('page', $res);
        RS('newsList', $this->newsList());
        V('index');
    }

    /**
     * Получить список новостей
     */
    private function newsList()
    {
        $list = [];

        //  Постраничная разбивка
        $sql = Qb('SELECT 1 FROM @@news');
        $pg  = PG($sql, 10);
        RS('pg', $pg);

        //  Загружем найденые товары
        $res = Q('SELECT N.`path`, P.`title`, P.`date`, P.`anons`, P.`photo` FROM @@news as P
                    LEFT JOIN @@nodes as N
                    ON P.id = N.object_id
                    WHERE N.object_type="news"
                    ORDER BY P.date DESC
                    LIMIT ?i, ?i',
            array(
                $pg['offset'],
                $pg['limit'],
            )
        );
        while ($r = $res->each()) {
            $r['photo'] = $this->getFM($r['photo'], 'a');
            $r['date']  = (new DateTime($r['date']))->format('d / m / Y');
            $list[]     = $r;
        }

        return $list;
    }
}
