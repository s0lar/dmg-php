<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.csrf.php
 * Type:     function
 * Name:     csrf
 * Purpose:  outputs a random magic answer
 * -------------------------------------------------------------
 */
function smarty_function_csrf($params, &$smarty)
{
    $answers = array('Да',
                     'Нет',
                     'Никоим образом',
                     'Перспектива так себе...',
                     'Спросите позже',
                     'Все может быть');

    $result = array_rand($answers);
    return $answers[$result];
}
?>