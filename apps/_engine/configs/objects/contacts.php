<?php

return [
    # types of fields
    'fields'    => [
        'title'       => 'string',
        'addr'        => 'string',
        'addr_full'   => 'text',
        'phone'       => 'text',
        'lat_lon'     => 'map',
        'link'        => 'string',
        'email'       => 'string',
        'description' => 'text',
        'sort'        => 'int',
    ],

    # labels of fields
    'ui'        => [
        'title'       => 'Город',
        'addr'        => 'Адрес',
        'addr_full'   => 'Адрес полный',
        'phone'       => 'Телефоны',
        'lat_lon'     => 'Широта Долгота',
        'link'        => 'Ссылка на Яндекс.карту',
        'email'       => 'Email',
        'description' => 'Описание',
        'sort'        => 'Порядок',
    ],

    #
//    'input_cfg' => [
//        'photo' => [
//            'type' => 'image',
//            'mask' => 'contacts',
//        ],
//    ],

    # node configuration
    'node'      => [
        # use "title" field for "object_title" in nodes table
        'object_title' => 'title',
        # use user input for "name" field in nodes table
        'name'         => '-user'
    ],

    # view
    'view'      => [
        'mode'       => 'list',
        'fields'     => ['title', 'addr', 'phone', 'sort'],
        'orderby'    => ' `sort` DESC ',
        'edit_field' => 'title',
        'limit'      => 150
    ],

    # labels for actions
    'labels'    => [
        'list'    => 'Филиалы',
        'add'     => 'Добавить филиал',
        'adding'  => 'Добавление филиала',
        'edit'    => 'Редактировать филиал',
        'editing' => 'Редактирование филиала',
        'delete'  => 'Удалить филиал'
    ]
];