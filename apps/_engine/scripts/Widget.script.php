<?php


class Widget_Script extends XAny_Script
{
	function run()
	{
		$request = RQ();
		$request->url = XURL($request->url)->views(array('html'))->parse();
		$router = new Router();
		
		try
		{
			$router->route();
		}
		catch (XException $e)
		{
			return array();
		}
		
		$controller = new $router->controller($router->node);

		return $controller->{$router->method}();
	}
}
?>