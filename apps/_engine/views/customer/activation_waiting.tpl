﻿{extends file='_layout.tpl'}

{block name=content}

<div class="main">
    <div class="page-head single_head">
        <div class="container">
            <div data-aos="fade-up" data-aos-delay="500" data-aos-duration="400" data-aos-easing="ease-out-cubic" class="page-head__desc">
                <div class="page-head__title">Регистрация</div>
            </div>
        </div>
    </div>
    <div class="page-main">
        <div class="container">
            <div class="page-auth">
                <div class="page-auth__left">
                    <div class="page-auth__form">
                        <div class="medium-title">Активация учетной записи пароля</div>
                        <p>На указанные вами e-mail была отправлена ссылка для активации аккаунта. <br />Для завершения регистрации, перейдите по ссылке в письме.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{/block}

{block name=footer}
{/block}