﻿{extends file='_layout.tpl'}

{block name=content}
    <div class="page page_checkout">
        <div class="container">
            <div class="page-head">
                <div class="page-head__top">
                    <div class="page-head__title">Корзина</div>
                </div>
                <div class="checkout-step">
                    <div class="checkout-step__elem is-active">
                        <div class="checkout-step__title">01. Состав заказа</div>
                    </div>
                    <div class="checkout-step__elem">
                        <div class="checkout-step__title">02. Контакты и доставка</div>
                    </div>
                    <div class="checkout-step__elem">
                        <div class="checkout-step__title">03. подтверждение</div>
                    </div>
                </div>
                <div class="checkout-counter">
                    <div class="checkout-counter__title">Состав заказа</div>
                    <div class="checkout-counter__number">01 / 03</div>
                </div>
            </div>
            <div class="checkout">
                <div class="checkout-flex">
                    <div class="checkout-flex__left">
                        <div class="checkout-help size_small">
                            <p>
                                Оформление заказа займет не более 2&nbsp;минут!
                                Пожалуйста, проверьте правильность выбранных
                                товарных позиций и их количество.
                            </p>
                            <div class="checkout-links">
                                <a class="link" href="/clients/oplata-dostavka-dokumenty/">Порядок оплаты</a>
                                <a class="link" href="/clients/oplata-dostavka-dokumenty/">Порядок доставки</a>
                            </div>
                        </div>
                        <div class="checkout-products">
                            {include file="_cart_list.tpl"}
                        </div>
                        <div class="checkout-next">
                            <a class="button style_green size_middle justify_space_between" href="/order/:delivery">
                                <span class="button__text">
                                    Следующий шаг
                                    <svg class="icon icon-arrow-triangle ">
                                        <use xlink:href="/theme/dmg_markup/build/svg/svg-symbols.svg#icon-arrow-triangle"></use>
                                    </svg>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {include file='_footer_form.tpl'}

    </div>
{/block}

{block name=footer}
{/block}