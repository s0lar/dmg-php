{include file='zobject/header.tpl' inline='true'}
	
	{* Error message *}
	{if isset($smarty.get.err) && $smarty.get.err == 'access'}
		<div class="alert alert-block">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<p>Ошибка. У вас нет доступа к данному разделу</p>
		</div>
		
	{elseif isset($error_access) && $error_access == 1}
		<div class="alert alert-block">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<p>Ошибка. У вас нет доступа к данному разделу</p>
		</div>
		
	{/if}

	{*
	{include file='zobject/blocks/list_controll.tpl' inline='true'}
	*}
	<p><b>Создание карты сайта</b></p>
	


	<div class="" style="padding:20px;border:1px solid #ddd;margin-top:-1px">
		

		<form method='post'>
			<table class='table2 mb20'>
				
				{foreach item=i from=$_list}

				<tr>
					<td><b>{$i.title}</b></td>
					<td>{$i.path}</td>
					<td>
						<select name='sitemap_changefreq[{$i.id}]' style='width:80px'>
							<option value="always" {if $i.sitemap_changefreq == 'always'}selected='selected'{/if}>always</option>
							<option value="hourly" {if $i.sitemap_changefreq == 'hourly'}selected='selected'{/if}>hourly</option>
							<option value="daily"  {if $i.sitemap_changefreq == 'daily'}selected='selected'{/if}>daily</option>
							<option value="weekly" {if $i.sitemap_changefreq == 'weekly'}selected='selected'{/if}>weekly</option>
							<option value="monthly" {if $i.sitemap_changefreq == 'monthly'}selected='selected'{/if}>monthly</option>
							<option value="yearly" {if $i.sitemap_changefreq == 'yearly'}selected='selected'{/if}>yearly</option>
							<option value="never"  {if $i.sitemap_changefreq == 'never'}selected='selected'{/if}>never</option>
						</select>
					</td>
					<td><input type='text' style='width:60px' name='sitemap_priority[{$i.id}]' value='{$i.sitemap_priority}' /></td>
					<td><input type='checkbox' name='sitemap_enable[{$i.id}]'   {if $i.sitemap_enable == 1} checked="checked"{/if}   /></td>
				</tr>

				{/foreach}

			</table>

			<p>&nbsp;</p>
			<input type="submit" value="Сохранить" class="btn">
		</form>

		<div class="pagination">
			{include file='blocks/pg.tpl'}
		</div>
		
	</div>



{include file='zobject/footer.tpl' inline='true'}